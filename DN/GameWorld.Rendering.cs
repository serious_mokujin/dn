﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Graphics;
using DN.Effects;
using DN.GUI;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace DN
{
	partial class GameWorld : IDisposable
    {
        ParallaxBackground background;
        public BloodSystem BloodSystem;
        MagicBackground mback;
        private Texture postProcessingFrame = new Texture(Game.g_screenSize);
        private VertexBuffer blurBuffer;

        private Shader blurShader;
        public bool bluring = false;
        public bool inverse = false;
        private float blurStrenght = 0f;
        private float blurDistance = 0f;

        private void InitRendering()
        {
            blurBuffer = new VertexBuffer(4);
            blurBuffer.DeclareNextAttribute("vposition", 2);

            blurBuffer.AddVertex(-1, -1);
            blurBuffer.AddVertex(1, -1);
            blurBuffer.AddVertex(1, 1);
            blurBuffer.AddVertex(-1, 1);
            blurBuffer.AddIndices(0, 1, 2, 0, 2, 3);
            blurBuffer.UpdateBuffer();

            background = new ParallaxBackground(this);
            BloodSystem = new BloodSystem(this);
            BloodSystem.Init();
            BloodSystem.BlendWith(back);

            mback = new MagicBackground();

            blurShader = new Shader();

            float v = Shader.Version;

            blurShader.LoadVertexFile(Path.Combine("Effects", v < 3.3f ? "v12" : "v33", "RadialBlur.vert"));
            blurShader.LoadFragmentFile(Path.Combine("Effects", v < 3.3f ? "v12" : "v33", "RadialBlur.frag"));

            blurShader.Link();

            blurBuffer.Attach(blurShader);
        }
        public void InitTextures()
        {
            TileMap.InitTextures();
        }

        Texture back = new Texture(Game.g_screenSize);
        public void Draw(float dt)
        {
            float spd = dt*5;
            if (bluring)
            {
                blurDistance = MathUtils.Lerp(blurDistance, 1.0f, spd);
                blurStrenght = MathUtils.Lerp(blurStrenght, 2.6f, spd);
            }
            else
            {
                if (blurStrenght > 0 || blurDistance > 0)
                {
                    blurDistance = MathUtils.Lerp(blurDistance, 0, spd);
                    blurStrenght = MathUtils.Lerp(blurStrenght, 0, spd);
                }
            }

            GL.ClearColor(0, 0, 0, 0);
            SpriteBatch.Instance.Begin(Camera.GetViewMatrix());
            RenderTiles(dt);
            SpriteBatch.Instance.End(back, true);

            BloodSystem.PredrawBloodTexture(dt);

            GL.ClearColor(0, 0, 0, 1);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            
            

            
            
            //BlueberryGame.SetRenderTarget(postProcessingFrame);//todo: get it back

            //GL.ClearColor(0, 0, 0, 0);
            //GL.Clear(ClearBufferMask.ColorBufferBit);
            //mback.Draw(dt);
            background.Draw(dt);

            BloodSystem.DrawBackground(dt);

            SpriteBatch.Instance.Begin(Camera.GetViewMatrix());
            GameObjectsManager.Draw(dt);
            SpriteBatch.Instance.End();

            //   GraphicsDevice.Instance.SetRenderTarget(null);//todo: get it back
            if(Game.g_Keyboard[Key.U]) postProcessingFrame.SaveToDisk("meow.png");
            
            blurBuffer.Bind();
            blurShader.Use();
            
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, postProcessingFrame.ID);

            blurShader.SetUniform("tex", 0);
            blurShader.SetUniform("sampleDist", blurDistance);
            blurShader.SetUniform("sampleStrength", blurStrenght);
            blurShader.SetUniform("inverse", inverse ? 1 : 0);
            GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, 0);

            GL.UseProgram(0);
             
            SpriteBatch.Instance.Begin(Camera.GetViewMatrix());
            //VisualizeQuadTree();
            PopupTextThrower.Instance.Draw(dt);
            SpriteBatch.Instance.End();
        }

        private void RenderTiles(float dt)
        {
            Rectangle rect = Camera.BoundingRectangle;
            rect.X /= 64;
            rect.Y /= 64;
            rect.Width /= 64;
            rect.Height /= 64;

            rect.Width += 2;
            rect.Height += 2;

            TileMap.Draw(rect);
        }

        Color[] colors = { Color.Red, Color.Green, Color.Yellow, Color.Violet, Color.Pink, Color.White, Color.Black, Color.Blue, Color.Brown };
        private void VisualizeQuadTree()
        {
            var nodes = QuadTree.Nodes;
            foreach (var n in nodes)
            {
                SpriteBatch.Instance.OutlineRectangle(n.Area, colors[n.Generation]);
            }
        }

		
		public void Dispose()
		{
			mback.Dispose();
			BloodSystem.Dispose();
			blurBuffer.Dispose();
			Game.g_Keyboard.KeyDown -= Game_g_Keyboard_KeyDown;
		}
    }
}
