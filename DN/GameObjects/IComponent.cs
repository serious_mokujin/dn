﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.TestGameObjects.Messages;

namespace DN.TestGameObjects
{
    public interface IComponent
    {
        int ComponentType { get; }

        void ProccesMessage(IMessage message);
        bool GetDependicies();
    }
}
