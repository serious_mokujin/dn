﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects;
using DN.TestGameObjects.Messages;
using GamepadExtension;
using OpenTK;
using OpenTK.Input;

namespace DN.TestGameObjects
{
    class PlayerControllComponent:Component
    {
        private PhysicComponent _physicComponent;
        private JumpComponent _jumpComponent;
        private LadderClimbingComponent _ladderClimbingComponent;

        public PlayerControllComponent(IGameObject gameObject) : base(gameObject)
        {
            Game.g_Gamepad.OnButtonPress += g_Gamepad_OnButtonPress;
            Game.g_Gamepad.OnButtonUp += g_Gamepad_OnButtonUp;
            Game.g_Keyboard.KeyDown += g_Keyboard_KeyDown;
            Game.g_Keyboard.KeyUp += g_Keyboard_KeyUp;
            Game.g_Keyboard.KeyRepeat = true;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                Update((UpdateMessage) message);
            }
        }

        private void Update(UpdateMessage message)
        {
            float dt = message.DT;

            Vector2 moveDirectionVector = Vector2.Zero;

            if (LeftKeyPressed())
                moveDirectionVector.X = -1;
            if (RightKeyPressed())
                moveDirectionVector.X = 1;
            if (UpKeyPressed())
                moveDirectionVector.Y = -1;
            if (DownKeyPressed())
                moveDirectionVector.Y = 1;

            Vector2 leftStickPos = Game.g_Gamepad.LeftStick.Position;
            if (leftStickPos.X != 0)
                moveDirectionVector = new Vector2(leftStickPos.X, leftStickPos.Y);


            if (moveDirectionVector != Vector2.Zero)
            {
                if(!_ladderClimbingComponent.ClimbLadder)
                    _physicComponent.Move(new Vector2(1, 0), dt*moveDirectionVector.X*38);
            }

            if ((UpKeyPressed() && _physicComponent.GetVelocity().Y >= 0 && !_jumpComponent.Jumping))
                _ladderClimbingComponent.TryClimbLadder();
            _ladderClimbingComponent.MoveLadder(moveDirectionVector, 3);
        }



        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            _jumpComponent = Owner.GetComponent<JumpComponent>();
            _ladderClimbingComponent = Owner.GetComponent<LadderClimbingComponent>();
            return _physicComponent != null && _jumpComponent != null && _ladderClimbingComponent != null;
        }

        private void g_Keyboard_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                _jumpComponent.StopJump();
            }
        }
        void g_Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                _jumpComponent.Jump();
            }
        }


        private void g_Gamepad_OnButtonPress(object sender, GamepadExtension.GamepadButtons e)
        {
            if (e.HasFlag(GamepadButtons.A))
            {
                if (DownKeyPressed())
                {
                  // ClimbLadder = false;
                }
                else
                    _jumpComponent.Jump();
                _ladderClimbingComponent.StopClimbLadder();
            }
        }
        private void g_Gamepad_OnButtonUp(object sender, GamepadButtons e)
        {
            if (e.HasFlag(GamepadButtons.A))
            {
                _jumpComponent.StopJump();
            }
        }

        private static bool DownKeyPressed()
        {
            return Game.g_Gamepad.DPad.Down || Game.g_Keyboard[Key.S] || Game.g_Gamepad.LeftStick.Position.Y > 0.3f;
        }

        private static bool UpKeyPressed()
        {
            return Game.g_Gamepad.DPad.Up || Game.g_Keyboard[Key.W] || Game.g_Gamepad.LeftStick.Position.Y < -0.3f;
        }

        private static bool RightKeyPressed()
        {
            return Game.g_Gamepad.DPad.Right || Game.g_Keyboard[Key.D] || Game.g_Gamepad.LeftStick.Position.X > 0.3f;
        }

        private static bool LeftKeyPressed()
        {
            return Game.g_Gamepad.DPad.Left || Game.g_Keyboard[Key.A] || Game.g_Gamepad.LeftStick.Position.X < -0.3f;
        }
    }
}
