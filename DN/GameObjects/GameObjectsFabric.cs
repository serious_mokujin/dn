﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Graphics.Fonts;
using Blueberry.Input;
using DN.GUI.Windows;
using DN.GameObjects.Components;
using DN.GameObjects.Components.ActionComponents;
using DN.GameObjects.Components.AnimationComponents;
using DN.GameObjects.Components.AnimationComponents.BatAnimations;
using DN.GameObjects.Components.AnimationComponents.HeroAnimations;
using DN.GameObjects.Components.AnimationComponents.Listeners;
using DN.GameObjects.Components.AnimationComponents.OgreAnimations;
using DN.GameObjects.Components.Behaviours;
using DN.GameObjects.Components.Controllers;
using DN.GameObjects.Components.DrawingComponents;
using DN.GameObjects.Components.Effects;
using DN.GameObjects.Components.ItemsActions;
using DN.GameObjects.Components.OutsideInteractingComponents;
using DN.GameObjects.Components.PhysicComponents;
using DN.GameObjects.Components.Sounds;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using OpenTK;
using OpenTK.Input;

namespace DN.GameObjects
{
    public enum GameObjectType
    {
        Hero,
        Bat,
        Troll,
        Sword,
        Granade,
        Bow,
        Arrow,
        Potion,
        Ogre,
        Spikes,
        TrollEmitier,
        Anvil,
        Letter
    }

    public enum EnemyType
    {
        Ogre, 
        Troll,
        Bat,
    }

    public class GameObjectsFabric
    {
        public BitmapFont LettersFont;

        private readonly TileMap _tileMap;
        public readonly GameWorld GameWorld;
        private readonly GameObjectsManager _gameObjectsManager;


        public GameObjectsFabric(GameWorld gameWorld, TileMap tileMap, GameObjectsManager gameObjectsManager)
        {
            GameWorld = gameWorld;
            _tileMap = tileMap;
            _gameObjectsManager = gameObjectsManager;
        }

        public IGameObject CreateObject(GameObjectType type, Point cell)
        {
            IGameObject gameObject;
            switch (type)
            {
                case GameObjectType.Bat:
                    gameObject = CreateBat(cell);
                    break;
                case GameObjectType.Troll:
                    gameObject = CreateTroll(cell);
                    break;
                case GameObjectType.Sword:
                    gameObject = CreateSword();
                    break;
                case GameObjectType.Bow:
                    gameObject = CreateBow(_gameObjectsManager);
                    break;
                case GameObjectType.Potion:
                    gameObject = CreatePotion();
                    break;
                case GameObjectType.Ogre:
                    gameObject = CreateOgre(cell);
                    break;
                case GameObjectType.Spikes:
                    gameObject = CreateSpikes(cell);
                    break;
                case GameObjectType.Granade:
                    gameObject = CreateGranade(cell);
                    break;
                case GameObjectType.Anvil:
                    gameObject = CreateAnvil(cell);
                    break;
                case GameObjectType.Letter:
                    gameObject = CreateLetter(cell);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
            return gameObject;
        }
        public IGameObject CreateEnemy(EnemyType type, Point cell, IGameObject[] targets)
        {
            IGameObject enemy;
            switch (type)
            {
                case EnemyType.Ogre:
                    enemy = CreateObject(GameObjectType.Ogre, cell);
                    AddBehaviour(enemy, new OgreBehaviour(enemy), targets);
                    break;
                case EnemyType.Troll:
                    enemy = CreateObject(GameObjectType.Troll, cell);
                    AddBehaviour(enemy, new TrollBehaviour(enemy), targets);
                    break;
                case EnemyType.Bat:
                    enemy = CreateObject(GameObjectType.Bat, cell);
                    AddBehaviour(enemy, new BatBehaviour(enemy), targets);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
            return enemy;
        }

        public IGameObject CreatePhysicObject(ObjectGroup group, ObjectGroup[] ignoreCollisionsGroup, Size size, Point cell,
                                              Vector2 maxVelocity, float friction, float acceleration = 0,
                                              bool gravityAffected = true, bool searchForCollisions = true,bool frictionOnMove = false)
        {
            IGameObject gameObject = new GameObject(group);
            gameObject.AddComponents(new BoundaryComponent(gameObject)
                                         {
                                             Size = size,
                                             Cell = cell
                                         },
                                     new PhysicComponent(gameObject, _tileMap, GameWorld.SolidObjectManager)
                                         {
                                             MaxVelocity = maxVelocity,
                                             Friction = friction,
                                             Acceleration = acceleration,
                                             GravityAffected = gravityAffected,
                                             FrictionOnMove = frictionOnMove
                                         },
                                     new CollisionComponent(gameObject, ignoreCollisionsGroup, GameWorld.QuadTree,
                                                            searchForCollisions));
            return gameObject;
        }

        public IGameObject CreateHero(Point cell, Color color1, Color color2)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Hero, null, new Size(42, 42),
                                                        cell, new Vector2(6, 15), 30.1f);
            gameObject.AutoChangeActivity = false;
            gameObject.Active = true;
            gameObject.AddComponents(new PickupingComponent(gameObject, 3),
                                     new JumpComponent(gameObject) {JumpAcceleration = 80, JumpLength = 0.1f},
                                     new LadderClimbingComponent(gameObject),
                                     new HealthComponent(gameObject,_gameObjectsManager, this, 75, 1),

                                     new DirectionComponent(gameObject),
                                     
                                     new ColorComponent(gameObject)
                                         {
                                             Color1 = color1,
                                             Color2 = color2
                                         },

                                     new AnimationControllerComponent(gameObject, new HeroAnimationLayout(color1, color2)),
                                     new WalkAnimationListener(gameObject),
                                     new IdleAnimationListener(gameObject),
                                     new JumpAnimationListener(gameObject),
                                     new FallingAnimationListener(gameObject),
                                     new ClimbAnimationListener(gameObject),
                                     
                                     new HeroFallingAnimationComponent(gameObject),
									 new HeroWalkAnimationComponent(gameObject),
                                     new HeroClimbAnimationComponent(gameObject),
                                     new HeroIdleAnimationComponent(gameObject),
                                     new HeroJumpAnimationComponent(gameObject),
                                     
                                     new LadderClimbingComponent(gameObject),
                                     
									 new OutsideUsingComponent(gameObject),
                                     new PopupTextOnDamageTaken(gameObject),
                                     new AddStatisticKillsOnDeath(gameObject, GameWorld),
                                     new DustFromMoving(gameObject, GameWorld),
                                     new DieOutsideOfRoom(gameObject, _tileMap),
                                     new LettersInventory(gameObject));
            return gameObject;
        }

        public void AddPlayerControlls(IGameObject gameObject, KeyboardDevice keyboardDevice, MouseDevice mouseDevice, GamepadDevice gamepadDevice)
        {
            gameObject.AddComponents(new PlayerControllComponent(gameObject, mouseDevice, keyboardDevice, gamepadDevice, this.GameWorld.Camera),
                                     new VibrateOnDamageTaken(gameObject, gamepadDevice));
        }

        private IGameObject CreateOgre(Point cell)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Enemy, new[] {ObjectGroup.Item}, new Size(96, 128),
                                                        cell,
                                                        new Vector2(15, 15), 25f, 7f);
            gameObject.AddComponents(new HealthComponent(gameObject, _gameObjectsManager, this, 100, 0.1f),
                                     new DamageComponent(gameObject)
                                         {
                                             CriticalChance = 0.05f,
                                             MinDamage = 4,
                                             MaxDamage = 5,
                                             CriticalDamage = 2
                                         },
                                     new CreateBloodOnGettingHit(gameObject, GameWorld),
                                     new DefusableComponent(gameObject),
                                     new DamageOnHitComponent(gameObject, new[] {ObjectGroup.Enemy}),
                                     new LootComponent(gameObject, _gameObjectsManager, this,
                                                       new[]
                                                           {
                                                               new Loot(GameObjectType.Potion, 0.05f),
                                                               new Loot(GameObjectType.Letter, 0.40f, 2)
                                                           }),
                                     new DropLootOnDie(gameObject),
                                     new PopupTextOnDamageTaken(gameObject),
                                     new AddStatisticKillsOnDeath(gameObject, GameWorld),
                                     new EarthquakeComponent(gameObject, _gameObjectsManager, GameWorld.Camera),
                                     new JumpComponent(gameObject) {JumpAcceleration = 110, JumpLength = 0.12f},
                                     new DieOutsideOfRoom(gameObject, _tileMap),
                                     new AnimationControllerComponent(gameObject, new OgreAnimationLayout()),
                                     new IdleAnimationListener(gameObject),
                                     new WalkAnimationListener(gameObject),
                                     new JumpAnimationListener(gameObject),
                                     new FallingAnimationListener(gameObject),
                                     new OgreWalkAnimationComponent(gameObject),
                                     new HeroIdleAnimationComponent(gameObject),
                                     new OgreJumpAnimationComponent(gameObject),
                                     new HeroFallingAnimationComponent(gameObject)
                );
            gameObject.AutoChangeActivity = false;
            gameObject.Active = true;
            return gameObject;
        }

        private IGameObject CreateBat(Point cell)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Enemy, new[] {ObjectGroup.Item},
                                                        new Size(20 + RandomTool.NextInt(0, 10), 20),
                                                        cell, new Vector2(4, 6),
                                                        0.1f, 0.5f + RandomTool.NextSingle(), false);
            gameObject.AddComponents(new HealthComponent(gameObject, _gameObjectsManager, this, 9, 0.3f),
                                     new DamageComponent(gameObject)
                                         {
                                             CriticalChance = 0.15f,
                                             MinDamage = 1,
                                             MaxDamage = 1,
                                             CriticalDamage = 2
                                         },
                                     new CreateBloodOnGettingHit(gameObject, GameWorld),
                                     new DefusableComponent(gameObject),
                                     new DamageOnHitComponent(gameObject, new[] {ObjectGroup.Enemy}),
                                     new LootComponent(gameObject, _gameObjectsManager, this,
                                                       new[]
                                                           {
                                                               new Loot(GameObjectType.Potion, 0.05f),
                                                               new Loot(GameObjectType.Letter, 0.40f, 2)
                                                           }),
                                     new DropLootOnDie(gameObject),
                                     new PopupTextOnDamageTaken(gameObject),
                                     new AddStatisticKillsOnDeath(gameObject, GameWorld),

                                     new AnimationControllerComponent(gameObject, new BatAnimationLayout()),
                                     new FlyAnimationListener(gameObject),
                                     new BatFlyAnimationComponent(gameObject),
                                     new SoundOnMakingDamage(gameObject, CM.I.Sound("bat5")),
                                     new SoundOnDeath(gameObject, CM.I.Sound("bat1"), CM.I.Sound("bat2"),
                                                      CM.I.Sound("bat3"),
                                                      CM.I.Sound("bat4"))
                );
            return gameObject;
        }

        public IGameObject CreatePotion()
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, null, new Size(21, 21),
                                                        _tileMap.GetRandomStandablePoint(), new Vector2(6, 15),
                                                        30.1f,0, true, false);
            gameObject.AddComponents(
                                     new PickupableComponent(gameObject),
                                     new PickuperHealthUpOnAction(gameObject,_gameObjectsManager, 10),
                                     new ActionComponent(gameObject),
                                     new SimpleDrawingComponent(gameObject, "potion"),
                                     new SoundOnAction(gameObject, CM.I.Sound("drinkPotion"), null, false),
                                     new DescriptionComponent(gameObject, "Heal potion"));
            return gameObject;
        }

        public IGameObject CreateSword()
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, new[] {ObjectGroup.Undefined},
                                                        new Size(42, 24), _tileMap.GetRandomStandablePoint(),
                                                        new Vector2(6, 15),20.1f);
            gameObject.AddComponents(new PickupableComponent(gameObject),
                                     new ActionComponent(gameObject) {TimeToFinishAction = 0.17f, IntervalTime = 0.3f},
                                     new StraightAttackingComponent(gameObject, 300.0f),
                                     new DirectionComponent(gameObject),
                                     new DamageComponent(gameObject)
                                         {
                                             MinDamage = 3,
                                             MaxDamage = 4,
                                             CriticalChance = 0.30f
                                         },
                                     new DamageOnHitComponent(gameObject),
                                     new DescriptionComponent(gameObject, "Sword"),
                                     new SoundOnAction(gameObject, CM.I.Sound("swordB"), null, false),
                                     new SoundOnMakingDamage(gameObject, CM.I.Sound("swordA")),
                                     new AnimationControllerComponent(gameObject, new SwordAnimationLayout()),
                                     new ActionAnimationListener(gameObject),
                                     new SwordAnimationComponent(gameObject)
                                     );

            return gameObject;
        }

        public IGameObject CreateBow(GameObjectsManager manager)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, new[] {ObjectGroup.Undefined},
                                                        new Size(32, 64), _tileMap.GetRandomStandablePoint(),
                                                        new Vector2(6, 15), 1.1f);
            gameObject.AddComponents(new PickupableComponent(gameObject),
                                     new DamageComponent(gameObject)
                                         {
                                             MinDamage = 6,
                                             MaxDamage = 9,
                                             CriticalChance = 0.15f
                                         },
                                     new ActionComponent(gameObject) {IntervalTime = 0.3f, AutoFinish = false, Interuptable = true},
                                     new DirectionDrawingComponent(gameObject, CM.I.tex("bow_sprite"), new Size(32, 64)),
                                     new DirectionComponent(gameObject),
                                     new BowComponent(gameObject, manager, this) {ProjectiveSpeed = 1},
                                     new SoundOnAction(gameObject, CM.I.Sound("bowStreches"), CM.I.Sound("bowShoot"), true),
                                     new DescriptionComponent(gameObject,"Bow"));
            return gameObject;
        }

        public IGameObject CreateArrow(float minDamage, float maxDamage, float critChance)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, new[] {ObjectGroup.Undefined},
                                                        new Size(4, 4),Point.Empty, new Vector2(50, 50),10f);
            AddFadingOut(gameObject, 1.0f, 10f);
            gameObject.AddComponents(
                new DirectionComponent(gameObject),
                new DirectionDrawingComponent(gameObject, CM.I.tex("arrow_sprite"), new Size(48, 10), 0, 0.5f, 0.5f),
                new DamageComponent(gameObject)
                    {
                        MinDamage = minDamage,
                        MaxDamage = maxDamage,
                        CriticalChance = critChance
                    },
                new DamageOnSpeed(gameObject, 18),
                new StuckToPointOnCollision(gameObject, _gameObjectsManager),
                new ChangeDirectionToMovingDirection(gameObject));



            gameObject.SendMessage(new ObjectsCollisionsStateMessage(false, new[] {ObjectGroup.Undefined}));
            gameObject.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));
            return gameObject;
        }

        private IGameObject CreateTroll(Point cell)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Enemy, new[] {ObjectGroup.Item}, new Size(34, 56),
                                                        cell, new Vector2(4, 15),
                                                        1.1f, 5f);
            gameObject.AddComponents(
                //new SimpleDrawingComponent(gameObject, "troll_sprite"),
                new HealthComponent(gameObject, _gameObjectsManager,this, 18, 0.2f),
                new CreateBloodOnGettingHit(gameObject, GameWorld),
                new DefusableComponent(gameObject),
                new JumpComponent(gameObject) {JumpAcceleration = 90f, JumpLength = 0.1f},
                new DamageComponent(gameObject)
                    {
                        MinDamage = 2,
                        MaxDamage = 3,
                        CriticalChance =  0.1f
                    },
                new DamageOnHitComponent(gameObject,new[] {ObjectGroup.Enemy}),
                new LootComponent(gameObject, _gameObjectsManager, this,
                                                       new[]
                                                           {
                                                               new Loot(GameObjectType.Potion, 0.05f),
                                                               new Loot(GameObjectType.Letter, 0.40f, 2)
                                                           }),
                new DropLootOnDie(gameObject),
                new PopupTextOnDamageTaken(gameObject),
                new AddStatisticKillsOnDeath(gameObject, GameWorld),
                new DustFromMoving(gameObject, GameWorld),

                new AnimationControllerComponent(gameObject, new TrollAnimationLayout()), 
                                     											//new HeroAnimationLayout()),
                new WalkAnimationListener(gameObject),
                new IdleAnimationListener(gameObject),
                new JumpAnimationListener(gameObject),
                 new FallingAnimationListener(gameObject),
                 new ClimbAnimationListener(gameObject),
                 
                 new TrollWalkAnimationComponent(gameObject),
                 new TrollClimbAnimationComponent(gameObject),
                 new TrollFallingAnimationComponent(gameObject),
                 new TrollJumpAnimationComponent(gameObject),
                 new HeroIdleAnimationComponent(gameObject)
               	);


            return gameObject;
        }
        public IGameObject CreateCorpse(IGameObject targetObject)
        {
        	IGameObject gameObject = new GameObject(ObjectGroup.Decoration);
        	BoundaryComponent targetBoundary = targetObject.GetComponent<BoundaryComponent>();
        	AnimationControllerComponent targetAnimation = targetObject.GetComponent<AnimationControllerComponent>();
        	
            gameObject.AddComponents(
        		new BoundaryComponent(gameObject){Position = targetBoundary.Position, Size = targetBoundary.Size},
                new AnimationControllerComponent(gameObject, targetAnimation.AnimationLayout), 
                new DeathAnimationComponent(gameObject, _gameObjectsManager)
                 
               	);
        	gameObject.SendMessage(new PlayAnimationMessage(Animations.Dying));

            return gameObject;
        }
        public IGameObject CreateDoor()
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, null, new Size(32, 64),
                                                        _tileMap.GetRandomStandablePoint(), new Vector2(4, 15),
                                                        1.1f, 5f);
            gameObject.AddComponents(
                                     new SimpleDrawingComponent(gameObject, "door_sprite"),
                                     new ActionComponent(gameObject),
                                     new OutsideUseableComponent(gameObject),
                                     new NextLevelOnOutsideAction(gameObject, GameWorld));


            return gameObject;
        }

        public IGameObject CreateSpikes(Point cell)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, null, new Size(16, 16),
                                                        cell, new Vector2(4, 15), 1.1f, 5f);
            AddFadingOut(gameObject, 1.0f, 10f);
            gameObject.AddComponents(new DamageComponent(gameObject)
                                         {
                                             MinDamage = 15,
                                             MaxDamage = 20
                                         },
                                     new SimpleDrawingComponent(gameObject, "spikes_sprite"),
                                     new UnfreezeOnMove(gameObject, RandomTool.NextSingle(0, 0.2f), RandomTool.NextInt(10,50)),
                                     new DamageOnHitComponent(gameObject),
                                     new StuckToPointOnDamageGiven(gameObject, _gameObjectsManager));
            gameObject.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));

            return gameObject;
        }

        public IGameObject CreateGameobjectEmitier(Point cell, GameObjectType type, int maxCount, float releaseTime)
        {
            IGameObject gameObject = new GameObject(ObjectGroup.Modificator);
            gameObject.AddComponents(new BoundaryComponent(gameObject)
                                         {
                                             Cell = cell
                                         },
                                     new GameObjectEmitierComponent(gameObject, this, _gameObjectsManager, type,
                                                                    maxCount, releaseTime));

            return gameObject;
        }
        public IGameObject CreateCellCollisionTriger(Point cell, ObjectGroup activatingObjectGroup, EventHandler eventOnCollision,
                                                     bool destroyOnCollision = false)
        {
            IGameObject gameObject = new GameObject(ObjectGroup.Item);
            gameObject.AddComponents(new BoundaryComponent(gameObject)
                                         {
                                             Cell = cell,
                                             Size = new SizeF(96,96)
                                         },
                                     new CollisionComponent(gameObject, null,
                                                            GameWorld.QuadTree)
                                         {
                                             Sleep = false
                                         },
                                     new CollisionTriggerComponent(gameObject, activatingObjectGroup,
                                                                   _gameObjectsManager, eventOnCollision, destroyOnCollision));

            return gameObject;
        }
        public IGameObject CreateWallObject(Point cell, bool placeAtCenterOfCell = true)
        {
            IGameObject gameObject = new GameObject(ObjectGroup.Item);
            gameObject.AddComponents(new BoundaryComponent(gameObject)
                                         {
                                             Cell = cell,
                                             Size = new SizeF(64, 64)
                                         },
                                     new CollisionComponent(gameObject, null, GameWorld.QuadTree),
                                     new SimpleDrawingComponent(gameObject, "wall_tile"),
                                     new DamageComponent(gameObject) {MinDamage = 2000, MaxDamage = 4000},
                                     new DamageOnSpeed(gameObject, 1));

            if (!placeAtCenterOfCell)
            {
                var boundary = gameObject.GetComponent<BoundaryComponent>();
                boundary.Position.Y -= 32;
                boundary.Position.X -= 32;
            }

            gameObject.OnRemove += (sender, args) =>
            {
                GameWorld.SolidObjectManager.Remove((sender as IGameObject).GetComponent<CollisionComponent>());
            };

            GameWorld.SolidObjectManager.Add(gameObject.GetComponent<CollisionComponent>());

            return gameObject;
        }


        public IGameObject CreateExplosion(Vector2 position, SizeF size, float damageMin, float damageMax, float critChance)
        {
            IGameObject gameObject = new GameObject(ObjectGroup.Modificator);
            gameObject.AddComponents(new BoundaryComponent(gameObject) 
                                        {
                                            Size = size, Position = position
                                        },
                                    new CollisionComponent(gameObject, null, GameWorld.QuadTree),
                                    new DamageComponent(gameObject)
                                        {
                                            MinDamage = damageMin,
                                            MaxDamage = damageMax,
                                            CriticalChance = critChance
                                        },
                                    new DamageOnHitComponent(gameObject),
                                    new WallDestroyingComponent(gameObject, GameWorld), 
                                    new SoundOnAction(gameObject, CM.I.Sound("explosion"), null, true),
                                    new PushingComponent(gameObject, 10),
                                    new ExplosionEffectSource(gameObject, this, _gameObjectsManager));
            gameObject.SendMessage(new ActionMessage(gameObject, ActionMessageType.Start));
            AddFadingOut(gameObject, 50, 0.1f);
            gameObject.SendMessage(new ObjectStanceMessage(StanceType.Deativated));
            return gameObject;
        }
        public IGameObject CreateExplosionEffect(Vector2 position, SizeF size)
        {
            IGameObject gameObject = new GameObject(ObjectGroup.Decoration);
            gameObject.AddComponents(new BoundaryComponent(gameObject) 
                                        {
                                            Size = size, Position = position
                                        },
                                    new ExplosionParticleEffect(gameObject, 2),
                                    new DestroyTimerComponent(gameObject, _gameObjectsManager, 2));
            
            gameObject.SendMessage(new ObjectStanceMessage(StanceType.Deativated));
            return gameObject;
        }
        public IGameObject CreateGranade(Point cell)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, null, new Size(20,20), cell,
                                                        new Vector2(6,15), 8 , 0, true, false, true);
            gameObject.AddComponents(new ActionComponent(gameObject) {AutoFinish = false, Interuptable = true},
                                     new PickupableComponent(gameObject),
                                     new ExplodeOnTimer(gameObject, _gameObjectsManager, this, 2.0f, new SizeF(256, 256),
                                                        10, 20, 0.3f),
                                     new ThrowableComponent(gameObject, GameWorld.TileMap)
                                         {ProjectiveSpeed = 5, MinTension = 1, MaxTension = 10, TensionGrowingSpeed = 5},
                                     new SimpleDrawingComponent(gameObject, "bomb_sprite"),
                                     new DirectionComponent(gameObject),
                                     new BouncingComponent(gameObject) {Bounciness = 1.7f},
                                     new DescriptionComponent(gameObject, "Bomb")
                                     );

            return gameObject;

        }

        public IGameObject CreateLetter(Point cell)
        {
            char ch = (char) RandomTool.NextByte(97, 122);
            Size size = LettersFont.MeasureSymbol(ch).ToSize();

            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item,
                                                        new ObjectGroup[] {ObjectGroup.Enemy, ObjectGroup.Item},
                                                        size, cell, new Vector2(6, 15), 55, 35, false);
            AddFadingOut(gameObject, 1, 0);
            gameObject.AddComponents(new CharDrawingComponent(gameObject, LettersFont, ch, Color.White),
                                     new DeactivateOnCollision(gameObject), new AddLetterOnCollision(gameObject, ch));

            AddBehaviour(gameObject, new FollowingBehaviour(gameObject), GameWorld.Heroes);

            return gameObject;
        }

        public IGameObject CreateAnvil(Point cell)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, null, new Size(64, 64), cell,
                                                        new Vector2(5, 15), 15, 0);
            gameObject.AddComponents(new CraftingComponent(gameObject, _gameObjectsManager),
                                     new OutsideUseableComponent(gameObject),
                                     new SimpleDrawingComponent(gameObject, "anvil_sprite"),
                                     new PushingComponent(gameObject, 1f, ObjectGroup.Hero, ObjectGroup.Enemy));


            return gameObject;
        }

        public IGameObject Chest(Point cell)
        {
            IGameObject gameObject = CreatePhysicObject(ObjectGroup.Item, null, new Size(48, 32), cell, new Vector2(5, 15), 30, 0, true, false, true);
            gameObject.AddComponents(new ActionComponent(gameObject)
                                         {
                                             AutoFinish = true,
                                             Interuptable = true,
                                             IntervalTime = 0,
                                             TimeToFinishAction = 0
                                         },
                                     new OutsideUseableComponent(gameObject),
                                     new LootComponent(gameObject, _gameObjectsManager, this, new[]
                                                                                                  {
                                                                                                      new Loot(GameObjectType.Potion,
                                                                                                          0.5f, 3)
                                                                                                  }),
                                     new DropLootOnAction(gameObject)
                );

            return gameObject;
        }

        private void AddBehaviour(IGameObject gameObject, IBehaviour behaviour, IGameObject[] targets)
        {
            gameObject.AddComponents(new BehaviourComponent(gameObject, targets, GameWorld, behaviour));
        }

        private void AddFadingOut(IGameObject gameObject, float speed, float time)
        {
            gameObject.AddComponents(new AlphaComponent(gameObject),
                                     new DestroyOnZeroAlpha(gameObject, _gameObjectsManager),
                                     new DropAlphaOnDeactivation(gameObject, speed, time));
        }
    }

}
