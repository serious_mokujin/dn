﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.TestGameObjects.Messages;

namespace DN.TestGameObjects
{
    public abstract class Component : IComponent
    {
        public abstract int ComponentType { get; }

        protected IGameObject Owner { get; private set; }

        public Component(IGameObject gameObject)
        {
            Owner = gameObject;
        }


        public abstract void ProccesMessage(IMessage message);

        public virtual bool GetDependicies()
        {
            return true;
        }
    }
}
