﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.TestGameObjects.Messages;
using OpenTK;

namespace DN.TestGameObjects
{
    class LadderClimbingComponent:Component
    {
        public bool ClimbLadder
        {
            get { return _physicComponent.MoveMethod == MoveMethod.Immediate; }
        }

        private PhysicComponent _physicComponent;



        public LadderClimbingComponent(IGameObject gameObject) : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            return base.GetDependicies();
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                Update((UpdateMessage)message);
            }
        }

        private void Update(UpdateMessage message)
        {
            if (!_physicComponent.OnLadder)
                StopClimbLadder();
        }

        public void MoveLadder(Vector2 direction, float speed)
        {
            if (ClimbLadder)
            {
                _physicComponent.Move(direction, speed);
            }
        }

        public void TryClimbLadder()
        {
            if (_physicComponent.OnLadder && !_physicComponent.OnGround)
            {
                _physicComponent.GravityAffected = false;
                _physicComponent.MoveMethod = MoveMethod.Immediate;
                
            }
        }
        public void StopClimbLadder()
        {
            _physicComponent.MoveMethod = MoveMethod.Smooth;
            _physicComponent.GravityAffected = true;
        }
    }
}
