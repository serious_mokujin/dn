﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects
{
    public interface IUpdatable
    {
        void Update(float dt);
    }
}
