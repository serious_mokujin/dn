﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.Helpers;

namespace DN.GameObjects.Components
{
    public class ActionComponent:Component
    {
        public bool Interuptable { get; set; }

        public float TimeToFinishAction
        {
            get { return _timer.Duration; }
            set { _timer.Duration = value; }
        }

        public float IntervalTime
        {
            get { return _intervalTimer.Duration; }
            set { _intervalTimer.Duration = value;}
        }

        public bool DoingAction
        {
            get
            {
                return _timer.Running;
            }
        }

        public bool CanDoAction
        {
            get { return !_intervalTimer.Running; }
        }


        public bool AutoFinish
        {
            get { return !_timer.Infinit; }
            set { _timer.Infinit = !value; }
        }

        private Timer _timer;
        private Timer _intervalTimer;


        public ActionComponent(IGameObject gameObject) 
            : base(gameObject)
        {
            _timer = new Timer {Repeat = false};
            _timer.UpdateEvent += UpdateTimer;
            _timer.TickEvent += TimerFinishEvent;
            AutoFinish = true;

            _intervalTimer = new Timer {Repeat = false};
            _intervalTimer.TickEvent += () => Owner.SendMessage(new ActionMessage(ActionMessageType.Ready));
        }

        private void TimerFinishEvent()
        {
            FinishAction();
        }

        private void UpdateTimer(float dt)
        {
            Owner.SendMessage(new ActionMessage(ActionMessageType.Update, dt));
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is UpdateMessage)
            {
                var m = (UpdateMessage) message;

                    _timer.Update(m.DT);
                if (AutoFinish || _intervalTimer.Running)
                    _intervalTimer.Update(m.DT);
            }
            if (message is ActionMessage)
            {
                var m = (ActionMessage) message;
                switch (m.Type)
                {
                    case ActionMessageType.StartRequest:
                        if (!DoingAction && CanDoAction)
                            Owner.SendMessage(new ActionMessage(ActionMessageType.Start));
                        break;
                    case ActionMessageType.Start:
                        _timer.Run();
                        break;
                    case ActionMessageType.Finish:
                        _timer.Stop();
                        _intervalTimer.Run();
                        break;
                    case ActionMessageType.Interupt:
                        if(!Interuptable)
                            break;
                        _timer.Stop();
                        _intervalTimer.Run();
                        break;
                    case ActionMessageType.InterupRequest:
                        if(!Interuptable)
                            break;
                        if (_timer.Running && !_intervalTimer.Running)
                            Owner.SendMessage(new ActionMessage(ActionMessageType.Interupt));
                        break;
                }
            }
        }

        public void DoAction()
        {
            if(DoingAction || !CanDoAction)
                return;

            Owner.SendMessage(new ActionMessage(ActionMessageType.Start));
        
        }

        public void InteruptAction()
        {
            if (DoingAction)
            {
                Owner.SendMessage(new ActionMessage(ActionMessageType.Interupt));
            }
        }

        public void FinishAction()
        {
            Owner.SendMessage(new ActionMessage(ActionMessageType.Finish));
        }
    }
}
