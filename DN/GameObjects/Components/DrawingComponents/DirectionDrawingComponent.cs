﻿using Blueberry.Graphics;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    class DirectionDrawingComponent:Component, IDrawable
    {
        DirectionComponent _directionComponent;
        BoundaryComponent _boundaryComponent;
        AlphaComponent _alphaComponent;

        Texture _sprite;

        float _angle;
        Size _size;
        float _offset;
        float _pivotX;
        float _pivotY;

        public DirectionDrawingComponent(IGameObject gameObject, Texture sprite, Size size, float offsetProportion = 0, float pivotX = 0, float pivotY = 0.5f)
            :base(gameObject)
        {
            _sprite = sprite;
            _size = size;
            this._offset = offsetProportion;
            this._pivotX = pivotX;
            this._pivotY = pivotY;
        }

        public override bool GetDependicies()
        {
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _alphaComponent = Owner.GetComponent<AlphaComponent>();
            return _boundaryComponent != null;
        }


        public override void ProccesMessage(IMessage message)
        {
        }

        public void SetDirectionComponent(DirectionComponent directionComponent)
        {
            _directionComponent = directionComponent;
        }

        public void Draw(float dt)
        {
            Vector2 pos = _boundaryComponent.Position;
            bool flip = false;
            if(_directionComponent != null)
            {
                pos += _directionComponent.Direction * (_offset * _size.Width);
                flip = _directionComponent.Direction.X < 0;
            }
            SpriteBatch.Instance.DrawTexture(_sprite, pos.X, pos.Y, _size.Width, _size.Height,
                                             RectangleF.Empty,
                                             new Color4(1f, 1f, 1f,_alphaComponent != null ? _alphaComponent.Alpha : 1.0f),
                                             _directionComponent != null ? _directionComponent.Angle : 0.0f,
                                             _pivotX, _pivotY,flip, flip);
        }
    }
}
