﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using OpenTK.Graphics;

namespace DN.GameObjects.Components.DrawingComponents
{
    class CharDrawingComponent:Component, IDrawable
    {
        private readonly BitmapFont _font;
        private readonly char _ch;
        private readonly Color4 _color;
        private BoundaryComponent _boundary;
        private AlphaComponent _alphaComponent;

        public CharDrawingComponent(IGameObject gameObject,BitmapFont font, char ch, Color4 color) : base(gameObject)
        {
            _font = font;
            _ch = ch;
            _color = color;
        }

        public override bool GetDependicies()
        {
            _boundary = Owner.GetComponent<BoundaryComponent>();
            _alphaComponent = Owner.GetComponent<AlphaComponent>();
            return _boundary != null;
        }

        public override void ProccesMessage(IMessage message)
        {
        }

        public void Draw(float dt)
        {
            SpriteBatch.Instance.PrintSymbol(_font, _ch, _boundary.Position,
                                             new Color4(_color.R, _color.G, _color.B,
                                                        _alphaComponent == null ? 1.0f : _alphaComponent.Alpha), 0,
                                             1, 0, 0);
        }
    }
}
