﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GUI;
using DN.GameObjects.Messages;
using OpenTK;
using OpenTK.Graphics;

namespace DN.GameObjects.Components
{
    public class PopupTextOnDamageTaken:Component
    {
        public PopupTextOnDamageTaken(IGameObject gameObject) : base(gameObject)
        {
        }


        public override void ProccesMessage(IMessage message)
        {
            var msg = message as DamageMessage;
            if (msg != null)
            {
                if (msg.DamageWay == DamageWay.Taken && msg.Dealed)
                    PopupTextThrower.Instance.CreatePopUp(msg.Amount.ToString() + (msg.CriticalStrike? "!":""), Color4.Red,
                                                             new Color4(1.0f, 0, 0, 0.0f),
                                                             Owner.GetComponent<BoundaryComponent>().Position,
                                                             RandomTool.NextUnitVector2(), 4, 0);
            }
        }
    }
}
