﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Graphics;
using DN.GameObjects.Messages;
using OpenTK;
using OpenTK.Graphics;
using DN.GameObjects.Components.StanceComponents;

namespace DN.GameObjects
{
    public class SimpleDrawingComponent:Component,IDrawable
    {
        private Texture _sprite;
        private BoundaryComponent _boundaryComponent;
        private AlphaComponent _alphaComponent;

        public SimpleDrawingComponent(IGameObject gameObject, string sprite) : base(gameObject)
        {
            _sprite = CM.I.tex(sprite);
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _alphaComponent = Owner.GetComponent<AlphaComponent>();
            return _boundaryComponent != null;
        }


        public override void ProccesMessage(IMessage message)
        {
        }

        public void Draw(float dt)
        {
            SpriteBatch.Instance.DrawTexture(_sprite, _boundaryComponent.Bounds, RectangleF.Empty,
                new Color4(1f, 1f, 1f, _alphaComponent != null? _alphaComponent.Alpha: 1.0f), 0.0f, new Vector2(0, 0));
        }
    }
}
