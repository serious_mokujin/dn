﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Components.AnimationComponents.Listeners;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects
{
    public class HealthComponent : Component, IUpdatable
    {
        public float MaxKnockBackY = 2;

        private GameObjectsManager _gameObjectsManager;
        private GameObjectsFabric _fabric;
        
        private readonly bool _knockback;
        private float _maxHealth;

        public float MaxHealth
        {
            get { return _maxHealth; }
            private set { _maxHealth = value; }
        }
        public float InvularabilityDuration 
        {
            get { return _invTimer.Duration; }
            private set { _invTimer.Duration = value; }
        }

        public float Health { get; private set; }
        public bool IsDead
        {
            get { return Health <= 0; }
        }
        public bool Vulnurable
        {
            get { return !_invTimer.Running; }
        }

        private Timer _invTimer;


        public HealthComponent(IGameObject gameObject,GameObjectsManager gameObjectsManager, GameObjectsFabric _fabric,
                               float maxHealth = 0, float invularabilityDuration = 0, bool knockback = true)
            : base(gameObject)
        {
            MaxHealth = maxHealth;
            Health = MaxHealth;           

            _gameObjectsManager = gameObjectsManager;
            this._fabric = _fabric;
            _knockback = knockback;
            _invTimer = new Timer();
            _invTimer.Repeat = false;
            InvularabilityDuration = invularabilityDuration;
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is DamageMessage)
            {
                DamageMessage dm = (DamageMessage) message;
                if (dm.DamageWay == DamageWay.Taken)
                    DealDamage(dm.Object, dm.Amount, dm.Direction, dm.Amount, dm.CriticalStrike);
            }
            else if (message is DieMessage)
            {
                Health = 0;
                _gameObjectsManager.AddObject(_fabric.CreateCorpse(Owner));
               	_gameObjectsManager.RemoveObject(Owner);
               	
            }
        }

        public bool DealDamage(IGameObject dealer, float amount, Vector2 knockbackDirection, float knockbackPower = 0.0f, bool criticalStrike = false)
        {
            if (!Vulnurable)
                return false;
            Health -= amount;

            if (IsDead)
            {
                Owner.SendMessage(new DieMessage());
            }
            if (_knockback)
            {
                while (Math.Abs((knockbackDirection*knockbackPower).Y) > MaxKnockBackY)
                    knockbackPower /= 2; //kinda hack
			
                Owner.SendMessage(new MoveMessage(knockbackDirection, knockbackPower, false));
            }
            _invTimer.Run();
            Owner.SendMessage(new DamageMessage(dealer, amount, DamageWay.Taken, knockbackDirection, criticalStrike, true));
            if(dealer != null)
                dealer.SendMessage(new DamageMessage(Owner, amount, DamageWay.Given, knockbackDirection, criticalStrike, true));
            return true;
        }

        public void Heal(float amount)
        {
            Health += amount;
            if(Health > MaxHealth)
            {
                Health = MaxHealth;
            }
        }

        public void Update(float dt)
        {
            _invTimer.Update(dt);
        }
    }
}
