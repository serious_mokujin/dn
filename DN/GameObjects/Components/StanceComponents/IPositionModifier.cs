﻿using OpenTK;
namespace DN.GameObjects.Components
{
    public interface IPositionModifier: IComponent
    {
        Vector2 Offset { get; }
    }
}
