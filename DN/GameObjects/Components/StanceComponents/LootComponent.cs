﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using System.Drawing;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.StanceComponents
{
    public struct Loot
    {
        public int Count;
        public GameObjectType Type;
        public float Probability;

        public Loot(GameObjectType type, float probability, int count = 1)
        {
            Count = count;
            Type = type;
            Probability = probability;
        }
    }
    public class LootComponent:Component
    {
        private GameObjectsFabric _fabric;
        private GameObjectsManager _gameObjectsManager;
        private Loot[] _loot;

        public LootComponent(IGameObject gameObject, GameObjectsManager gameObjectsManager,
                             GameObjectsFabric fabric, Loot[] loot)
            :base(gameObject)
        {
            _gameObjectsManager = gameObjectsManager;
            _fabric = fabric;
            _loot = loot;
        }

        public override void ProccesMessage(IMessage message)
        {
        }

        public void Drop()
        {
            for (int i = 0; i < _loot.Length; i++)
            {
                int c = _loot[i].Count;
                do
                {
                    if (RandomTool.NextBool(_loot[i].Probability))
                    {
                        IGameObject gameObject = _fabric.CreateObject(_loot[i].Type, Point.Empty);
                        gameObject.SendMessage(new MoveMessage(RandomTool.NextUnitVector2(),
                                                               RandomTool.NextSingle(6, 12), false));
                        gameObject.GetComponent<BoundaryComponent>().Cell =
                            Owner.GetComponent<BoundaryComponent>().Cell;
                        _gameObjectsManager.AddObject(gameObject);
                        c--;
                    }
                    else c = 0;
                } while (c > 0);

            }
        }
    }
}
