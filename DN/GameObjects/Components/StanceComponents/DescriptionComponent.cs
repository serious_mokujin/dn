﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.GameObjects.Components.PhysicComponents;
using DN.GameObjects.Components.ItemsActions;

namespace DN.GameObjects.Components.StanceComponents
{
    public class DescriptionComponent:Component
    {
        public string Name
        {
            get;
            private set;
        }
        public string Description
        {
            get;
            private set;
        }
        public DescriptionComponent(IGameObject gameObject, string name)
            :base(gameObject)
        {
            Name = name;
        }

        public override bool GetDependicies()
        { 
            Description = "";

            var _damageComponent = Owner.GetComponent<DamageComponent>();
            if (_damageComponent != null)
            {
                Description += "Deals damage from " + _damageComponent.MinDamage + "to " + _damageComponent.MaxDamage + 
                               ". Critical chance: " + _damageComponent.CriticalChance * 100 + "%.";
            }
            var _throwable = Owner.GetComponent<ThrowableComponent>();
            if (_throwable != null)
                Description += " Can be thrown.";

            var explodable = Owner.GetComponent<ExplodeOnTimer>();
            if (explodable != null)
                Description += " Explodes in " + explodable.Duration + "seconds";
            var healthOnUse = Owner.GetComponent<PickuperHealthUpOnAction>();
            if (healthOnUse != null)
            {
                Description += " Heals " + healthOnUse.Amount;
            }


            return true;
        }

        public override void ProccesMessage(IMessage message)
        {
        }
    }
}
