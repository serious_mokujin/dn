﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.StanceComponents
{
    public class AlphaComponent:Component
    {
        public float Alpha = 1.0f;

        public AlphaComponent(IGameObject gameObject)
            : base(gameObject)
        {}


        public override void ProccesMessage(Messages.IMessage message)
        {

        }
    }
}
