﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components
{
    class ChangeDirectionToMovingDirection:Component, IUpdatable
    {
        Vector2 _oldPosition;
        DirectionComponent _directionComponent;
        BoundaryComponent _boundaryComponent;
        

        public ChangeDirectionToMovingDirection(IGameObject gameObject) : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return _boundaryComponent != null && _directionComponent != null;
        }


        public override void ProccesMessage(IMessage message)
        {
        }

        public void Update(float dt)
        {
            _directionComponent.Direction = _boundaryComponent.Position - _oldPosition;
            _oldPosition = _boundaryComponent.Position;
        }
    }
}
