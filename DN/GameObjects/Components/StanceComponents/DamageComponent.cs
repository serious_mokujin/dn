﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.StanceComponents
{
    /// <summary>
    /// Stores information about damage
    /// </summary>
    public class DamageComponent:Component
    {
        public float MinDamage;
        public float MaxDamage;
        public float CriticalChance;
        public float CriticalDamage;

        public DamageComponent(IGameObject gameObject) : base(gameObject)
        {
            CriticalDamage = 2;
        }

        public override void ProccesMessage(IMessage message)
        {

        }
    }
}
