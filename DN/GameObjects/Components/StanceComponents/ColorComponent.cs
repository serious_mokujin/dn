﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.StanceComponents
{
    public class ColorComponent:Component
    {
        public Color Color1;
        public Color Color2;

        public ColorComponent(IGameObject gameObject) : base(gameObject)
        {
        }

        public override void ProccesMessage(IMessage message)
        {
        }


    }
}
