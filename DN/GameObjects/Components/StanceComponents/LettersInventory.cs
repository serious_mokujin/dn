﻿using System;
using System.Collections.Generic;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.StanceComponents
{
    public class LettersInventory:Component
    {
        public readonly int[] Letters;

        public LettersInventory(IGameObject gameObject)
            : base(gameObject)
        {
            Letters = new int[26];
            for (int i = 0; i < 26; i++)
            {
                Letters[i] = 100;
            }
        }

        public int GetByLetter(char ch)
        {
            return Letters[ch - 97];

        }

        public void Add(char ch)
        {
            Letters[ch - 97]++;
        }
        public void Remove(char ch)
        {
            Letters[ch - 97]--;
        }

        public override void ProccesMessage(IMessage message)
        {
            return;
        }
    }
}
