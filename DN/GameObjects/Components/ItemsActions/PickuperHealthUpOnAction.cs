﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ItemsActions
{
    class PickuperHealthUpOnAction:Component
    {
        private GameObjectsManager _manager;
        public float Amount{get; private set;}
        private PickupableComponent _pickupableComponent;

        public PickuperHealthUpOnAction(IGameObject gameObject, GameObjectsManager manager, float amount) : base(gameObject)
        {
            _manager = manager;
            Amount = amount;
        }

        public override bool GetDependicies()
        {
            _pickupableComponent = Owner.GetComponent<PickupableComponent>();
            return _pickupableComponent != null;
        }

        
        public override void ProccesMessage(IMessage message)
        {
            if (message is ActionMessage)
            {
                var m = (ActionMessage) message;

                if(m.Type == ActionMessageType.Finish)
                {
                    IGameObject gameObject = _pickupableComponent.Pickuper;
                    var h = gameObject.GetComponent<HealthComponent>();
                    if(h != null)
                    {
                        h.Heal(Amount);
                        _manager.RemoveObject(Owner);
                    }
                }
            }
        }
    }
}
