﻿using DN.GameObjects.Messages;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    public class StraightAttackingComponent: 
        Component, IPositionModifier,IUpdatable
    {
        private float _attackSpeed;
        public float CurrentOffset { get; private set; }
        public bool MoveBack { get;private set; }

        public Vector2 Offset { get { return new Vector2(CurrentOffset, CurrentOffset); } }


        public StraightAttackingComponent(IGameObject gameObject, float attackSpeed)
            : base(gameObject)
        {
            _attackSpeed = attackSpeed;
            
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is ActionMessage)
            {
                HandleActionMessage((ActionMessage)message);
            }
        }

        private void HandleActionMessage(ActionMessage message)
        {
            switch (message.Type)
            {
                case ActionMessageType.Start:
                    CurrentOffset = 0;
                    MoveBack = false;
                    break;
                case ActionMessageType.Finish:
                    MoveBack = true;
                    CurrentOffset = 0;
                    break;
                case ActionMessageType.Update:
                    CurrentOffset += message.DT * _attackSpeed;
                    break;
                case ActionMessageType.Interupt:
                    MoveBack = false;
                    CurrentOffset = 0;
                    break;
                default:
                    break;
            }
        }

        public void Update(float dt)
        {
            if (MoveBack)
            {
                CurrentOffset -= dt * _attackSpeed;
                if (CurrentOffset <= 0)
                {
                    MoveBack = false;
                    CurrentOffset = 0;
                }
            }
        }
    }
}
