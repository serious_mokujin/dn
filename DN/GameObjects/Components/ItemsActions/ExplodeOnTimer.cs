﻿using DN.GameObjects.Messages;
using DN.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.ItemsActions
{
    public class ExplodeOnTimer:Component, IUpdatable
    {
        public float Duration
        {
            get { return _explodeTimer.Duration; }
        }

        Timer _explodeTimer;

        GameObjectsManager _manager;
        GameObjectsFabric _fabric;
        private BoundaryComponent _boundaryComponent;

        SizeF _size; 
        float _damageMin;
        float _damageMax;
        float _critChance;
        public ExplodeOnTimer(IGameObject gameObject, GameObjectsManager manager, GameObjectsFabric fabric, float delay, SizeF size,
                              float damageMin, float damageMax, float critChance)
            : base(gameObject)
        {
            _explodeTimer = new Timer();
            _explodeTimer.TickEvent += TimerTickEvent;
            _explodeTimer.Duration = delay;

            _manager = manager;
            _fabric = fabric;

            _size = size;
            _damageMin = damageMin;
            _damageMax = damageMax;
            _critChance = critChance;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return _boundaryComponent != null;
        }

        private void TimerTickEvent()
        {
            IGameObject exp = _fabric.CreateExplosion(_boundaryComponent.Position, _size, _damageMin, _damageMax, _critChance);
            _manager.AddObject(exp);
            _manager.RemoveObject(Owner);
        }

        public override void ProccesMessage(Messages.IMessage message)
        {
            if (message is ActionMessage)
            {
                ActionMessage msg = (ActionMessage)message;
                if (msg.Type == ActionMessageType.Finish)
                {
                    if(!_explodeTimer.Running)
                    _explodeTimer.Run();
                }
            }
        }

        public void Update(float dt)
        {
            _explodeTimer.Update(dt);
        }
    }
}
