﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.OutsideInteractingComponents
{
    public class CraftingComponent:Component
    {
        private readonly GameObjectsManager _manager;
        private BoundaryComponent _boundaryComponent;

        public CraftingComponent(IGameObject gameObject, GameObjectsManager manager) : base(gameObject)
        {
            _manager = manager;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return base.GetDependicies();
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is ActionMessage)
            {
                ActionMessage msg = (ActionMessage) message;
                msg.Sender.SendMessage(new CraftingMessage(CraftingMessageType.Started, Owner));
            }
        }
        public bool CreateItem(string str)
        {
            var gameObject = CraftingManager.Instance.TryCraft(str);
            if (gameObject != null)
            {
                gameObject.GetComponent<BoundaryComponent>().Cell = _boundaryComponent.Cell;
                _manager.AddObject(gameObject);
                return true;
            }
            return false;
        }
    }
}
