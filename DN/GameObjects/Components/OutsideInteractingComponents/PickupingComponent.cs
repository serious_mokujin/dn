﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Components;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects
{
    public class PickupingComponent : Component, IUpdatable
    {
        public event EventHandler ItemChanged;

        public int MaxCount { get; private set; }


        public IGameObject[] PickedupObjects { get; private set; }
        public int CurrentItem { get; private set; }

        public IGameObject ChosenObject
        {
            get { return PickedupObjects[CurrentItem]; }
            private set { PickedupObjects[CurrentItem] = value; }
        }


        private BoundaryComponent _boundaryComponent;
        private DirectionComponent _directionComponent;

        private Vector2 PickupedItemPosition
        {
            get
            {
                if (_directionComponent != null)
                {
                    var angle = _directionComponent.Angle;

                    List<IPositionModifier> positionModifiers = ChosenObject.GetComponents<IPositionModifier>();
                    Vector2 _pmOffset = Vector2.Zero;
                    foreach (var pm in positionModifiers)
                    {
                        _pmOffset += pm.Offset;
                    }

                    return
                        new Vector2(
                            _boundaryComponent.Position.X +
                            (_boundaryComponent.Size.Width + _pmOffset.X)*(float) Math.Cos(angle),
                            _boundaryComponent.Position.Y +
                            (_boundaryComponent.Size.Height + _pmOffset.Y)*(float) Math.Sin(angle));
                }
                return _boundaryComponent.Position;
            }
        }



        private List<IGameObject> _collisions;
        private PickupableComponent[] _pickupableComponents;

        private bool _pickingUp = false;

        public PickupingComponent(IGameObject gameObject, int maxCount)
            : base(gameObject)
        {
            MaxCount = maxCount;
            _collisions = new List<IGameObject>();
            _pickupableComponents = new PickupableComponent[MaxCount];
            PickedupObjects = new IGameObject[MaxCount];
        }


        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            return base.GetDependicies();
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is CollisionMessage)
            {
                OnCollision((CollisionMessage) message);
            }
            else if (message is ActionMessage)
            {
                if (ChosenObject != null)
                {
                    ChosenObject.SendMessage(message);
                }
            }
            else if (message is DieMessage)
            {
                Drop();
            }
        }

        public void Update(float dt)
        {
            if (_pickingUp && _collisions.Count != 0)
                foreach (var gameObject in _collisions)
                {
                    if (gameObject == Owner)
                        continue;

                    PickupableComponent pickupableComponent = gameObject.GetComponent<PickupableComponent>();
                    if (pickupableComponent != null)
                    {
                        if (pickupableComponent.Pickuper != null)
                            continue;

                        Drop();
                        ChosenObject = gameObject;
                        ChosenObject.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));
                        ChosenObject.SendMessage(new ObjectsCollisionsStateMessage(false, new[] {Owner.ObjectGroup}));
                        var dirComp = ChosenObject.GetComponent<DirectionComponent>();
                        if (dirComp != null)
                            dirComp.ControllingDirectionComponent = Owner.GetComponent<DirectionComponent>();
                        pickupableComponent.SetPickuper(Owner);

                        _pickupableComponents[CurrentItem] = pickupableComponent;
                        if (ItemChanged != null)
                            ItemChanged(this, EventArgs.Empty);
                        break;
                    }
                }
            EndPickuping();

            if (ChosenObject != null)
            {
                BoundaryComponent b;
                if ((b = ChosenObject.GetComponent<BoundaryComponent>()) != null)
                {
                    b.Position = PickupedItemPosition;
                }
            }
        }

        private void OnCollision(CollisionMessage message)
        {
            _collisions.Add(message.CollidedObject);
        }

        public void Use()
        {
            if (ChosenObject != null)
            {
                ChosenObject.SendMessage(new ActionMessage(Owner, ActionMessageType.StartRequest));
            }
        }

        public void StartPickuping()
        {
            _pickingUp = true;
        }


        public void Drop()
        {
            if (_pickupableComponents[CurrentItem] != null)
            {
                _pickupableComponents[CurrentItem].SetPickuper(null);
                var dirComp = ChosenObject.GetComponent<DirectionComponent>();
                if (dirComp != null)
                    dirComp.ControllingDirectionComponent = null;
                ChosenObject.SendMessage(new ActionMessage(Owner, ActionMessageType.InterupRequest));
                ChosenObject.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                ChosenObject.SendMessage(new ObjectsCollisionsStateMessage(false));
                ChosenObject.GetComponent<BoundaryComponent>().Position = _boundaryComponent.Position;
                if (ItemChanged != null)
                    ItemChanged(this, EventArgs.Empty);
            }
            ChosenObject = null;
            _pickupableComponents[CurrentItem] = null;
        }

        public void EndPickuping()
        {
            _collisions.Clear();
            _pickingUp = false;
        }

        public void ChooseNext()
        {
            DeactivateObject(ChosenObject);
            CurrentItem += 1;
            if (CurrentItem >= MaxCount)
                CurrentItem = 0;
            ActivateObject(ChosenObject);
            if (ItemChanged != null)
                ItemChanged(this, EventArgs.Empty);
        }

        public void ChoosePrevious()
        {
            DeactivateObject(ChosenObject);
            CurrentItem -= 1;
            if (CurrentItem < 0)
                CurrentItem = MaxCount - 1;
            ActivateObject(ChosenObject);
            if (ItemChanged != null)
                ItemChanged(this, EventArgs.Empty);
        }

        private void ActivateObject(IGameObject gameObject)
        {
            if (gameObject == null)
                return;
            gameObject.Active = true;
            gameObject.AutoChangeActivity = true;
        }

        private void DeactivateObject(IGameObject gameObject)
        {
            if (gameObject == null)
                return;
            gameObject.Active = false;
            gameObject.AutoChangeActivity = false;
        }
    }
}
