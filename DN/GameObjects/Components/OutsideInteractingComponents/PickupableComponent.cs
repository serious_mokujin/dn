﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects
{
    public class PickupableComponent:Component
    {
        public IGameObject Pickuper { get; private set; }


        public PickupableComponent(IGameObject gameObject) : base(gameObject)
        {
            
        }

        public override bool GetDependicies()
        {
            Owner.SendMessage(new ObjectsCollisionsStateMessage(false));
            return true;
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is ActionMessage)
            {
                HandleActionMessage((ActionMessage)message);
            }
            return;
        }

        private void HandleActionMessage(ActionMessage actionMessage)
        {
            if (actionMessage.Type == ActionMessageType.Start)
            {
                Owner.SendMessage(new ObjectsCollisionsStateMessage(true, new []{Pickuper.ObjectGroup}));
            }
            else if (actionMessage.Type == ActionMessageType.Finish 
                  || actionMessage.Type == ActionMessageType.Interupt)
                Owner.SendMessage(new ObjectsCollisionsStateMessage(false, null));
        }

        public void SetPickuper(IGameObject gameObject)
        {
            Pickuper = gameObject;
            Owner.SendMessage(new PickupMessage(gameObject));
        }
    }
}
