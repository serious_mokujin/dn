﻿using DN.GameObjects.Messages;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.PhysicComponents
{
    class ThrowableComponent:Component, IPositionModifier
    {
        private readonly TileMap _tileMap;
        private float _tension;
        public float TensionGrowingSpeed;
        public float MaxTension;
        public float MinTension;

        DirectionComponent _directionComponent;

        private float _projectiveSpeed;

        public float ProjectiveSpeed
        {
            get { return _projectiveSpeed * (_tension); }
            set { _projectiveSpeed = value; }
        }

        public ThrowableComponent(IGameObject gameObject, TileMap tileMap)
            :base(gameObject)
        {
            _tileMap = tileMap;
        }

        public override bool GetDependicies()
        {
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            return _directionComponent != null;
        }

        public override void ProccesMessage(Messages.IMessage message)
        {
            if(message is ActionMessage)
                HandleActionMessage((ActionMessage)message);
        }
        private void HandleActionMessage(ActionMessage message)
        {
            switch (message.Type)
            {
                case ActionMessageType.Start:
                    _tension = MinTension;
                    break;
                case ActionMessageType.Update:
                    if (MaxTension > _tension)
                        _tension += message.DT * TensionGrowingSpeed;
                    if (MaxTension < _tension)
                        _tension = MaxTension;
                    break;
                case ActionMessageType.Interupt:
                    BoundaryComponent b = Owner.GetComponent<BoundaryComponent>();

                    var oldPos = b.Position;
                    Vector2 dir = _directionComponent.Direction;
                    dir.Normalize();

                    var pkc = Owner.GetComponent<PickupableComponent>();
                    if (pkc != null)
                    {
                        if (pkc.Pickuper != null)
                            pkc.Pickuper.GetComponent<PickupingComponent>().Drop();
                    }
                    b.Position = oldPos;

                    var list =_tileMap.GetCollisionsWithTiles(b.Bounds);
                    if(list.Any(p => p.CellType == CellType.Wall))
                    {
                        Owner.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));
                    }
                    else
                    {

                        Owner.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                        Owner.SendMessage(new MoveMessage(dir, ProjectiveSpeed, false,false, MoveType.Set));
                    }

                    Owner.SendMessage(new ActionMessage(Owner, ActionMessageType.Finish));
                    break;
            }
        }


        public Vector2 Offset
        {
            get { return new Vector2(-_tension, -_tension); }
        }
    }
}
