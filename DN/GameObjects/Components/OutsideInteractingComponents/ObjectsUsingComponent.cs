﻿using DN.GameObjects.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    class OutsideUsingComponent:Component, IUpdatable
    {
        List<IGameObject> _collidedObjects;
        OutsideActionMessage _outsideActionMessage;


        public OutsideUsingComponent(IGameObject gameObject)
            :base(gameObject)
        {
            _collidedObjects = new List<IGameObject>();
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is OutsideActionMessage)
            {
                HandleOutsideActionMessage((OutsideActionMessage)message);
            }
            else if (message is CollisionMessage)
            {
                HandleCollisionMessage((CollisionMessage)message);
            }
        }

        private void HandleOutsideActionMessage(OutsideActionMessage message)
        {
            _outsideActionMessage = message;

        }

        private void HandleCollisionMessage(CollisionMessage message)
        {
            _collidedObjects.Add(message.CollidedObject);
        }

        public void Update(float dt)
        {
            if (_outsideActionMessage != null)
            {
                foreach (var collidedObject in _collidedObjects)
                {
                    if (collidedObject.GetComponent<OutsideUseableComponent>() == null) continue;
                    collidedObject.SendMessage(_outsideActionMessage);
                    _outsideActionMessage = null;
                    break;
                }
            }
            _outsideActionMessage = null;
            _collidedObjects.Clear();
        }
    }
}
