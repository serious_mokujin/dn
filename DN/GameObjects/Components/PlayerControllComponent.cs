﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Input;
using DN.GameObjects.Components;
using DN.GameObjects.Messages;
using OpenTK;
using OpenTK.Input;

namespace DN.GameObjects
{
    class PlayerControllComponent:Component, IDisposable
    {
        private bool _attackButtonWasPressed;

        private PhysicComponent _physicComponent;
        CameraControllerComponent _cameraComponent;
        BoundaryComponent _boundaryComponent;

 //       private JumpComponent _jumpComponent;
        private LadderClimbingComponent _ladderClimbingComponent;
        private PickupingComponent _pickupingComponent;
        private DirectionComponent _directionComponent;

        public PlayerControllComponent(IGameObject gameObject) : base(gameObject)
        {
            Game.g_Gamepad.OnButtonPress += g_Gamepad_OnButtonPress;
            Game.g_Gamepad.OnButtonUp += g_Gamepad_OnButtonUp;
            Game.g_Gamepad.OnRightTrigger += GGamepadOnRightTrigger;

            Game.g_Mouse.ButtonUp += GMouseOnButtonUp;

            Game.g_Keyboard.KeyDown += g_Keyboard_KeyDown;
            Game.g_Keyboard.KeyUp += g_Keyboard_KeyUp;
            Game.g_Keyboard.KeyRepeat = true;

        }



        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            _ladderClimbingComponent = Owner.GetComponent<LadderClimbingComponent>();
            _pickupingComponent = Owner.GetComponent<PickupingComponent>();
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _cameraComponent = Owner.GetComponent<CameraControllerComponent>();
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();

            _physicComponent.CollisionWithTiles += PhysicComponentOnCollisionWithTiles;

            return _physicComponent != null
                && _ladderClimbingComponent != null
                && _pickupingComponent != null
                && _directionComponent != null
                && _boundaryComponent != null;
        }

        private void PhysicComponentOnCollisionWithTiles(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity)
        {
            if (_physicComponent.FallingDistance > 500)
                if(collidedTiles.Any(p => p.CellType == CellType.Wall && p.Direction.Y == 1))
                {
                    Game.g_Gamepad.Vibrate(1f, 1f, 0.3f);
                    Owner.GetComponent<HealthComponent>().DealDamage(1, new Vector2(0, 1), 0);
                }
        }

        private void GGamepadOnRightTrigger(object sender, GamepadDevice.TriggerState triggerState)
        {
            if(triggerState.value == 0)
            {
                Owner.SendMessage(new ActionMessage(ActionMessageType.InterupRequest));
            }
        }
        private void GMouseOnButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(e.Button == MouseButton.Left)
                Owner.SendMessage(new ActionMessage(ActionMessageType.InterupRequest));
        }


        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                Update((UpdateMessage) message);
            }
        }

        private void Update(UpdateMessage message)
        {
            float dt = message.DT;

            Vector2 moveDirectionVector = Vector2.Zero;

            if (LeftKeyPressed())
                moveDirectionVector.X = -1;
            if (RightKeyPressed())
                moveDirectionVector.X = 1;
            if (UpKeyPressed())
                moveDirectionVector.Y = -1;
            if (DownKeyPressed())
                moveDirectionVector.Y = 1;

            moveDirectionVector.NormalizeFast();
            Vector2 leftStickPos = Game.g_Gamepad.LeftStick.Position;
            if (leftStickPos.X != 0)
            {
                moveDirectionVector = new Vector2(leftStickPos.X, leftStickPos.Y);
            }

            if (Game.g_Gamepad.Connected)
            {
                if (Game.g_Gamepad.RightStick.Position != Vector2.Zero)
                    _directionComponent.Direction = Game.g_Gamepad.RightStick.Position;
            }
            else
            {
                if (_cameraComponent != null)
                {
                    Vector2 mouse = _cameraComponent.Camera.ToWorld(new Vector2(Game.g_Mouse.X, Game.g_Mouse.Y));
                    _directionComponent.Direction = Vector2.Normalize(mouse - _boundaryComponent.Position);
                    
                }
                else
                {
                    _directionComponent.Direction =
                        Vector2.Normalize(new Vector2(Game.g_Mouse.X - Game.g_screenSize.Width / 2,
                                                      Game.g_Mouse.Y - Game.g_screenSize.Height / 2));
                }
            }


            if ((UpKeyPressed() && _physicComponent.GetVelocity().Y >= 0))
            {
                _ladderClimbingComponent.TryClimbLadder();
            }

            if (moveDirectionVector != Vector2.Zero)
            {
                if(!_ladderClimbingComponent.ClimbLadder)
                    _physicComponent.Move(new Vector2(1, 0), dt*moveDirectionVector.X*18);
                else
                    _ladderClimbingComponent.MoveLadder(moveDirectionVector, 3);

            }

            if(Game.g_Gamepad.RightTrigger > 0.3f || Game.g_Mouse[MouseButton.Left])
            {
                _pickupingComponent.Use();
            }
        }


        private void g_Keyboard_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Owner.SendMessage(new EventMessage(Events.StopJump));
            }
        }
        void g_Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                
                if (!DownKeyPressed())
                    Owner.SendMessage(new EventMessage(Events.StartJump));
                _ladderClimbingComponent.StopClimbLadder();
            }
            else if (e.Key == Key.E)
            {
                _pickupingComponent.StartPickuping();
            }
        }


        private void g_Gamepad_OnButtonPress(object sender, GamepadButtonFlags e)
        {
            if (e.HasFlag(GamepadButtonFlags.A))
            {
                if (!DownKeyPressed())
                    Owner.SendMessage(new EventMessage(Events.StartJump));
                _ladderClimbingComponent.StopClimbLadder();
            }
            else if(e.HasFlag(GamepadButtonFlags.X))
            {
                _pickupingComponent.StartPickuping();
                Owner.SendMessage(new OutsideActionMessage(OutsideActionType.TryUse, Owner));
            }
        }
        private void g_Gamepad_OnButtonUp(object sender, GamepadButtonFlags e)
        {
            if (e.HasFlag(GamepadButtonFlags.A))
            {
                Owner.SendMessage(new EventMessage(Events.StopJump));
            }
        }

        private static bool DownKeyPressed()
        {
            return Game.g_Gamepad.DPad.Down || Game.g_Keyboard[Key.S] || Game.g_Gamepad.LeftStick.Position.Y > 0.3f;
        }

        private static bool UpKeyPressed()
        {
            return Game.g_Gamepad.DPad.Up || Game.g_Keyboard[Key.W] || Game.g_Gamepad.LeftStick.Position.Y < -0.3f;
        }

        private static bool RightKeyPressed()
        {
            return Game.g_Gamepad.DPad.Right || Game.g_Keyboard[Key.D] || Game.g_Gamepad.LeftStick.Position.X > 0.3f;
        }

        private static bool LeftKeyPressed()
        {
            return Game.g_Gamepad.DPad.Left || Game.g_Keyboard[Key.A] || Game.g_Gamepad.LeftStick.Position.X < -0.3f;
        }

        public void Dispose()
        {
            Game.g_Gamepad.OnButtonPress -= g_Gamepad_OnButtonPress;
            Game.g_Gamepad.OnButtonUp -= g_Gamepad_OnButtonUp;
            Game.g_Keyboard.KeyDown -= g_Keyboard_KeyDown;
            Game.g_Keyboard.KeyUp -= g_Keyboard_KeyUp;
        }
    }
}
