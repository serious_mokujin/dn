﻿using Blueberry;
using DN.GameObjects.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects
{
    public class CameraControllerComponent:Component
    {
        private BoundaryComponent _boundaryComponent;

        private Camera _camera;

        public Camera Camera { get { return _camera; } }

        public CameraControllerComponent(IGameObject gameObject, Camera camera)
            : base(gameObject)
        {
            _camera = camera;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return base.GetDependicies();
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is UpdateMessage)
            {
                Update((UpdateMessage)message);             
            }
        }

        private void Update(UpdateMessage message)
        {
            _camera.MoveTo(_boundaryComponent.Position);
            _camera.Update(message.DT);
        }
    }
}
