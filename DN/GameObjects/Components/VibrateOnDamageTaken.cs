﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components
{
    public class VibrateOnDamageTaken:Component
    {
        public VibrateOnDamageTaken(IGameObject gameObject) : base(gameObject)
        {
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is DamageMessage)
            {
                var m = (DamageMessage) message;
                if(m.DamageWay == DamageWay.Taken)
                    Game.g_Gamepad.Vibrate(0.5f, 0.5f, 0.2f);
            }
        }
    }
}
