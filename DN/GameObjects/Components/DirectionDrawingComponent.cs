﻿using Blueberry.Graphics;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    class DirectionDrawingComponent:Component
    {
        DirectionComponent _directionComponent;
        BoundaryComponent _boundaryComponent;


        Texture _sprite;

        float _angle;
        Size _size;

        public DirectionDrawingComponent(IGameObject gameObject, Texture sprite, Size size)
            :base(gameObject)
        {
            _sprite = sprite;
            _size = size;

        }

        public override bool GetDependicies()
        {
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();

            return _boundaryComponent != null;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is DrawMessage)
            {
                HandleDrawMessage((DrawMessage)message);
            }
            else if (message is UpdateMessage)
            {
                HandleUpdateMessage((UpdateMessage)message);
            }
            else if(message is PickupMessage)
            {
                if ((message as PickupMessage).Pickuper == null)
                {
                    SetDirectionComponent(Owner.GetComponent<DirectionComponent>());
                    return;
                }
                var dc = (message as PickupMessage).Pickuper.GetComponent<DirectionComponent>();
                SetDirectionComponent(dc);
            }
        }

        public void SetDirectionComponent(DirectionComponent directionComponent)
        {
            _directionComponent = directionComponent;
        }

        private void HandleUpdateMessage(UpdateMessage updateMessage)
        {
            if (_directionComponent != null)
            {
                _angle = _directionComponent.Angle;
            }
        }

        private void HandleDrawMessage(DrawMessage drawMessage)
        {
            SpriteBatch.Instance.DrawTexture(_sprite,
                                             new RectangleF(_boundaryComponent.Position.X, _boundaryComponent.Position.Y,
                                                            _size.Width, _size.Height),
                                             RectangleF.Empty, Color4.White, _angle, new Vector2(0.5f, 0.5f));
        }
    }
}
