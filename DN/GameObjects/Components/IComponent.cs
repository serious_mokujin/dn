﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects
{
    public interface IComponent
    {
        void ProccesMessage(IMessage message);
        bool GetDependicies();
    }
}
