﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Audio;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components
{
    public class EarthquakeComponent:Component, IDisposable
    {
        public float MinPower = 125;
        public float MaxPower = 165;

        public float Duration
        {
            get { return _durationTimer.Duration; }
            set { _durationTimer.Duration = value; }
        }

        private readonly GameObjectsManager _gameObjectsManager;
        private readonly Camera _camera;
        private bool _working;

        private readonly Timer _jerkTimer;
        private readonly Timer _durationTimer;

        private Vector2 _direction;
        private float _speed;

        private AudioClip sound;

        public EarthquakeComponent(IGameObject gameObject, GameObjectsManager gameObjectsManager, Camera camera)
            : base(gameObject)
        {
            _gameObjectsManager = gameObjectsManager;
            _camera = camera;
            _jerkTimer = new Timer{Repeat = true, Duration = 0.1f};
            _jerkTimer.UpdateEvent += MoveOnUpdateEvent;
            _jerkTimer.TickEvent += ChangeDirection;

            _durationTimer = new Timer();
            _durationTimer.TickEvent += () => Owner.SendMessage(new EarthquakeMessage(EarthquakeMessageType.End));

            sound = CM.I.Sound("earthquake");
        }

        private void ChangeDirection()
        {
            _direction = RandomTool.NextUnitVector2();
            _speed = RandomTool.RandFloat(MinPower, MaxPower);
        }

        private void MoveOnUpdateEvent(float dt)
        {
            _gameObjectsManager.SendGlobalMessage(new MoveMessage(_direction, _speed * dt, false, true));
            _gameObjectsManager.SendGlobalMessage(new EventMessage(Events.StopClimb));
            _camera.Rumble(0.1f, MinPower/10, 2);
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is EarthquakeMessage)
            {
                HandleEarthquakeMessage((EarthquakeMessage) message);
            }
            else if(message is UpdateMessage)
            {
                HandleUpdateMessage((UpdateMessage) message);
            }
        }

        private void HandleUpdateMessage(UpdateMessage message)
        {
            _jerkTimer.Update(message.DT);
            if(Duration != 0)
                _durationTimer.Update(message.DT);
        }

        private void HandleEarthquakeMessage(EarthquakeMessage message)
        {
            if (message.Type == EarthquakeMessageType.Begin)
            {
                if (!_working)
                {
                    sound.Stop();
                    sound.Play();
                    _jerkTimer.Run(true);
                    if (Duration != 0)
                        _durationTimer.Run();
                    _working = true;

                }
            }
            else
            {
                _jerkTimer.Stop();
                sound.Stop();
                if(Duration != 0)
                    _durationTimer.Stop();
                _working = false;
            }
        }

        public void Dispose()
        {
            sound.Stop();
        }
    }
}
