﻿using DN.GameObjects.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    class OutsideUseableComponent:Component
    {
        private IGameObject _user;

        public OutsideUseableComponent(IGameObject gameObject)
            : base(gameObject)
        {
 
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is OutsideActionMessage)
            {
                HandleOutsideActionMessage((OutsideActionMessage)message);
            }
            if (message is ActionMessage)
            {
                HandleActionMessage((ActionMessage)message);
            }
        }

        private void HandleActionMessage(ActionMessage actionMessage)
        {
            if (actionMessage.Type == ActionMessageType.Start)
                if (_user != null)
                    _user.SendMessage(new OutsideActionMessage(OutsideActionType.Using, Owner));
        }
        private void HandleOutsideActionMessage(OutsideActionMessage message)
        {
            if (message.Type == OutsideActionType.TryUse)
            {
                _user = message.Object;
                Owner.SendMessage(new ActionMessage(ActionMessageType.StartRequest));
            }
        }
    }
}
