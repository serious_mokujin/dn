﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components
{
    class MoveToDirection:Component
    {
        Vector2 _oldPosition;
        DirectionComponent _directionComponent;
        BoundaryComponent _boundaryComponent;
        

        public MoveToDirection(IGameObject gameObject) : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return _boundaryComponent != null && _directionComponent != null;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                _directionComponent.Direction = _boundaryComponent.Position - _oldPosition;
                _oldPosition = _boundaryComponent.Position;
            }
        }
    }
}
