﻿using DN.GameObjects.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.ActionComponents
{
    public class StuckToPointOnDamageGiven:StuckToPointOnCollision
    {
        public StuckToPointOnDamageGiven(IGameObject gameObject, GameObjectsManager manager)
            :base(gameObject, manager)
        { 
        }

        public override void ProccesMessage(Messages.IMessage message)
        {
            if (message is DamageMessage)
            {
                DamageMessage msg = (message as DamageMessage);

                if (msg.DamageWay == DamageWay.Given && msg.Dealed)
                {
                    StuckInObject(msg.Object);
                }
            }
        }
    }
}
