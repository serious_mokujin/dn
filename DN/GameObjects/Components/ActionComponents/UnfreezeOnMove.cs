﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ActionComponents
{
    class UnfreezeOnMove:Component
    {
        private readonly float _chance;
        private int _tries;

        public UnfreezeOnMove(IGameObject gameObject, float chance, int tries = 1) : base(gameObject)
        {
            _chance = chance;
            _tries = tries;
        }


        public override void ProccesMessage(IMessage message)
        {
            if(message is MoveMessage)
            {
                if(RandomTool.NextBool(_chance))
                {
                    _tries--;
                }
                if (_tries == 0)
                {
                    Owner.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                    _tries--;
                }
            }
        }
    }
}
