﻿using DN.GameObjects.Messages;
using DN.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.ActionComponents
{
    class GameObjectEmitierComponent: Component, IUpdatable
    {
        private readonly GameObjectsFabric _fabric;
        private readonly GameObjectsManager _manager;
        private GameObjectType _gameObjectType;
        private int _maxCount;
        private int _count;
        Timer _realiseTimer;
        private BoundaryComponent _boundaryComponent;


        public GameObjectEmitierComponent(IGameObject gameObject,GameObjectsFabric fabric, GameObjectsManager manager,
                                          GameObjectType gameObjectType,int maxCount, float realeseTime)
            :base(gameObject)
        {
            _fabric = fabric;
            _manager = manager;
            _gameObjectType = gameObjectType;
            _maxCount = maxCount;
            _realiseTimer = new Timer
                                {
                                    Repeat = true,
                                    Duration = realeseTime
                                };
            _realiseTimer.TickEvent += TickEvent;
            _realiseTimer.Run();
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();

            return _boundaryComponent != null;
        }

        private void TickEvent()
        {
            IGameObject gameObject = _fabric.CreateObject(_gameObjectType, _boundaryComponent.Cell);
            gameObject.OnRemove += (sender, args) => _count--;

            _manager.AddObject(gameObject);

            _count++;

        }

        
        public override void ProccesMessage(IMessage message)
        {}

        public void Update(float dt)
        {
            if (_count < _maxCount)
                _realiseTimer.Update(dt);
        }
    }
}
