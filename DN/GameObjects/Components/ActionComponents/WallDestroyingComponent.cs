﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ActionComponents
{
    class WallDestroyingComponent:Component, IUpdatable
    {
        private BoundaryComponent _boundaryComponent;
        private GameWorld _gameWorld;
        public WallDestroyingComponent(IGameObject gameObject, GameWorld gameWorld)
            : base(gameObject)
        {
            _gameWorld = gameWorld;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return base.GetDependicies();
        }

        public override void ProccesMessage(IMessage message)
        {
        }

        public void Update(float dt)
        {
            List<CollidedCell> cells = _gameWorld.TileMap.GetCollisionsWithTiles(_boundaryComponent.Bounds);

            foreach (var c in cells)
            {
                if (_boundaryComponent.Bounds.Contains(c.Rectangle))
                    _gameWorld.DestroyMapCell(c.Rectangle.X / 64, c.Rectangle.Y / 64);
            }
        }
    }
}
