﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Components;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects
{
    class BowComponent:Component, IUpdatable, IDrawable
    {
        public float TensionGrowingSpeed = 40;

        public float MaxTension = 18;
        public float MinTension = 2;



        private float _tension = 0;


        private float _projectiveSpeed;

        public float ProjectiveSpeed
        {
            get { return _projectiveSpeed * (_tension); }
            set { _projectiveSpeed = value; }
        }

        protected Vector2 ProjectivePosition
        {
            get
            {
                return new Vector2(_boundaryComponent.Position.X + (-_tension) * (float)Math.Cos(_rotation),
                                   _boundaryComponent.Position.Y - _tension * (float)Math.Sin(_rotation));
            }
        }

        private IGameObject _currentProjective;

        private BoundaryComponent _boundaryComponent;
        private DirectionComponent _directionComponent;
        private PickupableComponent _pickupableComponent;
        private float _rotation;

        private GameObjectsManager _manager;
        private GameObjectsFabric _fabric;

        private DirectionComponent _projectiveDirectionComponent;
        private DamageComponent _damageComponent;

        public BowComponent(IGameObject gameObject, GameObjectsManager manager, GameObjectsFabric fabric) : base(gameObject)
        {
            _fabric = fabric;

            _manager = manager;

        }

        private IGameObject GetArrow()
        {
            var arrow = _fabric.CreateArrow(_damageComponent.MinDamage, _damageComponent.MaxDamage, _damageComponent.CriticalChance);
            _projectiveDirectionComponent = arrow.GetComponent<DirectionComponent>();
            arrow.GetComponent<DirectionDrawingComponent>().SetDirectionComponent(_directionComponent);

            return arrow;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _pickupableComponent = Owner.GetComponent<PickupableComponent>();
            _damageComponent = Owner.GetComponent<DamageComponent>();
            Owner.GetComponent<ActionComponent>().AutoFinish = false;

            _currentProjective = GetArrow();
            return _boundaryComponent != null && _directionComponent != null; 
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is ActionMessage)
            {
                HandleActionMessage((ActionMessage)message);
            }
        }
        
        private void HandleActionMessage(ActionMessage message)
        {
            switch (message.Type)
            {
                case ActionMessageType.Start:
                    _tension = MinTension;
                    break;
                case ActionMessageType.Update:
                    if (MaxTension > _tension)
                        _tension += message.DT*TensionGrowingSpeed;
                    if (MaxTension < _tension)
                        _tension = MaxTension;
                    break;
                case ActionMessageType.Interupt:

                    if(_currentProjective == null)
                        return;
                    Vector2 dir = _directionComponent.Direction;

                    _currentProjective.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                    _currentProjective.SendMessage(new MoveMessage(dir, ProjectiveSpeed, false));

                    ObjectGroup[] objectGroup = null;

                    if (_pickupableComponent.Pickuper != null)
                    {
                        objectGroup = new []
                                          {
                                              Owner.ObjectGroup,
                                              _pickupableComponent.Pickuper.ObjectGroup
                                          };
                    }
                    else
                    {
                        objectGroup = new[]
                                          {
                                              Owner.ObjectGroup,
                                          };
                    }

                    _currentProjective.SendMessage(new ObjectsCollisionsStateMessage(true, objectGroup));
                    _currentProjective.GetComponent<DirectionDrawingComponent>().SetDirectionComponent(_projectiveDirectionComponent);
                    _manager.AddObject(_currentProjective);
                    _currentProjective = null;
                    _tension = MinTension;
                    break;
                case ActionMessageType.Ready:
                    _currentProjective = GetArrow();
                    break;
            }
        }

        public void Update(float dt)
        {
            if (_currentProjective != null)
            {
                _rotation = FunctionHelper.Vector2ToRadians(_directionComponent.Direction);
                _currentProjective.GetComponent<BoundaryComponent>().Position = ProjectivePosition;
                _currentProjective.Update(dt);
            }
        }

        public void Draw(float dt)
        {
            if (_currentProjective != null)
            {
                _currentProjective.Draw(dt);
            }
        }
    }
}
