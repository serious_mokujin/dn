﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.GameObjects.Components.StanceComponents;

namespace DN.GameObjects.Components.ActionComponents
{
    public class DropLootOnAction:Component
    {
        private bool _droped = false;
        private LootComponent _lootComponent;

        public DropLootOnAction(IGameObject gameObject) : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _lootComponent = Owner.GetComponent<LootComponent>();
            return true;
        } 

        public override void ProccesMessage(IMessage message)
        {
            if (message is ActionMessage)
            {
                var msg = (ActionMessage)message;
                switch (msg.Type)
                {
                    case ActionMessageType.Start:
                    case ActionMessageType.StartRequest:
                    case ActionMessageType.Finish:
                        if (!_droped)
                        {
                            _lootComponent.Drop();
                            _droped = true;                                 
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
