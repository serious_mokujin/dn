﻿using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using DN.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.ActionComponents
{
    class DropAlphaOnDeactivation:Component, IUpdatable
    {
        AlphaComponent _alphaComponent;
        bool _droping;

        Timer _timer;

        float _speed;
        public DropAlphaOnDeactivation(IGameObject gameObject, float speed, float delay)
            :base(gameObject)
        {
            _speed = speed;
            _timer = new Timer() 
            {
                Duration = delay 
            };
            _timer.TickEvent += () => _droping = true;
        }

        public override bool GetDependicies()
        {
            _alphaComponent = Owner.GetComponent<AlphaComponent>();
            return _alphaComponent != null;
        }

        
        public override void ProccesMessage(IMessage message)
        {
            if (message is ObjectStanceMessage)
            {
                if ((message as ObjectStanceMessage).Type == StanceType.Deativated)
                {
                    if(!_timer.Running)
                        _timer.Run();
                }
            }
        }

        public void Update(float dt)
        {
            _timer.Update(dt);
            if (_droping)
                _alphaComponent.Alpha -= dt * _speed;
        }
    }
}
