﻿using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.ActionComponents
{
    public class DestroyOnZeroAlpha:Component, IUpdatable
    {
        AlphaComponent _alphaComponent;
        GameObjectsManager _manager;

        public DestroyOnZeroAlpha(IGameObject gameObject, GameObjectsManager manager)
            :base(gameObject)
        {
            _manager = manager;
        }

        public override bool GetDependicies()
        {
            _alphaComponent = Owner.GetComponent<AlphaComponent>();
            return _alphaComponent != null;
        }


        public override void ProccesMessage(IMessage message)
        {
        }

        public void Update(float dt)
        {
            if (_alphaComponent.Alpha <= 0)
                _manager.RemoveObject(Owner);
        }
    }
}
