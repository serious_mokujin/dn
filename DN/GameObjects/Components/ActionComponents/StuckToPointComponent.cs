﻿using DN.GameObjects.Messages;
using OpenTK;
using System;

namespace DN.GameObjects.Components.ActionComponents
{
    public abstract class StuckToPointComponent:Component
    {
        protected bool StuckIn { get; private set; }

        public StuckToPointComponent(IGameObject gameObject)
            :base(gameObject)
        {
 
        }

        protected virtual void Stuck()
        {
            Owner.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));
            Owner.SendMessage(new DirectionStateMessage(DirectionState.Disabled));
            Owner.SendMessage(new ObjectStanceMessage(StanceType.Deativated));

            Owner.RemoveComponent<CollisionComponent>();

            StuckIn = true;
        }
    }
}
