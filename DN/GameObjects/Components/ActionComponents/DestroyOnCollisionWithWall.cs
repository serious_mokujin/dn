﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components
{
    public class DestroyOnCollisionWithWall:Component
    {
        private PhysicComponent _physic;
        private GameObjectsManager _gameObjectsManager;
        public DestroyOnCollisionWithWall(IGameObject gameObject, GameObjectsManager gameObjectsManager) : base(gameObject)
        {
            _gameObjectsManager = gameObjectsManager;
        }

        public override bool GetDependicies()
        {
            _physic = Owner.GetComponent<PhysicComponent>();
            _physic.CollisionWithTiles += PhysicOnCollisionWithTiles;
            return base.GetDependicies();
        }

        private void PhysicOnCollisionWithTiles(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity)
        {
            if (collidedTiles.Any(p => p.CellType == CellType.Wall))
            {
                _gameObjectsManager.RemoveObject(Owner);
                _physic.CollisionWithTiles -= PhysicOnCollisionWithTiles;
            }


        }


        public override void ProccesMessage(IMessage message)
        {
            return;
        }
    }
}
