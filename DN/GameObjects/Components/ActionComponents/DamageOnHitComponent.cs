﻿using Blueberry;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    class DamageOnHitComponent:Component
    {
        public ObjectGroup[] _ignoreGroup;

        private BoundaryComponent _boundary;
        private DamageComponent _damageComponent;

        public DamageOnHitComponent(IGameObject gameObject, ObjectGroup[] ignoreGroup = null)
            :base(gameObject)
        {
            _ignoreGroup = ignoreGroup;
        }

        public override bool GetDependicies()
        {
            _boundary = Owner.GetComponent<BoundaryComponent>();
            _damageComponent = Owner.GetComponent<DamageComponent>();
            return _boundary != null;
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is CollisionMessage)
            {
                CollisionMessage cm = (CollisionMessage)message;
                if(_ignoreGroup != null)
                    if(_ignoreGroup.Any(p => p == cm.CollidedObject.ObjectGroup))
                        return;

                DirectionComponent dirComp = Owner.GetComponent<DirectionComponent>();
                Vector2 dir = Vector2.Zero;

                float damage = (float)Math.Round(RandomTool.NextSingle(_damageComponent.MinDamage, _damageComponent.MaxDamage));
                bool crit = RandomTool.NextBool(_damageComponent.CriticalChance);
                if (crit)
                    damage = _damageComponent.MaxDamage * _damageComponent.CriticalDamage;

                if (dirComp != null)
                    dir = dirComp.Direction;
                else
                    dir = FunctionHelper.DirectionToObject(cm.CollidedObject.GetComponent<BoundaryComponent>().Position,
                                                                _boundary.Position);
                //todo: add knockback var
                cm.CollidedObject.SendMessage(new DamageMessage(Owner, damage, DamageWay.Taken, -dir, crit));
                Owner.SendMessage(new DamageMessage(cm.CollidedObject, damage, DamageWay.Given, -dir, crit));

            }
        }
    }
}
