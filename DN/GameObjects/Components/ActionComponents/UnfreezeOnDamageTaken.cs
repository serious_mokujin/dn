﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ActionComponents
{
    public class UnfreezeOnDamageTaken:Component
    {
        public UnfreezeOnDamageTaken(IGameObject gameObject) : base(gameObject)
        {
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is DamageMessage)
            {
                DamageMessage damageMessage = (DamageMessage) message;
                if(damageMessage.DamageWay == DamageWay.Taken)
                {
                    Owner.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                }
            }
        }
    }
}
