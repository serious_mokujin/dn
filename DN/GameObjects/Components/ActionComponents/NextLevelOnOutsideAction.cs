﻿using DN.GameObjects.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    public class NextLevelOnOutsideAction:Component
    {
        GameWorld _gameWorld;

        public NextLevelOnOutsideAction(IGameObject gameObject, GameWorld gameWorld)
            : base(gameObject)
        {
            _gameWorld = gameWorld;
        }

        
        public override void ProccesMessage(Messages.IMessage message)
        {
            if (message is ActionMessage)
            {
                HandleOutsideMessage((ActionMessage)message);
            }
        }

        private void HandleOutsideMessage(ActionMessage message)
        {
            if (message.Type == ActionMessageType.Start)
            {
                _gameWorld.FinishLevel();
            }
        }


    }
}
