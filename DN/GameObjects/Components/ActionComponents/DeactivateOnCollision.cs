﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ActionComponents
{
    public class DeactivateOnCollision:Component
    {
        public ObjectGroup[] IgnoreGroup;

        public DeactivateOnCollision(IGameObject gameObject) : base(gameObject)
        {
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is CollisionMessage)
            {
                var msg = (CollisionMessage) message;
                if(IgnoreGroup != null)
                    if (IgnoreGroup.Any(p=> msg.CollidedObject.ObjectGroup == p))
                        return;
                Owner.SendMessage(new ObjectStanceMessage(StanceType.Deativated));
            }
        }
    }
}
