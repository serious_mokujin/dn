﻿
using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ActionComponents
{
	/// <summary>
	/// Description of DestroyTimerComponent.
	/// </summary>
	public class DestroyTimerComponent : Component, IUpdatable
	{
        GameObjectsManager _manager;
		float timer;
		
        public DestroyTimerComponent(IGameObject gameObject, GameObjectsManager manager, float lifePeriod)
            :base(gameObject)
        {
        	timer = lifePeriod;
            _manager = manager;
        }

        public override void ProccesMessage(IMessage message)
        {
        }

	    public void Update(float dt)
	    {
            timer -= dt;
            if (timer <= 0)
            {
                _manager.RemoveObject(Owner);
            }
	    }
	}
}
