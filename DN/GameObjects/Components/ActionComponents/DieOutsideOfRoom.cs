﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ActionComponents
{
    class DieOutsideOfRoom:Component, IUpdatable
    {
        private readonly TileMap _tileMap;
        private BoundaryComponent _boundaryComponent;

        public DieOutsideOfRoom(IGameObject gameObject, TileMap tileMap) : base(gameObject)
        {
            _tileMap = tileMap;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return base.GetDependicies();
        }

        public override void ProccesMessage(IMessage message)
        {
        }

        public void Update(float dt)
        {
            if (_tileMap.Bounds.IntersectsWith(_boundaryComponent.Bounds))
                return;
            Owner.SendMessage(new DieMessage());
        }
    }
}
