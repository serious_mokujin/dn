﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components.ActionComponents
{
    public class DamageOnSpeed:Component, IUpdatable
    {
        private readonly float _maxSpeed;
        public ObjectGroup[] _ignoreGroup;


        private BoundaryComponent _boundary;
        private Vector2 oldPosition;
        private Vector2 position;
        private PhysicComponent _physics;
        private Vector2 _velocity;
        private DamageComponent _damageComponent;

        public DamageOnSpeed(IGameObject gameObject, float maxSpeed,
                             ObjectGroup[] ignoreGroup = null)
            :base(gameObject)
        {
            _maxSpeed = maxSpeed;
            _ignoreGroup = ignoreGroup;
        }

        public override bool GetDependicies()
        {
            _boundary = Owner.GetComponent<BoundaryComponent>();
            _physics = Owner.GetComponent<PhysicComponent>();
            _damageComponent = Owner.GetComponent<DamageComponent>();
            return _boundary != null; 
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is CollisionMessage)
            {
                CollisionMessage cm = (CollisionMessage)message;
                if(_ignoreGroup != null)
                    if(_ignoreGroup.Any(p => p == cm.CollidedObject.ObjectGroup))
                        return;

                DirectionComponent dirComp = Owner.GetComponent<DirectionComponent>();
                Vector2 dir = Vector2.Zero;

                
                float ratio = (_physics != null ? _physics.GetVelocity().Length: _velocity.Length)/ _maxSpeed;
                if (ratio > 1)
                {
                    ratio = 1;
                }
                if(ratio == 0)
                    return;
                float speedDamage = (_damageComponent.MaxDamage - _damageComponent.MinDamage) * ratio;
                float damage = (float)Math.Round(_damageComponent.MinDamage + speedDamage);

                bool crit = ratio == 1 || RandomTool.NextBool(_damageComponent.CriticalChance);
                if (crit)
                    damage = damage * _damageComponent.CriticalDamage;

                if (dirComp != null)
                    dir = dirComp.Direction;
                else
                    dir = FunctionHelper.DirectionToObject(cm.CollidedObject.GetComponent<BoundaryComponent>().Position,
                                                                _boundary.Position);
                cm.CollidedObject.SendMessage(new DamageMessage(Owner, damage, DamageWay.Taken, -dir, crit));
                Owner.SendMessage(new DamageMessage(cm.CollidedObject, damage, DamageWay.Given, -dir, crit));

            }
        }

        public void Update(float dt)
        {
            oldPosition = position;
            position = _boundary.Position;
            _velocity = oldPosition - position;
        }
    }
}
