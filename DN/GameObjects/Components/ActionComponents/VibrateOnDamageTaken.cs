﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects;
using DN.GameObjects.Messages;
using Blueberry.Input;

namespace DN.GameObjects.Components
{
    public class VibrateOnDamageTaken:Component
    {
        GamepadDevice _gamepad;
        public VibrateOnDamageTaken(IGameObject gameObject, GamepadDevice gamepad) : base(gameObject)
        {
            _gamepad = gamepad;
        }

        public override void ProccesMessage(IMessage message)
        {
            if (_gamepad == null)
                return;
            if(message is DamageMessage)
            {
                var m = (DamageMessage) message;
                if(m.DamageWay == DamageWay.Taken)
                    _gamepad.Vibrate(0.25f*m.Amount, 0.15f*m.Amount, 0.2f);
            }
        }
    }
}
