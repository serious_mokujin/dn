﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using OpenTK;
using DN.GameObjects.Components.ActionComponents;

namespace DN.GameObjects.Components
{
    public class StuckToPointOnCollision:StuckToPointComponent, IUpdatable
    {
        private PhysicComponent _physic;
        private BoundaryComponent _boundary;
        private BoundaryComponent _collidedBoundary;
        private GameObjectsManager _gameObjectsManager;


        private IGameObject _collidedObject;
        private Vector2 _posOffset;
        private bool _stuckInObject;

        public StuckToPointOnCollision(IGameObject gameObject, GameObjectsManager gameObjectsManager)
            : base(gameObject)
        {
            _gameObjectsManager = gameObjectsManager;
        }

        public override bool GetDependicies()
        {
            _physic = Owner.GetComponent<PhysicComponent>();
            _boundary = Owner.GetComponent<BoundaryComponent>();
            _physic.CollisionWithTiles += PhysicOnCollisionWithTiles;
            return base.GetDependicies();
        }

        private void PhysicOnCollisionWithTiles(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity)
        {
            if (collidedTiles.Any(p => p.CellType == CellType.Wall))
            {
                _physic.CollisionWithTiles -= PhysicOnCollisionWithTiles;
                Stuck();
            }
        }


        public override void ProccesMessage(IMessage message)
        {
            if(message is CollisionMessage)
            {
                HandleCollisionMessage((message as CollisionMessage));
            }
        }

        private void HandleCollisionMessage(CollisionMessage message)
        {
            StuckInObject(message.CollidedObject);
        }

        public void StuckInObject(IGameObject obj)
        {

            _stuckInObject = true;
            _collidedBoundary = obj.GetComponent<BoundaryComponent>();
            _posOffset = -_boundary.Position + _collidedBoundary.Position;
            _collidedObject = obj;

            Stuck();

            _collidedObject.OnRemove += CollidedObjectOnRemoveFromScene;
        }

        private void CollidedObjectOnRemoveFromScene(object sender, EventArgs e)
        {
            _stuckInObject = false;
            _gameObjectsManager.RemoveObject(Owner);//UNDONE: it shouldn't be removed
        }

        public void Update(float dt)
        {
            if (_stuckInObject)
                _boundary.Position = _collidedBoundary.Position - _posOffset;
        }
    }
}
