﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.ActionComponents
{
    public class AddLetterOnCollision:Component
    {
        private readonly char _ch;
        private bool added = false;
        public AddLetterOnCollision(IGameObject gameObject, char ch) : base(gameObject)
        {
            _ch = ch;
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is CollisionMessage)
            {
                if(added)
                    return;
                var msg = (CollisionMessage) message;
                var lettersInventory = msg.CollidedObject.GetComponent<LettersInventory>();
                if(lettersInventory == null)
                    return;
                lettersInventory.Add(_ch);
                added = true;
            }
        }
    }
}
