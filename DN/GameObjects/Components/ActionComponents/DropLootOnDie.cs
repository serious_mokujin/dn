﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Components;
using DN.GameObjects;
using DN.GameObjects.Messages;
using DN.GameObjects.Components.StanceComponents;

namespace DN.GameObjects.Components
{
    


    internal class DropLootOnDie : Component
    {
        private LootComponent _lootComponent;

        public DropLootOnDie(IGameObject gameObject)
            : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _lootComponent = Owner.GetComponent<LootComponent>();
            return true;
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is DieMessage)
            {
                if(_lootComponent != null)
                    _lootComponent.Drop();
            }
        }

    }

}
