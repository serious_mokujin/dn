﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Input;
using DN.GameObjects.Components;
using DN.GameObjects.Messages;
using OpenTK;
using OpenTK.Input;

namespace DN.GameObjects
{
    class PlayerControllComponent:Component, IDisposable, IUpdatable
    {
        public readonly MouseDevice Mouse;
        public readonly KeyboardDevice Keyboard;
        public readonly GamepadDevice Gamepad;
        public bool Active = true;

        private PhysicComponent _physicComponent;
        BoundaryComponent _boundaryComponent;

 //       private JumpComponent _jumpComponent;
        private LadderClimbingComponent _ladderClimbingComponent;
        private PickupingComponent _pickupingComponent;
        private DirectionComponent _directionComponent;

        private Camera _camera;
#if DEBUG
        private const bool SuperHeroMode = false;
#endif
        public PlayerControllComponent(IGameObject gameObject,MouseDevice mouse, KeyboardDevice keyboard , GamepadDevice gamepad, Camera camera) : base(gameObject)
        {
            Mouse = mouse;
            Keyboard = keyboard;
            Gamepad = gamepad;
            _camera = camera;
            
            if (Gamepad != null)
            {
                Gamepad.OnButtonPress += GGamepad1OnButtonPress;
                Gamepad.OnButtonUp += GGamepad1OnButtonUp;
                Gamepad.OnRightTrigger += GGamepad1OnRightTrigger;
            }
            if (Mouse != null)
            {
                Mouse.ButtonUp += GMouseOnButtonUp;
                Mouse.Move += MouseOnMove;
                Mouse.WheelChanged += MouseOnWheelChanged;
            }
            if (Keyboard != null)
            {
                Keyboard.KeyDown += g_Keyboard_KeyDown;
                Keyboard.KeyUp += g_Keyboard_KeyUp;
                Keyboard.KeyRepeat = true;
            }

        }

        private void MouseOnWheelChanged(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                _pickupingComponent.ChooseNext();
            }
            else
            {
                _pickupingComponent.ChoosePrevious();
            }
        }

        private void MouseOnMove(object sender, MouseMoveEventArgs mouseMoveEventArgs)
        {
            if (_camera != null)
            {
                Vector2 mouse = _camera.ToWorld(new Vector2(Game.g_Mouse.X, Game.g_Mouse.Y));
                _directionComponent.Direction = Vector2.Normalize(mouse - _boundaryComponent.Position);
            }
            else
            {
                _directionComponent.Direction =
                    Vector2.Normalize(new Vector2(Game.g_Mouse.X - Game.g_screenSize.Width / 2,
                                                  Game.g_Mouse.Y - Game.g_screenSize.Height / 2));
            }
        }


        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            _ladderClimbingComponent = Owner.GetComponent<LadderClimbingComponent>();
            _pickupingComponent = Owner.GetComponent<PickupingComponent>();
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();

            _physicComponent.CollisionWithTiles += PhysicComponentOnCollisionWithTiles;
#if DEBUG
            if (SuperHeroMode)
            {
                _physicComponent.GravityAffected = false;
                _physicComponent.MaxVelocity = new Vector2(40, 40);
            }
#endif



            return _physicComponent != null
                && _ladderClimbingComponent != null
                && _pickupingComponent != null
                && _directionComponent != null
                && _boundaryComponent != null;
        }

        //todo: remove it from here
        private void PhysicComponentOnCollisionWithTiles(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity)
        {
            if (_physicComponent.FallingDistance > 500)
                if(collidedTiles.Any(p => p.CellType == CellType.Wall && p.Direction.Y == 1))
                {
                   // _gamepad.Vibrate(1f, 1f, 0.3f);
                    Owner.GetComponent<HealthComponent>().DealDamage(null,
                                                                     (float)
                                                                     Math.Round(1*_physicComponent.FallingDistance/200),
                                                                     new Vector2(0, 1));
                    CM.I.Sound("heroFall").PlayDynamic();//will be removed(soon)
                }
        }




        public override void ProccesMessage(IMessage message)
        {
        }

        public void Update(float dt)
        {
            if (!Active)
                return;
            Vector2 moveDirectionVector = Vector2.Zero;

            if (LeftKeyPressed())
                moveDirectionVector.X = -1;
            if (RightKeyPressed())
                moveDirectionVector.X = 1;
            if (UpKeyPressed())
                moveDirectionVector.Y = -1;
            if (DownKeyPressed())
                moveDirectionVector.Y = 1;


            if (Gamepad != null && Gamepad.Connected)
            {
                Vector2 leftStickPos = Gamepad.LeftStick.Position;
                if (leftStickPos.X != 0)
                {
                    moveDirectionVector = new Vector2(leftStickPos.X, leftStickPos.Y);
                }
                if (moveDirectionVector.LengthFast > 1)
                    moveDirectionVector.NormalizeFast();

                if (Gamepad.RightStick.Position != Vector2.Zero)
                    _directionComponent.Direction = Gamepad.RightStick.Position;
                else if (Gamepad.LeftStick.Position != Vector2.Zero)
                    _directionComponent.Direction = Gamepad.LeftStick.Position;
            }


            if ((UpKeyPressed() && _physicComponent.GetVelocity().Y >= 0))
            {
                _ladderClimbingComponent.TryClimbLadder();
            }

            if (moveDirectionVector != Vector2.Zero)
            {
                if (!_ladderClimbingComponent.ClimbLadder)
                    _physicComponent.Move(new Vector2(1, 0), dt*moveDirectionVector.X*18);
                else
                    _ladderClimbingComponent.MoveLadder(moveDirectionVector, 3);

            }

#if DEBUG
            if (SuperHeroMode)
            {
                _ladderClimbingComponent.StopClimbLadder();
                _physicComponent.Move(moveDirectionVector, 75*dt, true);
            }
#endif

            if (ItemUsingKeyPressed())
            {
                _pickupingComponent.Use();
            }
            if (JumpKeyPressed())
            {
                if (!DownKeyPressed())
                    Owner.SendMessage(new EventMessage(Events.StartJump));
                if (!_physicComponent.OnCeiling || !UpKeyPressed())
                    _ladderClimbingComponent.StopClimbLadder();
            }
        }



        private void g_Keyboard_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            if (!Active)
                return;
            if (e.Key == Key.Space)
            {
                Owner.SendMessage(new EventMessage(Events.StopJump));
            }
        }
        void g_Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (!Active)
                return;
            if (e.Key == Key.E)
            {
                _pickupingComponent.StartPickuping();
                Owner.SendMessage(new OutsideActionMessage(OutsideActionType.TryUse, Owner));
            }
        }


        private void GGamepad1OnButtonPress(object sender, GamepadButtonFlags e)
        {
            if (!Active)
                return;
            if(e.HasFlag(GamepadButtonFlags.X))
            {
                _pickupingComponent.StartPickuping();
                Owner.SendMessage(new OutsideActionMessage(OutsideActionType.TryUse, Owner));
            }
          //  if(e.HasFlag(GamepadButtonFlags.))

        }
        private void GGamepad1OnButtonUp(object sender, GamepadButtonFlags e)
        {
            if (!Active)
                return;
            if (e.HasFlag(GamepadButtonFlags.A))
            {
                Owner.SendMessage(new EventMessage(Events.StopJump));
            }
        }


        private void GGamepad1OnRightTrigger(object sender, GamepadDevice.TriggerState triggerState)
        {
            if (!Active)
                return;
            if (triggerState.value == 0)
            {
                Owner.SendMessage(new ActionMessage(Owner, ActionMessageType.InterupRequest));
            }
        }
        private void GMouseOnButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!Active)
                return;
            if (e.Button == MouseButton.Left)
                Owner.SendMessage(new ActionMessage(Owner, ActionMessageType.InterupRequest));
        }

        private bool DownKeyPressed()
        {
            return (Gamepad != null && (Gamepad.DPad.Down || Gamepad.LeftStick.Position.Y > 0.3f))
                   || (Keyboard != null && Keyboard[Key.S]);
        }

        private bool UpKeyPressed()
        {
            return (Gamepad != null && (Gamepad.DPad.Up || Gamepad.LeftStick.Position.Y < -0.3f))
                   || (Keyboard != null && Keyboard[Key.W]);
        }

        private bool RightKeyPressed()
        {
            return (Gamepad != null && (Gamepad.DPad.Right || Gamepad.LeftStick.Position.X > 0.3f))
                   || (Keyboard != null && Keyboard[Key.D]);
        }

        private bool LeftKeyPressed()
        {
            return (Gamepad != null && (Gamepad.DPad.Left || Gamepad.LeftStick.Position.X < -0.3f))
                   || (Keyboard != null && Keyboard[Key.A]);
        }
        private bool ItemUsingKeyPressed()
        {
            return (Gamepad != null && Gamepad.RightTrigger > 0.3f) || (Mouse != null && Mouse[MouseButton.Left]);
        }

        private bool JumpKeyPressed()
        {
            return (Gamepad != null && Gamepad.A) || (Keyboard != null && Keyboard[Key.Space]);
        }


        public void Dispose()
        {
            if (Gamepad != null)
            {
                Gamepad.OnButtonPress -= GGamepad1OnButtonPress;
                Gamepad.OnButtonUp -= GGamepad1OnButtonUp;
            }
            if (Keyboard != null)
            {
                Keyboard.KeyDown -= g_Keyboard_KeyDown;
                Keyboard.KeyUp -= g_Keyboard_KeyUp;
            }
            if (Mouse != null)
            {
                Mouse.ButtonUp -= GMouseOnButtonUp;
                Mouse.Move -= MouseOnMove;
            }
        }
    }
}
