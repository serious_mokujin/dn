﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Input;
using DN.GUI;
using DN.GUI.Windows;
using DN.GameObjects.Components.OutsideInteractingComponents;
using DN.GameObjects.Components.StanceComponents;
using DN.GameObjects.Messages;
using OpenTK.Input;

namespace DN.GameObjects.Components.Controllers
{

    /// <summary>
    /// Works with crafting GUI
    /// </summary>
    class PlayerCraftingControll:Component, IDisposable
    {
        private PlayerControllComponent _playerControll;
        private CraftingGUI _craftingGUI;

        private KeyboardDevice _keyboard;
        private MouseDevice _mouse;
        private GamepadDevice _gamepad;

        public PlayerCraftingControll(IGameObject gameObject, CraftingGUI craftingGUI,
                                      KeyboardDevice keyboard,MouseDevice mouse ,GamepadDevice gamepad)
            : base(gameObject)
        {
            _craftingGUI = craftingGUI;

            SetControlls(keyboard, mouse, gamepad);
        }

        public void SetControlls(KeyboardDevice keyboard,MouseDevice mouse ,GamepadDevice gamepad)
        {
            if (_keyboard != null)
                _keyboard.KeyDown -= GKeyboardOnKeyDown;
            if (_gamepad != null)
                _gamepad.OnButtonPress -= GGamepad1OnOnButtonDown;

            _keyboard = keyboard;
            _mouse = mouse;
            _gamepad = gamepad;
            if (_keyboard != null)
                _keyboard.KeyDown += GKeyboardOnKeyDown;
            if (gamepad != null)
                _gamepad.OnButtonPress += GGamepad1OnOnButtonDown;
        }

        private void GGamepad1OnOnButtonDown(object sender, GamepadButtonFlags e)
        {
            if(e.HasFlag(GamepadButtonFlags.B))
            {
                Owner.SendMessage(new CraftingMessage(CraftingMessageType.Finished, Owner));
            }
        }

        private void GKeyboardOnKeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if(e.Key == Key.Escape)
            {
                Owner.SendMessage(new CraftingMessage(CraftingMessageType.Finished, Owner));
            }
        }

        public override bool GetDependicies()
        {
            _playerControll = Owner.GetComponent<PlayerControllComponent>();
            return _playerControll != null;
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is CraftingMessage)
            {
                var msg = (CraftingMessage) message;
                if(msg.Type == CraftingMessageType.Started && (_craftingGUI.State != ElementState.Opened || _craftingGUI.State != ElementState.Opening))
                {
                    _playerControll.Active = false;
                    if (_craftingGUI.State != ElementState.Opened)
                    {
                        _craftingGUI.Open();
                        _craftingGUI.CraftingComponent = msg.Sender.GetComponent<CraftingComponent>();
                        _craftingGUI.SetControlls(_keyboard, _mouse, _gamepad);
                    }
                }
                else if(!_playerControll.Active)
                {
                    _playerControll.Active = true;
                    _craftingGUI.Close();
                    _craftingGUI.CraftingComponent = null;
                }

            }
        }

        public void Dispose()
        {
            if(_keyboard != null)
                _keyboard.KeyDown -= GKeyboardOnKeyDown;
            if(_gamepad != null)
                _gamepad.OnButtonDown -= GGamepad1OnOnButtonDown;
        }
    }
}
