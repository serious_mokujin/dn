﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.Controllers
{
    public class CollisionTriggerComponent : Component
    {
        private BoundaryComponent _ownerBondary;

        public event EventHandler OnCollision;


        private readonly ObjectGroup _objectGroup;
        private readonly GameObjectsManager _manager;
        private readonly bool _destroyOnAction;

        public CollisionTriggerComponent(IGameObject gameObject, ObjectGroup objectGroup,
                                         GameObjectsManager manager, EventHandler eventOnCollision,
                                         bool destroyOnAction = false)
            : base(gameObject)
        {
            _objectGroup = objectGroup;
            _manager = manager;
            _destroyOnAction = destroyOnAction;
            OnCollision += eventOnCollision;
        }

        public override bool GetDependicies()
        {
            _ownerBondary = Owner.GetComponent<BoundaryComponent>();
            return base.GetDependicies();
        }


        public override void ProccesMessage(IMessage message)
        {
            if(message is CollisionMessage)
            {
                var mess = (message as CollisionMessage);
                if (mess.CollidedObject.ObjectGroup == _objectGroup)
                {
                    BoundaryComponent collidedBoundary = mess.CollidedObject.GetComponent<BoundaryComponent>();
                    if (_ownerBondary.Bounds.Contains(collidedBoundary.Bounds))
                    {
                        if (OnCollision != null)
                            OnCollision(Owner, EventArgs.Empty);
                        if (_destroyOnAction)
                        {
                            OnCollision = null;
                            _manager.RemoveObject(Owner);
                        }
                    }
                }
            }
        }

    }
}