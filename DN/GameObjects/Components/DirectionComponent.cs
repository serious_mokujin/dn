﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components
{
    public class DirectionComponent:Component
    {
        private Vector2 _direction;
        private DirectionState _directionState = DirectionState.Enabled;


        public Vector2 Direction
        {
            get { return _direction; }
            set
            {
                if(_directionState == DirectionState.Disabled)
                    return;
                _direction = value;
                _direction.Normalize();
            }
        }

        public float Angle
        {
            get { return FunctionHelper.Vector2ToRadians(Direction); }
        }


        public DirectionComponent(IGameObject gameObject) : base(gameObject)
        {
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        //maybe there will be added smth like smooth rotation
        public override void ProccesMessage(IMessage message)
        {
            if(message is DirectionStateMessage)
            {
                _directionState = (message as DirectionStateMessage).DirectionState;
            }
        }
    }
}
