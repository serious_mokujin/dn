﻿using Blueberry;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components
{
    class DamageOnHitComponent:Component
    {
        public ObjectGroup[] _ignoreGroup;
        public float MaxDamage { get; private set; }
        public float MinDamage { get; private set; }
        public float CriticalHitChance { get; set; }

        private BoundaryComponent _boundary;

        public DamageOnHitComponent(IGameObject gameObject, float minDamage, float maxDamage, ObjectGroup[] ignoreGroup = null)
            :base(gameObject)
        {
            MaxDamage = maxDamage;
            MinDamage = minDamage;
            _ignoreGroup = ignoreGroup;
        }

        public override bool GetDependicies()
        {
            _boundary = Owner.GetComponent<BoundaryComponent>();
            return _boundary != null;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is CollisionMessage)
            {
                CollisionMessage cm = (CollisionMessage)message;
                if(_ignoreGroup != null)
                    if(_ignoreGroup.Any(p => p == cm.CollidedObject.ObjectGroup))
                        return;

                HealthComponent hc = cm.CollidedObject.GetComponent<HealthComponent>();
                DirectionComponent dirComp = Owner.GetComponent<DirectionComponent>();
                Vector2 dir = Vector2.Zero;

                float damage = (float)Math.Round(RandomTool.RandFloat(MinDamage, MaxDamage));
                bool crit = RandomTool.RandBool(CriticalHitChance);
                if (crit)
                    damage = MaxDamage * 2;

                if (dirComp != null)
                    dir = dirComp.Direction;
                else
                    dir = FunctionHelper.DirectionToObject(cm.CollidedObject.GetComponent<BoundaryComponent>().Position,
                                                                _boundary.Position);
                if (hc != null)
                    if (hc.DealDamage(damage, -dir, damage * 30, crit))//todo: add knockback var
                        Owner.SendMessage(new DamageMessage(damage, DamageWay.Given, dir, crit));
            }
        }
    }
}
