﻿
using System;
using Blueberry;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of JumpAnimationComponent.
	/// </summary>
	public class JumpAnimationComponent : AnimationComponent
    {
        public override Animations Type { get { return Animations.Jump; } }
        protected AnimationLayout layout;
        private float jumpTimer;
        protected BoundaryComponent bounds;
		protected PhysicComponent physics;
		
        protected Vector2 walkLegAnchor = new Vector2(10, 0);
        protected Vector2 jumpAnimStart;
        
		Vector2 oldVelocity; // need to detect collision with celling (velocity difference must be high)
        
        public JumpAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            bounds = Owner.GetComponent<BoundaryComponent>();
            physics = Owner.GetComponent<PhysicComponent>();
            
            return layout != null && bounds != null && physics != null && r;
        }

        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
            leftLegRotation = layout.parts[2].rotation;
            rightLegRotation = layout.parts[3].rotation;

            jumpAnimStart = bounds.Position;
        }
        protected override void StartLoop()
        {
            jumpTimer = 0;
            mainTimer.Run(0.2f);
            oldVelocity = physics.GetVelocity();
        }

        protected float leftLegRotation, rightLegRotation;
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);
            layout.parts[2].LerpPos(-walkLegAnchor - (bounds.Position - jumpAnimStart), preparationInterval);
            layout.parts[3].LerpPos(walkLegAnchor - (bounds.Position - jumpAnimStart), preparationInterval);
            layout.parts[0].LerpPos(-(bounds.Position - jumpAnimStart)/2, preparationInterval);
            layout.parts[1].LerpPos(-(bounds.Position - jumpAnimStart)/2, preparationInterval);
            
            
        }
        protected override void Loop(float dt)
        {
        	if(physics.GetVelocity().Y == 0 && oldVelocity.Y < 0)
        	{
        		Owner.SendMessage(new EventMessage(Events.CellingReached));
        		return;
        	}
            base.Loop(dt);
            jumpTimer += dt * (1 - (jumpTimer / 2));
            if (jumpTimer >= 2)
                jumpTimer = 2;
            float interval = jumpTimer / 2f;
            
            layout.LerpPos(Vector2.Zero, interval);
            
            layout.parts[2].LerpRot(0, interval);
            layout.parts[3].LerpRot(0, interval);
            
            oldVelocity = physics.GetVelocity();
        }
	}
}
