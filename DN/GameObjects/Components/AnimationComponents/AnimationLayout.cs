﻿using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.AnimationComponents
{
    public class AnimationLayout
    {
    	public readonly AnimationPart[] parts;
        public AnimationLayout(int partCount)
        {
        	parts = new AnimationPart[partCount];
        }

        public void Draw(Vector2 position)
        {
            for (int i = 0; i < parts.Length; i++)
            {
                parts[i].Draw(position);
            }
        }
        
        public void LerpPos(Vector2 pos, float interpolation)
        {
        	for (int i = 0; i < parts.Length; i++) 
        	{
        		parts[i].LerpPos(pos, interpolation);
        	}
        }
        public void LerpRot(float rot, float interpolation)
        {
        	for (int i = 0; i < parts.Length; i++) 
        	{
        		parts[i].LerpRot(rot, interpolation);
        	}
        }
    }
    public class AnimationPart
    {
        public Vector2 position;
        public float rotation;
        public SizeF size;
        public Vector2 distance;
        public Color4 color;
		public bool flipH;
		public bool flipV;
		public float scale;
        public float originX;
        public float originY;

        public AnimationPart()
        {
            position = Vector2.Zero;
            rotation = 0;
            color = Color4.White;
            flipH = false;
            flipV = false;
            scale = 1;
            originX = 0.5f;
            originY = 0.5f;
        }
        public virtual void Draw(Vector2 pos)
        { }
        
        public void LerpPos(Vector2 nextPos, float interpolation)
        {
        	position = MathUtils.Lerp(position, nextPos, interpolation);
        }
        public void LerpRot(float nextRot, float interpolation)
        {
        	rotation = MathUtils.Lerp(rotation, nextRot, interpolation);
        }
    }
    public class AnimationLetterPart : AnimationPart
    {
        private BitmapFont _font;
        private readonly Color _color;

        public AnimationLetterPart(char letter, BitmapFont font, Color color)
            : base()
        {
            this.letter = letter;
            _font = font;
            this.color = color;
            size = _font.MeasureSymbol(letter);
        }

        public char letter;
        public override void Draw(Vector2 pos)
        {
            SpriteBatch.Instance.PrintSymbol(_font, letter, pos + distance / Game.g_CameraScale + position,
                                             color, rotation, scale / Game.g_CameraScale, originX, originY, flipH, flipV);
        }
    }
    public class AnimationTexturePart : AnimationPart
    {
        private Texture texture;
        public AnimationTexturePart(Texture tex)
        {
            this.texture = tex;
            this.size = texture.Size;
        }
        public override void Draw(Vector2 pos)
        {
            SpriteBatch.Instance.DrawTexture(texture, pos + distance + position, Rectangle.Empty, color, rotation,
                                             new Vector2(originX, originY), Vector2.One*scale/Game.g_CameraScale, flipH,flipV);
        }
    }
}
