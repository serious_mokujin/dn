﻿using DN.GameObjects.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.AnimationComponents
{
    public abstract class AnimationComponent:Component, IUpdatable
    {
        public AnimationControllerComponent Controller { get; private set; }
        public bool Preparing { get; protected set; }
        public virtual Animations Type { get { return Animations.Nothing; } }

        protected AnimationTimer mainTimer = new AnimationTimer();

        float preparationTimer;
        float preparationPeriod;
        private float uninterceptionTimeout;
        private bool _interceptable;
        public bool Interceptable
        {
            get { return _interceptable; }
            private set 
            {
                _interceptable = value;
                Controller.Hold = !value;
            }
        }

        protected float preparationInterval { get; private set; }


        public AnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
            uninterceptionTimeout = 0;
            
        }

        protected void SetPreparationTimer(float period)
        {
            preparationTimer = 0;
            preparationPeriod = period;
            preparationInterval = 0;
        }

        protected virtual void StartPreparation()
        {
        }
        protected virtual void StartLoop()
        {

        }
        protected virtual void Prepare(float dt)
        {
            preparationTimer += dt;
            if (preparationTimer >= preparationPeriod)
            {
                Preparing = false;
                StartLoop();
                return;
            }
            preparationInterval = preparationTimer / preparationPeriod;
        }

        protected virtual void Loop(float dt)
        {
            mainTimer.Update(dt);
        }


        public override bool GetDependicies()
        {
            Controller = Owner.GetComponent<AnimationControllerComponent>();
            return base.GetDependicies() && Controller != null;
        }
        protected void SetUninterceptible(float timeout)
        {
            uninterceptionTimeout = timeout;
            if (uninterceptionTimeout > 0)
            {
                Interceptable = false;
            }
            else
            {
                Interceptable = true;
            }
        }

        public override void ProccesMessage(Messages.IMessage message)
        {
            if (message is PlayAnimationMessage)
            {
                if ((message as PlayAnimationMessage).Animation == Type && !Controller.Hold)
                {
                    StartPreparation();
                    Preparing = true;
                }

            }

        }

        public virtual void Update(float dt)
        {
            if (Controller.Current == Type)
            {
                if (uninterceptionTimeout > 0)
                {
                    uninterceptionTimeout -= dt;
                    if (uninterceptionTimeout < 0)
                    {
                        uninterceptionTimeout = 0;
                        Interceptable = true;
                    }
                }
                if (Preparing)
                {
                    Prepare(dt);
                }
                else
                {
                    Loop(dt);
                }
            }
        }
    }
}
