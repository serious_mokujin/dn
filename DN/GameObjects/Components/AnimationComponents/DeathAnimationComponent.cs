﻿
using System;
using Blueberry;
using Blueberry.Animations;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of DeathAnimation.
	/// </summary>
	public class DeathAnimationComponent : AnimationComponent
    {
        public override Animations Type { get { return Animations.Dying; } }
        protected AnimationLayout layout;
        protected BoundaryComponent bounds;
		FloatAnimation scaleAnim;
		bool dying = false;
		GameObjectsManager _manager;
		
        public DeathAnimationComponent(IGameObject gameObject, GameObjectsManager manager)
            : base(gameObject)
        {
        	this._manager = manager;
        	scaleAnim = new FloatAnimation(1, 0, 0.5f, LoopMode.None, v=>v*v*v*v);
        }

        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            bounds = Owner.GetComponent<BoundaryComponent>();
            return layout != null && r && bounds != null;
        }
        protected override void StartPreparation()
        {
        	SetUninterceptible(0.5f);
            SetPreparationTimer(0.1f);
        }
        protected override void StartLoop()
        {
            //mainTimer.Run(0.2f);
            scaleAnim.Play();
            dying = true;
        }
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);

        }
        public override void Update(float dt)
        {
            base.Update(dt);
            if (dying && scaleAnim.State == PlaybackState.Stop)
            {
                _manager.RemoveObject(Owner);
            }
        }
        protected override void Loop(float dt)
        {
            base.Loop(dt);
            for (int i = 0; i < layout.parts.Length; i++) 
            {
            	layout.parts[i].scale = scaleAnim.Value;
            	layout.parts[i].position -= 10*dt*(Vector2.Normalize(layout.parts[i].distance));
            	layout.parts[i].rotation += dt*25;
            }
        }
	}
}
