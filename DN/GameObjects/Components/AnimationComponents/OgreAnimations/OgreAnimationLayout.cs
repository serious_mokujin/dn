﻿
using System;
using System.Drawing;
using Blueberry.Graphics.Fonts;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents.OgreAnimations
{
	/// <summary>
	/// Description of OgreAnimationLayout.
	/// </summary>
	public class OgreAnimationLayout : AnimationLayout
    {
        public AnimationLetterPart g;
        public AnimationLetterPart e;
        public AnimationLetterPart r;
        public AnimationLetterPart o;

        public OgreAnimationLayout()
            : base(4)
        {
            BitmapFont font = CM.I.Font("consolas72bold");
            o = new AnimationLetterPart('O', font, Color.SandyBrown);
            g = new AnimationLetterPart('G', font, Color.SandyBrown);
            r = new AnimationLetterPart('R', font, Color.SaddleBrown);
            e = new AnimationLetterPart('E', font, Color.SaddleBrown);
            o.distance = new Vector2(-o.size.Width / 2 - 2, -o.size.Height / 2 - 2);
            g.distance = new Vector2(g.size.Width / 2 + 2, -g.size.Height / 2 - 2);
            r.distance = new Vector2(-r.size.Width / 2 - 2, r.size.Height / 2 + 2);
            e.distance = new Vector2(e.size.Width / 2 + 2, e.size.Height / 2 + 2);
            parts[0] = o;
            parts[1] = g;
            parts[2] = r;
            parts[3] = e;
        }
    
	}
}
