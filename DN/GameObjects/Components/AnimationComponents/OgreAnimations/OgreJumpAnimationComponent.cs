﻿
using System;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents.OgreAnimations
{
	/// <summary>
	/// Description of OgreJumpAnimation.
	/// </summary>
	public class OgreJumpAnimationComponent : JumpAnimationComponent
	{
		public OgreJumpAnimationComponent(IGameObject gameObject) : base(gameObject)
		{
			walkLegAnchor = new Vector2(30, 0);
		}
	}
}
