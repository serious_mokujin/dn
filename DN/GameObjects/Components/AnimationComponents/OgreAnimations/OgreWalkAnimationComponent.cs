﻿
using System;
using Blueberry;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents.OgreAnimations
{
	/// <summary>
	/// Description of OgreWalkAnimationComponent.
	/// </summary>
	public class OgreWalkAnimationComponent : WalkAnimationComponent
	{
		public OgreWalkAnimationComponent(IGameObject gameObject) : base(gameObject)
		{
			walkLegDistance = 20;
			walkLegAnchor = new OpenTK.Vector2(20, 0);
		}
				protected override void Prepare(float dt)
		{
			base.Prepare(dt);
			Vector2 pos = new Vector2(physics.GetVelocity().X > 0 ? -5 : 5, 0);
            
            layout.parts[0].LerpPos(pos, preparationInterval);
            layout.parts[1].LerpPos(pos, preparationInterval);
            layout.parts[0].LerpRot(physics.GetVelocity().X > 0 ? -0.15f : 0.15f, preparationInterval);
            layout.parts[1].LerpRot(physics.GetVelocity().X > 0 ? -0.15f : 0.15f, preparationInterval);
            
		}
		protected override void Loop(float dt)
		{
			base.Loop(dt);
			
			Vector2 pos = new Vector2(-5, 0);
            MathUtils.RotateVector2(ref pos, MathHelper.Pi * (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));
            layout.parts[0].position =  pos;
            layout.parts[1].position = pos;

            if(walkdir > 0)
            {
            	layout.parts[0].rotation = MathUtils.Lerp(0, walkdir * 0.15f, (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));
            	layout.parts[1].rotation = MathUtils.Lerp(0, walkdir * 0.15f, (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));
            }
            else
            {
            	layout.parts[0].rotation = MathUtils.Lerp(walkdir * 0.15f, 0, (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));
            	layout.parts[1].rotation = MathUtils.Lerp(walkdir * 0.15f, 0, (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));	
            }
		}
	}
}
