﻿using System;
using Blueberry;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of TrollWalkingAnimationComponent.
	/// </summary>
	public class TrollWalkAnimationComponent : WalkAnimationComponent
	{
        float handsDistance = 2f;
        float handsRotation = 0.15f;
        
        public TrollWalkAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        	walkLegDistance = 10;
        	walkLegAnchor = new Vector2(10, 0);
        }

        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
           	layout.parts[3].flipH = layout.parts[2].flipH = physics.GetVelocity().X < 0;
           	
        }
        /*
        protected override void StartLoop()
        {
            mainTimer.Run(0.2f);
            addTimer.Run(0.4f);
            addTimer.Interval = 0;
            addTimer.Phase = physics.GetVelocity().X > 0;
            mainTimer.Phase = physics.GetVelocity().X > 0;
        }*/
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);

            Vector2 pos = new Vector2(0, physics.GetVelocity().X > 0 ? handsDistance : -handsDistance);
            
            layout.parts[0].LerpPos(-pos, preparationInterval);
            layout.parts[1].LerpPos(pos, preparationInterval);
            
            layout.parts[0].LerpRot(physics.GetVelocity().X > 0 ? -handsRotation: handsRotation, preparationInterval);
            layout.parts[1].LerpRot(physics.GetVelocity().X > 0 ? -handsRotation: handsRotation, preparationInterval);
            layout.parts[4].LerpRot(physics.GetVelocity().X > 0 ? -0.15f : 0.15f, preparationInterval);
            
            layout.parts[4].LerpPos(new Vector2(physics.GetVelocity().X > 0 ? -5 : 5, 0), preparationInterval);
            
        }
        protected override void Loop(float dt)
        {
            base.Loop(dt);
            if (mainTimer.Phase)
            {
            	layout.parts[0].position.Y = MathUtils.Lerp(-handsDistance, handsDistance, mainTimer.Interval);
            	layout.parts[1].position.Y = MathUtils.Lerp(handsDistance, -handsDistance, mainTimer.Interval);
            	layout.parts[0].rotation = MathUtils.Lerp(-handsRotation, handsRotation, mainTimer.Interval);
            	layout.parts[1].rotation = MathUtils.Lerp(-handsRotation, handsRotation, mainTimer.Interval);
            }
            else
            {
            	layout.parts[0].position.Y = MathUtils.Lerp(handsDistance, -handsDistance, mainTimer.Interval);
            	layout.parts[1].position.Y = MathUtils.Lerp(-handsDistance, handsDistance, mainTimer.Interval);
            	layout.parts[0].rotation = MathUtils.Lerp(handsRotation, -handsRotation, mainTimer.Interval);
            	layout.parts[1].rotation = MathUtils.Lerp(handsRotation, -handsRotation, mainTimer.Interval);
            }
			
            Vector2 pos = new Vector2(-5, 0);
            MathUtils.RotateVector2(ref pos, MathHelper.Pi * (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));
            layout.parts[4].position =  pos;
            if(walkdir < 0)
            	layout.parts[4].rotation = MathUtils.Lerp(walkdir * 0.15f, 0, (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));
			else
				layout.parts[4].rotation = MathUtils.Lerp(0, walkdir * 0.15f, (addTimer.Phase ? addTimer.Interval : 1 - addTimer.Interval));
        }
	}
}
