﻿using System;
using System.Drawing;
using Blueberry.Graphics.Fonts;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of TrollAnimationLayout.
	/// </summary>
	public class TrollAnimationLayout: AnimationLayout
    {
        public AnimationLetterPart t;
        public AnimationLetterPart r;
        public AnimationLetterPart o;
        public AnimationLetterPart ll;
        public AnimationLetterPart lr;

        public TrollAnimationLayout()
            : base(5)
        {
            BitmapFont font = CM.I.Font("consolas25bold");
            t = new AnimationLetterPart('T', font, Color.DarkGreen);
            r = new AnimationLetterPart('R', font,Color.Green);
            o = new AnimationLetterPart('O', font, Color.Green);
            ll = new AnimationLetterPart('L', font, Color.Green);
            lr = new AnimationLetterPart('L', font, Color.Green);
            
            t.distance = new Vector2(0, -t.size.Height-10);
            r.distance = new Vector2(-r.size.Width / 2 - 4, -r.size.Height / 2 - 4);
            o.distance = new Vector2(o.size.Width / 2 + 4, -o.size.Height / 2 - 4);
            ll.distance = new Vector2(-ll.size.Width / 2 - 2, ll.size.Height / 2 + 2);
            lr.distance = new Vector2(lr.size.Width / 2 + 10, lr.size.Height / 2 + 2);
            parts[0] = r;
            parts[1] = o;
            parts[2] = ll;
            parts[3] = lr;
            parts[4] = t;
        }
	}
}
