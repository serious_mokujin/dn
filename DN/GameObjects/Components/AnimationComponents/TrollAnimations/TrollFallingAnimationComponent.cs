﻿using System;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of TrollFallingAnimationComponent.
	/// </summary>
	public class TrollFallingAnimationComponent : FallingAnimationComponent
	{
		public TrollFallingAnimationComponent(IGameObject gameObgect) : base(gameObgect, 5)
		{
			letterDisplacements = new Vector2[] { new Vector2(-5), new Vector2(5, -5), new Vector2(-5, 5), new Vector2(5), Vector2.Zero};
		}
		protected override void StartPreparation()
		{
			base.StartPreparation();
			layout.parts[3].flipH = layout.parts[2].flipH = physics.GetVelocity().X < 0;
		}
		protected override void Loop(float dt)
		{
			base.Loop(dt);
			Vector2 v = physics.GetVelocity();
			if(v.X != 0)
				layout.parts[3].flipH = layout.parts[2].flipH = v.X < 0;
		}
	}
}
