﻿
using System;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of TrollClimbingAnimation.
	/// </summary>
	public class TrollClimbAnimationComponent : ClimbAnimationComponent
	{
		public TrollClimbAnimationComponent(IGameObject gameObject) : base(gameObject)
		{
			letterDistances = new Vector2[]{ new Vector2(-5), new Vector2(5, -5), new Vector2(-5f/3f, 5f/3f), new Vector2(5f/3f) };
		}
		
		protected override void Loop(float dt)
		{
			base.Loop(dt);
			if(!pause)
			{
				Vector2 v = physics.GetVelocity();
				if(v.X != 0)
					layout.parts[3].flipH = layout.parts[2].flipH = v.X < 0;
			}
		}
	}
}
