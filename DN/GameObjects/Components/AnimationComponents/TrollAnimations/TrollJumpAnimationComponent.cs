﻿
using System;
using Blueberry;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of TrollJumpAnimationComponent.
	/// </summary>
	public class TrollJumpAnimationComponent : JumpAnimationComponent
	{
		public TrollJumpAnimationComponent(IGameObject gameObject) : base(gameObject)
		{
		}
		protected override void Prepare(float dt)
		{
			base.Prepare(dt);
			layout.parts[2].rotation = MathUtils.Lerp(leftLegRotation, MathUtils.AngleBetween(Vector2.UnitY, jumpAnimStart - bounds.Position)* Math.Abs(physics.GetVelocity().Y)/10, preparationInterval);
            layout.parts[3].rotation = MathUtils.Lerp(rightLegRotation, MathUtils.AngleBetween(Vector2.UnitY, jumpAnimStart - bounds.Position)* Math.Abs(physics.GetVelocity().Y)/10, preparationInterval);
		}
		protected override void Loop(float dt)
		{
			base.Loop(dt);
			Vector2 v = physics.GetVelocity();
			if(v.X != 0)
				layout.parts[3].flipH = layout.parts[2].flipH = v.X < 0;
		}
	}
}
