﻿
using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.AnimationComponents.Listeners
{
	/// <summary>
	/// Description of IdleAnimationListener.
	/// </summary>
	public class IdleAnimationListener : Component, IUpdatable
    {
		PhysicComponent _physics;
		AnimationControllerComponent _controller;
		
        public IdleAnimationListener(IGameObject gameObject)
            : base(gameObject)
        {
        }
        public override bool GetDependicies()
        {
        	_physics = Owner.GetComponent<PhysicComponent>();
        	_controller = Owner.GetComponent<AnimationControllerComponent>();
            return base.GetDependicies() && _physics != null && _controller != null;
        }
        public override void ProccesMessage(IMessage message)
        {
        }
        void HandleEvent(EventMessage message)
        {
        }

	    public void Update(float dt)
	    {
            if (_controller.Current != Animations.Idle && _physics.GetVelocity().LengthFast < 0.5f && _physics.OnGround)
            {
                Owner.SendMessage(new PlayAnimationMessage(Animations.Idle));
            }
	    }
    }
}
