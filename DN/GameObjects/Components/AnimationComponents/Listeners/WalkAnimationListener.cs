﻿using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.AnimationComponents.Listeners
{
	/// <summary>
	/// Description of WalkAnimationListener.
	/// </summary>
	public class WalkAnimationListener : Component, IUpdatable
    {
		PhysicComponent _physics;
		AnimationControllerComponent _controller;
		
        public WalkAnimationListener(IGameObject gameObject)
            : base(gameObject)
        {
        }
        public override bool GetDependicies()
        {
        	_physics = Owner.GetComponent<PhysicComponent>();
        	_controller = Owner.GetComponent<AnimationControllerComponent>();
            return base.GetDependicies() && _physics != null && _controller != null;
        }
        public override void ProccesMessage(IMessage message)
        {
        }
        void HandleEvent(EventMessage message)
        {
        
        }

	    public void Update(float dt)
	    {
            if (_controller.Current != Animations.Walk && _physics.OnGround && Math.Abs(_physics.GetVelocity().X) > 1 &&
                _physics.GetVelocity().Y >= 0)
            {
                Owner.SendMessage(new PlayAnimationMessage(Animations.Walk));
            }
	    }
    }
}
