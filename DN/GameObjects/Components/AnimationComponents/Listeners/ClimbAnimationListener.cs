﻿
using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.AnimationComponents.Listeners
{
	/// <summary>
	/// Description of ClimbAnimationListener.
	/// </summary>
	public class ClimbAnimationListener : Component
    {
		AnimationControllerComponent _controller;
		
        public ClimbAnimationListener(IGameObject gameObject)
            : base(gameObject)
        {
        }
        public override bool GetDependicies()
        {
        	_controller = Owner.GetComponent<AnimationControllerComponent>();
            return base.GetDependicies() && _controller != null;
        }
        public override void ProccesMessage(IMessage message)
        {
        	if(message is EventMessage)
        		HandleEvent(message as EventMessage);
        }
        void HandleEvent(EventMessage message)
        {
        	if(message.Event == Events.Climb)
	            if (_controller.Current != Animations.Climb)
        		{
            		Owner.SendMessage(new PlayAnimationMessage(Animations.Climb));
        		}
        }
	}
}
