using System;
using DN.GameObjects.Components.AnimationComponents;
using DN.GameObjects;
using DN.GameObjects.Messages;

namespace DN
{
    public class ActionAnimationListener: Component
    {
        AnimationControllerComponent _controller;
        
        public ActionAnimationListener(IGameObject gameObject)
            : base(gameObject)
        {
        }
        public override bool GetDependicies()
        {
            _controller = Owner.GetComponent<AnimationControllerComponent>();
            return base.GetDependicies() && _controller != null;
        }
        public override void ProccesMessage(IMessage message)
        {
            if(message is EventMessage)
                HandleEvent(message as EventMessage);
        }
        void HandleEvent(EventMessage message)
        {
            if(message.Event == Events.Climb)
                if (_controller.Current != Animations.Climb)
            {
                Owner.SendMessage(new PlayAnimationMessage(Animations.Climb));
            }
        }
    }
}

