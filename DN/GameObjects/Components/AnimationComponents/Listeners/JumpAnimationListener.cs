﻿
using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.AnimationComponents.Listeners
{
	/// <summary>
	/// Description of JumpAnimationListener.
	/// </summary>
	public class JumpAnimationListener : Component
    {
		AnimationControllerComponent _controller;
		
        public JumpAnimationListener(IGameObject gameObject)
            : base(gameObject)
        {
        }
        public override bool GetDependicies()
        {
        	_controller = Owner.GetComponent<AnimationControllerComponent>();
            return base.GetDependicies() && _controller != null;
        }
        public override void ProccesMessage(IMessage message)
        {
        	if(message is EventMessage)
        		HandleEvent(message as EventMessage);
        }
        void HandleEvent(EventMessage message)
        {
        	if(message.Event == Events.Jump)
	            if (_controller.Current != Animations.Jump)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Jump));
        }
	}
}
