﻿
using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.AnimationComponents.Listeners
{
	/// <summary>
	/// Description of FallingAnimationListener.
	/// </summary>
	public class FallingAnimationListener : Component
    {
		AnimationControllerComponent _controller;
		
        public FallingAnimationListener(IGameObject gameObject)
            : base(gameObject)
        {
        }
        public override bool GetDependicies()
        {
        	_controller = Owner.GetComponent<AnimationControllerComponent>();
            return base.GetDependicies() && _controller != null;
        }
        public override void ProccesMessage(IMessage message)
        {
        	if(message is EventMessage)
        		HandleEvent(message as EventMessage);
        }
        void HandleEvent(EventMessage message)
        {
        	if(message.Event == Events.Fall || message.Event == Events.CellingReached)
	            if (_controller.Current != Animations.Falling)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Falling));
        }
	}
}
