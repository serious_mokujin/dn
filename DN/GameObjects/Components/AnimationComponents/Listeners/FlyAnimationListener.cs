﻿
using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.AnimationComponents.Listeners
{
	/// <summary>
	/// Description of FlyAnimationListener.
	/// </summary>
	public class FlyAnimationListener : Component, IUpdatable
    {
		AnimationControllerComponent _controller;
		PhysicComponent physics;
		
        public FlyAnimationListener(IGameObject gameObject)
            : base(gameObject)
        {
        }
        public override bool GetDependicies()
        {
        	_controller = Owner.GetComponent<AnimationControllerComponent>();
        	physics = Owner.GetComponent<PhysicComponent>();
            return base.GetDependicies() && _controller != null && physics != null;
        }
        public override void ProccesMessage(IMessage message)
        {
        }

        public void Update(float dt)
        {
            if (!physics.OnGround)
                if (_controller.Current != Animations.Fly)
                    Owner.SendMessage(new PlayAnimationMessage(Animations.Fly));
        }
    }
}
