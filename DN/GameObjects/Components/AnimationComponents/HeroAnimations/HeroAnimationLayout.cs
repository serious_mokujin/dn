﻿using System.Drawing;
using Blueberry;
using Blueberry.Graphics.Fonts;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.AnimationComponents
{
    public class HeroAnimationLayout : AnimationLayout
    {
        public readonly Color Color1;
        public readonly Color Color2;
        public AnimationLetterPart h;
        public AnimationLetterPart e;
        public AnimationLetterPart r;
        public AnimationLetterPart o;

        public HeroAnimationLayout(Color color1, Color color2)
            : base(4)
        {
            Color1 = color1;
            Color2 = color2;
            BitmapFont font = CM.I.Font("consolas24");

            h = new AnimationLetterPart('H', font, color2);
            e = new AnimationLetterPart('E', font, color2);
            r = new AnimationLetterPart('R', font,color1);
            o = new AnimationLetterPart('O', font, color1);
            h.distance = new Vector2(-h.size.Width / 2 * 1.2f, -h.size.Height / 2 * 1.2f);
            e.distance = new Vector2(e.size.Width / 2 * 1.2f, -e.size.Height / 2 * 1.2f);
            r.distance = new Vector2(-r.size.Width / 2 * 1.2f, r.size.Height / 2 * 1.2f);
            o.distance = new Vector2(o.size.Width / 2 * 1.2f, o.size.Height / 2 * 1.2f);
            parts[0] = h;
            parts[1] = e;
            parts[2] = r;
            parts[3] = o;
        }
    }
}
