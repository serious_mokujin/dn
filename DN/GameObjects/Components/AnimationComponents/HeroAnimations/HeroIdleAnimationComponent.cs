﻿using Blueberry;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.AnimationComponents
{
    class HeroIdleAnimationComponent : AnimationComponent
    {
        public override Animations Type { get { return Animations.Idle; } }
        private AnimationLayout layout;

        public HeroIdleAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            return r && layout != null;
        }

        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
        }
        protected override void StartLoop()
        {
            mainTimer.Run(0.8f);
            mainTimer.Phase = true;
        }
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);

            layout.LerpPos(Vector2.Zero, preparationInterval);
            layout.LerpRot(0, preparationInterval);
        }
        protected override void Loop(float dt)
        {
            base.Loop(dt);
            
            layout.parts[0].position = MathUtils.Lerp(Vector2.Zero, new Vector2(-1.6f, -1.6f), mainTimer.Phase ? mainTimer.Interval : 1 - mainTimer.Interval);
            layout.parts[1].position = MathUtils.Lerp(Vector2.Zero, new Vector2(1.6f, -1.6f), mainTimer.Phase ? mainTimer.Interval : 1 - mainTimer.Interval);
        }
    }
}
