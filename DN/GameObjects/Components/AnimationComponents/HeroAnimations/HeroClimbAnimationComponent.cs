﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
    class HeroClimbAnimationComponent:ClimbAnimationComponent
    {
        
        public HeroClimbAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        	letterDistances = new Vector2[]{ new Vector2(-5), new Vector2(5, -5), new Vector2(-5f/3f, 5f/3f), new Vector2(5f/3f) };
        }

    }
}