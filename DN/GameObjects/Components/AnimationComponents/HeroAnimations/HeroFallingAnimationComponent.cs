﻿using System;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents.HeroAnimations
{
	/// <summary>
	/// Description of HeroFallingAnimationComponent.
	/// </summary>
	public class HeroFallingAnimationComponent : FallingAnimationComponent
	{
		public HeroFallingAnimationComponent(IGameObject gameObject) : base(gameObject, 4)
		{
			letterDisplacements = new Vector2[] { new Vector2(-5), new Vector2(5, -5), new Vector2(-5, 5), new Vector2(5)};
		}
	}
}
