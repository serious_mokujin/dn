﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
    class HeroJumpAnimationComponent:JumpAnimationComponent
    {
        public HeroJumpAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }
		protected override void Prepare(float dt)
		{
			base.Prepare(dt);
			layout.parts[2].rotation = MathUtils.Lerp(leftLegRotation, MathUtils.AngleBetween(Vector2.UnitY, jumpAnimStart - bounds.Position)* Math.Abs(physics.GetVelocity().Y)/10, preparationInterval);
            layout.parts[3].rotation = MathUtils.Lerp(rightLegRotation, MathUtils.AngleBetween(Vector2.UnitY, jumpAnimStart - bounds.Position)* Math.Abs(physics.GetVelocity().Y)/10, preparationInterval);
		}
    }
}
