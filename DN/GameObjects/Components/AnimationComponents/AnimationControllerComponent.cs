﻿using DN.GameObjects.Components.AnimationComponents;
using DN.GameObjects.Messages;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.AnimationComponents
{

    public class AnimationControllerComponent : Component, IUpdatable, IDrawable
    {
        public Animations Current { get; private set; }
        public bool Hold { get; set; }
        public AnimationLayout AnimationLayout { get; private set; }
        protected BoundaryComponent _boundary;

        public AnimationControllerComponent(IGameObject gameObject, AnimationLayout layout)
            : base(gameObject)
        {
            AnimationLayout = layout;
            Current = Animations.Nothing;
        }

        public override bool GetDependicies()
        {
            _boundary = Owner.GetComponent<BoundaryComponent>();
            return base.GetDependicies() && _boundary != null;
        }
        public override void ProccesMessage(Messages.IMessage message)
        {
            if (message is PlayAnimationMessage)
            {
                if (!Hold)
                    Current = ((PlayAnimationMessage)message).Animation;
            }
            if (message is EventMessage && !Hold)
                HandleEvent(message as EventMessage);
        }
        protected virtual void DetectAnimations()
        { }

        protected virtual void HandleEvent(EventMessage evn)
        {
        }

        public void Update(float dt)
        {
                DetectAnimations();
        }

        public void Draw(float dt)
        {
            AnimationLayout.Draw(_boundary.Position);
        }
    }
    public enum Animations : byte
    {
        Nothing = 0x01, // really just nothing, NOTHING AT ALL!!1111 
        Idle = 0x02,
        Walk = 0x04,
        Climb = 0x08,
        Jump = 0x10,
        Balancing = 0x11,
        Falling = 0x12,
        Landing = 0x14,
        Fly = 0x18,
        Dying = 0x20,
        Action = 0x40

    }

}
