﻿
using System;
using Blueberry;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of ClimbingAnimation.
	/// </summary>
	public class ClimbAnimationComponent:AnimationComponent
    {
        public override Animations Type { get { return Animations.Climb; } }
        protected AnimationLayout layout;

        protected Vector2[] letterDistances;
        
        float climbAmplitude = 20;
        Vector2 climbDirection; // in wich direction we want to climb 
        protected PhysicComponent physics;
        protected BoundaryComponent bounds;
		Vector2 lastPosition; // need to detect actual displacement of hero
		
        public ClimbAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            physics = Owner.GetComponent<PhysicComponent>();
            bounds = Owner.GetComponent<BoundaryComponent>();
            return layout != null && physics != null && bounds != null && r;
        }

        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
        }
        protected override void StartLoop()
        {
            mainTimer.Run(0.2f, false, 0.5f);
        }
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);

            for (int i = 0; i < 4; i++) 
            	layout.parts[i].LerpPos(letterDistances[i], preparationInterval);
            
            layout.LerpRot(0, preparationInterval);
            lastPosition = bounds.Position;
        }

        protected bool pause = false;
        protected override void Loop(float dt)
        {
        	Vector2 displacement = bounds.Position - lastPosition;
        	Vector2 newDirection = Vector2.Normalize(displacement);
        	newDirection.X = (float)Math.Round(newDirection.X, 5);
            newDirection.Y = (float)Math.Round(newDirection.Y, 5);
                
            if (displacement != Vector2.Zero) // if there was any displacement
            {
            	if (pause || Math.Abs(MathUtils.AngleBetween(climbDirection, newDirection)) > 0.1f)
                {
            		if(climbDirection != newDirection)
            		{
                    	localPrepare = true;
                    	localPrepareTime = localPreparePeriod;
            		}
                }
                else
                {
                	// why always so complicated conditions for such simple things..
                	// now it's calculating slowing depends on velocity
                	
                	if(displacement.X == 0)
                		base.Loop(dt*Math.Abs(displacement.Y)/3);
                	else if(displacement.Y == 0)
                		base.Loop(dt*Math.Abs(displacement.X)/3);
                	else
                		base.Loop(dt*displacement.LengthFast/3);
                }
                pause = false;
            }
            else{pause = true;}
			
			
            if (!pause)
            {
                if (newDirection == -climbDirection)
                {
                    localPrepare = false;
                    mainTimer.Interval = 1 - mainTimer.Interval;
                }
                else if (newDirection != climbDirection)
                {
                    localPrepareTo = newDirection;
                }
                climbDirection = newDirection;
            
                lastPosition = bounds.Position;
                if (localPrepare)
                {
                    LocalPrepare(dt);
                    return;
                }
	            
	            int phs = mainTimer.Phase ? -1 : 1;
					
            	layout.parts[0].position = phs * climbDirection * (climbAmplitude / 2) - 
            	                           phs * climbDirection * climbAmplitude * 
            	                           mainTimer.Interval + letterDistances[0];  
                layout.parts[1].position = -phs * climbDirection * (climbAmplitude / 2) +
                                           phs * climbDirection * climbAmplitude * 
                                           mainTimer.Interval + letterDistances[1];

                layout.parts[2].position = -phs * climbDirection * (climbAmplitude / 4) +
                                           phs * climbDirection * (climbAmplitude / 2) * 
                                           mainTimer.Interval + letterDistances[2];

            	layout.parts[3].position = phs * climbDirection * (climbAmplitude / 4) -
                                           phs * climbDirection * (climbAmplitude / 2) * 
                                           mainTimer.Interval + letterDistances[3];
            }
        }

        private float localPrepareTime = 0;
        private float localPreparePeriod = .1f;
        private bool localPrepare = false;
        private Vector2[] prepareBeginPositions = new Vector2[4];
        private Vector2 localPrepareTo;

        private void LocalPrepare(float dt)
        {
        	
            if (localPrepareTime == localPreparePeriod)
            {
            	for (int i = 0; i < 4; i++) 
            		prepareBeginPositions[i] = layout.parts[i].position;
            }
            localPrepareTime -= dt;
            float interval = 1 - localPrepareTime/localPreparePeriod;
            if (localPrepareTime <= 0)
            {
                localPrepare = false;
            }
            else
            {
            	int phs = mainTimer.Phase ? -1 : 1;
                
            	layout.parts[0].position = MathUtils.Lerp(prepareBeginPositions[0],
            	                                          phs * climbDirection * (climbAmplitude / 2) - 
            	                                          phs * climbDirection * climbAmplitude * 
            	                                          mainTimer.Interval + letterDistances[0],
                                                   		  interval);  
                layout.parts[1].position = MathUtils.Lerp(prepareBeginPositions[1], 
            	                                          -phs * climbDirection * (climbAmplitude / 2) +
                                                   		  phs * climbDirection * climbAmplitude * 
                                                   		  mainTimer.Interval + letterDistances[1],
                                                   		  interval);

                layout.parts[2].position = MathUtils.Lerp(prepareBeginPositions[2], 
                                                          -phs * climbDirection * (climbAmplitude / 4) +
                                                          phs * climbDirection * (climbAmplitude / 2) * 
                                                          mainTimer.Interval + letterDistances[2],
                                                          interval);

            	layout.parts[3].position = MathUtils.Lerp(prepareBeginPositions[3],
            	                                          phs * climbDirection * (climbAmplitude / 4) -
                                                          phs * climbDirection * (climbAmplitude / 2) * 
                                                          mainTimer.Interval + letterDistances[3],
                                                          interval);
            }
        }
	}
}
