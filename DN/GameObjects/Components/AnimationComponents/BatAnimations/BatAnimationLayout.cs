﻿
using System;
using System.Drawing;
using Blueberry.Graphics.Fonts;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents.BatAnimations
{
	/// <summary>
	/// Description of BatAnimationLayout.
	/// </summary>
	public class BatAnimationLayout : AnimationLayout
	{
		public AnimationLetterPart b;
        public AnimationLetterPart a;
        public AnimationLetterPart t;
        
		public BatAnimationLayout() : base(3)
		{
			BitmapFont font = CM.I.Font("consolas16");
            b = new AnimationLetterPart('b', font, Color.White);
            a = new AnimationLetterPart('A', font, Color.White);
            t = new AnimationLetterPart('t', font, Color.White);
            b.distance = new Vector2(-b.size.Width / 2 - a.size.Width/2, 0);
            a.distance = new Vector2(0, 0);
            t.distance = new Vector2(a.size.Width/2 + t.size.Width/2, 0);
            parts[0] = b;
            parts[1] = a;
            parts[2] = t;
		}
	}
}
