﻿
using System;
using Blueberry;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents.BatAnimations
{
	/// <summary>
	/// Description of BatFlyAnimation.
	/// </summary>
	public class BatFlyAnimationComponent : AnimationComponent
	{
        public override Animations Type { get { return Animations.Fly; } }
        private AnimationLayout layout;

        PhysicComponent physics;
		float wingsDistance = 8;
		
        public BatFlyAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            physics = Owner.GetComponent<PhysicComponent>();
            return r && layout != null && physics != null;
        }

        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
            
        }
        protected override void StartLoop()
        {
            mainTimer.Run(0.2f);
            mainTimer.Interval = 0.5f;
            mainTimer.Phase = physics.GetVelocity().X > 0;
        }
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);

            Vector2 pos = new Vector2(physics.GetVelocity().X > 0 ? -5 : 5, 0);
            
            layout.LerpPos(Vector2.Zero, preparationInterval);
            layout.LerpRot(0, preparationInterval);
        }
        protected override void Loop(float dt)
        {
            base.Loop(dt);

            int walkdir = physics.GetVelocity().X > 0 ? -1 : 1;
            if (mainTimer.Phase)
            {
            	layout.parts[0].position.Y = MathUtils.Lerp(-wingsDistance, wingsDistance, mainTimer.Interval);
            	layout.parts[2].position.Y = MathUtils.Lerp(-wingsDistance, wingsDistance, mainTimer.Interval);
            }
            else
            {
            	layout.parts[0].position.Y = MathUtils.Lerp(wingsDistance, -wingsDistance, mainTimer.Interval);
            	layout.parts[2].position.Y = MathUtils.Lerp(wingsDistance, -wingsDistance, mainTimer.Interval);
            }
        }
	}
}
