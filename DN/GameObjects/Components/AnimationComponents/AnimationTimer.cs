﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Components.AnimationComponents
{
    public class AnimationTimer
    {
        private float timer;
        private float period;
        private float interval;
        private bool phase;

        public float Interval
        {
            get { return interval; }
            set { interval = value; timer = interval * period; }
        }
        public float Period { get { return period; } }
        public bool Phase { get { return phase; } set { phase = value; } }
        public event Action OnPhaseChanging;

        public void Run(float period, bool phase = false, float startInterval = 0)
        {
            timer = startInterval * period;
            this.phase = phase;
            this.period = period;
        }

        public void Update(float dt)
        {
            timer += dt;
            if (timer >= period)
            {
                timer -= period;
                phase = !phase;
                if (OnPhaseChanging != null)
                    OnPhaseChanging();
            }
            interval = timer / period;
        }
    }
}
