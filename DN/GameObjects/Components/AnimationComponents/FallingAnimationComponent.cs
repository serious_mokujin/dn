﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
    public class FallingAnimationComponent:AnimationComponent
    {
        public override Animations Type { get { return Animations.Falling; } }
        protected AnimationLayout layout;
        protected BoundaryComponent bounds;
		protected 	PhysicComponent physics;
		
        Vector2[] velocities;// velH, velE, velR, velO;
        private Vector2 fallingLastPos;
        private float fallingDistance = 3;
        private float fallingAcceleration = 800;
		private bool prepareFromCelling; // sygnalise, when need to prepare body parts after colliding with celling
		private int _animateLettersCount; // how many letters in layout needs to be animated
		protected Vector2[] letterDisplacements;
		
        public FallingAnimationComponent(IGameObject gameObject, int animateLettersCount)
            : base(gameObject)
        {
        	_animateLettersCount = animateLettersCount;
        	velocities = new Vector2[animateLettersCount];
        }

        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            bounds = Owner.GetComponent<BoundaryComponent>();
            physics = Owner.GetComponent<PhysicComponent>();
            return layout != null && r && bounds != null && physics != null;
        }
		public override void ProccesMessage(IMessage message)
		{
			base.ProccesMessage(message);
			if(message is EventMessage)
				if((message as EventMessage).Event == Events.CellingReached)
					prepareFromCelling = true;
		}
        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
        }
        protected override void StartLoop()
        {
        	for (int i = 0; i < _animateLettersCount; i++) 
        	{
        		velocities[i] = new Vector2(RandomTool.NextSingle(-5, 5), RandomTool.NextSingle(-5, 5));
        		velocities[i].Normalize();
        		velocities[i] *= 15;
        	}

            fallingLastPos = bounds.Position;
            mainTimer.Run(0.2f);
            prepareFromCelling = false;
        }
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);

            layout.parts[0].LerpPos(new Vector2(-5), dt);
            layout.parts[1].LerpPos(new Vector2(5, -5), dt);
            layout.parts[2].LerpPos(new Vector2(-5, 5), dt);
            layout.parts[3].LerpPos(new Vector2(5), dt);
            if(prepareFromCelling)
            {
	            layout.LerpPos(Vector2.Zero, dt*10);
	            layout.LerpRot(0, dt*10);
            }

        }
        void IntegrateVelocity(float shiftX, float shiftY, int index, float dt)
        {
        	Vector2 a = new Vector2(shiftX - layout.parts[index].position.X, shiftY - layout.parts[index].position.Y);
        	a.Normalize();
        	a *= fallingAcceleration;
        	velocities[index] += a * dt;
        }
        protected override void Loop(float dt)
        {
            base.Loop(dt);
            
            IntegrateVelocity(-fallingDistance, -fallingDistance, 0, dt);
            IntegrateVelocity(fallingDistance, -fallingDistance, 1, dt);
            IntegrateVelocity(-fallingDistance, fallingDistance, 2, dt);
            IntegrateVelocity(fallingDistance, fallingDistance, 3, dt);
            // integrate positions
            for (int i = 0; i < 4; i++) 
            	layout.parts[i].position += velocities[i] * dt;

            layout.LerpRot(MathUtils.AngleBetween(-Vector2.UnitY, fallingLastPos - bounds.Position), dt / 2);

            fallingLastPos = bounds.Position;
        }
    }
}
