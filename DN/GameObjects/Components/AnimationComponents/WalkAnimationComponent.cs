﻿
using System;
using Blueberry;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
	/// <summary>
	/// Description of WalkAnimationComponent.
	/// </summary>
	public class WalkAnimationComponent : AnimationComponent
    {
        public override Animations Type { get { return Animations.Walk; } }
        protected AnimationLayout layout;

        protected float walkLegDistance = 10f;
        protected Vector2 walkLegAnchor = new Vector2(10f, 0);
        protected PhysicComponent physics;
        protected AnimationTimer addTimer;
		protected int walkdir;
		
        public WalkAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
            addTimer = new AnimationTimer();
        }

        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            physics = Owner.GetComponent<PhysicComponent>();
            return r && layout != null && physics != null;
        }

        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
            
        }
        protected override void StartLoop()
        {
            mainTimer.Run(0.2f);
            addTimer.Run(0.4f);
            addTimer.Interval = 0;
            addTimer.Phase = physics.GetVelocity().X > 0;
            mainTimer.Phase = physics.GetVelocity().X > 0;
        }
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);
			layout.parts[2].LerpPos(-walkLegAnchor, preparationInterval);
            layout.parts[3].LerpPos(walkLegAnchor, preparationInterval);
            
            layout.parts[2].LerpRot(0, preparationInterval);
            layout.parts[3].LerpRot(0, preparationInterval);
        }
        protected override void Loop(float dt)
        {
            base.Loop(dt);
            addTimer.Update(dt);

            walkdir = physics.GetVelocity().X > 0 ? -1 : 1;
            if (mainTimer.Phase)
            {
            	layout.parts[3].position.Y = 0;
                layout.parts[3].position.X = MathUtils.Lerp(-walkdir * walkLegDistance, walkdir * walkLegDistance, mainTimer.Interval);

                layout.parts[2].position.X = walkdir * walkLegDistance;
                layout.parts[2].position.Y = 0;
                MathUtils.RotateVector2(ref layout.parts[2].position, -walkdir * MathHelper.Pi * mainTimer.Interval);

                layout.parts[2].LerpRot(0, mainTimer.Interval);
                layout.parts[3].LerpRot(-0.2f * walkdir, mainTimer.Interval);
            }
            else
            {
                layout.parts[3].position.X = walkdir*walkLegDistance;
                layout.parts[3].position.Y = 0;
                MathUtils.RotateVector2(ref layout.parts[3].position, -walkdir*MathHelper.Pi*mainTimer.Interval);

                layout.parts[2].position.Y = 0;
                layout.parts[2].position.X = MathUtils.Lerp(-walkdir*walkLegDistance, walkdir*walkLegDistance, mainTimer.Interval);

                layout.parts[2].LerpRot(-0.2f * walkdir, mainTimer.Interval);
                layout.parts[3].LerpRot(0, mainTimer.Interval);
            }

            
        }
	}
}
