using System;
using DN.GameObjects.Components.AnimationComponents;
using DN.GameObjects;
using OpenTK;
using Blueberry;
using DN.GameObjects.Components;
using DN.GameObjects.Messages;

namespace DN
{
    public class SwordAnimationComponent: AnimationComponent
    {
        public override Animations Type { get { return Animations.Action; } }
        private AnimationLayout layout;
        DirectionComponent _directionComponent;
        float _offset = -0.1f;

        public SwordAnimationComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }
        
        public override bool GetDependicies()
        {
            bool r = base.GetDependicies();
            layout = this.Controller.AnimationLayout;
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            return r && layout != null;
        }

        public override void ProccesMessage(IMessage message)
        {
            //todo: it really shouldn't be here
            if(message is PickupMessage)
            {
                if ((message as PickupMessage).Pickuper == null)
                {
                    SetDirectionComponent(Owner.GetComponent<DirectionComponent>());
                    return;
                }
                var dc = (message as PickupMessage).Pickuper.GetComponent<DirectionComponent>();
                SetDirectionComponent(dc);
            }
        }
        
        public void SetDirectionComponent(DirectionComponent directionComponent)
        {
            _directionComponent = directionComponent;
        }

        public override void Update(float dt)
        {
            base.Update(dt);
            if (_directionComponent != null)
            {
                layout.parts [0].rotation = _directionComponent.Angle;
                layout.parts [0].flipH = layout.parts [0].flipV = _directionComponent.Direction.X < 0; 
                layout.parts [0].position = _directionComponent.Direction * (_offset * layout.parts [0].size.Width);
            } else
            {
                layout.parts [0].rotation = 0;
                layout.parts [0].flipH = layout.parts [0].flipV = false; 
                layout.parts [0].position = Vector2.UnitX * (_offset * layout.parts [0].size.Width);
            }
        }

        protected override void StartPreparation()
        {
            SetPreparationTimer(0.1f);
            
        }
        protected override void StartLoop()
        {
            mainTimer.Run(0.3f);
            mainTimer.Interval = 0f;
        }
        protected override void Prepare(float dt)
        {
            base.Prepare(dt);
            
            layout.LerpPos(Vector2.Zero, preparationInterval);
            layout.LerpRot(0, preparationInterval);
        }
        protected override void Loop(float dt)
        {
            base.Loop(dt);
        }
    }
}

