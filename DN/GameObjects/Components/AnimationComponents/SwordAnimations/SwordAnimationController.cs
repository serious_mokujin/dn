﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.AnimationComponents.SwordAnimations
{
    class SwordAnimationController:AnimationControllerComponent
    {
        
        PhysicComponent _physics;

        public SwordAnimationController(IGameObject gameObject):base(gameObject, new SwordAnimationLayout())
        {}
        public override bool GetDependicies()
        {
            bool b = base.GetDependicies();
           
            _physics = Owner.GetComponent<PhysicComponent>();
            return b && _physics != null;
        }

        protected override void DetectAnimations()
        {
            if (Current != Animations.Walk && _physics.OnGround && Math.Abs(_physics.GetVelocity().X) > 1 &&
                _physics.GetVelocity().Y >= 0)
            {
                Owner.SendMessage(new PlayAnimationMessage(Animations.Walk));
            }
            else if (Current != Animations.Idle && _physics.GetVelocity().LengthFast < 0.5f && _physics.OnGround)
            {
                Owner.SendMessage(new PlayAnimationMessage(Animations.Idle));
            }
        }
        protected override void HandleEvent(EventMessage evn)
        {
            switch (evn.Event)
            {
                case Events.Jump:
                    if (Current != Animations.Jump)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Jump));
                    break;
                case Events.Land:
                    if (Current != Animations.Landing)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Landing));

                    break;
                case Events.Fall:
                    if (Current != Animations.Falling)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Falling));
                    break;
                case Events.Climb:
                    if (Current != Animations.Climb)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Climb));
                    break;
                case Events.Walk:
                    if (Current != Animations.Walk)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Walk));
                    break;
                case Events.Idle:
                    if (Current != Animations.Idle)
                        Owner.SendMessage(new PlayAnimationMessage(Animations.Idle));
                    break;

            }
        }
    }
}
