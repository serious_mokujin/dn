﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;

namespace DN.GameObjects.Components.AnimationComponents
{
    public  class SwordAnimationLayout : AnimationLayout
    {
        public AnimationPart body;

        public SwordAnimationLayout()
            : base(1)
        {
            body = new AnimationTexturePart(CM.I.tex("sword_sprite"));
            body.originX = -0.1f;
            body.originY = 0.5f;
            parts[0] = body;
        }
    }
}
