﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Components;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects
{
    class BowComponent:Component
    {
        public float TensionGrowingSpeed = 40;

        public float MaxTension = 18;
        public float MinTension = 2;



        private float _tension = 0;


        private float _projectiveSpeed;

        public float ProjectiveSpeed
        {
            get { return _projectiveSpeed * (_tension); }
            set { _projectiveSpeed = value; }
        }

        protected Vector2 ProjectivePosition
        {
            get
            {
                return new Vector2(_boundaryComponent.Position.X + (-_tension) * (float)Math.Cos(_rotation),
                                   _boundaryComponent.Position.Y - _tension * (float)Math.Sin(_rotation));
            }
        }

        private GameObject _currentProjective;

        private BoundaryComponent _boundaryComponent;
        private DirectionComponent _directionComponent;
        private PickupableComponent _pickupableComponent;
        private float _rotation;

        private GameObjectsManager _manager;
        private GameObjectsFabric _fabric;

        private DirectionComponent _projectiveDirectionComponent;

        public BowComponent(IGameObject gameObject, GameObjectsManager manager, GameObjectsFabric fabric) : base(gameObject)
        {
            _fabric = fabric;

            _manager = manager;

        }

        private GameObject GetArrow()
        {
            GameObject gm = (GameObject)_fabric.CreateArrow();
            _projectiveDirectionComponent = gm.GetComponent<DirectionComponent>();
            gm.GetComponent<DirectionDrawingComponent>().SetDirectionComponent(_directionComponent);
            _manager.AddObject(gm);
            return gm;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _pickupableComponent = Owner.GetComponent<PickupableComponent>();
            Owner.GetComponent<ActionComponent>().AutoFinish = false;

            _currentProjective = GetArrow();
            return _boundaryComponent != null && _directionComponent != null; 
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                HandleUpdateMessage((UpdateMessage)message);
            }
            else if (message is ActionMessage)
            {
                HandleActionMessage((ActionMessage)message);
            }
        }
        private void HandleUpdateMessage(UpdateMessage updateMessage)
        {
            if (_currentProjective != null)
            {
                _rotation = FunctionHelper.Vector2ToRadians(_directionComponent.Direction);
                _currentProjective.GetComponent<BoundaryComponent>().Position = ProjectivePosition;
            }
        }
        
        private void HandleActionMessage(ActionMessage message)
        {
            switch (message.Type)
            {
                case ActionMessageType.Start:
                    _tension = MinTension;
                    break;
                case ActionMessageType.Update:
                    if (MaxTension > _tension)
                        _tension += message.DT*TensionGrowingSpeed;
                    if (MaxTension < _tension)
                        _tension = MaxTension;
                    break;
                case ActionMessageType.Interupt:

                    if(_currentProjective == null)
                        return;
                    Vector2 dir = _directionComponent.Direction;

                    _currentProjective.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                    _currentProjective.SendMessage(new MoveMessage(dir, ProjectiveSpeed, false));

                    ObjectGroup[] objectGroup = null;

                    if (_pickupableComponent.Pickuper != null)
                    {
                        objectGroup = new []
                                          {
                                              Owner.ObjectGroup,
                                              _pickupableComponent.Pickuper.ObjectGroup
                                          };
                    }
                    else
                    {
                        objectGroup = new[]
                                          {
                                              Owner.ObjectGroup,
                                          };
                    }

                    _currentProjective.SendMessage(new ObjectsCollisionsStateMessage(true, objectGroup));
                    _currentProjective.GetComponent<DirectionDrawingComponent>().SetDirectionComponent(_projectiveDirectionComponent);
                    _currentProjective = null;
                    break;
                case ActionMessageType.Ready:
                    _currentProjective = GetArrow();
                    break;
            }
        }
    }
}
