﻿using DN.GameObjects.Messages;
using OpenTK;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DN.GameObjects
{
    //todo: fix jumps
    public class JumpComponent:Component
    {
        public bool Jumping {get { return _jump; }}

        public float JumpAcceleration;
        public float JumpLength;

        private bool _jump;

        PhysicComponent _physicComponent;
        private LadderClimbingComponent _ladderClimbingComponent;

        float _elapsed = 0;

        
        public JumpComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }


        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            _ladderClimbingComponent = Owner.GetComponent<LadderClimbingComponent>();
            return _physicComponent != null;
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is UpdateMessage)
            {
                Update((UpdateMessage) message);
            }
            else if (message is EventMessage)
            {
                Event((EventMessage)message);
            }
        }

        private void Update(UpdateMessage message)
        {
            if (_jump)
            {
                _elapsed += message.DT;
                _physicComponent.Move(-GameWorld.GravityDirection, JumpAcceleration * message.DT, false);

                if (_elapsed >= JumpLength)
                    StopJump();
            }
        }
        private void Event(EventMessage message)
        {
            switch (message.Event)
            {
                case Events.StartJump:
                    if (_physicComponent.OnGround ||(_ladderClimbingComponent != null && _ladderClimbingComponent.ClimbLadder))
                    {
                        _jump = true;
                        _physicComponent.SetMoveY(-1);

                        Owner.SendMessage(new EventMessage(Events.Jump));

                    }
                    break;
                case Events.StopJump:
                    _jump = false;
                    _elapsed = 0;
                    break;
            }
        }

        public void Jump()
        {
            Owner.SendMessage(new EventMessage(Events.StartJump));
        }
        public void StopJump()
        {
            Owner.SendMessage(new EventMessage(Events.StopJump));
        }
    }
}
