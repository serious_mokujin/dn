﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Audio;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.Sounds
{
    class SoundOnAction:Component
    {
        private readonly AudioClip _actionEndSound;
        private readonly AudioClip _actionStartSound;
        private AudioRemoteControll _startSoundRemote;
        private AudioRemoteControll _endSoundRemote;

        private readonly bool _interupt;


        public SoundOnAction(IGameObject gameObject,
                             AudioClip actionStartSound,
                             AudioClip actionEndSound, bool interupt)
                             : base(gameObject)
        {
            _actionStartSound = actionStartSound;
            _actionEndSound = actionEndSound;
            _interupt = interupt;
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is ActionMessage)
            {
                switch ((message as ActionMessage).Type)
                {
                    case ActionMessageType.Start:
                        if (_actionStartSound != null)
                        {
                            _startSoundRemote = _actionStartSound.PlayDynamic();
                        }
                        break;
                    case ActionMessageType.Finish:
                    case ActionMessageType.Interupt:
                        if (_actionEndSound != null)
                        {
                            _endSoundRemote = _actionEndSound.PlayDynamic();
                            if (_interupt && _startSoundRemote.Connected)
                                _startSoundRemote.Break();
                        }
                        break;
                }
            }
        }
    }
}
