﻿
using System;
using Blueberry;
using Blueberry.Audio;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.Sounds
{
	/// <summary>
	/// Description of SoundOnDie.
	/// </summary>
	public class SoundOnDeath : Component
	{
		private AudioClip[] _sounds;
        public SoundOnDeath(IGameObject gameObject, params AudioClip[] sounds) : base(gameObject)
        {
            _sounds = sounds;
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is DieMessage)
            {
               	_sounds[RandomTool.NextInt(0, _sounds.Length)].PlayDynamic();
            }
        }
	}
}
