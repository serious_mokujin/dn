﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Blueberry;
using Blueberry.Audio;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.Sounds
{
    public class SoundOnMakingDamage:Component
    {
    	private AudioClip[] _sounds;
        public SoundOnMakingDamage(IGameObject gameObject, params AudioClip[] sounds) : base(gameObject)
        {
            _sounds = sounds;
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is DamageMessage)
            {
                var m = (DamageMessage) message;
                if(m.DamageWay == DamageWay.Given && m.Dealed)
                	_sounds[RandomTool.NextInt(0, _sounds.Length)].PlayDynamic();
            }
        }
    }
}
