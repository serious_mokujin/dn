﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components
{
    public class AddStatisticKillsOnDeath:Component
    {
        private readonly GameWorld _gameWorld;

        public AddStatisticKillsOnDeath(IGameObject gameObject, GameWorld gameWorld) : base(gameObject)
        {
            _gameWorld = gameWorld;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is DieMessage)
            {
                _gameWorld.Statistic.AddKill(Owner.ObjectGroup);
            }
        }
    }
}
