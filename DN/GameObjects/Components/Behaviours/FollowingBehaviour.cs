﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components.Behaviours
{
    public class FollowingBehaviour:Behaviour
    {
        private Vector2 _direction;

        public FollowingBehaviour(IGameObject owner) : base(owner)
        {
        }

        public override void Update(float dt)
        {
            base.Update(dt);
            IGameObject curTarget = GetClosestTarget();
            if(curTarget == null)
                return;
            var curBoundary = curTarget.GetComponent<BoundaryComponent>();

            if(BoundaryComponent.Distance(curBoundary, OwnerBoundaryComponent) < 300)
            {
                _direction = FunctionHelper.DirectionToObject(OwnerBoundaryComponent.Position,
                                                              curBoundary.Position);
                OwnerPhysicComponent.Move(_direction, OwnerPhysicComponent.Acceleration * dt);
            }

        }
    }
}
