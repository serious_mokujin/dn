﻿using System;
using Blueberry;
using DN.Effects;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;
using System.Linq;

namespace DN.GameObjects.Components.Behaviours
{
    class BatBehaviour : Behaviour
    {
        private Timer _changeDirectionTimer;
        private Timer _retreatTimer;
        private bool SawPlayer = false;
        private Vector2 _direction;
        private bool _run;
        private IGameObject _currentTarged;

        public BatBehaviour(IGameObject owner) 
            : base(owner)
        {
        }

        

        public override void Initialize()
        {
            base.Initialize();

            OwnerPhysicComponent.CollisionWithTiles += Creature_CollisionWithTiles;
            
            _changeDirectionTimer = new Timer
                                       {
                                           Repeat = true,
                                           Duration = RandomTool.NextInt(5) + 5
                                       };

            _changeDirectionTimer.TickEvent += OnTimerTick;
            _changeDirectionTimer.UpdateEvent += OnTimerUpdate;
            _changeDirectionTimer.Run(true);

            _retreatTimer = new Timer
                                {
                                    Duration = 0.5f,
                                    Repeat = false
                                };
            _retreatTimer.StartEvent += StartRetreatEvent;
            _retreatTimer.UpdateEvent += OnTimerUpdate;
            _retreatTimer.TickEvent += RetreatEnd;


        }

        private void RetreatEnd()
        {
            _run = false;
        }

        public override void ProccesMessage(IMessage message)
        {
            if(_run) return;
            if (!(message is CollisionMessage)) return;

            var cm = (CollisionMessage)message;
            if (cm.CollidedObject == _currentTarged)
                _retreatTimer.Run();
        }

        private void StartRetreatEvent()
        {
            if (_currentTarged == null)
                return;
            var curBoundary = _currentTarged.GetComponent<BoundaryComponent>();
            _direction = -FunctionHelper.DirectionToObject(OwnerBoundaryComponent.Position,
                                  curBoundary.Position);
            _run = true;
        }

        private void Creature_CollisionWithTiles(IGameObject sender, CollidedCell[] collidedCell, Vector2 velocity)
        {
            if (collidedCell.Any(f => f.CellType == CellType.Wall))
                _changeDirectionTimer.Finish();
        }

        public override void Update(float dt)
        {
            base.Update(dt);
            _currentTarged = GetClosestTarget();
            if (_currentTarged == null)
                return;
            var curBoundary = _currentTarged.GetComponent<BoundaryComponent>();
            if (!SawPlayer)
                if (BoundaryComponent.Distance(OwnerBoundaryComponent, curBoundary) < 500)
                    if (FunctionHelper.GetLineOfSight(BehaviourComponent.GameWorld.TileMap,
                                                      OwnerBoundaryComponent.Cell,
                                                      curBoundary.Cell))
                        SawPlayer = true;

            if (_run)
            {
                _retreatTimer.Update(dt);
            }
            else if (SawPlayer)
            {
                _direction = FunctionHelper.DirectionToObject(OwnerBoundaryComponent.Position,
                                                              curBoundary.Position);
                OwnerPhysicComponent.Move(_direction, 10 * dt);
            }
            else
            {
                _changeDirectionTimer.Update(dt);
            }
        }

        private void OnTimerTick()
        {
            _direction = RandomTool.NextUnitVector2();
        }
        private void OnTimerUpdate(float dt)
        {
            OwnerPhysicComponent.Move(_direction, 10 * dt);
        }
    }
}
