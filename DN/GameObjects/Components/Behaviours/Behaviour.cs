﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.Behaviours
{
    public abstract class Behaviour:IBehaviour
    {
        protected IGameObject Owner { get; set; }

        public BehaviourComponent BehaviourComponent { get; set; }
        protected BoundaryComponent OwnerBoundaryComponent{get;private set;}
        protected PhysicComponent OwnerPhysicComponent { get; private set; }
        protected IGameObject[] Targets { get; private set; }

        public Behaviour(IGameObject owner)
        {
            Owner = owner;

        }

        public virtual void Initialize()
        {
            OwnerBoundaryComponent = Owner.GetComponent<BoundaryComponent>();
            OwnerPhysicComponent = Owner.GetComponent<PhysicComponent>();
            Targets = BehaviourComponent.Targets;
        }
        public virtual void Update(float dt)
        {
           // if (_collisionComponent != null)
           //     _collisionComponent.Sleep = (BoundaryComponent.Distance(OwnerBoundaryComponent, Targets) > 300);
        }

        public virtual void ProccesMessage(IMessage message)
        {
        }

        protected IGameObject GetClosestTarget()
        {
            IGameObject b = null;
            float min = float.MaxValue;
            foreach (var target in Targets)
            {
                if(target == null)
                    continue;
                if(target.GetComponent<HealthComponent>().IsDead)
                    continue;
                float v = BoundaryComponent.Distance(target.GetComponent<BoundaryComponent>(), OwnerBoundaryComponent);
                if (v < min)
                {
                    min = v;
                    b = target;
                }
            }
            return b;
        }
    }
}
