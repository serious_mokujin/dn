﻿using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.Behaviours
{
    public interface IBehaviour
    {
        BehaviourComponent BehaviourComponent { get; set; }
        void ProccesMessage(IMessage message);
        void Initialize();
        void Update(float dt);
    }
}
