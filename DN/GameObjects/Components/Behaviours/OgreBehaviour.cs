﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;
using DN.GameObjects.Components;

namespace DN.GameObjects.Components.Behaviours
{
    class OgreBehaviour : Behaviour
    {
        private enum State
        {
            Stay,
            ChasingHero,
            Jumping,
            Earthquake

        }

        private State _state;
        private Timer _timer;
        private Timer _chasingStateTimer;
        private Timer _jumpingStateTimer;
        private Timer _earthQuakeTimer;


        private Timer _jumpingTimer;

        private sbyte _dir = -1;

        public OgreBehaviour(IGameObject gameObject)
            : base(gameObject)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
            _timer = new Timer {Duration = 3};
            _timer.Run();

            _chasingStateTimer = new Timer {Duration = 8, Repeat = true};
            _chasingStateTimer.TickEvent += () => _state = RandomTool.NextBool() ? State.Jumping : State.ChasingHero;
       
            _chasingStateTimer.Run();

            _jumpingStateTimer = new Timer();
            _jumpingStateTimer.TickEvent += () =>
                                                {
                                                    _state = RandomTool.NextBool() ? State.Earthquake : State.Earthquake;

                                                    if (_state == State.Earthquake)
                                                    {
                                                        Owner.SendMessage(
                                                            new EarthquakeMessage(EarthquakeMessageType.Begin));
                                                        _earthQuakeTimer.Run();
                                                    }
                                                };
          
            _jumpingStateTimer.Duration = 6;
            _jumpingStateTimer.Repeat = true;
            _jumpingStateTimer.Run();

            _jumpingTimer = new Timer {Duration = 1, Repeat = true};
            _jumpingTimer.TickEvent += () =>
                                           {
                                               Owner.SendMessage(new EventMessage(Events.StartJump));
                                               _dir = RandomTool.NextSign();
                                           };

                                                 
            _jumpingTimer.Run();

            _earthQuakeTimer = new Timer {Duration = 4};
            _earthQuakeTimer.TickEvent += () =>
                                              {
                                                  Owner.SendMessage(new EarthquakeMessage(EarthquakeMessageType.End));
                                                  _state = State.ChasingHero;
                                              };

            //_state = State.ChasingHero;
        }

        public override void Update(float dt)
        {
            IGameObject currentTarged = GetClosestTarget();
            if (currentTarged == null)
                return;
            BoundaryComponent curBoundary = currentTarged.GetComponent<BoundaryComponent>();

            switch (_state)
            {
                case State.Stay:
                    if (FunctionHelper.GetLineOfSight(BehaviourComponent.GameWorld.TileMap,
                                                      OwnerBoundaryComponent.Cell,
                                                      curBoundary.Cell))
                        if(BoundaryComponent.Distance(OwnerBoundaryComponent,curBoundary) < 500)
                        _state = State.ChasingHero;
                    break;
                case State.ChasingHero:
                    OwnerPhysicComponent.Move(
                        new Vector2(curBoundary.Position.X < OwnerBoundaryComponent.Position.X ? -1 : 1, 0),
                                    OwnerPhysicComponent.Acceleration*dt);
                    if(curBoundary.Position.Y < OwnerBoundaryComponent.Position.Y - OwnerBoundaryComponent.Size.Height)
                        Owner.SendMessage(new EventMessage(Events.StartJump));
                    _chasingStateTimer.Update(dt);
                    break;
                case State.Jumping:
                    OwnerPhysicComponent.Move(new Vector2(_dir, 0), OwnerPhysicComponent.Acceleration * dt);
                    _jumpingStateTimer.Update(dt);
                    _jumpingTimer.Update(dt);
                    break;
                case State.Earthquake:
                    // do something..
                    _earthQuakeTimer.Update(dt);
                    break;
            }
            if (OwnerPhysicComponent.GetVelocity().Y > 0)
            {
                OwnerPhysicComponent.Move(GameWorld.GravityDirection, 1 * dt, true);
            }
        }
    }
}
