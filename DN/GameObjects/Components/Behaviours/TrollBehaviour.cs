﻿using System.Linq;
using Blueberry;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components.Behaviours
{
    public class TrollBehaviour : Behaviour
    {
        private bool _sawPlayer;
        private int _hDirection = 1;

        public TrollBehaviour(IGameObject owner)
                                : base(owner)
        {

        }

        private Timer _moveTimer;
        private Timer _waitTimer;

        

        public override void Initialize()
        {
            base.Initialize();
            _moveTimer = new Timer {Duration = 5};
            _moveTimer.TickEvent += OnMoveTimerTickEvent;
            _moveTimer.UpdateEvent += OnMoveTimerUpdateEvent;
            _moveTimer.Run();
            _waitTimer = new Timer {Duration = 7};
            _waitTimer.TickEvent += OnWaitTimerTickEvent;

            _hDirection = RandomTool.NextSign();
            OwnerPhysicComponent.CollisionWithTiles += Creature_CollisionWithTiles;

        }

        private void Creature_CollisionWithTiles(IGameObject sender, CollidedCell[] collidedtiles, Vector2 velocity)
        {
            if (collidedtiles.Any(p => p.Direction.X != 0))
            {
                if (!_sawPlayer)
                    _hDirection = _hDirection*-1;
                   else
                {
                    Owner.SendMessage(new EventMessage(Events.StartJump));
                }
            }
        }

        private void OnMoveTimerUpdateEvent(float dt)
        {
            OwnerPhysicComponent.Move(new Vector2(_hDirection, 0), OwnerPhysicComponent.Acceleration *dt);
        }

        private void OnWaitTimerTickEvent()
        {
            _moveTimer.Run();
        }

        private void OnMoveTimerTickEvent()
        {
            if (RandomTool.NextBool())
            {
                _moveTimer.Run();
                _moveTimer.Duration = RandomTool.NextInt(1, 4);
                _hDirection = RandomTool.NextSign();
            }
            else
            {
                _moveTimer.Duration = RandomTool.NextInt(1, 4);
                _waitTimer.Run();
            }
        }


        public override void Update(float dt)
        {
            base.Update(dt);
            IGameObject currentTarged = GetClosestTarget();
            if(currentTarged == null)
                return;
            BoundaryComponent curBoundary = currentTarged.GetComponent<BoundaryComponent>();

            if (!_sawPlayer)
            {
                if (FunctionHelper.GetLineOfSight(BehaviourComponent.GameWorld.TileMap,
                                                  OwnerBoundaryComponent.Cell,
                                                  curBoundary.Cell))
                {
                    _sawPlayer = true;
                }
                else
                {
                    _waitTimer.Update(dt);
                    _moveTimer.Update(dt);
                }
            }
            else
            {
                Vector2 dir = curBoundary.Position.X > OwnerBoundaryComponent.Position.X
                                  ? new Vector2(1, 0)
                                  : new Vector2(-1, 0);
                  OwnerPhysicComponent.Move(dir, OwnerPhysicComponent.Acceleration * dt);

                  if (OwnerBoundaryComponent.Position.Y > curBoundary.Position.Y ||
                    OwnerPhysicComponent.OnLeftEdge || OwnerPhysicComponent.OnRightEdge)
                    Owner.SendMessage(new EventMessage(Events.StartJump));
            }
        }
    }
}
