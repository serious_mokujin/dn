﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Components.Behaviours;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components
{
    public class BehaviourComponent:Component, IUpdatable
    {
        public IGameObject[] Targets
        {
            get;
            set;
        }
        public GameWorld GameWorld
        {
            get;
            set;
        }

        private IBehaviour _behaviour;

        public BehaviourComponent(IGameObject gameObject, IGameObject[] targets, 
                                  GameWorld gameWorld, IBehaviour behaviour = null) 
            : base(gameObject)
        {
            Targets = targets;
            GameWorld = gameWorld;
            if(behaviour != null)
                SetBehaviour(behaviour);           
        }

        public void SetBehaviour(IBehaviour behaviour)
        {
            _behaviour = behaviour;
            _behaviour.BehaviourComponent = this;
            _behaviour.Initialize();
        }


        public override void ProccesMessage(IMessage message)
        {
            _behaviour.ProccesMessage(message);
        }

        public void Update(float dt)
        {
            if (_behaviour == null)
                return;
            _behaviour.Update(dt);
        }
    }
}
