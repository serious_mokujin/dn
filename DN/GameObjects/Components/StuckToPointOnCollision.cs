﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components
{
    public class StuckToPointOnCollision:Component
    {
        private PhysicComponent _physic;
        private BoundaryComponent _boundary;
        private BoundaryComponent _collidedBoundary;
        private GameObjectsManager _gameObjectsManager;


        private IGameObject _collidedObject;
        private Vector2 _posOffset;
        private bool _stuckInObject;
        private bool _stuckInWall;

        public StuckToPointOnCollision(IGameObject gameObject, GameObjectsManager gameObjectsManager)
            : base(gameObject)
        {
            _gameObjectsManager = gameObjectsManager;
        }

        public override bool GetDependicies()
        {
            _physic = Owner.GetComponent<PhysicComponent>();
            _boundary = Owner.GetComponent<BoundaryComponent>();
            _physic.CollisionWithTiles += PhysicOnCollisionWithTiles;
            return base.GetDependicies();
        }

        private void PhysicOnCollisionWithTiles(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity)
        {
            if (collidedTiles.Any(p => p.CellType == CellType.Wall))
            {
                _physic.CollisionWithTiles -= PhysicOnCollisionWithTiles;
                _stuckInWall = true;
                Owner.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));
                Owner.SendMessage(new DirectionStateMessage(DirectionState.Disabled));
                Owner.RemoveComponent<CollisionComponent>();
            }
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is CollisionMessage)
            {
                HandleCollisionMessage((message as CollisionMessage));
            }
            if(message is UpdateMessage)
            {
                if (_stuckInObject)
                    _boundary.Position = _collidedBoundary.Position - _posOffset;
            }
        }

        private void HandleCollisionMessage(CollisionMessage message)
        {
            _stuckInObject = true;
            _collidedBoundary = message.CollidedObject.GetComponent<BoundaryComponent>();
            _posOffset = _boundary.Position - _collidedBoundary.Position;
            _collidedObject = message.CollidedObject;

            Owner.RemoveComponent<CollisionComponent>();
            Owner.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));
            Owner.SendMessage(new DirectionStateMessage(DirectionState.Disabled));

            _collidedObject.GetComponent<CollisionComponent>().OnRemoveFromScene += OnRemoveFromScene;
        }

        private void OnRemoveFromScene(object sender)
        {
            _stuckInObject = false;
            _stuckInWall = false;
            _gameObjectsManager.RemoveObject(Owner);//UNDONE: it shouldn't be removed
        }
    }
}
