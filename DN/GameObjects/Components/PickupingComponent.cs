﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Components;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects
{
    public class PickupingComponent: Component
    {
        private BoundaryComponent _boundaryComponent;
        private DirectionComponent _directionComponent;

        private Vector2 PickupedItemPosition
        {
            get
            {
                if (_directionComponent != null)
                {
                    var angle = _directionComponent.Angle;

                    List<IPositionModifier> positionModifiers = PickedupObject.GetComponents<IPositionModifier>();
                    Vector2 _pmOffset = Vector2.Zero;
                    foreach (var pm in positionModifiers)
                    {
                        _pmOffset += pm.Offset;
                    }

                    return new Vector2(_boundaryComponent.Position.X + (_boundaryComponent.Size.Width + _pmOffset.X) * (float) Math.Cos(angle),
                                       _boundaryComponent.Position.Y + (_boundaryComponent.Size.Height + _pmOffset.Y) * (float)Math.Sin(angle));
                }
                return _boundaryComponent.Position;
            }
        }

        private List<IGameObject> _collisions;

        private PickupableComponent _pickupableComponent;
        public IGameObject PickedupObject { get;private set; }
    
        bool _pickingUp = false;

        public PickupingComponent(IGameObject gameObject)
            :base(gameObject)
        {
            _collisions = new List<IGameObject>();
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            return base.GetDependicies();
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                Update((UpdateMessage)message);
            }
            else if(message is CollisionMessage)
            {
                OnCollision((CollisionMessage) message);
            }
            else if(message is ActionMessage)
            {
                if(PickedupObject != null)
                {
                    PickedupObject.SendMessage(message);
                }
            }
            else if(message is DieMessage)
            {
                Drop();
            }
        }

        private void Update(UpdateMessage message)
        {
            if(_pickingUp && _collisions.Count != 0)
                foreach (var gameObject in _collisions)
                {
                    if(gameObject == Owner)
                        continue;

                    PickupableComponent pickupableComponent = gameObject.GetComponent<PickupableComponent>();
                    if (pickupableComponent != null)
                    {
                        if(pickupableComponent.Pickuper != null)
                            continue;

                        Drop();
                        PickedupObject = gameObject;
                        PickedupObject.SendMessage(new PhysicChangeStateMessage(PhysicState.Deactivated));
                        PickedupObject.SendMessage(new ObjectsCollisionsStateMessage(false, new[] { Owner.ObjectGroup }));
                        pickupableComponent.SetPickuper(Owner);


                        _pickupableComponent = pickupableComponent;
                        break;
                    }
                }
            EndPickuping();

            if(PickedupObject != null)
            {
                BoundaryComponent b;
                if((b = PickedupObject.GetComponent<BoundaryComponent>()) != null)
                {
                    b.Position = PickupedItemPosition;
                }
                DirectionComponent d;
                if ((d = PickedupObject.GetComponent<DirectionComponent>()) != null)
                {
                    DirectionComponent od;
                    if((od = Owner.GetComponent<DirectionComponent>()) != null)
                        d.Direction = od.Direction;
                }
            }
        }
        private void OnCollision(CollisionMessage message)
        {
            _collisions.Add(message.CollidedObject);
        }

        public void Use()
        {
            if(PickedupObject != null)
            {
                PickedupObject.SendMessage(new ActionMessage(ActionMessageType.StartRequest));
            }
        }

        public void StartPickuping()
        {
            _pickingUp = true;
        }


        public void Drop()
        {
            if (_pickupableComponent != null)
            {
                _pickupableComponent.SetPickuper(null);

                PickedupObject.SendMessage(new ActionMessage(ActionMessageType.InterupRequest));
                PickedupObject.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                PickedupObject.SendMessage(new ObjectsCollisionsStateMessage(false));
                PickedupObject.GetComponent<BoundaryComponent>().Position = _boundaryComponent.Position;
            }

            PickedupObject = null;
            _pickupableComponent = null;
        }

        public void EndPickuping()
        {
            _collisions.Clear();
            _pickingUp = false;
        }
    }
}
