﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;
using OpenTK;
using DN.Helpers;

namespace DN.GameObjects
{
    public enum MoveMethod
    {
        Smooth,
        Immediate
    }

    public delegate void CollisionsWithTilesEventHandler(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity);

    public class PhysicComponent : Component, IQuadTreeItem
    {

        public event CollisionsWithTilesEventHandler CollisionWithTiles;

        public MoveMethod MoveMethod;


        public List<CollidedCell> Collisions;

        public bool IgnoreCollisions = false;
        public bool Freeze;

        public Vector2 MaxVelocity;

        public float Friction;
        public float Acceleration;

        public bool GravityAffected = true;

        public bool Sleepy = false;
        private Timer _sleepTimer; 

        private TileMap _tileMap;

        private Vector2 _velocity;

        private BoundaryComponent _boundaryComponent;
        
        public Vector2 GetVelocity()
        {
            return _velocity;
        }

        public float FallingDistance { get; private set; }

        public bool OnGround
        {
            get
            {
                return Collisions.Any(p => p.CellType == CellType.Wall
                                          && p.Direction.Y == GameWorld.GravityDirection.Y);
            }
        }

        public bool OnLadder
        {
            get { return Collisions.Any(p => p.CellType == CellType.Ladder); }
        }

        public bool OnLeftEdge
        {
            get
            {
                List<CollidedCell> cells = Collisions.FindAll(a => (a.CellType == CellType.Wall
                                                                    && a.Direction.Y == GameWorld.GravityDirection.Y));

                if (cells.Count == 1)
                {
                    Point cell = cells[0].Rectangle.Location;
                    cell.X /= 64;
                    cell.Y /= 64;

                    if (_tileMap[cell.X + 1, cell.Y] != CellType.Wall)
                        if (cells[0].Rectangle.Right - _boundaryComponent.Position.X < 0)
                            return true;
                }
                return false;
            }
        }

        public bool OnRightEdge
        {
            get
            {
                List<CollidedCell> cells = Collisions.FindAll(a => (a.CellType == CellType.Wall
                                                                    && a.Direction.Y == GameWorld.GravityDirection.Y));

                if (cells.Count == 1)
                {
                    Point cell = cells[0].Rectangle.Location;
                    cell.X /= 64;
                    cell.Y /= 64;

                    if (_tileMap[cell.X - 1, cell.Y] != CellType.Wall)
                        if (cells[0].Rectangle.Left - _boundaryComponent.Position.X > 0)
                            return true;
                }
                return false;
            }
        }
        
        public PhysicComponent(IGameObject gameObject, TileMap tileMap, bool sleepy = false) : base(gameObject)
        {
            Collisions = new List<CollidedCell>(10);
            _sleepTimer = new Timer();
            _sleepTimer.Duration = 1;
            _sleepTimer.TickEvent += (() => { Freeze = true; });
            Sleepy = sleepy;
            if (Sleepy)
                _sleepTimer.Run();
            _tileMap = tileMap;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return _boundaryComponent != null;
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                Update((UpdateMessage)message);
            }
            else if (message is MoveMessage)
            {
                if(Freeze)
                    return;

                MoveMessage m = (MoveMessage) message;

                if(!m.Grounded || OnGround)
                    Move(m.Direction, m.Speed);
            }
            else if (message is PhysicChangeStateMessage)
            {
                Freeze = (message as PhysicChangeStateMessage).State == PhysicState.Deactivated;
            }
        }

        private void Update(UpdateMessage message)
        {
            float dt = message.DT;
            if (dt > 0.1f)
                return;


            if (Freeze) return;

            Vector2 oldvel = _velocity;

            if (GravityAffected)
            {
                if (_velocity.Y < MaxVelocity.Y)
                    Move(GameWorld.GravityDirection, GameWorld.G*dt, false);
            }
            if (OnGround && MoveMethod == MoveMethod.Smooth)
                if (!_movedX)
                    UpdateFriction(ref _velocity.X, Friction, dt);


            Vector2 pos = _boundaryComponent.Position;
            Vector2 vel = _velocity;


            // if (MoveMethod == MoveMethod.Smooth)
            vel *= 50*dt;

            CheckCollisions(ref vel, ref pos);

            _boundaryComponent.Position = pos;

            _velocity = new Vector2(vel.X == 0 ? 0 : _velocity.X,
                                    vel.Y == 0 ? 0 : _velocity.Y);
            if (_velocity.Y <= 0)
            {
                FallingDistance = 0;
            }
            else FallingDistance += vel.Y;

            _boundaryComponent.Position += vel;




            if (MoveMethod == MoveMethod.Immediate)
                _velocity = Vector2.Zero;

            if (MoveMethod == GameObjects.MoveMethod.Smooth && _velocity.Y >= 1f && !OnGround)
            {
                if (!_falling)
                {
                    Owner.SendMessage(new EventMessage(Events.Fall));
                }
                _falling = true;
            }
            else
                _falling = false;
            

            _movedX = false;
            if (Sleepy)
                if (_velocity != Vector2.Zero)
                         _sleepTimer.Restart();
            _sleepTimer.Update(dt);
                
        }
        private bool _falling;


        private void CheckCollisions(ref Vector2 vel, ref Vector2 pos)
        {
            CheckCollisionsWithTiles(ref vel, ref pos);
        }

        private void CheckCollisionsWithTiles(ref Vector2 offset, ref Vector2 position)
        {
            //   if (IgnoreWalls)
            //       return;

            Collisions.Clear();
            List<CollidedCell> tilesX = null;
            List<CollidedCell> tilesY = null;
            List<CollidedCell> tiles = null;

            Vector2 oldVel = _velocity;

            // if (offset.X != 0)
            {
                float oldOffset = offset.X;
                if (offset.X < 1 && offset.X > 0)
                    offset.X = 1;
                if (offset.X > -1 && offset.X < 0)
                    offset.X = -1;

                tilesX = _tileMap.GetCollisionsWithTiles(new RectangleF((_boundaryComponent.Left + offset.X),
                                                                     _boundaryComponent.Top,
                                                                     _boundaryComponent.Size.Width,
                                                                     _boundaryComponent.Size.Height));

                foreach (var cell in tilesX)
                    if (cell.CellType == CellType.Wall)
                    {
                        if (offset.X > 0)
                        {
                            position.X = cell.Rectangle.X - _boundaryComponent.Size.Width / 2;
                            cell.Direction = new Point(1, 0);
                        }
                        else if (offset.X < 0)
                        {
                            position.X = cell.Rectangle.X + cell.Rectangle.Width + _boundaryComponent.Size.Width / 2;
                            cell.Direction = new Point(-1, 0);
                        }
                        offset.X = 0;
                        break;
                    }
                if (offset.X != 0)
                    offset.X = oldOffset;
            }

            //if (offset.Y != 0)
            {
                float oldOffset = offset.Y;
                if (offset.Y < 1 && offset.Y > 0)
                    offset.Y = 1;
                if (offset.Y > -1 && offset.Y < 0)
                    offset.Y = -1;

                tilesY = _tileMap.GetCollisionsWithTiles(new RectangleF((_boundaryComponent.Left),
                                                                     _boundaryComponent.Top + offset.Y,
                                                                     _boundaryComponent.Size.Width,
                                                                     _boundaryComponent.Size.Height));
                foreach (var cell in tilesY)
                    if (cell.CellType == CellType.Wall)
                    {
                        if (offset.Y > 0)
                        {
                            position.Y = cell.Rectangle.Y - _boundaryComponent.Size.Height / 2;
                            cell.Direction = new Point(0, 1);
                        }
                        else if (offset.Y < 0)
                        {
                            position.Y = cell.Rectangle.Y + cell.Rectangle.Height + _boundaryComponent.Size.Height / 2;
                            cell.Direction = new Point(0, -1);
                        }
                        offset.Y = 0;
                        break;
                    }
                if (offset.Y != 0)
                    offset.Y = oldOffset;
            }


            if (offset.X != 0 && offset.Y != 0)
            {
                tiles = _tileMap.GetCollisionsWithTiles(new RectangleF((_boundaryComponent.Left + offset.X),
                                                                    _boundaryComponent.Top + offset.Y,
                                                                    _boundaryComponent.Size.Width,
                                                                    _boundaryComponent.Size.Height));
                foreach (var cell in tiles)
                    if (cell.CellType == CellType.Wall)
                    {
                        offset.X = 0;
                        offset.Y = 0;
                    }
            }

            if (tilesX != null)
                Collisions.AddRange(tilesX);
            if (tilesY != null)
                Collisions.AddRange(tilesY);
            if (tiles != null)
                Collisions.AddRange(tiles);


            //remove dublicating cells
            Queue<CollidedCell> toDelete = new Queue<CollidedCell>();
            for (int i = 0; i < Collisions.Count / 2; i++)
                for (int j = Collisions.Count - 1; j >= Collisions.Count / 2; j--)
                    if (i != j)
                    {
                        Rectangle rect1 = Collisions[i].Rectangle;
                        Rectangle rect2 = Collisions[j].Rectangle;

                        if (rect1.X == rect2.X && rect1.Y == rect2.Y)
                            toDelete.Enqueue(Collisions[j]);
                    }


            while (toDelete.Count > 0)
                Collisions.Remove(toDelete.Dequeue());



            if (CollisionWithTiles != null)
                CollisionWithTiles(this.Owner, Collisions.ToArray(), oldVel);
        }

        private void UpdateFriction(ref float vel, float friction, float dt)
        {
            if (vel > 0)
            {
                vel -= friction * dt;
                if (vel < 0)
                    vel = 0;
            }
            else if (vel < 0)
            {
                vel += friction * dt;
                if (vel > 0)
                    vel = 0;
            }
        }

        private void CheckOverSpeed()
        {
            Vector2 vel = _velocity;
            if (MaxVelocity.X != 0)
                CheckOverSpeed(ref vel.X, MaxVelocity.X);
            if (MaxVelocity.Y != 0)
                CheckOverSpeed(ref vel.Y, MaxVelocity.Y);
            _velocity = vel;
        }

        private void CheckOverSpeed(ref float velocity, float maxVelocity)
        {
            if (velocity > 0)
            {
                if (velocity > maxVelocity)
                    velocity = maxVelocity;
            }
            else if (velocity < 0)
            {
                if (velocity < -maxVelocity)
                    velocity = -maxVelocity;
            }
        }


        public void SetMove(Vector2 velocity, bool checkOverspeed = true)
        {
            _velocity = velocity;
            if (checkOverspeed)
                CheckOverSpeed();
            _movedX = true;
        }

        public void SetMove(Vector2 direction, float speed, bool checkOverspeed = true)
        {
            _velocity = direction * speed;
            if (checkOverspeed)
                CheckOverSpeed();
        }

        public void SetMoveY(float speed, bool checkOverspeed = true)
        {
            _velocity.Y = speed;
            if (checkOverspeed)
                CheckOverSpeed();
        }

        public void SetMoveX(float speed, bool checkOverspeed = true)
        {
            _velocity.X = speed;
            if (checkOverspeed)
                CheckOverSpeed();
            if (speed != 0)
                _movedX = true;
        }

        public void Move(Vector2 direction, float speed, bool checkOverspeed = true)
        {
            _velocity += direction * speed;

            if (checkOverspeed)
            {
                CheckOverSpeed();
            }
            if(speed != 0 && direction.X != 0)
                _movedX = true;
        }


        public event PositionChangeHandler OnPositionChange;
        public event RemoveFromSceneHandler OnRemoveFromScene;

        private bool _movedX;

        public Rectangle Bounds
        {
            get { return new Rectangle((int)_boundaryComponent.Bounds.X, (int)_boundaryComponent.Bounds.Y, (int)_boundaryComponent.Bounds.Width, (int)_boundaryComponent.Bounds.Height); }
        }
    }
}