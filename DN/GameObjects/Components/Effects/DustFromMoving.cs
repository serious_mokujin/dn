﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Blueberry;
using Blueberry.Particles;
using Blueberry.Particles.Shapes;
using DN.Effects;
using DN.Effects.ParticleEffects;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.Effects
{
    class DustFromMoving:Component, IDrawable, IUpdatable
    {
        ParticleEmitter emitter;
        DustParticleStateManager dustParticleManager;
        
        private GameWorld _world;
        private BoundaryComponent _boundaryComponent;
        private PhysicComponent _physicsComponent;

        public DustFromMoving(IGameObject gameObject, GameWorld world)
            : base(gameObject)
        {
            _world = world;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _physicsComponent = Owner.GetComponent<PhysicComponent>();
            if (_boundaryComponent != null)
            {
            	dustParticleManager = new DustParticleStateManager(60);
            	emitter	= new ParticleEmitter(60, 1, new PointShape(), dustParticleManager);
            	emitter.ReleaseQuantity = 1;
            }
            return _boundaryComponent != null && _physicsComponent != null;
        }


        public override void ProccesMessage(IMessage message)
        {
        }
        float triggerTimer = 0;

        public void Draw(float dt)
        {
            emitter.Render();
        }

        public void Update(float dt)
        {
            Vector2 vel = _physicsComponent.GetVelocity();
            float len = vel.LengthFast;
            dustParticleManager.SpreadingDirection = -vel;
            if (len > 0.5f && _physicsComponent.OnGround)
            {
                triggerTimer += dt;
                float period = (1 / (len * 5)) / 2;
                if (triggerTimer >= period)
                {
                    emitter.Trigger((float)Game.g_TotalTime, new Vector2(_boundaryComponent.Position.X, _boundaryComponent.Bottom));
                    triggerTimer -= period;
                }
            }
            emitter.Update((float)Game.g_TotalTime, dt);
        }
    }
}
