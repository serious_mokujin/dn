﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Blueberry;
using Blueberry.Animations;
using Blueberry.Audio;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components
{
    public class EarthquakeComponent:Component, IDisposable, IUpdatable
    {
        public float MinPower = 125;
        public float MaxPower = 165;

        public float Duration
        {
            get { return _durationTimer.Duration; }
            set { _durationTimer.Duration = value; }
        }

        private readonly GameObjectsManager _gameObjectsManager;
        private readonly Camera _camera;
        private bool _working;

        private readonly Timer _jerkTimer;
        private readonly Timer _durationTimer;

        private Vector2 _direction;
        private float _speed;

        private AudioClip sound;
        private AudioRemoteControll _soundRemote;
        private FloatAnimation volumeAnim;

        public EarthquakeComponent(IGameObject gameObject, GameObjectsManager gameObjectsManager, Camera camera)
            : base(gameObject)
        {
            _gameObjectsManager = gameObjectsManager;
            _camera = camera;
            _jerkTimer = new Timer{Repeat = true, Duration = 0.1f};
            _jerkTimer.UpdateEvent += MoveOnUpdateEvent;
            _jerkTimer.TickEvent += ChangeDirection;

            _durationTimer = new Timer();
            _durationTimer.TickEvent += () => Owner.SendMessage(new EarthquakeMessage(EarthquakeMessageType.End));

            sound = CM.I.Sound("earthquake");
            volumeAnim = new FloatAnimation(1, 0, 2, LoopMode.None);
            volumeAnim.OnFinish += obj => _soundRemote.Break();
            volumeAnim.OnAnimate += (arg1, arg2) => { _soundRemote.Volume = arg2; };
            Owner.SendMessage(new EarthquakeMessage(EarthquakeMessageType.Begin));
        }

        private void ChangeDirection()
        {
            _direction = RandomTool.NextUnitVector2();
            _speed = RandomTool.NextSingle(MinPower, MaxPower);
        }

        private void MoveOnUpdateEvent(float dt)
        {
            _gameObjectsManager.SendGlobalMessage(new MoveMessage(_direction, _speed * dt, false, true));
            _gameObjectsManager.SendGlobalMessage(new EventMessage(Events.StopClimb));
            _camera.RumblePosition(0.1f, MinPower * 2 * dt, 2);

        }

        
        public override void ProccesMessage(IMessage message)
        {
            if (message is EarthquakeMessage)
            {
                HandleEarthquakeMessage((EarthquakeMessage) message);
            }
        }

        private void HandleEarthquakeMessage(EarthquakeMessage message)
        {
            if (message.Type == EarthquakeMessageType.Begin)
            {
                if (!_working)
                {
                    if(_soundRemote != null && _soundRemote.Connected)
                        _soundRemote.Break();
                    _soundRemote = sound.PlayDynamic();
                    _soundRemote.IsLooped = true;

                    _jerkTimer.Run(true);
                    if (Duration != 0)
                        _durationTimer.Run();
                    _working = true;

                }
            }
            else
            {
                _jerkTimer.Stop();

                volumeAnim.Play(true);
                if(Duration != 0)
                    _durationTimer.Stop();
                _working = false;
            }
        }

        public void Dispose()
        {
            if(_soundRemote != null && _soundRemote.Connected)
                _soundRemote.Break();
        }

        public void Update(float dt)
        {
            _jerkTimer.Update(dt);
            if (Duration != 0)
                _durationTimer.Update(dt);
        }
    }
}
