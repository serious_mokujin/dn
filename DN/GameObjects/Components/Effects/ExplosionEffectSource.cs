﻿
using System;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components.Effects
{
	/// <summary>
	/// Description of ExplosionSource.
	/// </summary>
	public class ExplosionEffectSource : Component
	{
		GameObjectsFabric fabric;
		GameObjectsManager manager;
		BoundaryComponent bounds;
		
		public ExplosionEffectSource(IGameObject gameObject, GameObjectsFabric fabric, GameObjectsManager manager) : base(gameObject)
		{
			this.fabric = fabric;
			this.manager = manager;
		}
		public override bool GetDependicies()
		{
			bounds = Owner.GetComponent<BoundaryComponent>();
			return base.GetDependicies() && bounds != null;
		}
		public override void ProccesMessage(DN.GameObjects.Messages.IMessage message)
		{
			if(message is ActionMessage)
				HandleAction(message as ActionMessage);
		}
		void HandleAction(ActionMessage message)
		{
			if(message.Type == ActionMessageType.Start)
			{
				IGameObject gameObject = fabric.CreateExplosionEffect(bounds.Position, bounds.Size);
				manager.AddObject(gameObject);
			}
		}
		
	}
}
