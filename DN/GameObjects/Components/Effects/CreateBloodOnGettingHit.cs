﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Blueberry.Particles;
using Blueberry.Particles.Shapes;
using DN.Effects;
using DN.Effects.ParticleEffects;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.Effects
{
    class CreateBloodOnGettingHit:Component, IDrawable, IUpdatable
    {
    	ParticleEmitter emitter;
    	BloodParticleStateManager bloodParticleManager;
    	
        private GameWorld _world;
        private BoundaryComponent _boundaryComponent;


        public CreateBloodOnGettingHit(IGameObject gameObject, GameWorld world) : base(gameObject)
        {
            _world = world;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            if(_boundaryComponent != null)
            {
            	bloodParticleManager = new BloodParticleStateManager(60);
            	emitter	= new ParticleEmitter(60, 1, new PointShape(), bloodParticleManager);
            	emitter.ReleaseQuantity = 3;
            	bloodParticleManager.OnLeavingSpot += _world.BloodSystem.AddSpot;
            }
            return _boundaryComponent != null;
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is DamageMessage)
            {
                HandleDamageMessage((DamageMessage)message);
            }
        }
        float spreadingTime;
        float spreadingInterval;
        float timer;
        
        private void HandleDamageMessage(DamageMessage message)
        {
            if(message.DamageWay == DamageWay.Given)
                return;
            
            spreadingTime += message.Amount / (spreadingTime + 2);
            spreadingInterval = 0.5f/message.Amount;
            bloodParticleManager.SpreadingDirection = message.Direction*message.Amount / 2;
        }

        public void Draw(float dt)
        {
            emitter.Render();
        }

        public void Update(float dt)
        {
            if (emitter != null)
            {
                if (spreadingTime > 0)
                {
                    timer += dt;
                    if (timer > spreadingInterval)
                    {
                        emitter.Trigger((float)Game.g_TotalTime, _boundaryComponent.Position);//wtf is total
                        timer = 0;
                    }
                    spreadingTime -= dt;
                }
                emitter.Update((float)Game.g_TotalTime, dt);
            }
        }
    }
}
