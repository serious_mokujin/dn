﻿
using System;
using Blueberry.Particles;
using Blueberry.Particles.Shapes;
using DN.Effects.ParticleEffects;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.Effects
{
	/// <summary>
	/// Description of ExplosionParticleEffect.
	/// </summary>
	public class ExplosionParticleEffect : Component,IUpdatable,IDrawable
	{
		ParticleEmitter emitter;
        ExplosionParticleStateManager explosionParticleManager;
        
        private BoundaryComponent _boundaryComponent;

        float _term;
        
        public ExplosionParticleEffect(IGameObject gameObject, float time)
            : base(gameObject)
        {
        	_term = time;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            if (_boundaryComponent != null)
            {
            	explosionParticleManager = new ExplosionParticleStateManager(200);
            	emitter	= new ParticleEmitter(200, _term, new PointShape(), explosionParticleManager);
            	emitter.ReleaseQuantity = 15;
            }
            return _boundaryComponent != null;
        }


        public override void ProccesMessage(IMessage message)
        {
        }
        float triggerTimeout;

	    public void Update(float dt)
	    {
            triggerTimeout -= dt;
            if (triggerTimeout <= 0)
            {
                emitter.Trigger((float)Game.g_TotalTime, _boundaryComponent.Position);
                triggerTimeout = 0.005f;
            }
            emitter.Update((float)Game.g_TotalTime, dt);
	    }

	    public void Draw(float dt)
	    {
           	emitter.Render();
	    }
	}
}
