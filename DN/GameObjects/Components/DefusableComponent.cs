﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components
{
    class DefusableComponent:Component
    {
        private PhysicComponent _physicComponent;

        public DefusableComponent(IGameObject gameObject) : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            return _physicComponent != null;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is CollisionMessage)
            {
                HandleCollisionMessage((CollisionMessage)message);
            }
        }

        private void HandleCollisionMessage(CollisionMessage message)
        {
            int dir = message.CollidedObject.GetComponent<BoundaryComponent>().Position.X <
                Owner.GetComponent<BoundaryComponent>().Position.X? 1:-1;

            _physicComponent.Move(new Vector2(dir, 0), 1,true);
        }
    }
}
