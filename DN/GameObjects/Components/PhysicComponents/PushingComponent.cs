﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects.Components.PhysicComponents
{
    class PushingComponent:Component
    {
        private readonly float _speed;
        private readonly ObjectGroup[] _ignoreObjects;
        private BoundaryComponent _boundaryComponent;

        public PushingComponent(IGameObject gameObject, float speed, params ObjectGroup[] ignoreObjectGroup) : base(gameObject)
        {
            _speed = speed;
            _ignoreObjects = ignoreObjectGroup;
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return base.GetDependicies();
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is CollisionMessage)
            {
                var msg = (CollisionMessage) message;

                if(_ignoreObjects != null)
                for (int i = 0; i < _ignoreObjects.Count(); i++)
                    if(_ignoreObjects[i] == msg.CollidedObject.ObjectGroup)
                        return;

                var collidedObjectBoundary = msg.CollidedObject.GetComponent<BoundaryComponent>();


              //  var dir = new Vector2(collidedObjectBoundary.Position.X < _boundaryComponent.Position.X ? -1 : 1,
                //                      0);
               var dir = FunctionHelper.DirectionToObject(_boundaryComponent.Position, collidedObjectBoundary.Position);
                dir *= 1;
                //dir.Normalize();
                msg.CollidedObject.SendMessage(new MoveMessage(dir, _speed, false));
            }
        }
    }
}
