﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects
{
    class LadderClimbingComponent:Component, IUpdatable
    {
        public bool ClimbLadder
        {
            get { return _physicComponent.MoveMethod == MoveMethod.Immediate; }
        }

        private PhysicComponent _physicComponent;



        public LadderClimbingComponent(IGameObject gameObject) : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            return (_physicComponent != null) && base.GetDependicies();
        }


        public override void ProccesMessage(IMessage message)
        {
            if(message is EventMessage)
            {
                HandleEventMessage((message as EventMessage));
            }
            else if(message is MoveMessage)
            {
                HandleMoveMessage((message as MoveMessage));
            }
        }

        public void Update(float dt)
        {
            if (!_physicComponent.OnLadder || _physicComponent.OnGround)
                if (ClimbLadder)
                    StopClimbLadder();
        }

        private void HandleMoveMessage(MoveMessage moveMessage)
        {
            if(ClimbLadder)
                _physicComponent.Move(moveMessage.Direction, moveMessage.Speed, moveMessage.CheckOverspeed);
        }

        private void HandleEventMessage(EventMessage eventMessage)
        {
            switch (eventMessage.Event)
            {
                case Events.Jump:
                    StopClimbLadder();
                    break;
                case Events.StartClimb:
                    TryClimbLadder();
                    break;
                case Events.StopClimb:
                    StopClimbLadder();
                    break;
            }
        }

        public void MoveLadder(Vector2 direction, float speed)
        {
            if (ClimbLadder)
            {
                _physicComponent.Move(direction, speed);
            }
        }

        public void TryClimbLadder()
        {
            if (_physicComponent.OnLadder 
                && (_physicComponent.Collisions.Count(p => p.CellType == CellType.Ladder) == 2 || !_physicComponent.OnGround) 
                && !ClimbLadder)
            {
                Owner.SendMessage(new EventMessage(Events.Climb));
                _physicComponent.GravityAffected = false;
                _physicComponent.MoveMethod = MoveMethod.Immediate;
                
            }
        }
        public void StopClimbLadder()
        {
            if(!ClimbLadder)
                return;
            _physicComponent.MoveMethod = MoveMethod.Smooth;
            _physicComponent.GravityAffected = true;
           // _physicComponent.SetMove(new Vector2(0, 0));
        }


    }
}
