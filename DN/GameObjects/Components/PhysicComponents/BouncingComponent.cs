﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects.Components.PhysicComponents
{
    public class BouncingComponent:Component
    {
        public float Bounciness = 2;//i'm not sure it is right to write like this

        private PhysicComponent _physicComponent;

        public BouncingComponent(IGameObject gameObject) : base(gameObject)
        {
        }

        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            _physicComponent.CollisionWithTiles += PhysicComponentOnCollisionWithTiles;
            return _physicComponent != null;
        }

        private void PhysicComponentOnCollisionWithTiles(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity)
        {
            foreach (var collidedTile in collidedTiles)
            {
                if(collidedTile.CellType == CellType.Wall)
                {
                    if (collidedTile.Direction.X != 0 && collidedTile.Direction.Y != 0)
                        continue;
                    if (Math.Abs(velocity.X) <= 0.01f)
                        velocity.X = 0;
                    if (Math.Abs(velocity.Y) <= 0.01f)
                        velocity.Y = 0;
                    if (velocity == Vector2.Zero)
                        return;
                    if (collidedTile.Direction.X != 0)
                        velocity.X *= -1;
                    if (collidedTile.Direction.Y != 0)
                        velocity.Y *= -1;
                    Vector2 direction = velocity;
                    direction.Normalize();


                        
                    float speed = velocity.Length / Bounciness;
                   // Console.WriteLine("Speed {0}", speed*direction);
                    Owner.SendMessage(new MoveMessage(direction, speed, false, false, MoveType.Set));
                    break;
                }
            }
        }

        public override void ProccesMessage(IMessage message)
        {
            return;
        }
    }
}
