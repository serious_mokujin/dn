﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Messages;
using OpenTK;
using DN.Helpers;

namespace DN.GameObjects
{
    public enum MoveMethod
    {
        Smooth,
        Immediate
    }

    public delegate void CollisionsWithTilesEventHandler(IGameObject sender, CollidedCell[] collidedTiles, Vector2 velocity);

    public class PhysicComponent : Component, IUpdatable
    {

        public event CollisionsWithTilesEventHandler CollisionWithTiles;

        public MoveMethod MoveMethod;


        public List<CollidedCell> Collisions;

        public bool IgnoreCollisions = false;
        public bool Freeze;

        public Vector2 MaxVelocity;

        public float Friction;
        public float Acceleration;

        public bool GravityAffected = true;
        public bool FrictionOnMove = false;


        public bool Sleepy = false;
        private Timer _sleepTimer; 

        private TileMap _tileMap;
        private readonly SolidObjectManager _solidObjectManager;

        private Vector2 _velocity;

        private BoundaryComponent _boundaryComponent;
        
        public Vector2 GetVelocity()
        {
            return _velocity;
        }

        public float FallingDistance { get; private set; }

        public bool OnGround { get; set; }

        public bool OnCeiling { get; set; }

        public bool OnLadder { get; set; }
        

        public bool OnLeftEdge
        {
            get
            {
                List<CollidedCell> cells = Collisions.FindAll(a => (a.CellType == CellType.Wall
                                                                    && a.Direction.Y == GameWorld.GravityDirection.Y));

                if (cells.Count == 1)
                {
                    Point cell = cells[0].Rectangle.Location;
                    cell.X /= 64;
                    cell.Y /= 64;
                    if (_tileMap.InRange(cell.X + 1, cell.Y))
                    if (_tileMap[cell.X + 1, cell.Y] != CellType.Wall)
                        if (cells[0].Rectangle.Right - _boundaryComponent.Position.X < 0)
                            return true;
                }
                return false;
            }
        }

        public bool OnRightEdge
        {
            get
            {
                List<CollidedCell> cells = Collisions.FindAll(a => (a.CellType == CellType.Wall
                                                                    && a.Direction.Y == GameWorld.GravityDirection.Y));

                if (cells.Count == 1)
                {
                    Point cell = cells[0].Rectangle.Location;
                    cell.X /= 64;
                    cell.Y /= 64;

                    if(_tileMap.InRange(cell.X - 1, cell.Y))
                    if (_tileMap[cell.X - 1, cell.Y] != CellType.Wall)
                        if (cells[0].Rectangle.Left - _boundaryComponent.Position.X > 0)
                            return true;
                }
                return false;
            }
        }
        
        public PhysicComponent(IGameObject gameObject, TileMap tileMap, SolidObjectManager solidObjectManager, bool sleepy = false) : base(gameObject)
        {
            Collisions = new List<CollidedCell>(10);
            _sleepTimer = new Timer();
            _sleepTimer.Duration = 1;
            _sleepTimer.TickEvent += (() => { Freeze = true; });
            Sleepy = sleepy;
            if (Sleepy)
                _sleepTimer.Run();
            _tileMap = tileMap;
            _solidObjectManager = solidObjectManager;
        }


        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return _boundaryComponent != null;
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is MoveMessage)
            {
                if(Freeze)
                    return;

                MoveMessage m = (MoveMessage) message;
                switch (m.MoveType)
                {
                    case MoveType.Set:
                        if (!m.Grounded || OnGround)
                            SetMove(m.Direction, m.Speed);
                        break;
                    case MoveType.Add:
                        if (!m.Grounded || OnGround)
                            Move(m.Direction, m.Speed);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

            }
            else if (message is PhysicChangeStateMessage)
            {
                Freeze = (message as PhysicChangeStateMessage).State == PhysicState.Deactivated;
            }
        }

        public void Update(float dt)
        {
            if (dt > 0.1f)
                return;


            if (Freeze) return;



            if (GravityAffected)
            {
                if (_velocity.Y < MaxVelocity.Y)
                    Move(GameWorld.GravityDirection, GameWorld.G*dt, false);
            }
            if (OnGround && MoveMethod == MoveMethod.Smooth)
                if (!_movedX || FrictionOnMove)
                {
                    UpdateFriction(ref _velocity.X, Friction, dt);
                }
            Vector2 oldVel = _velocity;


            Vector2 pos = _boundaryComponent.Position;
            Vector2 vel = _velocity;


            // if (MoveMethod == MoveMethod.Smooth)
            vel *= 50*dt;

            CheckCollisions(ref vel, ref pos);

            _boundaryComponent.Position = pos;

            _velocity = new Vector2(vel.X == 0 ? 0 : _velocity.X,
                                    vel.Y == 0 ? 0 : _velocity.Y);



            _boundaryComponent.Position += vel;

            if (MoveMethod == MoveMethod.Immediate)
                _velocity = Vector2.Zero;

            if (MoveMethod == MoveMethod.Smooth && _velocity.Y >= 1f && !OnGround)
            {
                if (!_falling)
                {
                    Owner.SendMessage(new EventMessage(Events.Fall));
                }
                _falling = true;
            }
            else
                _falling = false;

            _movedX = false;

            if (CollisionWithTiles != null)
                CollisionWithTiles(Owner, Collisions.ToArray(), oldVel);
            if (_velocity.Y <= 0)
            {
                FallingDistance = 0;
            }
            else FallingDistance += vel.Y;
                
        }
        private bool _falling;


        private void CheckCollisions(ref Vector2 vel, ref Vector2 pos)
        {
            CheckCollisionsWithTiles(ref vel, ref pos);
        }

        private void CheckCollisionsWithTiles(ref Vector2 offset, ref Vector2 position)
        {
            //   if (IgnoreWalls)
            //       return;

            Collisions.Clear();
            List<CollidedCell> tilesX = null;
            List<CollidedCell> tilesY = null;
            List<CollidedCell> tiles = null;

            var collidedSolidObjects = new List<IGameObject>();

            if (offset.X != 0 || offset.Y != 0)
            {
                OnLadder = false;
                OnGround = false;
                OnCeiling = false;
            }

            if (offset.X != 0)
                offset.X = CheckAndHandleCollisionsInOffset(new Vector2(offset.X, 0), ref position, ref tilesX, collidedSolidObjects).X;
            if(offset.Y != 0)
                offset.Y = CheckAndHandleCollisionsInOffset(new Vector2(0, offset.Y), ref position, ref tilesY, collidedSolidObjects).Y;
            if(offset.X != 0 && offset.Y != 0)
                offset = CheckAndHandleCollisionsInOffset(new Vector2(offset.X, offset.Y), ref position, ref tiles, collidedSolidObjects);
            if (tilesX != null)
                Collisions.AddRange(tilesX);
            if (tilesY != null)
                Collisions.AddRange(tilesY);
            if (tiles != null)
                Collisions.AddRange(tiles);



            //remove dublicating cells
            var toDelete = new Queue<CollidedCell>();
            for (int i = 0; i < Collisions.Count/2; i++)
                for (int j = Collisions.Count - 1; j >= Collisions.Count/2; j--)
                    if (i != j)
                    {
                        Rectangle rect1 = Collisions[i].Rectangle;
                        Rectangle rect2 = Collisions[j].Rectangle;

                        if (rect1.X == rect2.X && rect1.Y == rect2.Y)
                            toDelete.Enqueue(Collisions[j]);
                    }
            for (int i = 0; i < collidedSolidObjects.Count; i++)
            {
                for (int j = i + 1; j < collidedSolidObjects.Count; j++)
                {
                    if (collidedSolidObjects[i] == collidedSolidObjects[j])
                    {
                        collidedSolidObjects.RemoveAt(j);
                        j--;
                    }

                }
            }


            while (toDelete.Count > 0)
                Collisions.Remove(toDelete.Dequeue());

            foreach (var solidObject in collidedSolidObjects)
                solidObject.SendMessage(new CollisionMessage(Owner));// maybe also send to owner
        }

        private Vector2 CheckAndHandleCollisionsInOffset(Vector2 offset, ref Vector2 position, ref List<CollidedCell> tiles,
                                                      List<IGameObject> collidedSolidObjects)
        {
            Vector2 oldOffset = offset;

            Round(ref offset.X);
            Round(ref offset.Y);

            RectangleF rect = new RectangleF((_boundaryComponent.Left + offset.X),
                                              _boundaryComponent.Top + offset.Y,
                                              _boundaryComponent.Size.Width,
                                              _boundaryComponent.Size.Height);

            tiles = _tileMap.GetCollisionsWithTiles(rect);
            var collidedObjects = _solidObjectManager.GetCollisions(rect);
            collidedSolidObjects.AddRange(collidedObjects.Select(p => p.CollidedObject));
            tiles.AddRange(collidedObjects);

            foreach (var cell in tiles)
                if (cell.CellType == CellType.Wall)
                {
                    Vector2 oldPos = position;

                    if (offset.X > 0)
                    {
                        position.X = cell.Rectangle.X - _boundaryComponent.Size.Width/2;
                        cell.Direction = new Point(1, 0);
                        offset.X = 0;
                    }
                    else if (offset.X < 0)
                    {
                        position.X = cell.Rectangle.X + cell.Rectangle.Width + _boundaryComponent.Size.Width/2;
                        cell.Direction = new Point(-1, 0);
                        offset.X = 0;
                    }
                    if (offset.Y > 0)
                    {
                        position.Y = cell.Rectangle.Y - _boundaryComponent.Size.Height/2;
                        cell.Direction = new Point(0, 1);
                        OnGround = true;
                        offset.Y = 0;
                    }
                    else if (offset.Y < 0)
                    {
                        position.Y = cell.Rectangle.Y + cell.Rectangle.Height + _boundaryComponent.Size.Height/2;
                        cell.Direction = new Point(0, -1);
                        OnCeiling = true;
                        offset.Y = 0;
                    }

                    foreach (CollidedCell p in _tileMap.GetCollisionsWithTiles(_boundaryComponent.Bounds))
                    {
                        if (p.CellType == CellType.Wall)
                        {
                            position = oldPos;
                            break;
                        }
                    }
                }
                else if (cell.CellType == CellType.Ladder)
                {
                    OnLadder = true;
                }

            if (offset.X != 0)
                offset.X = oldOffset.X;
            if (offset.Y != 0)
                offset.Y = oldOffset.Y;
            return offset;
        }

        private void Round(ref float offset)
        {
            if (offset < 1 && offset > 0)
                offset = 1;
            if (offset > -1 && offset < 0)
                offset = -1;
        }

        private void UpdateFriction(ref float vel, float friction, float dt)
        {
            if (vel > 0)
            {
                vel -= friction * dt;
                if (vel < 0)
                    vel = 0;
            }
            else if (vel < 0)
            {
                vel += friction * dt;
                if (vel > 0)
                    vel = 0;
            }
        }

        private void CheckOverSpeed()
        {
            Vector2 vel = _velocity;
            if (MaxVelocity.X != 0)
                CheckOverSpeed(ref vel.X, MaxVelocity.X);
            if (MaxVelocity.Y != 0)
                CheckOverSpeed(ref vel.Y, MaxVelocity.Y);
            _velocity = vel;
        }

        private void CheckOverSpeed(ref float velocity, float maxVelocity)
        {
            if (velocity > 0)
            {
                if (velocity > maxVelocity)
                    velocity = maxVelocity;
            }
            else if (velocity < 0)
            {
                if (velocity < -maxVelocity)
                    velocity = -maxVelocity;
            }
        }


        public void SetMove(Vector2 velocity, bool checkOverspeed = true)
        {
            _velocity = velocity;
            if (checkOverspeed)
                CheckOverSpeed();
            _movedX = true;
        }

        public void SetMove(Vector2 direction, float speed, bool checkOverspeed = true)
        {
            _velocity = direction * speed;
            if (checkOverspeed)
                CheckOverSpeed();
        }

        public void SetMoveY(float speed, bool checkOverspeed = true)
        {
            _velocity.Y = speed;
            if (checkOverspeed)
                CheckOverSpeed();
        }

        public void SetMoveX(float speed, bool checkOverspeed = true)
        {
            _velocity.X = speed;
            if (checkOverspeed)
                CheckOverSpeed();
            if (speed != 0)
                _movedX = true;
        }

        public void Move(Vector2 direction, float speed, bool checkOverspeed = true)
        {
            _velocity += direction * speed;

            if (checkOverspeed)
            {
                CheckOverSpeed();
            }
            if(speed != 0 && direction.X != 0)
                _movedX = true;
        }

        private bool _movedX;
    }
}