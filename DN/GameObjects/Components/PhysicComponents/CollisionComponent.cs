﻿using Blueberry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects
{
    public class CollisionComponent : Component, IUpdatable, IQuadTreeItem, IDisposable
    {
        public bool SearchForCollisions { get; private set; }

        public float SleepTime
        {
            get { return _sleepTimer.Duration; }
            set { _sleepTimer.Duration = value; }
        }

        public IGameObject GetOwner//...
        {
            get { return Owner; }
        }

        public bool Sleep { get; set; }



        protected ObjectGroup[] _defaultGroup;
        protected ObjectGroup[] _currentGroup;

        private QuadTree<CollisionComponent> _quadTree;
        BoundaryComponent _boundary;



        private Vector2 _lastPosition;

        private Timer _sleepTimer;

        public Rectangle Bounds
        {
            get 
            {                
                return new Rectangle((int)_boundary.Bounds.X,
                                       (int)_boundary.Bounds.Y,
                                       (int)_boundary.Bounds.Width,
                                       (int)_boundary.Bounds.Height); 
            }
        }

        public event PositionChangeHandler OnPositionChange;
        public event RemoveFromSceneHandler OnRemoveFromScene;

        public CollisionComponent(IGameObject gameObject, ObjectGroup[] defaultGroups, QuadTree<CollisionComponent> quadTree, bool searchForCollision = true)
            :base(gameObject)
        {
            _defaultGroup = defaultGroups;
            _currentGroup = defaultGroups;
            _quadTree = quadTree;
            SearchForCollisions = true;
            _sleepTimer = new Timer {Repeat = true};
            _sleepTimer.TickEvent += TickEvent;
            _sleepTimer.Run();

            SearchForCollisions = searchForCollision;
        }

        private void TickEvent()
        {
            GetCollisions();
        }

        private void GetCollisions()
        {
            var list = _quadTree.Query(Bounds);
            foreach (var collisionComponent in list)
            {
                if (collisionComponent == this)
                    continue;
                if (_currentGroup != null)
                    if (_currentGroup.Any(p => p == collisionComponent.Owner.ObjectGroup || p == ObjectGroup.Undefined))
                        continue;
                Owner.SendMessage(new CollisionMessage(collisionComponent.Owner));
            }
        }

        public override bool GetDependicies()
        {
            _boundary = Owner.GetComponent<BoundaryComponent>();
            _lastPosition = _boundary.Position;

            _quadTree.Insert(this);

            return _boundary != null;
        }


        public override void ProccesMessage(IMessage message)
        {
            if (message is ObjectsCollisionsStateMessage)
            {
                ObjectsCollisionsStateMessage m = (ObjectsCollisionsStateMessage)message;
                SearchForCollisions = m.CollisionsEnabled;

                if (m.ObjectGroup == null || !m.SetNewObjectGroup)
                    _currentGroup = _defaultGroup;
                else
                    _currentGroup = m.ObjectGroup;
            }
        }

        public void Dispose()
        {
            if(OnRemoveFromScene != null)
                OnRemoveFromScene(this);
//            _quadTree.RemoveItem(this);
        }

        public void Update(float dt)
        {
            if (_lastPosition != _boundary.Position)
            {
                _lastPosition = _boundary.Position;
                if (OnPositionChange != null)
                    OnPositionChange(this);
            }

            if (!SearchForCollisions)
                return;
            if (Sleep)
                _sleepTimer.Update(dt);
            else
            {
                GetCollisions();
            }
        }
    }
}
