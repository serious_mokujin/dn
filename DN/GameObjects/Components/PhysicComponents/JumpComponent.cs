﻿using DN.GameObjects.Messages;
using OpenTK;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DN.GameObjects
{
    //todo: fix jumps
    public class JumpComponent:Component, IUpdatable
    {
        public bool Jumping {get { return _jump; }}

        public float JumpAcceleration;
        public float JumpLength;

        private bool _jump;

        PhysicComponent _physicComponent;
        private LadderClimbingComponent _ladderClimbingComponent;

        float _elapsed = 0;

        
        public JumpComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }



        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            _ladderClimbingComponent = Owner.GetComponent<LadderClimbingComponent>();
            return _physicComponent != null;
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is EventMessage)
            {
                Event((EventMessage)message);
            }
        }

        private void Event(EventMessage message)
        {
            switch (message.Event)
            {
                case Events.StartJump:
                    if ((_physicComponent.OnGround ||(_ladderClimbingComponent != null && _ladderClimbingComponent.ClimbLadder))
                        && !_physicComponent.OnCeiling)
                    {
                        _jump = true;
                        _physicComponent.SetMoveY(-1);

                        Owner.SendMessage(new EventMessage(Events.Jump));
                    }
                    break;
                case Events.StopJump:
                    _jump = false;
                    _elapsed = 0;
                    break;
            }
        }

        public void Jump()
        {
            Owner.SendMessage(new EventMessage(Events.StartJump));
        }
        public void StopJump()
        {
            Owner.SendMessage(new EventMessage(Events.StopJump));
        }

        public void Update(float dt)
        {
            if (_jump)
            {
                if (_physicComponent.OnCeiling)
                    StopJump();

                float DT = dt;
                _elapsed += DT;
                if (_elapsed >= JumpLength)
                {
                    DT = Math.Abs(_elapsed - JumpLength - DT);
                    StopJump();
                }
                _physicComponent.Move(-GameWorld.GravityDirection, JumpAcceleration * DT, false);


            }
        }
    }
}
