﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects.Components;
using DN.GameObjects;
using DN.GameObjects.Messages;

namespace DN.GameObjects.Components
{
    public struct Loot
    {
        public int Count;
        public GameObjectType Type;
        public float Probability;

        public Loot(GameObjectType type, float probability, int count = 1)
        {
            Count = count;
            Type = type;
            Probability = probability;
        }
    }


    internal class DropLootOnDie : Component
    {
        private GameObjectsFabric Fabric { get; set; }
        private GameObjectsManager _gameObjectsManager;
        private Loot[] _loot;

        public DropLootOnDie(IGameObject gameObject, GameObjectsManager gameObjectsManager,
                             GameObjectsFabric fabric, Loot[] loot)
            : base(gameObject)
        {
            Fabric = fabric;
            _loot = loot;
            _gameObjectsManager = gameObjectsManager;
            var l = new Loot(GameObjectType.Hero, 213);

        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is DieMessage)
            {
                for (int i = 0; i < _loot.Length; i++)
                {
                    int c = _loot[i].Count;
                    do
                    {
                        if (RandomTool.RandBool(_loot[i].Probability))
                        {
                            IGameObject gameObject = Fabric.CreateObject(_loot[i].Type, Point.Empty);
                            gameObject.SendMessage(new MoveMessage(RandomTool.NextUnitVector2(),
                                                                   RandomTool.RandFloat(6, 12), false));
                            gameObject.GetComponent<BoundaryComponent>().Cell =
                                Owner.GetComponent<BoundaryComponent>().Cell;
                            _gameObjectsManager.AddObject(gameObject);
                            c--;
                        }
                        else c = 0;
                    } while (c > 0);

                }
            }
        }
    }

}
