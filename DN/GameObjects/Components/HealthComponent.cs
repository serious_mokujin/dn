﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using DN.Helpers;
using OpenTK;

namespace DN.GameObjects
{
    public class HealthComponent : Component
    {
        public float MaxKnockBackY = 2;

        private GameObjectsManager _gameObjectsManager;
        private float _maxHealth;

        public float MaxHealth
        {
            get { return _maxHealth; }
            private set { _maxHealth = value; }
        }
        public float InvularabilityDuration 
        {
            get { return _invTimer.Duration; }
            private set { _invTimer.Duration = value; }
        }

        public float Health { get; private set; }
        public bool IsDead
        {
            get { return Health <= 0; }
        }
        public bool Vulnurable
        {
            get { return !_invTimer.Running; }
        }

        private Timer _invTimer;


        public HealthComponent(IGameObject gameObject,GameObjectsManager gameObjectsManager, float maxHealth = 0, float invularabilityDuration = 0)
            : base(gameObject)
        {
            MaxHealth = maxHealth;
            Health = MaxHealth;           

            _gameObjectsManager = gameObjectsManager;
            _invTimer = new Timer();
            _invTimer.Repeat = false;
            InvularabilityDuration = invularabilityDuration;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is UpdateMessage)
            {
                UpdateMessage ms = (UpdateMessage)message;
                _invTimer.Update(ms.DT);
            }
        }

        public bool DealDamage(float amount, Vector2 knockbackDirection, float knockbackPower = 0.0f, bool criticalStrike = false)
        {
            if (!Vulnurable)
                return false;
            Health -= amount;

            if (IsDead)
            {
                Health = 0;
                _gameObjectsManager.RemoveObject(Owner);// not sure it should be done here
                Owner.SendMessage(new DieMessage());
            }
            while (Math.Abs((knockbackDirection * knockbackPower).Y) > MaxKnockBackY)
                knockbackPower /= 2;//kinda hack

            Owner.SendMessage(new MoveMessage(knockbackDirection, knockbackPower, false));
            Owner.SendMessage(new DamageMessage(amount, DamageWay.Taken, knockbackDirection, criticalStrike));
            _invTimer.Run();
            return true;
        }

        public void Heal(float amount)
        {
            Health += amount;
            if(Health > MaxHealth)
            {
                Health = MaxHealth;
            }
        }

    }
}
