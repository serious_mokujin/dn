﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;

namespace DN.GameObjects
{
    public abstract class Component : IComponent
    {
        protected IGameObject Owner { get; private set; }

        public Component(IGameObject gameObject)
        {
            Owner = gameObject;
        }


        public abstract void ProccesMessage(IMessage message);

        public virtual bool GetDependicies()
        {
            return true;
        }
    }
}
