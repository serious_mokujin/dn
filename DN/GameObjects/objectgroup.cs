﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects
{
    public enum ObjectGroup
    {
        Undefined,
        Hero,
        Enemy,
        Item,
        Modificator,
        Decoration
    }
}
