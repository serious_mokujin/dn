﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects
{
    public enum ObjectState
    {
        AlwaysActive,
        AlwaysDeactivated
    }

    public interface IGameObject: IDisposable
    {
        /// <summary>
        /// non active objects will not be updated
        /// </summary>
        bool Active { get; set; }

        /// <summary>
        /// if true object will be updated even beyond bounds of camera
        /// </summary>
        bool AutoChangeActivity { get; set; }
        event EventHandler OnRemove;

        T GetComponent<T>() where T : IComponent;
        List<T> GetComponents<T>() where T : IComponent;

        ObjectGroup ObjectGroup { get; }

        void AddComponents(params IComponent[] component);

        void Update(float dt);

        void Draw(float dt);

        void SendMessage(IMessage message);

        void RemoveComponent<T>() where T: IComponent;
    }
}
