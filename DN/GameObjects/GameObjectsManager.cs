﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Geometry;
using DN.GameObjects.Messages;
using Blueberry;
using System.Drawing;

namespace DN.GameObjects
{
    public class GameObjectsManager
    {
        private List<IGameObject> _gameObjects;
        private Queue<IGameObject> _addNewObjectsQueue;
        private Queue<IGameObject> _deleteObjectsQueue;

        private Camera _camera;
        private Rectangle _cameraRect;


        public GameObjectsManager(Camera camera)
        {
            _camera = camera;
            _gameObjects = new List<IGameObject>(150);
            _addNewObjectsQueue = new Queue<IGameObject>(150);
            _deleteObjectsQueue = new Queue<IGameObject>(150);
        }

        public void AddObject(IGameObject gameObject)
        {
            if (gameObject == null) throw new ArgumentNullException("gameObject");
            _addNewObjectsQueue.Enqueue(gameObject);
        }

        public void RemoveObject(IGameObject gameObject)
        {
            if (gameObject == null) throw new ArgumentNullException("gameObject");
            _deleteObjectsQueue.Enqueue(gameObject);
        }

        public void Clear(params IGameObject[] ignoreList)
        {
            UpdateObjectsEnqueues();
            foreach (var gameObject in _gameObjects.Where(gameObject => ignoreList.All(p => p != gameObject)))
            {
                RemoveObject(gameObject);
            }
            UpdateObjectsEnqueues();
        }


        public void SendGlobalMessage(IMessage message)
        {
            foreach (var gameObject in _gameObjects)
            {
                gameObject.SendMessage(message);
            }
        }

        public void Update(float dt)
        {
            _cameraRect = _camera.BoundingRectangle;
            _cameraRect.Inflate(_cameraRect.Width, _cameraRect.Height);


            for (int i = 0; i < _gameObjects.Count; i++)
            {
                var gameObject = _gameObjects[i];
                if(gameObject.AutoChangeActivity)
                    gameObject.Active = IsActive(gameObject);
                if (gameObject.Active)
                    gameObject.Update(dt);
            }
            UpdateObjectsEnqueues();
        }

        private bool IsActive(IGameObject gameObject)
        {
            var bc = gameObject.GetComponent<BoundaryComponent>();
            return bc == null || _cameraRect.Contains(bc.Position);
        }

        public void Draw(float dt)
        {
            for (int i = 0; i < _gameObjects.Count; i++)
            {
                var gameObject = _gameObjects[i];
                if (gameObject.Active)
                    gameObject.Draw(dt);
            }
        }


        private void UpdateObjectsEnqueues()
        {
            while (_addNewObjectsQueue.Count > 0)
            {
                IGameObject gameObject = _addNewObjectsQueue.Dequeue();
                _gameObjects.Add(gameObject);
            }
            while (_deleteObjectsQueue.Count > 0)
            {
                IGameObject obj = _deleteObjectsQueue.Dequeue();
                obj.Dispose();
                _gameObjects.Remove(obj);
            }
        }
    }
}
