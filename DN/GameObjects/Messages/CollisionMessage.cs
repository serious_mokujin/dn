﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public class CollisionMessage : IMessage
    {
        public IGameObject CollidedObject { get; private set; }

        public CollisionMessage(IGameObject gameObject)
        {
            CollidedObject = gameObject;
        }
    }
}
