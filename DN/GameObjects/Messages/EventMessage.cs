﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public enum Events
    {
        Jump,
        StopJump,
        StartJump,
        Land,
        Fall,
        Climb,
        StartClimb,
        StopClimb,
        Walk,
        Idle,
        CellingReached
    }
    public class EventMessage: IMessage
    {
        public Events Event { get; private set; }

        public EventMessage(Events ev)
        {
            Event = ev;
        }
    }
}
