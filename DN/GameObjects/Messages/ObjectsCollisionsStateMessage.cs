﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{


    class ObjectsCollisionsStateMessage:IMessage
    {
        public bool CollisionsEnabled { get; private set; }
        public bool SetNewObjectGroup { get;private set; }
        public ObjectGroup[] ObjectGroup { get; private set; }

        //objectGroup = null sets Collision components to default state
        public ObjectsCollisionsStateMessage(bool collisionsEnabled, ObjectGroup[] objectGroup)
        {
            CollisionsEnabled = collisionsEnabled;
            ObjectGroup = objectGroup;
            SetNewObjectGroup = true;
        }
        public ObjectsCollisionsStateMessage(bool collisionsEnabled)
        {
            CollisionsEnabled = collisionsEnabled;
            SetNewObjectGroup = false;
        }
    }
}
