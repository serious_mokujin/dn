﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public enum PhysicState
    {
        Activated,
        Deactivated
    }

    public class PhysicChangeStateMessage:IMessage
    {
        public PhysicState State { get; private set; }

        public PhysicChangeStateMessage(PhysicState state)
        {
            State = state;
        }
    }
}
