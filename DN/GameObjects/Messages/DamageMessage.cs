﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;

namespace DN.GameObjects.Messages
{
    public enum DamageWay
    {
        Given,
        Taken
    }

    public class DamageMessage:IMessage
    {
        public bool Dealed { get; private set; }
        public float Amount { get; private set; }
        public DamageWay DamageWay{ get; private set; }
        public Vector2 Direction { get; private set; }
        public bool CriticalStrike { get; private set; }

        public IGameObject Object { get; private set; }

        public DamageMessage(IGameObject obj, float amount, DamageWay damageWay, Vector2 direction, bool criticalStrike, bool dealed = false)
        {
            Amount = amount;
            DamageWay = damageWay;
            Direction = direction;
            CriticalStrike = criticalStrike;
            Object = obj;
            Dealed = dealed;
        }
    }
}
