﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public enum OutsideActionType
    {
        TryUse,
        Using
    }
    public class OutsideActionMessage : IMessage
    {
        public OutsideActionType Type { get; private set; }
        public IGameObject Object { get; private set; }

        public OutsideActionMessage(OutsideActionType type, IGameObject obj = null)
        {
            Type = type;
            Object = obj;
        }
    }

}
