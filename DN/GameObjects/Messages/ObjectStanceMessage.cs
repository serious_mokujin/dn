﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{

    public enum StanceType
    {
        Activated,
        Deativated
    }
    public class ObjectStanceMessage:IMessage
    {
        public StanceType Type { get; private set; }

        public ObjectStanceMessage(StanceType stanceType)
        {
            Type = stanceType;
        }
    }
}
