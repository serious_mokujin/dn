﻿using DN.GameObjects.Components.AnimationComponents;
using DN.GameObjects.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    class PlayAnimationMessage: IMessage
    {
        public Animations Animation { get; private set; }

        public PlayAnimationMessage(Animations animation)
        {
            Animation = animation;
        }
    }
}
