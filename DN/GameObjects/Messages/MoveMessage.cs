﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;

namespace DN.GameObjects.Messages
{
    public enum MoveType
    {
        Set,
        Add
    }

    public class MoveMessage:IMessage
    {
        public Vector2 Direction{ get;private set; }
        public float Speed { get;private set; }
        public bool CheckOverspeed { get; private set; }
        public bool Grounded { get; private set; }
        public MoveType MoveType { get; set; }

        public MoveMessage(Vector2 direction, float speed, bool checkOverspeed = true, bool grounded = false, MoveType moveType = MoveType.Add)
        {
            Direction = direction;
            Speed = speed;
            CheckOverspeed = checkOverspeed;
            Grounded = grounded;
            MoveType = moveType;
        }
    }
}
