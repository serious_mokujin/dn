﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public enum DirectionState
    {
        Disabled,
        Enabled
    }

    public class DirectionStateMessage:IMessage
    {
        public DirectionState DirectionState { get; private set; }

        public DirectionStateMessage(DirectionState directionState)
        {
            DirectionState = directionState;
        }
    }
}
