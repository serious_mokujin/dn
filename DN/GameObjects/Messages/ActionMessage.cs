﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public enum ActionMessageType
    {
        Start,
        StartRequest,
        Finish,
        Update,
        Interupt,
        InterupRequest,
        Ready
    }

    public class ActionMessage:IMessage
    {
        public ActionMessageType Type { get; private set; }
        public float DT { get; private set; }
        public IGameObject Sender { get; private set; }


        public ActionMessage(IGameObject sender, ActionMessageType type, float dt = 0.0f)
        {
            Sender = sender;
            Type = type;
            DT = dt;
        }
    }
}
