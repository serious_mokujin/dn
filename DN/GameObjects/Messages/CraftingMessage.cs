﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public enum CraftingMessageType
    {
        Started,
        Finished,
    }

    public class CraftingMessage:IMessage
    {
        public IGameObject Sender { get;private set; }
        public CraftingMessageType Type { get; private set; }

        public CraftingMessage(CraftingMessageType type, IGameObject sender)
        {
            Type = type;
            Sender = sender;
        }
    }
}
