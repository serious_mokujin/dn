﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{
    public class PickupMessage:IMessage
    {
        public IGameObject Pickuper { get; private set; }

        public PickupMessage(IGameObject pickuper)
        {
            Pickuper = pickuper;
        }

    }
}
