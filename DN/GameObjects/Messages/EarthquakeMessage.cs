﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GameObjects.Messages
{

    public enum EarthquakeMessageType
    {
        Begin,
        End
    }

    public class EarthquakeMessage:IMessage
    {
        public EarthquakeMessageType Type { get; private set; }

        public EarthquakeMessage(EarthquakeMessageType type)
        {
            Type = type;
        }
    }
}
