﻿using DN.TestGameObjects.Messages;
using OpenTK;
using System;
using System.Linq;
using System.Text;

namespace DN.TestGameObjects
{
    //todo: fix jumps
    public class JumpComponent:Component
    {
        public bool Jumping {get { return _jump; }}

        public float JumpAcceleration;
        public float JumpLength;

        private bool _jump;

        PhysicComponent _physicComponent;
        private LadderClimbingComponent _ladderClimbingComponent;

        float _elapsed = 0;

        
        public JumpComponent(IGameObject gameObject)
            : base(gameObject)
        {
        }


        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override bool GetDependicies()
        {
            _physicComponent = Owner.GetComponent<PhysicComponent>();
            _ladderClimbingComponent = Owner.GetComponent<LadderClimbingComponent>();
            return _physicComponent != null;
        }

        public override void ProccesMessage(IMessage message)
        {
            if (message is UpdateMessage)
            {
                Update((UpdateMessage)message);
            }
            //todo: add collision message
        }

        private void Update(UpdateMessage message)
        {
            if (_jump)
            {
                _elapsed += message.DT;
                _physicComponent.Move(-GameWorld.GravityDirection, JumpAcceleration * message.DT, false);

                if (_elapsed >= JumpLength)
                    StopJump();
            }
        }

        public void Jump()
        {
            if (_physicComponent.OnGround || (_ladderClimbingComponent != null && _ladderClimbingComponent.ClimbLadder))
            {
                _jump = true;
                _physicComponent.SetMoveY(-0.1f);
            }
        }
        public void StopJump()
        {
            _jump = false;
            _elapsed = 0;
        }
    }
}
