﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.GameObjects
{
    public class GameObject:IGameObject
    {
        private List<IComponent> _components;
        private List<IUpdatable> _updatableComponents;
        private List<IDrawable> _drawableComponents;

        public bool Active { get; set; }
        public bool AutoChangeActivity { get; set; }
        public ObjectGroup ObjectGroup { get;private set; }
        
        public event EventHandler OnRemove;

        public GameObject(ObjectGroup objectGroup)
        {
            _components = new List<IComponent>();
            _updatableComponents = new List<IUpdatable>();
            _drawableComponents = new List<IDrawable>();

            ObjectGroup = objectGroup;
            AutoChangeActivity = true;
        }


        public T GetComponent<T>() where T : IComponent
        {
            for (int i = 0; i < _components.Count; i++)
            {
                IComponent p = _components[i];
                if (p is T)
                    return (T) p;
            }
            return default(T);
        }

        public List<T> GetComponents<T>() where T : IComponent
        {
            List<T> list = new List<T>();
            for (int i = 0; i < _components.Count; i++)
            {
                IComponent p = _components[i];
                if (p is T) list.Add((T) p);
            }
            return list;
        }

        public void AddComponents(params IComponent[] components)
        {
            foreach (var component in components)
            {
                _components.Add(component);
                if (component is IUpdatable)
                {
                    _updatableComponents.Add((IUpdatable)component);
                }
                if (component is IDrawable)
                {
                    _drawableComponents.Add((IDrawable)component);
                }
            }
            foreach (IComponent component in components)
            {
                if (!component.GetDependicies()) throw new Exception("Depedicies were not found " + component);
            }
        }

        public void Update(float dt)
        {
            for (int i = 0; i < _updatableComponents.Count; i++)
            {
                var component = _updatableComponents[i];
                component.Update(dt);
            }
        }

        public void Draw(float dt)
        {
            for (int i = 0; i < _drawableComponents.Count; i++)
            {
                var component = _drawableComponents[i];
                component.Draw(dt);
            }
        }

        public void SendMessage(IMessage message)
        {
            for (int index = 0; index < _components.Count; index++)
            {
                var component = _components[index];
                component.ProccesMessage(message);
            }
        }

        public void RemoveComponent<T>() where T : IComponent
        {
            IComponent component = GetComponent<T>();
            if (component != null)
            {
                _components.Remove(component);
                if(component is IUpdatable)
                {
                    _updatableComponents.Remove((IUpdatable)component);
                }
                if(component is IDrawable)
                {
                    _drawableComponents.Remove((IDrawable)component);
                }
            }
        }

        public void Dispose()
        {
            foreach (var component in _components)
            {
                if(component is IDisposable)
                    (component as IDisposable).Dispose();
            }
            if (OnRemove != null)
                OnRemove(this, EventArgs.Empty);
            OnRemove = null;
        }
    }
}
