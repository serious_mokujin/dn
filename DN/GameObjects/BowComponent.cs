﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.Helpers;
using DN.TestGameObjects.Components;
using DN.TestGameObjects.Messages;
using OpenTK;

namespace DN.TestGameObjects
{
    class BowComponent:Component
    {
        public float TensionGrowingSpeed = 40;

        public float MaxTension = 18;
        public float MinTension = 2;



        private float _tension = 0;


        private float _projectiveSpeed;

        public float ProjectiveSpeed
        {
            get { return _projectiveSpeed * _tension; }
            set { _projectiveSpeed = value; }
        }

        protected Vector2 ProjectivePosition
        {
            get
            {
                return new Vector2(_boundaryComponent.Position.X + (-_tension) * (float)Math.Cos(_rotation),
                                   _boundaryComponent.Position.Y - _tension * (float)Math.Sin(_rotation));
            }
        }

        private GameObject _currentProjective;

        private BoundaryComponent _boundaryComponent;
        private DirectionComponent _directionComponent;
        private PickupableComponent _pickupableComponent;
        private float _rotation;
        private IGameObject _projectivePrototype;

        private GameObjectsManager _manager;

        public BowComponent(IGameObject gameObject, IGameObject projectivePrototype, GameObjectsManager manager) : base(gameObject)
        {
            _projectivePrototype = projectivePrototype;
            _currentProjective = (GameObject)_projectivePrototype.Clone();
            _manager = manager;
            _manager.AddObject(_currentProjective);
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            _directionComponent = Owner.GetComponent<DirectionComponent>();
            _pickupableComponent = Owner.GetComponent<PickupableComponent>();
            Owner.GetComponent<ActionComponent>().AutoFinish = false;
            return _boundaryComponent != null && _directionComponent != null; 
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is UpdateMessage)
            {
                HandleUpdateMessage((UpdateMessage)message);
            }
            else if (message is ActionMessage)
            {
                HandleActionMessage((ActionMessage)message);
            }
        }
        private void HandleUpdateMessage(UpdateMessage updateMessage)
        {
            if (_currentProjective != null)
            {
                _rotation = FunctionHelper.Vector2ToRadians(_directionComponent.Direction);
                _currentProjective.GetComponent<BoundaryComponent>().Position = ProjectivePosition;
            }
        }

        private void HandleActionMessage(ActionMessage message)
        {
            switch (message.Type)
            {
                case ActionMessageType.Start:
                    _tension = MinTension;
                    break;
                case ActionMessageType.Update:
                    if (MaxTension > _tension)
                        _tension += message.DT*TensionGrowingSpeed;
                    if (MaxTension < _tension)
                        _tension = MaxTension;
                    break;
                case ActionMessageType.Interupt:

                    Vector2 dir = FunctionHelper.RadiansToVector2(_rotation);

                    _currentProjective.SendMessage(new MoveMessage(dir, ProjectiveSpeed, false));
                    _currentProjective.SendMessage(new PhysicChangeStateMessage(PhysicState.Activated));
                    _currentProjective.SendMessage(new ObjectsCollisionsStateMessage(false, new[]
                                                                                                {
                                                                                                    Owner.ObjectGroup,
                                                                                                    _pickupableComponent.Pickuper.ObjectGroup
                                                                                                }));
                    //_currentProjective.Move(new Vector2(0, -1), 2, false); // small hack
                    _currentProjective = null;
                    break;
                case ActionMessageType.Ready:
                    _currentProjective = (GameObject) _projectivePrototype.Clone();
                    _currentProjective.SendMessage(new ObjectsCollisionsStateMessage(false, new[] { ObjectGroup.Undefined }));
                    _manager.AddObject(_currentProjective);
                    break;
            }
        }
    }
}
