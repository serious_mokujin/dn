﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.TestGameObjects.Messages;

namespace DN.TestGameObjects
{
    public class HealthComponent : Component
    {
        private float _maxHelath;

        public float MaxHealth
        {
            get { return _maxHelath; }
            private set { _maxHelath = value; }
        }

        public float Health { get; private set; }
        public bool IsDead
        {
            get { return Health <= 0; }
        }


        public HealthComponent(IGameObject gameObject, float maxHealth = 0)
            : base(gameObject)
        {
            MaxHealth = maxHealth;
            Health = MaxHealth;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            return;//nothing to do here
        }

        public void DealDamage(float amount)
        {
            Health -= amount;
            if (IsDead)
            {
                Health = 0;
                Owner.SendMessage(new DieMessage());
            }
        }

        public void Heal(float amount)
        {
            Health += amount;
            if(Health > MaxHealth)
            {
                Health = MaxHealth;
            }
        }

    }
}
