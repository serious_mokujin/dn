﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using DN.TestGameObjects.Messages;
using OpenTK;
using OpenTK.Graphics;

namespace DN.TestGameObjects
{
    public class SimpleDrawingComponent:Component
    {
        private Texture _sprite;
        private BoundaryComponent _boundaryComponent;

        public SimpleDrawingComponent(IGameObject gameObject, string sprite) : base(gameObject)
        {
            _sprite = CM.I.tex(sprite);
        }

        public override bool GetDependicies()
        {
            _boundaryComponent = Owner.GetComponent<BoundaryComponent>();
            return _boundaryComponent != null;
        }

        public override int ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        public override void ProccesMessage(IMessage message)
        {
            if(message is DrawMessage)
            {
                Draw((DrawMessage)message);
            }
        }

        private void Draw(DrawMessage message)
        {
            SpriteBatch.Instance.DrawTexture(_sprite, _boundaryComponent.Bounds, RectangleF.Empty,
                                             Color4.White, 0.0f, new Vector2(0,0));
        }
    }
}
