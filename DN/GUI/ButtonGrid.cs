﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Input;
using DN.Helpers;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;

namespace DN.GUI
{


    public class ButtonGrid : GUIObject, IDisposable
    {
        public bool Active = false;

        private readonly float _minAlpha;
        private readonly float _maxAlpha;
        private readonly float _alphaChangingSpeed;
        private readonly int[] _values;
        private List<Button> _buttons;
        private Button _focusedButton;
        private Point _focusedButtonCell;


        private KeyboardDevice _keyboard;
        private MouseDevice _mouse;
        private GamepadDevice _gamepad;

        private Vector2 _targedPosition;
        private Point _pos;
        private int buttonWidth;
        private int buttonHeight;

        public ButtonGrid(BitmapFont captionFont, BitmapFont valueFont,
                          Vector2 position, Size size,
                          Color4 color, Color4 focusedColor,
                          float minAlpha, float maxAlpha, float alphaChangingSpeed,
                          int rows,
                          string[] captions, int[] values,
                          EventHandler onButtonPress)
        {
            _minAlpha = minAlpha;
            _maxAlpha = maxAlpha;
            _alphaChangingSpeed = alphaChangingSpeed;
            _values = values;
            _buttons = new List<Button>(captions.Length);

            Size = size;
            _targedPosition = position;

            buttonWidth = (int) ((position.X + size.Width)/rows);
            buttonHeight = (int) ((position.Y + size.Height)/(Math.Round((float) captions.Length/rows) + 1));



            int i = 0, j = 0, k = 0;
            foreach (string str in captions)
            {
                var button = new Button(captionFont, valueFont)
                                 {
                                     Caption = str,
                                     Color = color,
                                     FocusedColor = focusedColor,
                                     Size = new Size(buttonWidth, buttonHeight),
                                     Position = new Vector2(buttonWidth*i, buttonHeight*j),
                                     Alpha = 0.5f,
                                     Value = values[k],
                                     Parent = this
                                 };
                button.OnPress += onButtonPress;
                button.Position = new Vector2(position.X + i*buttonWidth, position.Y + j*buttonHeight);
                i++;
                if (i >= rows)
                {
                    i = 0;
                    j++;
                }
                if (k < _values.Length - 1)
                    k++;
                _buttons.Add(button);
            }

           // Game.g_Mouse.Move += GMouseOnMove;
           // Game.g_Mouse.ButtonDown += GMouseOnButtonDown;
           // Game.g_Gamepad_1.OnButtonPress += GGamepad1OnButtonPress;
            //if (Game.g_Gamepad_1.Connected)
            {
                _focusedButtonCell.X = 0;
                _focusedButtonCell.Y = 0;
                ChangeFocusTo(new Point(_focusedButtonCell.X * buttonWidth, _focusedButtonCell.Y * buttonHeight));
            }
        }




        public override void Update(float dt)
        {
            UpdateButtonsAlpha(dt);

            for (int i = 0; i < _values.Count(); i++)
            {
                _buttons[i].Value = _values[i];
            }
        }

        private void UpdateButtonsAlpha(float dt)
        {
            foreach (var button in _buttons)
            {
                button.Update(dt);
                if (button.Focused)
                {
                    FunctionHelper.Approximation(ref button.Alpha, _maxAlpha, dt*_alphaChangingSpeed);
                }
                else
                {
                    FunctionHelper.Approximation(ref button.Alpha, _minAlpha, dt * _alphaChangingSpeed);
                }
            }
        }

        public override void Draw(float dt)
        {
            if (!Active)
                return;
            foreach (var button in _buttons)
            {
                button.Draw(dt);
            }
        }

        private void GMouseOnButtonDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            if(!Active)
                return;
            if (_focusedButton != null)
                _focusedButton.Press();
        }

        private void GMouseOnMove(object sender, MouseMoveEventArgs e)
        {
            //   if (Game.g_Gamepad_1.Connected)
            //       return;
            if (!Active)
                return;
            FocusToMousePosition();
        }

        private void FocusToMousePosition()
        {
            if (!Active)
                return;
            _pos = new Point(Game.g_Mouse.X, Game.g_Mouse.Y);
            _focusedButtonCell = new Point(_pos.X/buttonWidth, _pos.Y/buttonHeight);
            ChangeFocusTo(_pos);
        }

        private void GGamepad1OnButtonPress(object sender, GamepadButtonFlags e)
        {
            if (!Active)
                return;
            Point pos = Point.Empty;
            if(e.HasFlag(GamepadButtonFlags.A))
            {
                if (_focusedButton != null)
                    _focusedButton.Press();
            }
            if (e.HasFlag(GamepadButtonFlags.DPadLeft))
                pos.X = -1;
            else if (e.HasFlag(GamepadButtonFlags.DPadRight))
                pos.X = 1;
            else if (e.HasFlag(GamepadButtonFlags.DPadUp))
                pos.Y = -1;
            else if (e.HasFlag(GamepadButtonFlags.DPadDown))
                pos.Y = 1;

            if (pos.X != 0 || pos.Y != 0)
            {
                _focusedButtonCell.X += pos.X;
                _focusedButtonCell.Y += pos.Y;

                if (!ChangeFocusTo(new Point(_focusedButtonCell.X*buttonWidth, _focusedButtonCell.Y*buttonHeight)))
                {
                    _focusedButtonCell.X -= pos.X;
                    _focusedButtonCell.Y -= pos.Y;
                    ChangeFocusTo(new Point(_focusedButtonCell.X*buttonWidth, _focusedButtonCell.Y*buttonHeight));
                }
            }
        }

        private bool ChangeFocusTo(Point pos)
        {
            if (_focusedButton != null)
            {
                _focusedButton.Focused = false;
                _focusedButton = null;
            }
            foreach (var button in _buttons)
            {
                if (button.Bounds.Contains(pos))
                {
                    _focusedButton = button;
                    _focusedButton.Focused = true;
                    return true;
                }
            }
            return false;
        }


        public void Dispose()
        {
            _mouse.Move -= GMouseOnMove;
            _mouse.ButtonDown -= GMouseOnButtonDown;
            _gamepad.OnButtonPress -= GGamepad1OnButtonPress;
        }

        internal void SetControlls(KeyboardDevice keyboard, MouseDevice mouse, GamepadDevice gamepad)
        {
            if (_gamepad != null)
                _gamepad.OnButtonPress -= GGamepad1OnButtonPress;
            if (_mouse != null)
            {
                _mouse.Move -= GMouseOnMove;
                _mouse.ButtonDown -= GMouseOnButtonDown;
            }
            _keyboard = keyboard;
            _mouse = mouse;
            _gamepad = gamepad;
            if (_gamepad != null)
                gamepad.OnButtonPress += GGamepad1OnButtonPress;
            if (_mouse != null)
            {
                _mouse.Move += GMouseOnMove;
                _mouse.ButtonDown += GMouseOnButtonDown;
            }
        }
    }
}
