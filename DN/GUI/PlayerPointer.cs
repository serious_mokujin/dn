﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Graphics;
using DN.GameObjects;
using DN.Helpers;
using OpenTK;
using OpenTK.Graphics;

namespace DN.GUI
{
    class PlayerPointer:GUIObject
    {
        private readonly IGameObject _player;
        private readonly Camera _camera;
        private readonly Color4 _color;

        private Vector2 _position;
        private BoundaryComponent _boundaryComponent;
        private bool _inCamera;
        private Vector2 dir;

        public PlayerPointer(IGameObject player, Camera camera, Color4 color)
        {
            _player = player;
            _boundaryComponent = _player.GetComponent<BoundaryComponent>();

            _camera = camera;
            _color = color;
        }

        public override void Update(float dt)
        {
            if (_boundaryComponent.Bounds.IntersectsWith(_camera.BoundingRectangle))
            {
                _inCamera = true;
                return;
            }
            _inCamera = false;
      //      _camera.ToWorld(Game.g_Mouse.X, Game.g_Mouse.Y));
            dir = -FunctionHelper.DirectionToObject(_camera.Position,  _boundaryComponent.Position);
            dir.Normalize();
        //    _position.X = - (float) Math.Cos(dir.X)*(_camera.BoundingRectangle.Width)/2;
        //    _position.Y = - (float) Math.Sin(dir.Y)*(_camera.BoundingRectangle.Height)/2;

            _position.X = -_camera.BoundingRectangle.Width * dir.X /2;
            _position.Y = -_camera.BoundingRectangle.Height * dir.Y / 2;

            _position *= Game.g_CameraScale;

            _position.X += Game.g_screenSize.Width/2;
            _position.Y += Game.g_screenSize.Height/2;
        }

        public override void Draw(float dt)
        {
            if(_inCamera)
                return;

            SpriteBatch.Instance.FillPolygon(new []
                                                 {
                                                     new Vector2( 32, - 16), 
                                                     new Vector2(32, 16), 
                                                     new Vector2(0, 0), 
                                                 }, _position, FunctionHelper.Vector2ToRadians(dir), 1, _color);
        }
    }
}
