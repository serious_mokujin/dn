﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.GUI
{
    public sealed class GUIManager:IDisposable
    {
        private readonly List<GUIObject> _guiObjects;

        public GUIManager()
        {
            _guiObjects = new List<GUIObject>();
        }

        public void Add(GUIObject guiObject)
        {
            _guiObjects.Add(guiObject);
        }
        public void Remove(GUIObject guiObject)
        {
            if (guiObject is IDisposable)
                (guiObject as IDisposable).Dispose();
            _guiObjects.Remove(guiObject);
        }


        public void Update(float dt)
        {
            foreach (var guiObject in _guiObjects)
            {
                guiObject.Update(dt);
            }
        }

        public void Draw(float dt)
        {
            foreach (var guiObject in _guiObjects)
            {
                guiObject.Draw(dt);
            }
        }

        public void Dispose()
        {
            foreach (var guiObject in _guiObjects)
            {
                if (guiObject is IDisposable)
                    (guiObject as IDisposable).Dispose();
            }
            _guiObjects.Clear();
        }
    }
}
