﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Input;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;

namespace DN.GUI
{
    public delegate void ItemChosenEventHandler(GUIObject sender, string item);


    public class TreeItem<T>
    {
        public TreeItem<T> Parent;
        public TreeItem<T>[] Children;
        public T Value;

        public TreeItem(T value, params TreeItem<T>[] elements)
        {
            Value = value;
            Children = elements;
            for (int i = 0; i < Children.Length; i++)
            {
                if (Children[i] != null)
                    Children[i].Parent = this;
            }
        }

        public int Count()
        {
            return Children.Length;
        }
    }

    class Menu:GUIObject, IDisposable
    {
        public event ItemChosenEventHandler ItemChosen;
        public float ChosenItemZoom = 2.0f;

        private BitmapFont _font;
        private float _fontHeight;

        private TreeItem<String> _tree;
        private float[] _itemsZoom;
        private int _currentItem = 0;
        private int _itemsCount;

        private float _widthOffset;
        private float _heightOffset;

        public Menu(BitmapFont font, TreeItem<String> tree)
        {
            Game.g_Keyboard.KeyDown += GKeyboardOnKeyDown;
            Game.g_Gamepad_1.OnButtonPress += GGamepad1OnButtonDown;
            Game.g_Mouse.Move += GMouseOnMove;
            Game.g_Mouse.ButtonDown += GMouseOnButtonDown;

            _font = font;
            _fontHeight = _font.LineSpacing; //_font.MeasureSymbol('#').Height;

            _tree = tree;

            _itemsCount = tree.Count();
            _itemsZoom = new float[_itemsCount];

            //todo: add offset measuring
            SizeF offset = _font.Measure(_tree.Children.Select(p => p.Value).Aggregate("", (max, cur) => max.Length > cur.Length ? max : cur));
            _heightOffset =  offset.Height;
        }

        private void GMouseOnButtonDown(object sender, MouseButtonEventArgs e)
        {
            int item = GetItem(e.X, e.Y);
            if (item != -1)
            {
                _currentItem = item;
                MakeChoise();
            }
        }

        private void GMouseOnMove(object sender, MouseMoveEventArgs e)
        {
            int item = GetItem(e.X, e.Y) ;
            if (item != -1)
            {
                _currentItem = item;
            }

        }

        private void GGamepad1OnButtonDown(object sender, GamepadButtonFlags e)
        {
            if(e.HasFlag(GamepadButtonFlags.DPadDown))
            {
                MoveDown();
            }
            if (e.HasFlag(GamepadButtonFlags.DPadUp))
            {
                MoveUp();
            }
            if (e.HasFlag(GamepadButtonFlags.A) || e.HasFlag(GamepadButtonFlags.Start))
            {
                MakeChoise();
            }
        }

        private void GKeyboardOnKeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if(e.Key == Key.Down || e.Key == Key.S)
            {
                MoveDown();
            }
            else if(e.Key == Key.Up || e.Key == Key.W)
            {
                MoveUp();
            }
            else if(e.Key == Key.Enter || e.Key == Key.Space)
            {
                MakeChoise();
            }
        }

        public override void Update(float dt)
        {
            for (int i = 0; i < _itemsCount; i++)
            {
                if(i == _currentItem)
                {
                    if (_itemsZoom[i] < 1.0f)
                        _itemsZoom[i] = MathUtils.Lerp(_itemsZoom[i], 1.0f, dt * 10);
                    if (_itemsZoom[i] > 1.0f)
                        _itemsZoom[i] = 1.0f;
                }
                else
                {
                    if (_itemsZoom[i] > 1.0f / ChosenItemZoom)
                        _itemsZoom[i] = MathUtils.Lerp(_itemsZoom[i], 1.0f / ChosenItemZoom, dt * 10);
                    if (_itemsZoom[i] < 1.0f / ChosenItemZoom)
                        _itemsZoom[i] = 1.0f / ChosenItemZoom;
                }
            }
        }

        public override void Draw(float dt)
        {
            for (int i = 0; i < _tree.Count(); i++)
            {
                SpriteBatch.Instance.PrintText(_font, _tree.Children[i].Value, ElementPosition(i), Color4.White, 0, _itemsZoom[i]);
            }
            //for (int i = 0; i < _tree.Children.Length; i++)
            //{
            //    var item = _tree.Children[i];
            //    Vector2 position = ElementPosition(i);
            //    SizeF size = _font.Measure(item.Value);
            //    RectangleF bounds = new RectangleF(position.X - size.Width/2, position.Y - size.Height/2,
            //                                       size.Width, size.Height);
            //    SpriteBatch.Instance.FillRectangle(bounds, new Color4(0.2f, 0.2f, 0.2f, 0.5f));
            //}
        }

        private void MoveDown()
        {
            _currentItem++;
            if (_currentItem >= _itemsCount)
                _currentItem = 0;
        }
        private void MoveUp()
        {
            _currentItem--;
            if (_currentItem < 0)
                _currentItem = _itemsCount - 1;
        }
        private void MakeChoise()
        {
            ItemChosen(this, _tree.Children[_currentItem].Value);

            if(_tree.Children[_currentItem].Value == "Back")
                ChangeTo(_tree.Parent);
            else 
                ChangeTo(_tree.Children[_currentItem]);
        }
        private void ChangeTo(TreeItem<String> tree)
        {
            if(tree.Count() == 0)
                return;
            _tree = tree;

            if(_tree.Value != "")
            {
                if(_tree.Children.All(p => p.Value != "Back"))
                {
                    Array.Resize(ref _tree.Children, _tree.Children.Length + 1);
                    _tree.Children[_tree.Children.Length - 1] = new TreeItem<string>("Back");
                }
            }
            _itemsCount = _tree.Count();
            _itemsZoom = new float[_itemsCount];

            _currentItem = 0;
        }

        private Vector2 ElementPosition(int element)
        {
            return new Vector2(Position.X, Position.Y + element*_fontHeight*ChosenItemZoom - _heightOffset);
        }

        private int GetItem(float x, float y)
        {
            for (int i = 0; i < _tree.Children.Length; i++)
            {
                var item = _tree.Children[i];
                Vector2 position = ElementPosition(i);
                SizeF size = _font.Measure(item.Value);
                RectangleF bounds = new RectangleF(position.X - size.Width/2, position.Y - size.Height/2,
                                                   size.Width, size.Height);

                if (bounds.Contains(x, y))
                    return i;
            }
            return -1;
        }

        public void Dispose()
        {
            Game.g_Keyboard.KeyDown -= GKeyboardOnKeyDown;
            Game.g_Gamepad_1.OnButtonPress -= GGamepad1OnButtonDown;
            Game.g_Mouse.Move -= GMouseOnMove;
            Game.g_Mouse.ButtonDown -= GMouseOnButtonDown;
        }
    }
}
