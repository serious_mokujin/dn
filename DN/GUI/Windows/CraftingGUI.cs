﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DN.GameObjects.Components.OutsideInteractingComponents;
using DN.GameObjects.Components.StanceComponents;
using DN.Helpers;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using Blueberry.Input;

namespace DN.GUI.Windows
{
    public enum ElementState
    {
        Opening,
        Closing,
        Opened,
        Closed
    }

    public class CraftingGUI:GUIObject,IDisposable
    {
        public CraftingComponent CraftingComponent;

        public ElementState State;

        private readonly GUIManager _guiManager;
        private readonly LettersInventory _lettersInventory;
        private ButtonGrid _grid;
        private Label _label;

        private readonly Vector2 _targedPosition;

        public CraftingGUI(GUIManager guiManager, LettersInventory lettersInventory, Vector2 targedPosition)
        {
            _guiManager = guiManager;
            _lettersInventory = lettersInventory;
            _targedPosition = targedPosition;

            var stringArray = new string[28];
            int j = 0;
            for (char m = 'A'; m <= 'Z'; m++)
            {
                stringArray[j++] = new string(m, 1);
            }
            stringArray[j++] = "Delete";
            stringArray[j++] = "Craft!";

            _grid = new ButtonGrid(CM.I.Font("Middle"), CM.I.Font("Small"), new Vector2(0, 0),
                                   new Size(Game.g_screenSize.Width, Game.g_screenSize.Height/2),
                                   Color4.Black, Color4.White, 0.7f,
                                   0.99f, 0.5f, 8, stringArray, lettersInventory.Letters,
                                   OnButtonPress);
            _grid.Parent = this;
            Close();
            guiManager.Add(_grid);
            _label = new Label(CM.I.Font("Big"))
                         {
                             Parent = this,
                             Color = new Color4(0, 0, 0, 0.8f),
                             TextColor = Color.White,
                             X = Game.g_screenSize.Width/2,
                             Y = Game.g_screenSize.Height/1.5f
                         };
            guiManager.Add(_label);


        }
        public void SetControlls(KeyboardDevice keyboard, MouseDevice mouse, GamepadDevice gamepad)
        {
            _grid.SetControlls(keyboard, mouse, gamepad);
        }


        public void Open()
        {
            if (State == ElementState.Opened || State == ElementState.Opening)
                return;
            _grid.Active = true;
            State = ElementState.Opening;
        }
        public void Close()
        {
            if (State == ElementState.Closed || State == ElementState.Closing)
                return;
            State = ElementState.Closing;

            if(_label != null)
            for (int i = 0; i < _label.Text.Length; i++)
            {
                _lettersInventory.Add(_label.Text.ToLower()[i]);
                _label.Text = "";
            }

        }


        private void OnButtonPress(object sender, EventArgs e)
        {

            string caption = ((Button) sender).Caption;
            if (caption == "Delete")
            {
                if (_label.Text.Any())
                {
                    _lettersInventory.Add(_label.Text.ToLower()[_label.Text.Count() - 1]);
                    _label.Text = _label.Text.Remove(_label.Text.Count() - 1, 1);
                }
            }
            else if (caption == "Craft!")
            {
                if (CraftingComponent.CreateItem(_label.Text.ToLower()))
                {
                    _label.Text = "";
                }
            }
            else
            {
                char ch = caption.ToLower()[0];
                if (_lettersInventory.GetByLetter(ch) > 0)
                {
                    _label.Text += caption;
                    _lettersInventory.Remove(ch);
                }
            }
        }

        public override void Update(float dt)
        {
            if (State == ElementState.Opening)
            {
                bool finished = false;

              //  float x = X;
             //   finished = FunctionHelper.Approximation(ref x, _targedPosition.X, dt * 2000);
            //    X = x;

                float y = Y;
                finished = FunctionHelper.Approximation(ref y, _targedPosition.Y, dt * 3000);
                Y = y;
                if (finished)
                {
                    State = ElementState.Opened;
                }
            }
            else if (State == ElementState.Closing)
            {
                bool finished = false;

                float y = Y;
                finished = FunctionHelper.Approximation(ref y, -Size.Height, dt*2000);
                Y = y;
                if (finished)
                {
                    State = ElementState.Closed;
                    _grid.Active = false;
                }
            }
        }

        public override void Draw(float dt)
        {
        }

        public void Dispose()
        {
            _guiManager.Remove(_grid);
        }
    }
}
