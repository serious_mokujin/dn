﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry.Animations;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using DN.GameObjects;
using DN.Helpers;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace DN.GUI
{
    public class HealthBar:GUIObject
    {
        private readonly HealthComponent _healthComponent;
        private Texture tex;
		float blinkWidth;
		float lastHealth;
		FloatAnimation blinkAnim = new FloatAnimation(0, 1, 0.1f, LoopMode.LoopWithReversing, v=>v*v*v);
		int blinkCounter;
		float Health{get{return _healthComponent.Health/_healthComponent.MaxHealth;}}
		float BlinkSector{get{return blinkWidth/_healthComponent.MaxHealth;}}
		float BlinkSectorPixels {get{return BlinkSector * tex.Size.Width; }}
		float HealthPixels {get{return Health * tex.Size.Width; }}
		
		BitmapFont _font;
		string _text;
		SizeF _size;
		public new SizeF Size{get{return _size;}}
        public HealthBar(HealthComponent healthComponent, string text, BitmapFont font):base()
        {
            _healthComponent = healthComponent;
            _font = font;
            _text = text;
            //PredrawTexture(font, text);
            blinkAnim.OnExtremum += (Animation<float> f, float v) => { blinkCounter++; if(blinkCounter>=10) { blinkAnim.Stop(); blinkWidth = 0; blinkCounter = 0; } };
        	lastHealth = _healthComponent.Health;
        	_size = font.Measure(text);
        	_size.Width *= 1.3f;
        	_size.Height *= 1.5f;
        }

        public override void Update(float dt)
        {
        	if(_healthComponent.Health < lastHealth)
        	{
        		blinkWidth += lastHealth - _healthComponent.Health;
        		lastHealth = _healthComponent.Health;
        		if(blinkAnim.State != PlaybackState.Play)
        			blinkAnim.Play();
        	}
        	if(!predrawn) PredrawTexture(_font, _text);
        	//if(Game.g_Keyboard[Key.U]) PredrawTexture(_font, _text);
        }
        bool predrawn = false;
        void PredrawTexture(BitmapFont font, string text)
        {
        	tex = new Texture(font.Measure(text).ToSize());
        	GL.ClearColor(0, 0, 0, 0);
            SpriteBatch.Instance.Begin();
            SpriteBatch.Instance.PrintText(font, text, 0, 0, Color4.White, 0, 1, 0, 0, false, false);
            SpriteBatch.Instance.End(tex, false);
            predrawn = true;
        }
        public override void Draw(float dt)
        {
        	SpriteBatch.Instance.FillEllipse(Position.X, Position.Y, tex.Size.Width*1.5f, tex.Size.Height*1.2f, Color4.Black, new Color4(0,0,0,1), 10);
            SpriteBatch.Instance.DrawTexture(tex, Position, new Color4(1, 1f, 1f, 0.05f));
            
            SpriteBatch.Instance.DrawTexture(tex,new Vector2(Position.X-tex.Size.Width/2, Position.Y-tex.Size.Height/2), new RectangleF(0,0,HealthPixels, tex.Size.Height),new Color4(1-Health, Health, 0.5f, 1), 0, Vector2.Zero, Vector2.One);
            if(blinkWidth > 0)
            {
            	SpriteBatch.Instance.DrawTexture(tex, new Vector2(Position.X-tex.Size.Width/2 + HealthPixels, Position.Y-tex.Size.Height/2), new RectangleF(HealthPixels,0,BlinkSectorPixels, tex.Size.Height),new Color4(1, 0,0,blinkAnim.Value), 0, Vector2.Zero, Vector2.One);
            	SpriteBatch.Instance.FillEllipse(Position.X - tex.Size.Width/2+HealthPixels + (BlinkSectorPixels/2), Position.Y, BlinkSectorPixels*1.3f, tex.Size.Height*1.7f, new Color4(1, 0, 0, this.blinkAnim.Value), new Color4(1, 0, 0, 0), 10);
            }
            if(Health == 0)
            	SpriteBatch.Instance.PrintText(_font, "X__x\'", Position.X, Position.Y, Color4.White, 0, 1, 0.5f, 0.5f);
        }
    }
}
