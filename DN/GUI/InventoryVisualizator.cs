﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using DN.GameObjects;
using DN.GameObjects.Components.StanceComponents;
using OpenTK.Graphics;
using DN.Helpers;

namespace DN.GUI
{
    internal class InventoryVisualizator : GUIObject
    {
        public Size MaxSize;

        private readonly PickupingComponent _pickupingComponent;
        private readonly BitmapFont _font;

        private float Scale
        {
            get { return Game.g_CameraScale; }
        }

        private string _text;
        private SizeF _fontSize;
        private float _alpha = 0;
        private float _nextAlpha = 0;
        private Timer _dropAlphaTimer;
        private bool _changeAlpha = false;

        public InventoryVisualizator(PickupingComponent pickupingComponent, BitmapFont font)
        {
            _pickupingComponent = pickupingComponent;
            _pickupingComponent.ItemChanged += PickupingComponentOnItemChanged;
            _font = font;
            _fontSize = font.MeasureSymbol('j');

            _dropAlphaTimer = new Timer();
            _dropAlphaTimer.Repeat = false;
            _dropAlphaTimer.Duration = 4;
            _dropAlphaTimer.TickEvent += () => 
                                            {
                                                _changeAlpha = true;
                                                _nextAlpha = 0;
                                            };
        }

        private void PickupingComponentOnItemChanged(object sender, EventArgs eventArgs)
        {
            _text = null;
            if (_pickupingComponent.ChosenObject == null)
            {
                return;
            }
            var dc = _pickupingComponent.ChosenObject.GetComponent<DescriptionComponent>();
            if (dc == null)
            {
                return;
            }

            _text = dc.Description;
            //string maxString = _text.Length;

          //  Size.Width = (int) (_font.Measure(maxString).Width*Scale);
     //       Size.Height = (int)(_fontSize.Height * _text.Count * Scale);//(int) (_font.Measure(maxString).Height*Scale);
            _nextAlpha = 1f;
            _changeAlpha = true;

            _dropAlphaTimer.Restart();
        }

        public override void Update(float dt)
        {
            if (_changeAlpha)
                _changeAlpha = !FunctionHelper.Approximation(ref _alpha, _nextAlpha, dt);
            _dropAlphaTimer.Update(dt);
        }

        public override void Draw(float dt)
        {
            if (_text == null)
                return;
            if (_alpha < 0)
                return;
            RectangleF bounds = Bounds;
            ProcessedText jt = _font.ProcessText(_text, MaxSize.Width, false);
            


          //  SpriteBatch.Instance.FillRectangle(bounds, new Color4(0, 0, 0.0f, _alpha));
          //  SpriteBatch.Instance.PrintText();


            //    SpriteBatch.Instance.PrintText(_font, str, Position.X, Position.Y + i*_fontSize.Height*Scale, new Color4(1, 1, 1, _alpha), 0, Scale, 0, 0);
        }

        private List<string> SplitBySize(string str)
        {
            List<string> strings = new List<string>();

            Split(str, strings);

            return strings;
        }

        private bool Split(string a, List<string> strings)
        {
            string b = a;
            string c = "";
            while (_font.Measure(b).Width*Scale > MaxSize.Width)
            {
                char l = b[b.Length - 1];
                b = b.Remove(b.Length - 1, 1);
                c = c.Insert(0, l.ToString());
            }
            strings.Add(b);
            if (_font.Measure(c).Width*Scale > MaxSize.Width)
                Split(c, strings);
            else
            {
                strings.Add(c);
            }


            return false;
        }
    }
}
