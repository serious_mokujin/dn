﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using OpenTK;
using OpenTK.Graphics;

namespace DN.GUI
{
    internal class PopUpText
    {
        public string Text;
        public Vector2 Position;
        public Vector2 Direction;
        private float _endSpeed;
        private float _speed;
        public Color4 Color;
        private Color4 _endColor;
        public bool Active;

        public PopUpText(string text, Color4 startColor, Color4 endColor, Vector2 position,
                         Vector2 direction, float startSpeed, float endSpeed)
        {
            Set(text, startColor, endColor, position, direction, startSpeed, endSpeed);
            Active = true;
        }

        public void Set(string text, Color4 startColor, Color4 endColor, Vector2 position,
                        Vector2 direction, float startSpeed, float endSpeed)
        {
            Text = text;
            Position = position;
            Direction = direction;
            _endSpeed = endSpeed;
            _speed = startSpeed;
            Color = startColor;
            _endColor = endColor;
        }

        public void Update(float dt)
        {
            if (!Active)
                return;

            _speed = MathUtils.Lerp(_speed, _endSpeed, dt * 4);
            Position += Direction * _speed;
            if (_speed < 0.2)
                Color = new Color4(MathUtils.Lerp(Color.R, _endColor.R, dt * 2),
                    MathUtils.Lerp(Color.G, _endColor.G, dt * 2),
                    MathUtils.Lerp(Color.B, _endColor.B, dt * 2),
                    MathUtils.Lerp(Color.A, _endColor.A, dt * 2));

            if (_speed < 0.05)
                _speed = 0;
            if (Color.A < 0.05f)
                Active = false;
        }
    }

    public class PopupTextThrower:GUIObject
    {
        private static PopupTextThrower _instance;
        static public PopupTextThrower Instance
        {
            get { return _instance; }
        }

        private BitmapFont _font;

        private List<PopUpText> _items;

        static public void Init(BitmapFont font)
        {
            _instance = new PopupTextThrower(font);
        }

        protected PopupTextThrower(BitmapFont font)
        {
            _font = font;
            _items = new List<PopUpText>(32);
        }

        public void CreatePopUp(string text, Color4 startColor, Color4 endColor, Vector2 position,
                                Vector2 direction, float startSpeed, float endSpeed)
        {
            for (int i = 0; i < _items.Count; i++)
            {
                if (!_items[i].Active)
                {
                    _items[i].Set(text, startColor, endColor, position, direction,startSpeed, endSpeed);
                    _items[i].Active = true;
                    return;
                }
            }
            _items.Add(new PopUpText(text, startColor, endColor, position, direction,startSpeed, endSpeed));
        }

        public override void Update(float dt)
        {
            foreach (var popUpText in _items)
            {
                popUpText.Update(dt);
            }
        }

        public override void Draw(float dt)
        {
            foreach (var popUpText in _items)
            {
                if (popUpText.Active)
                    SpriteBatch.Instance.PrintText(_font, popUpText.Text, popUpText.Position, popUpText.Color);
            }
        }
    }
}
