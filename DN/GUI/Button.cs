﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using OpenTK;
using OpenTK.Graphics;

namespace DN.GUI
{
    public class Button:GUIObject
    {
        public string Caption;
        public event EventHandler OnPress;

        public bool Focused;
        public Color4 Color;
        public Color4 FocusedColor;


        public float Alpha = 1.0f;

        private readonly BitmapFont _captionFont;
        private readonly BitmapFont _valueFont;

        public Button(BitmapFont captionFont, BitmapFont valueFont)
        {
            _captionFont = captionFont;
            _valueFont = valueFont;
        }

        public int Value;

        public void Press()
        {
            OnPress(this, EventArgs.Empty);
        }

        public override void Update(float dt)
        {
            
        }

        public override void Draw(float dt)
        {
            Color4 clr = Focused ? FocusedColor : Color;
            clr = new Color4(clr.R, clr.G, clr.B, Alpha);

            SpriteBatch.Instance.FillRectangle(Bounds,clr , 0, new Vector2(0, 0));

            clr = Focused ? Color : FocusedColor;
            clr = new Color4(clr.R, clr.G, clr.B, Alpha *2);
            SpriteBatch.Instance.PrintText(_captionFont, Caption, Position, clr, 0, 1, 0.0f, 0.0f);
            if(Value != 0)
            SpriteBatch.Instance.PrintText(_valueFont, Value.ToString(),
                                           new Vector2(Position.X,
                                                       Position.Y + Size.Height - _captionFont.Measure(Value.ToString()).Height),
                                           clr, 0, 1, 0.0f, 0.0f);
        }
    }
}
