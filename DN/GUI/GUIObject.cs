﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OpenTK;

namespace DN.GUI
{
    public abstract class GUIObject
    {
        public GUIObject Parent = null;

        private float _x, _y;

        public float X
        {
            get
            {
                return _x  + (Parent != null? Parent.X : 0);
            }
            set
            {
                _x = value - (Parent != null ? Parent.X : 0);
            }
        }
        public float Y
        {
            get
            {
                return _y + (Parent != null ? Parent.Y : 0);
            }
            set
            {
                _y = value - (Parent != null ? Parent.Y : 0);
            }
        }

        public Vector2 Position
        {
            get { return new Vector2(X, Y); }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public Size Size;

        public virtual RectangleF Bounds
        {
            get
            {
                return new RectangleF(X, Y, Size.Width, Size.Height);
            }
        }

        public abstract void Update(float dt);
        public abstract void Draw(float dt);
    }
}
