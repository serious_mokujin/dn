﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using OpenTK;
using OpenTK.Graphics;

namespace DN.GUI
{
    class Label:GUIObject
    {
        private string _text;
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                 _text = value;
                fontSize = _font.Measure(_text);
            }
        }

        private RectangleF _bounds
        {
            get { return new RectangleF(Position.X - fontSize.Width / 2, Position.Y, fontSize.Width, fontSize.Height); }
        }


        public Color4 Color;
        public Color4 TextColor;
        private readonly BitmapFont _font;
        private SizeF fontSize;

   //     private RectangleF _bounds;

        public Label(BitmapFont font)
        {
            _font = font;
            Text = "";
        }

        public override void Update(float dt)
        {
        }

        public override void Draw(float dt)
        {
            SpriteBatch.Instance.FillRectangle(_bounds, Color);
            SpriteBatch.Instance.PrintText(_font, _text, _bounds.Location, TextColor, 0, 1, 0, 0);
        }
    }
}
