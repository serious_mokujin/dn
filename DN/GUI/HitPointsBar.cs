﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using DN.GameObjects;
using OpenTK.Graphics;

namespace DN.GUI
{
    public class HitPointsBar : GUIObject
    {
        private HealthComponent _healthComponent;
        Texture heart = CM.I.tex("heart");

        public HitPointsBar(HealthComponent healthComponent)
        {
            _healthComponent = healthComponent;
        }

        public override void Update(float dt)
        {
        }

        public override void Draw(float dt)
        {
            float c;
            int i;

            for (i = 0; i < _healthComponent.Health; i += 3)
            {
                SpriteBatch.Instance.DrawTexture(heart, X + 36 * i / 3, Y, 32, 32, Rectangle.Empty, Color.White);
            }
            c = i - _healthComponent.Health;
            if (c >= 0)
            {
                SpriteBatch.Instance.DrawTexture(heart,
                                                 X + 36 * i / 3, Y, 32, 32,
                                                 Rectangle.Empty, new Color4(1, 1, 1, 1-0.33f*c));
            }
        }
    }
}
