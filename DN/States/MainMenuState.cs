﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using DN.GameObjects;
using DN.GameObjects.Messages;
using DN.GUI;
using OpenTK;
using OpenTK.Input;

namespace DN.States
{
    public enum PlayType
    {
        OnePlayer,
        GamepadGamepad,
        KeyboardGamepad
    }

    public class MainMenuState:GameState
    {
        private GUIManager _guiManager;
        private Menu _menu;
        public MainMenuState(StateManager stateManager) : base(stateManager)
        {
        }

        internal override void LoadContent()
        {
        	_guiManager = new GUIManager();
        }

        internal override void UnloadContent()
        {
        	_guiManager.Dispose();
        }


        internal override void Init()
        {
            _menu = new Menu(CM.I.Font("consolas24"), BuildMenuTree());
            _menu.ItemChosen += MenuOnItemChosen;
            _menu.Position = new Vector2(Game.g_screenRect.Width / 2, Game.g_screenRect.Height/2);
            
            _guiManager.Add(_menu);
        }

        private void MenuOnItemChosen(GUIObject sender, string item)
        {
            switch (item)
            {
                case "One player":
                    ChangeToLevelGeneration(PlayType.OnePlayer);
                    break;
                case "Keyboard <=> GamePad":
                    ChangeToLevelGeneration(PlayType.KeyboardGamepad);
                    break;
                case "GamePad <=> GamePad":
                    ChangeToLevelGeneration(PlayType.GamepadGamepad);
                    break;
                case "Exit":
                    StateManager.Exit = true;
                    break;
            }
        }

        private void ChangeToLevelGeneration(PlayType playType)
        {
            _menu.ItemChosen -= MenuOnItemChosen;
            _guiManager.Dispose();
            StateManager.SetState(new LevelGenerationState(StateManager, playType));
        }

        internal override void Update(float dt)
        {
        	_guiManager.Update(dt);
        }

        internal override void Draw(float dt)
        {
            SpriteBatch.Instance.Begin();
            _guiManager.Draw(dt);
            SpriteBatch.Instance.End();
        }
        
        private TreeItem<String> BuildMenuTree()
        {
            var KeyboardGamepad = new TreeItem<string>("Keyboard <=> GamePad");
            var GamepadGamepad = new TreeItem<string>("GamePad <=> GamePad");
            var onePlayers = new TreeItem<string>("One player");
            var twoPlayers = new TreeItem<string>("Two players", KeyboardGamepad, GamepadGamepad);
            var newGame = new TreeItem<string>("New game", onePlayers, twoPlayers);
            var exit = new TreeItem<string>("Exit");
            var tree = new TreeItem<string>("", newGame, exit);

            return tree;
        }

        public override void Dispose()
        {
        	
            base.Dispose();
        }


    }
}
