﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Input;
using DN.LevelGeneration;
using DN.GameObjects;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;

namespace DN.States
{
    public class LevelGenerationState : GameState
    {
        private LevelGenerator levelGenerator;
        private readonly PlayType _playType;
        private GameWorld _gameWorld;

        public LevelGenerationState(StateManager stateManager, PlayType playType , GameWorld gameWorld = null)
            : base(stateManager)
        {
            _playType = playType;
            _gameWorld = gameWorld;
        }

        internal override void LoadContent(){}

        internal override void UnloadContent(){}

        private BossRoomComponent _bossRoomCreationComponent;
        private GameObjectsFabric fabric;
        internal override void Init()
        {
            if (_gameWorld == null)
                _gameWorld = new GameWorld(100, 70);
            levelGenerator = new LevelGenerator(_gameWorld, true)
                                 {
                                     RoomsMaxWidth = 5,
                                     RoomsMaxHeight = 7,
                                     RoomCount = 0,
                                     WallSmoothing = 0.5f
                                 };
            fabric = new GameObjectsFabric(_gameWorld, _gameWorld.TileMap, _gameWorld.GameObjectsManager);
            
            levelGenerator.AddComponent(new CraftingRoomCreationComponent(levelGenerator, _gameWorld, 6, 10, 6, 10, fabric));

            switch (_playType)
            {
                case PlayType.OnePlayer:
                    levelGenerator.AddComponent(new HeroCreationComponent(_gameWorld, fabric, 0, Game.g_Keyboard,
                                                                          Game.g_Mouse, Game.g_Gamepad_1));
                    break;
                case PlayType.GamepadGamepad:
                    levelGenerator.AddComponent(new HeroCreationComponent(_gameWorld, fabric, 0, null, null,
                                                                          Game.g_Gamepad_1));
                    levelGenerator.AddComponent(new HeroCreationComponent(_gameWorld, fabric, 1, null, null,
                                                                          Game.g_Gamepad_2));
                    break;
                case PlayType.KeyboardGamepad:
                    levelGenerator.AddComponent(new HeroCreationComponent(_gameWorld, fabric, 0, Game.g_Keyboard,
                                                                          Game.g_Mouse, null));
                    levelGenerator.AddComponent(new HeroCreationComponent(_gameWorld, fabric, 1, null, null,
                                                                          Game.g_Gamepad_1));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            levelGenerator.AddComponent(_bossRoomCreationComponent = new BossRoomComponent(_gameWorld, levelGenerator));
            _gameWorld.LevelComponents.Add(_bossRoomCreationComponent);

         //   var killingWalls = new KillingWallsPlacer(_gameWorld.TileMap, _gameWorld.GameObjectsManager, fabric);
        //    levelGenerator.AddComponent(killingWalls);
            levelGenerator.GenerationFinishedEvent += OnFinishGeneration;
         //   _gameWorld.LevelComponents.Add(killingWalls);

            var thread = new Thread(levelGenerator.Generate);
            thread.Name = "Level generation thread";
            thread.Start();
        }



        internal override void Update(float dt)
        {
        }
        internal override void Draw(float dt)
        {
            SpriteBatch.Instance.Begin();
            SpriteBatch.Instance.PrintText(CM.I.Font("Middle"),
                                           "Loading please wait... " + ((int) (levelGenerator.Stage) + 1)*20,
                                           new Vector2(50, 59),
                                           Color4.White, 0, 1, 0, 0);
            SpriteBatch.Instance.End();
        }


        private void OnFinishGeneration()
        {
            var heroRect = new Rectangle(_gameWorld.Heroes[0].GetComponent<BoundaryComponent>().Cell.X - 25,
                                         _gameWorld.Heroes[0].GetComponent<BoundaryComponent>().Cell.Y - 25, 50, 50);

            if (_playType == PlayType.OnePlayer)
            {
                AddSword();
                AddBow();
            }
            else
            {
                AddSword();
                AddSword();
            }

            fabric.LettersFont = CM.I.Font("Middle");



            var gog = new GameObjectGenerator(_gameWorld.TileMap, _gameWorld.GameObjectsManager, fabric,
                                              _gameWorld.Heroes, heroRect,
                                              new Rectangle(_bossRoomCreationComponent.RoomPosition.X,
                                                            _bossRoomCreationComponent.RoomPosition.Y,
                                                            _gameWorld.TileMap.Width,
                                                            _gameWorld.TileMap.Height));
            gog.AddObjects(GameObjectType.Granade, 5);
            gog.AddObjects(GameObjectType.Anvil, 0);
            gog.AddObjects(GameObjectType.Bow, 10);
            gog.AddObjects(GameObjectType.Sword, 10);
            gog.AddEnemy(EnemyType.Bat, 7);
            gog.AddEnemy(EnemyType.Troll,15);
                        
			gog.Settle();

            //_gameWorld.Heroes[0].GetComponent<BoundaryComponent>().Position =
             //   _bossRoomCreationComponent.Boss.GetComponent<BoundaryComponent>().Position;

            levelGenerator.GenerationFinishedEvent -= OnFinishGeneration;
            StateManager.SetState(new PlayState(StateManager, _gameWorld, _bossRoomCreationComponent.Boss));

            CraftingManager.Instance.Init(fabric);

        }

        private void AddSword()
        {
            var bo = fabric.CreateSword();
            bo.GetComponent<BoundaryComponent>().Cell = _gameWorld.Heroes[0].GetComponent<BoundaryComponent>().Cell;
            _gameWorld.GameObjectsManager.AddObject(bo);
        }
        private void AddBow()
        {
            var bo = fabric.CreateBow(this._gameWorld.GameObjectsManager);
            bo.GetComponent<BoundaryComponent>().Cell = _gameWorld.Heroes[0].GetComponent<BoundaryComponent>().Cell;
            _gameWorld.GameObjectsManager.AddObject(bo);
        }
    }
}
