﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using Blueberry;
using Blueberry.Animations;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Input;
using DN.GameObjects;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;

namespace DN.States
{
    class EndLevelState:GameState, IDisposable
    {
        private BitmapFont _font;
        private string _text;

        private float _fontHeight;
        FloatAnimation alphaAnim = new FloatAnimation(0, 1, 1, LoopMode.LoopWithReversingInterpolation, v =>v * v * v);
        public EndLevelState(StateManager stateManager,Statistic statistic, bool win) : base(stateManager)
        {
            var st = statistic.GetStatistic();
            if(!win)
            {
                _text = "Before your death you murdered " + st[ObjectGroup.Enemy] + " enemies";
            }
            else
            {
                _text = "On your way to escape you murdered " + st[ObjectGroup.Enemy] + " enemies";
            }

            
            Game.g_Gamepad_1.OnButtonPress += GGamepad1OnOnButtonDown;
            Game.g_Keyboard.KeyDown += GKeyboardOnKeyDown;
        }

        private void GKeyboardOnKeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                StateManager.SetState(new MainMenuState(StateManager));
            }
        }

        private void GGamepad1OnOnButtonDown(object sender, GamepadButtonFlags e)
        {
            if(e.HasFlag(GamepadButtonFlags.Start))
            {
                StateManager.SetState(new MainMenuState(StateManager));
                
            }
        }

        internal override void LoadContent()
        {
        }

        internal override void UnloadContent()
        {
        }

        internal override void Init()
        {
            _font = CM.I.Font("Middle");
            _fontHeight = _font.MeasureSymbol('#').Width;
            alphaAnim.Play();
        }

        internal override void Update(float dt)
        {
        }

        internal override void Draw(float dt)
        {
            SpriteBatch.Instance.Begin();

            SpriteBatch.Instance.FillRectangle(Game.g_screenRect, Color4.White);
            SpriteBatch.Instance.PrintText(_font, _text, new Vector2(5,5), Color4.Black, 0.0f, 1, 0.0f, 0.0f);
            SpriteBatch.Instance.PrintText(_font, "Press start or enter to continue",
                                           new Vector2(
                                               5 + Game.g_screenSize.Width/2 -
                                               _font.Measure("Press start or enter to continue").Width/2,
                                               Game.g_screenSize.Height - _fontHeight - 15),
                                           new Color4(0, 0, 0, (float)alphaAnim), 0.0f, 1, 0.0f, 0.0f);

            SpriteBatch.Instance.End();
        }

        public override void Dispose()
        {
            base.Dispose();
            alphaAnim.Dispose();
            Game.g_Gamepad_1.OnButtonPress -= GGamepad1OnOnButtonDown;
            Game.g_Keyboard.KeyDown -= GKeyboardOnKeyDown;

            Game.g_Gamepad_1.Update(0);
        }
    }
}
