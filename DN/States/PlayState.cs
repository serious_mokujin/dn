﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using Blueberry;
using Blueberry.Animations;
using Blueberry.Audio;
using Blueberry.Diagnostics;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Input;
using DN.GameObjects;
using DN.GameObjects.Components.AnimationComponents;
using DN.LevelGeneration;
using OpenTK;
using OpenTK.Audio;
using OpenTK.Graphics;
using OpenTK.Input;
using DN.GUI;
using DN.GUI.Windows;
using DN.GameObjects.Components.Controllers;
using DN.GameObjects.Components.StanceComponents;

namespace DN.States
{
    public class PlayState:GameState
    {
        private GUIManager _guiManager;
        private CraftingGUI _craftingGUI;

        private GameWorld _gameWorld;
        private readonly IGameObject _boss;
        private bool _bossDead = false;

        private bool _heroDead = false;
        private bool _restartGame;
        private BitmapFont _deathFont;

        private FloatAnimation alphaAnim;
        private FloatAnimation offsetAnim;

        private string DEATHText;
        
        public PlayState(StateManager stateManager, GameWorld gameWorld, IGameObject boss) : base(stateManager)
        {
            _gameWorld = gameWorld;
            _boss = boss;
            Game.g_Keyboard.KeyDown += GKeyboardOnKeyDown;
            Game.g_Gamepad_1.OnButtonPress += GGamepad1OnButtonPress;
            alphaAnim = new FloatAnimation(0, 1f, 6, LoopMode.None);
            alphaAnim.OnStart += (Animation<float> obj) => _gameWorld.bluring = true;

            
            offsetAnim = new FloatAnimation(-600, 600, 6, LoopMode.None, v => (q(v*2 - 1)+1)/2);
            offsetAnim.OnFinish += (Animation<float> obj) => _restartGame = true;
            InitGUI();

        }

        private void InitGUI()
        {

            _guiManager = new GUIManager();

            _craftingGUI = new CraftingGUI(_guiManager, _gameWorld.Heroes[0].GetComponent<LettersInventory>(), new Vector2(0, 0));
            _craftingGUI.Size = Game.g_screenSize;
            _guiManager.Add(_craftingGUI);
			
            for (int i = 0; i < 4; i++) 
            	if(_gameWorld.Heroes[i] != null)
                    AddHealthBar(_gameWorld.Heroes[i], i);
            PopupTextThrower.Init(CM.I.Font("Middle"));

            foreach (var hero in _gameWorld.Heroes.Where(p => p != null))
            {
                PlayerControllComponent controll = hero.GetComponent<PlayerControllComponent>();
                hero.RemoveComponent<PlayerCraftingControll>();
                hero.AddComponents(new PlayerCraftingControll(hero, _craftingGUI, controll.Keyboard, controll.Mouse, controll.Gamepad));


                var hal = hero.GetComponent<ColorComponent>();
                var playerPointer = new PlayerPointer(hero, _gameWorld.Camera, hal.Color2);
                _guiManager.Add(playerPointer);
            }

            InventoryVisualizator inventoryVisualizator =
                new InventoryVisualizator(_gameWorld.Heroes[0].GetComponent<PickupingComponent>(),
                                          CM.I.Font("monofur16Abold"))
                                          {
                                              MaxSize = new Size((int)(400 * Game.g_CameraScale),
                                                                                           (int)(256 * Game.g_CameraScale))};
            _guiManager.Add(inventoryVisualizator);

        }
        void AddHealthBar(IGameObject hero, int id)
        {
        	HealthBar healthBar = new HealthBar(hero.GetComponent<HealthComponent>(),(id+1) + " player health", CM.I.Font("monofur24bold"));
            Vector2 position = Vector2.Zero;
        	switch (id) {
            	case 0:
            		position = new Vector2(Game.g_screenRect.Left + healthBar.Size.Width/2, Game.g_screenRect.Bottom - healthBar.Size.Height/2);
            		break;
            	case 1:
            		position = new Vector2(Game.g_screenRect.Right - healthBar.Size.Width/2, Game.g_screenRect.Bottom - healthBar.Size.Height/2);
            		break;
            	case 3:
            		position = new Vector2(Game.g_screenRect.Left + healthBar.Size.Width/2, Game.g_screenRect.Top + healthBar.Size.Height/2);
            		break;
            	case 4:
            		position = new Vector2(Game.g_screenRect.Right - healthBar.Size.Width/2, Game.g_screenRect.Top + healthBar.Size.Height/2);
            		break;
            }
        	healthBar.Position = position;
            _guiManager.Add(healthBar);
        }
        float q(float x)
        {
            return (float)(4*Math.Pow(x, 3) + 3*Math.Pow(x, 2) + x + 0);
        }

        private void GGamepad1OnButtonPress(object sender, GamepadButtonFlags e)
        {
            if (_heroDead || _bossDead)
                if (e.HasFlag(GamepadButtonFlags.Start))
                    _restartGame = true;
        }

        private void GKeyboardOnKeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if(_heroDead || _bossDead)
                if(e.Key == Key.Enter)
                    _restartGame = true;
        }

        internal override void Init()
        {
            _gameWorld.InitTextures();

            //CM.I.Sound("backgroundSound").Play();
        }

        internal override void LoadContent()
        {
            _deathFont = CM.I.Font("Big");
        }

        internal override void UnloadContent()
        {
        }

        internal override void Update(float dt)
        {
            _guiManager.Update(dt);
            _gameWorld.Update(_heroDead || _bossDead ? dt/50 : dt);
            if (_gameWorld.LevelFinished || _restartGame)
            {
                StateManager.SetState(new EndLevelState(StateManager, _gameWorld.Statistic, _bossDead));
                return;
            }
            if (!_heroDead && !_bossDead)
            {
                _heroDead = _gameWorld.EveryoneIsDead();
                _bossDead = _boss.GetComponent<HealthComponent>().IsDead;
                if (_heroDead)
                {
                    PlayEndAnim();
                    DEATHText = "You are dead!";
                }
                else if (_bossDead)
                {
                    PlayEndAnim();
                    DEATHText = "You've won!";
                }
            }
        }

        private void PlayEndAnim()
        {
            alphaAnim.Play();
            offsetAnim.Play();
        }

        internal override void Draw(float dt)
        {
            _gameWorld.Draw(_heroDead || _bossDead ? dt / 10 : dt);
            if (_heroDead || _bossDead)
            {
                SpriteBatch.Instance.Begin();

                SpriteBatch.Instance.FillRectangle(Game.g_screenRect, new Color4(1, 1, 1, alphaAnim.Value));

                SpriteBatch.Instance.PrintText(_deathFont, DEATHText, Game.g_screenSize.Width / 2 + offsetAnim.Value,
                                               Game.g_screenSize.Height / 4, new Color4(0, 0, 0, 1f), 0, 1, 0.5f, 0.5f);
                SpriteBatch.Instance.End();
            }
            else
            {
                SpriteBatch.Instance.Begin();
                _guiManager.Draw(dt);
                SpriteBatch.Instance.End();
            }
            
        }

        public override void Dispose()
        {
            base.Dispose();
            alphaAnim.Dispose();
            offsetAnim.Dispose();
            Game.g_Keyboard.KeyDown -= GKeyboardOnKeyDown;
            Game.g_Gamepad_1.OnButtonPress -= GGamepad1OnButtonPress;
            _gameWorld.Dispose();
        }



    }
}
