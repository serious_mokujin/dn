﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blueberry.Particles;
using Blueberry.Particles.Shapes;
using DN.LevelGeneration;
using DN.States;
using OpenTK;
using Blueberry;
using Blueberry.Graphics;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Drawing;
using OpenTK.Input;
using OpenTK.Audio;
using Blueberry.Audio;
using System.Threading;
using Blueberry.Diagnostics;
using Blueberry.Graphics.Fonts;
using Blueberry.Input;

namespace DN
{
	class SimpleImpl:ParticleStateManager<SimpleImpl.ParticleState>, IParticleStateManager
	{
		public struct ParticleState
		{
			public Color4 Color;
			public Vector2 Position;
		}
		
		public SimpleImpl(int capacity):base(capacity)
		{
		}
		
		public unsafe void Trigger(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (ParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->Color = RandomTool.NextColor4();
                    state->Position = meta->ReleaseInfo.Position;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Update(ref ParticleIterator iterator, float deltaSeconds)
		{
			var meta = iterator.First;

            fixed (ParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;

                    state->Position = meta->ReleaseInfo.Position + meta->ReleaseInfo.Direction * meta->Age * 25;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Render(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (ParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    SpriteBatch.Instance.FillCircle(state->Position.X, state->Position.Y,2,state->Color, 5);
                }
                while (iterator.MoveNext(&meta));
            }
		}
	}
	
    public class ParticleTestGame:GameWindow
    {
    	ParticleEmitter emitter;
    	float dts;
        public ParticleTestGame(Size screenSize, bool fullscreen)
            : base(screenSize.Width, screenSize.Height, GraphicsMode.Default, "Devil's nightmare", fullscreen ? GameWindowFlags.Fullscreen : GameWindowFlags.Default)
        {
            VSync = VSyncMode.Off;
            Keyboard.KeyRepeat = false;
            Keyboard.KeyDown += Keyboard_KeyDown;
            emitter = new ParticleEmitter(100, 3, new LineShape(0f, 200, MathUtils.RotateVector2(Vector2.UnitX, 0.78f), false), new SimpleImpl(100));
            emitter.ReleaseQuantity = 5;
        }

        void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
        	emitter.Trigger(dts, new Vector2(200, 200));
        }

        protected override void OnLoad(EventArgs e)
        {
            GL.ClearColor(Color4.Black);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            float dt = (float)e.Time;
			dts += dt;
            if (Keyboard[Key.Escape])
                Exit();
            emitter.Update(dts, dt);
            base.OnUpdateFrame(e);
        }
        
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            GL.Clear(ClearBufferMask.ColorBufferBit);
            
            SpriteBatch.Instance.Begin();
            emitter.Render();
            SpriteBatch.Instance.End();
            
            float dt = (float)e.Time;

            SwapBuffers();
        }
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }
    }
}
