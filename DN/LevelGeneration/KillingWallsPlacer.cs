﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DN.GameObjects;
using Blueberry.Animations;

namespace DN.LevelGeneration
{
    public class KillingWallsPlacer:ILevelComponent
    {
        private readonly GameObjectsManager _manager;
        private readonly GameObjectsFabric _fabric;
        private readonly TileMap _tileMap;

        private List<KillingWallsControllerComponent> _controllers;
        
        private FloatAnimation _closingAnim;
        private FloatAnimation _openingAnim;

        public KillingWallsPlacer(TileMap tileMap, GameObjectsManager manager, GameObjectsFabric fabric)
        {
            _manager = manager;
            _fabric = fabric;

            _controllers = new List<KillingWallsControllerComponent>();
            _tileMap = tileMap;


        }

        public void Init()
        {
            InitializeControllers();
        }
        
        public void Update(float dt)
        {
            foreach (var item in _controllers)
            {
                item.Update(dt);
            }
        }

        private void InitializeControllers()
        {
            var tunnels = GetTunels();
            foreach (var item in tunnels)
            {
                _closingAnim = new FloatAnimation(0, 64, 1, LoopMode.None, p => p * p * p);
                _openingAnim = new FloatAnimation(64, 0, 3, LoopMode.None, p => p * p * p);

                var controller = new KillingWallsControllerComponent(_manager, _fabric, item.Item1, item.Item2,
                                                                     new Point(0, 1), _closingAnim, _openingAnim);

                controller.AddTrigger(new Point(item.Item1.X + (item.Item2.X - item.Item1.X)/2, item.Item1.Y + 1),
                                      ObjectGroup.Hero, false, ClosingWay.LeftToRight);
                controller.AddTrigger(new Point(item.Item2.X - (item.Item2.X - item.Item1.X)/2 + 1, item.Item2.Y),
                                      ObjectGroup.Hero, false, ClosingWay.RightToLeft);

                controller.Init();

                _controllers.Add(controller);
            }
        }

        private List<Tuple<Point, Point>> GetTunels()
        {
            var tunnels = new List<Tuple<Point, Point>>();

            for (int j = 1; j < _tileMap.Height - 1; j++)
            {
                for (int i = 1; i < _tileMap.Width - 1; i++)
                {
                    int tunelLength = TunelLength(i, j);
                    if (tunelLength >= 4)
                    {
                        tunnels.Add(new Tuple<Point, Point>(new Point(i, j), new Point(i + tunelLength, j)));
                        i += tunelLength;//may be wrong
                    }
                }
            }
            return tunnels;
        }

        private int TunelLength(int x, int y)
        {
            int l = 0;
            while (x < _tileMap.Width 
                   && _tileMap[x, y] == CellType.Free
                   && _tileMap[x, y + 1] == CellType.Wall
                   && _tileMap[x, y - 1] == CellType.Wall)
            {
                l++;
                x++;
            }
            return l;
        }
    }
}
