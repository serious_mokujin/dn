﻿using Blueberry;
using DN.GameObjects;
using DN.LevelGeneration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DN
{
    public class CraftingRoomCreationComponent:ILevelComponent
    {
        GameWorld _gameWorld;
        TileMap _tileMap;
        LevelGenerator _levelGenerator;
        GameObjectsFabric _fabric;

        int _minWidth;
        int _maxWidth;
        int _minHeight;
        int _maxHeight;
        private Point enterPos;
        private Point p;
        private int width;
        private int height;


        public CraftingRoomCreationComponent(LevelGenerator levelGenerator, GameWorld gameWorld,
                                             int minWidth, int maxWidth, int minHeight, int maxHeight,
                                             GameObjectsFabric gameObjectFabric)
        {
            _gameWorld = gameWorld;
            _tileMap = _gameWorld.TileMap;
            _levelGenerator = levelGenerator;
            _fabric = gameObjectFabric;

            _maxWidth = maxWidth;
            _minWidth = minWidth;
            _maxHeight = maxHeight;
            _minHeight = minHeight;
        }

        public void Init()
        {
            CreateRoom();
            if (!p.IsEmpty)
            {
                IGameObject anvil = _fabric.CreateAnvil(new Point(p.X + width / 2, (p.Y + height / 2) - 1));
                _gameWorld.GameObjectsManager.AddObject(anvil);
            }
        }

        private void CreateRoom()
        {
            
           // int width = 0, height = 0;
            int tries = 100;
            do
            {
                width = RandomTool.NextInt(_minWidth, _maxWidth);
                height = RandomTool.NextInt(_minHeight, _maxHeight);
                p = GetFreeRectangle(width, height);
            } while (p.IsEmpty && tries-- > 0);

            if (!p.IsEmpty)
            {
                _levelGenerator.AddBoundedRoom(p.X, p.Y, width, height);
                enterPos = Point.Empty;
                switch (RandomTool.NextInt(0, 3))
                {
                    case 0:
                        enterPos = new Point(width/2, 0);
                        break;
                    case 1:
                        enterPos = new Point(0, height/2);
                        break;
                    case 2:
                        enterPos = new Point(width,  height/2);
                        break;
                  //  case 3:
                  //      enterPos = new Point(width/ 2, height);
                  //      break;
                }
                _tileMap[p.X + enterPos.X, p.Y + enterPos.Y] = CellType.Free;
                _tileMap[p.X + width/2, p.Y + height/2] = CellType.Wall;
            }
        }

        public void Update(float dt)
        {
        }

        private Point GetFreeRectangle(int width, int height)
        {
            for (int i = 1; i < _tileMap.Width - 1; i++)
            {
                for (int j = 1; j < _tileMap.Height - 1; j++)
                {
                    if (CheckRectangle(i, j, width, height))
                        return new Point(i, j);
                }
            }
            return Point.Empty;
        }
        private bool CheckRectangle(int x, int y, int width, int height)
        {
            for (int i = x - 1; i <= x + width + 1; i++)
            {
                for (int j = y - 1; j <= y + height + 1; j++)
                {
                    if (!_tileMap.InRange(i, j))
                        return false;
                    if (_tileMap[i, j] != CellType.Free)
                        return false;
                }
            }
            return true;
        }
    }
}
