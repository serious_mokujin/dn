﻿using System.Collections.Generic;
using System;
using System.Drawing;
using System.Linq;
using Blueberry;
using Blueberry.Graphics;

namespace DN.LevelGeneration
{
    public enum Stage:sbyte
    {
        Nothing = -1,
        Miners = 0,
        Nature = 1,
        Adventurer = 2,
        WayChecker = 3,
        Final = 4
    }

    public delegate void FinishGenerationEventHandler();

    public class LevelGenerator
    {
        private List<ILevelComponent> _components; 

        public Stage Stage { get;private set; }
        public event FinishGenerationEventHandler GenerationFinishedEvent;

        public int RoomCount;
        public int RoomsMaxWidth;
        public int RoomsMaxHeight;

        /// <summary>
        /// percantage of chance to smooth walls, 0 - 100
        /// </summary>
        public float WallSmoothing = 50;

        internal TileMap TileMap;
        internal ResourseMap ResourseMap;
        internal int MaxAllowedWidth { get; set; }

        /// <summary>
        /// Points which has to be reached 
        /// </summary>
        internal byte[,] ImportantPointsMap;
        internal int ImportantsPointsCount;
        private List<Miner> _miners;

        internal int Width;
        internal int Height;

        public bool Fast;

        public LevelGenerator(GameWorld gameWorld, bool fast = false)
        {
            _components = new List<ILevelComponent>();

            _miners = new List<Miner>();
            TileMap = gameWorld.TileMap;

            Fast = fast;
        }

        public void Generate()
        {
            Width = TileMap.Width;
            Height = TileMap.Height;
            ImportantPointsMap = new byte[Width,Height];
            MaxAllowedWidth = TileMap.Width - TileMap.Width/3;

            Stage = Stage.Nothing;
            _miners.Clear();

            ResourseMap = new ResourseMap(Width, Height);
            do
            {
                UpdateMiners();
                if (_miners.Count == 0)
                {
                    Stage += 1;
                    SetCurrentStage();
                    if (Stage == Stage.Final)
                    {
                        GenerationFinishedEvent();
                        _miners.Clear();
                        break;
                    }
                }
            } while (true);
        }

        public void UpdateComponents()
        {
            foreach (var levelGenerationComponent in _components)
                levelGenerationComponent.Init();
        }

        public void AddComponent(ILevelComponent component)
        {
            _components.Add(component);
        }
        
        private void SetCurrentStage()
        {
            Point p = Point.Empty;
            switch (Stage)
            {
                case Stage.Miners:
                    ResourseMap = new ResourseMap(Width, Height);

                    TileMap.FillWith(CellType.Wall);
                    var m = new Miner(this, 4, Height - 2);
                    m.Init();
                    _miners.Add(m);

                    m = new Miner(this, MaxAllowedWidth - 4, Height - 2);
                    _miners.Add(m);
                    m.Init();
                    break;
                case Stage.Nature:
                    
                    for (int i = 0; i < RoomCount; i++)
                        AddRoomAtRandomPosition();

                    int height = RandomTool.NextInt(2, Height - 2);

                    MakeConnectionX(TileMap.GetLeftCell(height, CellType.Free),
                                   TileMap.GetRightCell(height, CellType.Free));

                    SmoothWalls();

                    UpdateComponents();
                    DetermineImportantPoints();

                    break;
                case Stage.Adventurer:
                    p = GetFreeGroundCell();
                    var adv = new Adventurer(this, p.X, p.Y);
                    _miners.Add(adv);
                    break;
                case Stage.WayChecker:
                    if (Fast)
                        break;
                    p = GetFreeGroundCell();
                    Stage = Stage.WayChecker;
                    _miners.Add(new WayChecker(this, p.X, p.Y));
                    break;
            }
        }


        public bool InRange(int x, int y)
        {
            return x > 0 && y > 0 && x < Width - 1 && y < Height - 1 && x < MaxAllowedWidth;
        }

        private void SmoothWalls()
        {
            for (int i = 2; i < TileMap.Width - 2; i++)
                for (int j = 2; j < TileMap.Height - 2; j++)

                    if (TileMap[i, j] == CellType.Wall)
                    {
                        int wallCount = GetCellCountAround(i, j, CellType.Wall);
                        if (wallCount == 4)
                            if (RandomTool.NextBool(WallSmoothing))
                                TileMap[i, j] = CellType.Free;
                    }
        }

        internal void MakeConnectionX(Point p1, Point p2)
        {
            for (int i = p1.X; i < p2.X; i++)
            {
                TileMap[i, p1.Y] = CellType.Free;
            }
        }
        internal void MakeConnectionY(Point p1, Point p2)
        {
            for (int i = p1.Y; i < p2.Y; i++)
            {
                TileMap[p1.X, i] = CellType.Free;
            }
        }

        private void UpdateMiners()
        {
            foreach (var miner in _miners)
                miner.Step();

            for (int i = 0; i < _miners.Count; i++)
                if (_miners[i].Cell.Y <= 0)
                {
                    _miners[i].Remove();
                    _miners.Remove(_miners[i]);
                    i--;
                }
        }

        private void DetermineImportantPoints()
        {
            for (int i = 1; i < MaxAllowedWidth - 1; i++)
                for (int j = 1; j < TileMap.Height - 1; j++)
                    if (TileMap[i, j] == CellType.Free && TileMap[i, j + 1] == CellType.Wall)
                    {
                        ImportantPointsMap[i, j] = 1;
                        ImportantsPointsCount++;
                    }

            
            //for (int i = 1; i < TileMap.Width - 1; i++)
            //    for (int j = 1; j < TileMap.Height - 1; j++)
            //    {
            //        if (CalculateLineLength(ImportantPointsMap, i, j) < 2)
            //        {
            //            ImportantPointsMap[i, j] = 0;
            //        }
            //    }

        }

        private int CalculateLineLength(byte[,] _map, int x, int y)
        {
            int i = 0;
            while (_map[x + i, y] == 1)
                i++;

            int j = 0;
            while (_map[x - j - 1, y] == 1)
                j--;

            return i + j;
        }


        private int GetCellCountAround(int x , int y, CellType type = CellType.Free)
        {
            int count = 0;

            for (int i = x - 1; i <= x + 1; i++)
                for (int j = y - 1; j <= y + 1; j++)
                    if (TileMap[i, j] == type)
                        count++;
            return count;
        }

        private void AddRoomAtRandomPosition()
        {
            int x, y, width, height;

            do
            {
                width = RandomTool.NextInt(0, RoomsMaxWidth);
                height = RandomTool.NextInt(0, RoomsMaxHeight);
                x = RandomTool.NextInt(2, Width - width - 2);
                y = RandomTool.NextInt(2, Height - height - 2);
            } while (TileMap[x, y] == CellType.Wall);
            AddRoom(x, y, width, height);
        }

        internal void AddRoom(int x, int y, int width, int height)
        {
            for (int i = x; i <= x + width; i++)
                for (int j = y; j <= y + height; j++)
                    TileMap[i, j] = CellType.Free;
        }

        internal void AddBoundedRoom(int x, int y, int width, int height)
        {
            for (int i = x; i <= x + width; i++)
                for (int j = y; j <= y + height; j++)
                {
                    if(TileMap.InRange(i, j))
                    TileMap[i, j] = CellType.Free;
                }

            Point pos = new Point(0, 0);
            Point dir = new Point(1, 0);
            int P = width * 2 + height * 2;
            do
            {
                TileMap[pos.X + x, pos.Y + y] = CellType.Wall;
                if (pos.X + dir.X > width)
                {
                    dir.Y = 1;
                    dir.X = 0;
                }
                else if (pos.X + dir.X < 0)
                {
                    dir.Y = -1;
                    dir.X = 0;
                }
                if (pos.Y + dir.Y > height)
                {
                    dir.X = -1;
                    dir.Y = 0;
                }
                pos.X += dir.X;
                pos.Y += dir.Y;
                //else if (pos.Y + dir.Y < 0)
                //{
                //    dir.X = 1;
                //    dir.Y = 0;
                //}

            } while (P-->0);
        }

        private Point GetFreeGroundCell()
        {
            while (true)
            {
                var p = new Point(RandomTool.NextInt(1, TileMap.Width - 1),
                                    RandomTool.NextInt(1, TileMap.Height - 1));

                if (TileMap[p.X, p.Y] == CellType.Free && TileMap[p.X, p.Y + 1] == CellType.Wall)
                    return p;
            }
        }

        public void PrintDebug()
        {
            for (int j = 0; j < Height; j++)
            {
                Console.WriteLine();
                for (int i = 0; i < Width; i++)
                {
                    Console.Write((byte)TileMap[i, j]);
                }
            }
        }
    }
}
