﻿using System.Drawing;
using Blueberry;
using Blueberry.Input;
using DN.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Input;

namespace DN.LevelGeneration
{
    public class HeroCreationComponent:ILevelComponent
    {
        private GameObjectsFabric _fabric;
        private readonly int _index;
        private readonly KeyboardDevice _keyboardDevice;
        private readonly MouseDevice _mouseDevice;
        private readonly GamepadDevice _gamepadDevice;
        public GameWorld _gameWorld { get; set; }

        public HeroCreationComponent(GameWorld gameWorld, GameObjectsFabric fabric, int index,
                                     KeyboardDevice keyboardDevice, MouseDevice mouseDevice, GamepadDevice gamepadDevice )
        {
            _gameWorld = gameWorld;
            _fabric = fabric;
            _index = index;
            _keyboardDevice = keyboardDevice;
            _mouseDevice = mouseDevice;
            _gamepadDevice = gamepadDevice;
        }

        public void Init()
        {
            var cell = _gameWorld.TileMap.GetBottomFreeCell();

            Color color1 = RandomTool.NextColor(new ColourRange
                                                   {
                                                       Red = new Range(128, 255),
                                                       Blue = new Range(128, 255),
                                                       Green = new Range(128, 255)
                                                   });
            Color color2 = RandomTool.NextColor(new ColourRange
            {
                Red = new Range(128, 255),
                Blue = new Range(128, 255),
                Green = new Range(128, 255)
            });

            _gameWorld.InsertHero(_index, _fabric, cell, color1, color2, _keyboardDevice, _mouseDevice, _gamepadDevice);
        }

        public void Update(float dt)
        {
            
        }

    }
}
