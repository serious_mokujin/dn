﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry.Animations;
using DN.GameObjects;
using OpenTK;

namespace DN.LevelGeneration
{
    public enum ClosingWay
    {
        LeftToRight,
        RightToLeft
    }

    public enum State
    {
        Opened,
        Closed,
        Closing,
        Opening
    }

    public class KillingWallsControllerComponent:ILevelComponent
    {
        private readonly GameObjectsManager _manager;
        private readonly GameObjectsFabric _fabric;

        public readonly Point StartPoint;
        public readonly Point EndPoint;

        private readonly Point _closingDirection;

        private readonly FloatAnimation _closingAnim;
        private readonly FloatAnimation _openingAnim;

        private List<BoundaryComponent> _walls;
        private Dictionary<IGameObject, ClosingWay> _triggerDictionary;

        private State _state;
        private ClosingWay _closingWay;

        private List<Vector2> _wallBasePosition;
        private float _offset;
        private Vector2 NextWallPosition(int i)
        {
            return new Vector2(_wallBasePosition[i].X + _offset * _closingDirection.X,
                               _wallBasePosition[i].Y + _offset * _closingDirection.Y);
        }
        private int _currentWall;

        public KillingWallsControllerComponent(GameObjectsManager manager, GameObjectsFabric fabric,
                                               Point startPoint, Point endPoint, Point closingDirection,
                                               FloatAnimation closingAnim, FloatAnimation openingAnim)
        {
            _manager = manager;
            _fabric = fabric;

            StartPoint = startPoint;
            EndPoint = endPoint;

            _closingDirection = closingDirection;

            _closingAnim = closingAnim;
            _closingAnim.OnFinish += new Action<Animation<float>>(_closingAnimOnFinish);

            _openingAnim = openingAnim;
            _openingAnim.OnFinish += new Action<Animation<float>>(_openingAnim_OnFinish);

            _triggerDictionary = new Dictionary<IGameObject, ClosingWay>();
        }

        void _openingAnim_OnFinish(Animation<float> obj)
        {
            _state = State.Opened;
            _currentWall = 0;
        }

        void _closingAnimOnFinish(Animation<float> obj)
        {
            if(_closingWay == ClosingWay.LeftToRight)
                _currentWall++;
            else
                _currentWall--;

            if (_walls.Count <= _currentWall || _currentWall == -1)
            {
                _openingAnim.Play();
                _closingAnim.Stop();
                _state = State.Opening;
                return;
            }
            _closingAnim.Reset();
            _closingAnim.Play();

        }

        public void AddTrigger(Point p, ObjectGroup group, bool destroyOnCollision, ClosingWay closingWay)
        {
            IGameObject trigger = _fabric.CreateCellCollisionTriger(p, group, EventOnCollision, destroyOnCollision);
            _triggerDictionary.Add(trigger, closingWay);
            _manager.AddObject(trigger);
        }

        private void EventOnCollision(object sender, EventArgs eventArgs)
        {
            if(_state == State.Opening || _state == State.Closing || _state == State.Closed)
                return;
            IGameObject gameObject = (IGameObject) sender;

            _state = State.Closing;
            _closingWay = _triggerDictionary[gameObject];

            _currentWall = _closingWay == ClosingWay.LeftToRight ? 0 
                                                                 : _walls.Count - 1;

            _closingAnim.Play();

        }

        public void Init()
        {
            _walls = new List<BoundaryComponent>();
            _wallBasePosition = new List<Vector2>();
            for (int i = StartPoint.X; i <= EndPoint.X; i++)
            {
                for (int j = StartPoint.Y; j <= EndPoint.Y; j++)
                {
                    var wall = _fabric.CreateWallObject(new Point(i, j), false);
                    var b = wall.GetComponent<BoundaryComponent>();

                    _walls.Add(b);
                    _wallBasePosition.Add(b.Position);
                    _manager.AddObject(wall);
                }
            }
        }

        public void Update(float dt)
        {
            switch (_state)
            {
                case State.Closing:
                 //   _closingAnim.Animate(dt);
                    _offset = (float)_closingAnim;
                    _walls[_currentWall].Position = NextWallPosition(_currentWall);
                    break;
                case State.Opening:
                 //   _openingAnim.Animate(dt);
                    _offset = (float)_openingAnim;
                    for (int i = 0; i < _walls.Count; i++)
                        _walls[i].Position = NextWallPosition(i);
                    break;
            }

        }
    }
}
