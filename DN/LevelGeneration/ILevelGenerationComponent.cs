﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.LevelGeneration
{
    public interface ILevelGenerationComponent
    {
        void DoWork();
    }
}
