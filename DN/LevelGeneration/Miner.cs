﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blueberry;

namespace DN.LevelGeneration
{
    internal class Miner
    {
        public Point Cell
        {
            get { return _cell; }
        }

        public float ChanseToChangeSize = 0.5f;
        public int MaxSize = 2;
        public int MinSize = 0;

        private int _size = 1;

        protected byte[,] _exploredMap;

        protected Point _cell;
        protected Point _direction;

        protected readonly LevelGenerator _levelGenerator;

        public Miner(LevelGenerator levelGenerator, int x, int y)
        {
            _cell.X = x;
            _cell.Y = y;
            _levelGenerator = levelGenerator; 
        }

        public virtual void Init()
        {
            _exploredMap = new byte[_levelGenerator.Width, _levelGenerator.Height];
            _exploredMap[_cell.X, _cell.Y] = 1;
        }

        public virtual void Remove()
        {
        }

        public virtual void Step()
        {
            _direction = GetDirection();
            MoveInDirection();

            _levelGenerator.ResourseMap.GatherResourses(this);
            if (_cell.Y > 0)
            {
                RemoveTiles();
            }

            if (RandomTool.NextBool(ChanseToChangeSize))
                _size = RandomTool.NextInt(MinSize, MaxSize);
        }

        private void RemoveTiles()
        {
            for (int i = -_size; i <= _size; i++)
                for (int j = -_size; j <= _size; j++)
                    if(_levelGenerator.InRange(_cell.X + i, _cell.Y + j))
                        _levelGenerator.TileMap[_cell.X + i, _cell.Y + j] = CellType.Free;
        }

        private void MoveInDirection()
        {
            _cell.X += _direction.X;
            _cell.Y += _direction.Y;
            while (_direction.X != 0)
            {
                _exploredMap[_cell.X - _direction.X, _cell.Y] += 1;
                _exploredMap[_cell.X - _direction.X, _cell.Y] *= 4;
                _direction.X -= Math.Sign(_direction.X);
            }
            while (_direction.Y != 0)
            {
                _exploredMap[_cell.X, _cell.Y - _direction.Y] += 1;
                _exploredMap[_cell.X, _cell.Y - _direction.Y] *= 4;
                _direction.Y -= Math.Sign(_direction.Y);
            }

        }

        /// <returns>returns non diagonal direction</returns>
        private Point GetDirection()
        {
            var res = new int[4];
            var p = new Point[4];

            Point dir = Point.Empty;

            res[0] = GetCellPrice(0, -1, ref dir);
            p[0] = new Point(0, -1 + dir.Y);

            res[1] = GetCellPrice(0, 1, ref dir);
            p[1] = new Point(0, 1 + dir.Y);

            res[2] = GetCellPrice(-1, 0, ref dir);
            p[2] = new Point(-1 + dir.X, 0);

            res[3] = GetCellPrice(1, 0, ref dir);
            p[3] = new Point(1 + dir.X, 0);

            int max = 0;
            if (res[0] == res[1] && res[0] == res[2] && res[0] == res[3])
                max = RandomTool.NextByte(0, 3);
            else
                for (int i = 0; i < 4; i++)
                    if (res[i] != Int32.MinValue)
                        if (res[i] > res[max])
                            max = i;
            return p[max];
        }

        private int GetCellPrice(int offsetX, int offsetY, ref Point direction)
        {
            var p = new Point(_cell.X + offsetX, _cell.Y + offsetY);
            int cellPriceSum = 0;


            direction = Point.Empty;

            if (p.Y <= 0)
                return Int32.MaxValue;
            if (!_levelGenerator.InRange(p.X, p.Y))
                return Int32.MinValue;
            if (!_levelGenerator.InRange(p.X, p.Y + 1))
                return Int32.MinValue;

            try
            {
                while (_levelGenerator.TileMap[p.X + direction.X, p.Y + direction.Y] == CellType.Free)
                {
                    if(!_levelGenerator.InRange(p.X + direction.X + offsetX, p.Y + direction.Y + offsetY))
                        break;
                    direction.X += offsetX;
                    direction.Y += offsetY;
                    cellPriceSum += _exploredMap[p.X + direction.X, p.Y + direction.Y];
                    //        PrintDebug(p.X + direction.X, p.Y + direction.Y);
                }
            }
            catch (Exception)
            {
               PrintDebug();
            }

            if (p.Y + direction.Y <= 0)
                return Int32.MaxValue;



            if (p.X + direction.X <= 1 || p.X + direction.X >= _levelGenerator.Width - 1 
                || p.Y +direction.Y >= _levelGenerator.Height - 1)
                return Int32.MinValue;


            return  _levelGenerator.ResourseMap.GetAvarageTile(p.X + direction.X, p.Y + direction.Y) -
                    _exploredMap[p.X + direction.X, p.Y + direction.Y] - cellPriceSum;
        }

        public void PrintDebug()
        {
            Console.Clear();
            _levelGenerator.TileMap.PrintDebug();
            Console.SetCursorPosition(_cell.X, _cell.Y + 1);
            Console.Write('X');
            Console.ReadKey();
        }
        public void PrintDebug(int x, int y)
        {
            Console.Clear();
            _levelGenerator.TileMap.PrintDebug();
            Console.SetCursorPosition(_cell.X, _cell.Y + 1);
            Console.Write('X');
            Console.SetCursorPosition(x, y + 1);
            Console.Write('#');
            Console.ReadKey();
        }
    }
}
