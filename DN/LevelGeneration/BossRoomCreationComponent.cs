﻿using Blueberry.Animations;
using DN.GameObjects.Messages;
using OpenTK;

namespace DN.LevelGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using Blueberry;
    using DN.GameObjects;

    internal class BossRoomComponent : ILevelComponent
    {
        public IGameObject Boss { get; private set; }
        private readonly GameWorld _gameWorld;
        private readonly LevelGenerator _levelGenerator;
        private List<IGameObject> _closingWallList;
        private List<BoundaryComponent> _closingWallBoundaryList;
        private int _currentWall;
        private FloatAnimation _closingWallAnim;


        public Point RoomPosition;
        private int w;
        private int h;
        private Vector2 _wallBasePos;
        private int _tunelX;
        private int entranceX;
        private int entranceY;

        private AnimationManager _animationManager;

        public BossRoomComponent(GameWorld gameWorld, LevelGenerator levelGenerator)
        {
            _gameWorld = gameWorld;
            _levelGenerator = levelGenerator;
            _closingWallBoundaryList = new List<BoundaryComponent>(32);
            _closingWallList = new List<IGameObject>(32);
            _closingWallAnim = new FloatAnimation(0, 64, 1.0f, LoopMode.None,
                                           v => v * v * v);
            _currentWall = 0;
            _closingWallAnim.OnFinish += animation =>
                                             {
                                                 BoundaryComponent cur =
                                                     _closingWallBoundaryList[_closingWallBoundaryList.Count - _currentWall - 1];
                                                 gameWorld.GameObjectsManager.RemoveObject(
                                                     _closingWallList[_closingWallBoundaryList.Count - _currentWall - 1]);

                                                 _gameWorld.TileMap[cur.Cell.X, cur.Cell.Y] = CellType.Wall;

                                                 _currentWall++;
                                                 if (_currentWall < _closingWallBoundaryList.Count)
                                                     animation.Play();
                                             };
         //   _animationManager = new AnimationManager(false);
        }

        private float Sqr(float f)
        {
            return f*f;
        }

        public void Init()
        {
            CreateRoomOnMap();

            var fabric = new GameObjectsFabric(_gameWorld, _gameWorld.TileMap, _gameWorld.GameObjectsManager);
            fabric.LettersFont = CM.I.Font("Middle");
            CreateClosingWalls(fabric);
            CreateOgre(fabric);
            CreateSpikes(fabric);
        }

        public void Update(float dt)
        {
            if (_closingWallAnim.State == PlaybackState.Play)
            {
                _closingWallBoundaryList[_closingWallBoundaryList.Count - _currentWall - 1].Position.Y = _wallBasePos.Y + (float)_closingWallAnim;
               // _closingWallAnim.Animate(dt);

            }
        }
        
        private void CreateRoomOnMap()
        {
            var tileMap = _levelGenerator.TileMap;
            byte[,] bossRoom = new byte[,]
                                   {
                                       {0, 0, 0, 0, 2, 0, 0, 0, 1},
                                       {0, 0, 0, 0, 2, 2, 2, 2, 1},
                                       {0, 0, 0, 0, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 0, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 0, 0, 1, 0, 0, 1},
                                       {0, 0, 0, 0, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 2, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 2, 0, 2, 2, 0, 1},
                                       {0, 0, 0, 2, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 2, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 2, 0, 1, 0, 0, 1},
                                       {0, 0, 0, 0, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 0, 0, 0, 0, 0, 1},
                                       {0, 0, 0, 0, 2, 0, 2, 0, 1},
                                   };
            w = bossRoom.GetLength(0);
            h = bossRoom.GetLength(1);

            RoomPosition = Point.Empty;
            do
            {
                int Y = RandomTool.NextInt(0, tileMap.Height - h);
                RoomPosition = tileMap.GetRightCell(Y, CellType.Free);
            } while ((RoomPosition.X == 0 || RoomPosition.Y == 0) && !tileMap.InRange(RoomPosition.X + w, RoomPosition.Y));

            //RoomPosition = tileMap.GetRightCell(CellType.Free);


            RoomPosition.X = tileMap.Width - w - 1; 

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    tileMap[i + RoomPosition.X, j + RoomPosition.Y] = (CellType)bossRoom[i, j];
                }
            }


            entranceX = RoomPosition.X - 1;
            entranceY = RoomPosition.Y + h - 2;

            int x = entranceX;
            int y = entranceY; 
            _levelGenerator.MaxAllowedWidth = entranceX + 1;

            while(tileMap[x, y] == CellType.Wall)
            {
                tileMap[x--, y] = CellType.Free;
                _tunelX++;
            }

            ClearEverythingUnderRoom(tileMap);
        }

        private void ClearEverythingUnderRoom(TileMap tileMap)
        {
            for (int j = RoomPosition.Y + h; j < tileMap.Height; j++)
                for (int i = RoomPosition.X; i < RoomPosition.X + w; i++)
                    tileMap[i, j] = CellType.Free;
        }

        private void CreateClosingWalls(GameObjectsFabric fabric)
        {
            int i = entranceX;
            while(i >= 0 && _gameWorld.TileMap[i, entranceY - 1] == CellType.Wall 
                         && _gameWorld.TileMap[i, entranceY] != CellType.Wall)
            {
                var wall = fabric.CreateWallObject(new Point(i, entranceY));
                _gameWorld.GameObjectsManager.AddObject(wall);

                var wallBoundary = wall.GetComponent<BoundaryComponent>();
                wallBoundary.Position.Y -= 32;
                wallBoundary.Position.X += 32;

                _wallBasePos = wallBoundary.Position;
                _closingWallBoundaryList.Add(wallBoundary);
                _closingWallList.Add(wall);
                i--;
            }

            IGameObject trigger = fabric.CreateCellCollisionTriger(new Point(entranceX - _tunelX/2, entranceY), ObjectGroup.Hero,
                                                                   OnCollisionWithTrigger);
            var b = trigger.GetComponent<BoundaryComponent>();
            b.Position += new Vector2(b.Size.Width/2, b.Size.Height/2);
            _gameWorld.GameObjectsManager.AddObject(trigger);
        }



        private void CreateOgre(GameObjectsFabric fabric)
        {
            Boss = fabric.CreateEnemy(EnemyType.Ogre, new Point(RoomPosition.X + w/2, RoomPosition.Y + h/2), _gameWorld.Heroes);
            _gameWorld.GameObjectsManager.AddObject(Boss);
        }

        private void CreateSpikes(GameObjectsFabric fabric)
        {
            IGameObject sp = fabric.CreateSpikes(Point.Empty);
            BoundaryComponent boundary = sp.GetComponent<BoundaryComponent>();


            for (int cellX = RoomPosition.X; cellX < RoomPosition.X + w; cellX+= 2)
            {
                for (int i = 0; i < 64; i += (int)boundary.Size.Width)
                {
                    IGameObject spike = fabric.CreateSpikes(new Point(cellX, RoomPosition.Y));
                    spike.GetComponent<BoundaryComponent>().Position = new Vector2(cellX * 64 + i + boundary.Size.Width/2,
                                                                                   RoomPosition.Y * 64 + boundary.Size.Height/2);
                    _gameWorld.GameObjectsManager.AddObject(spike);
                }
            }
        }
        private void OnCollisionWithTrigger(object sender, EventArgs eventArgs)
        {
            _closingWallAnim.Play();
        }
    }
}
