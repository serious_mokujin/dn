﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DN.GameObjects;

namespace DN.LevelGeneration
{
    public class GameObjectGenerator
    {
        private readonly TileMap _tileMap;
        private readonly GameObjectsManager _gameObjectsManager;
        private readonly GameObjectsFabric _fabric;
        private readonly Rectangle[] _restrictionBounds;

        private readonly IGameObject[] _targets;

        public Dictionary<GameObjectType, int> _config;
        public Dictionary<EnemyType, int> _enemiesConfig;

        public GameObjectGenerator(TileMap tileMap, GameObjectsManager gameObjectsManager,
                             GameObjectsFabric fabric, IGameObject[] targets, params Rectangle[] restrictionBounds)
        {
            _tileMap = tileMap;
            _gameObjectsManager = gameObjectsManager;
            _fabric = fabric;
            _restrictionBounds = restrictionBounds;
            _targets = targets;

            _config =  new Dictionary<GameObjectType, int>();
            _config.Clear();
            _enemiesConfig = new Dictionary<EnemyType, int>();
            _enemiesConfig.Clear();
        }

        public void AddObjects(GameObjectType type, int count)
        {
            _config.Add(type, count);
        }
        public void AddEnemy(EnemyType type, int count)
        {
            _enemiesConfig.Add(type, count);
        }

        public void Settle()
        {
            foreach (var kvp in _config)
                for (int i = 0; i < kvp.Value; i++)
                {
                    Point p;
                    bool ok = false;
                    do
                    {
                        p = _tileMap.GetRandomPoint();
                        
                        foreach (var b in _restrictionBounds)
                        {
                            if (!b.Contains(p))
                            {
                                ok = true;
                            }
                            else
                            {
                                ok = false;
                                break;
                            }
                        }
                    } while (!ok);

                    var gameObject = _fabric.CreateObject(kvp.Key, p);
                    _gameObjectsManager.AddObject(gameObject);
                }
            foreach (var kvp in _enemiesConfig)
                for (int i = 0; i < kvp.Value; i++)
                {
                    Point p;
                    bool ok = false;
                    do
                    {
                        p = _tileMap.GetRandomPoint();
                        foreach (var b in _restrictionBounds)
                        {
                            if (!b.Contains(p))
                            {
                                ok = true;
                            }
                            else
                            {
                                ok = false;
                                break;
                            }
                        }
                    } while (!ok);

                    var gameObject = _fabric.CreateEnemy(kvp.Key, p, _targets);
                    _gameObjectsManager.AddObject(gameObject);
                }
        }
    }
}
