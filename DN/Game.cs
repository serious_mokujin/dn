﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.LevelGeneration;
using DN.States;
using OpenTK;
using Blueberry;
using Blueberry.Graphics;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Drawing;
using OpenTK.Input;
using OpenTK.Audio;
using Blueberry.Audio;
using System.Threading;
using Blueberry.Diagnostics;
using Blueberry.Graphics.Fonts;
using Blueberry.Input;
using Blueberry.Animations;


namespace DN
{
    public class Game:BlueberryGame
    {
        #region GLOBAL
        
        public static Size g_screenSize;
        public static Rectangle g_screenRect;

        public static KeyboardDevice g_Keyboard;
        public static MouseDevice g_Mouse;
        public static GamepadDevice g_Gamepad_1;
        public static GamepadDevice g_Gamepad_2;
        public static double g_TotalTime = 0;
        public static Size g_screenCapacity = new Size(15, 8);
        public static float g_CameraScale;

        #endregion

        private StateManager _stateManager;
        private AnimationManager _animationManager;

        public Game(Size screenSize, bool fullscreen)
            : base(screenSize.Width, screenSize.Height, "Devil's nightmare", fullscreen, 1.0)
        {
            g_screenSize = screenSize;
            g_screenRect = new Rectangle(0, 0, screenSize.Width, screenSize.Height);
            g_CameraScale = (g_screenSize.Width / 64.0f) / g_screenCapacity.Width;

            VSync = fullscreen ? VSyncMode.On : VSyncMode.Off;
            Keyboard.KeyRepeat = false;
            _animationManager = new AnimationManager(false);
        }

        protected override void Load()
        {
            GL.ClearColor(Color4.Black);
            LoadContent();
            g_Keyboard = Keyboard;
            g_Mouse = Mouse;
            g_Gamepad_1 = new GamepadDevice(UserIndex.One);
            g_Gamepad_2 = new GamepadDevice(UserIndex.Two);
            _stateManager = new StateManager();
            _stateManager.SetState(new MainMenuState(_stateManager));
      //      DiagnosticsCenter.Instance.Add(SpriteBatch.Instance);
        }


        private void LoadContent()
        {
            CM.I.LoadFont("Big", Path.Combine("Content", "Fonts", "monofur.ttf"), 48);
            CM.I.LoadFont("Middle", Path.Combine("Content", "Fonts", "monofur.ttf"), 24);
            CM.I.LoadFont("Small", Path.Combine("Content", "Fonts", "monofur.ttf"), 14);
            CM.I.LoadFont("speculum16", Path.Combine("Content", "Fonts", "speculum.ttf"), 16);
            //CM.I.LoadFont("consolas24", Path.Combine("Content", "Fonts", "consola.ttf"), 24*g_CameraScale, FontStyle.Regular);
            CM.I.LoadFont("consolas24", Path.Combine("Content", "Fonts", "consola.ttf"), FontStyle.Regular, 42, g_CameraScale, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789<=>");

            CM.I.LoadFont("consolas9", Path.Combine("Content", "Fonts", "consola.ttf"), FontStyle.Regular, 16, g_CameraScale, "arow");
            CM.I.LoadFont("consolas16", Path.Combine("Content", "Fonts", "consola.ttf"), FontStyle.Regular, 21, g_CameraScale, "abtswordA");
            CM.I.LoadFont("consolas25bold", Path.Combine("Content", "Fonts", "consola.ttf"), FontStyle.Bold, 48, g_CameraScale, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			//CM.I.LoadFont("consolas72bold", Path.Combine("Content", "Fonts", "consola.ttf"), 72, FontStyle.Bold);
            CM.I.LoadFont("consolas72bold", Path.Combine("Content", "Fonts", "consola.ttf"), FontStyle.Bold, 42*3, g_CameraScale, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
            CM.I.LoadFont("monofur16bold", Path.Combine("Content", "Fonts", "monofur.ttf"), 16, FontStyle.Bold);
            CM.I.LoadFont("monofur24bold", Path.Combine("Content", "Fonts", "monofur.ttf"), 24, FontStyle.Bold);
            CM.I.LoadFont("monofur16Abold", Path.Combine("Content", "Fonts", "monofur.ttf"), 24*g_CameraScale, FontStyle.Italic);

            CM.I.LoadSound("swordA", Path.Combine("Content", "Sounds", "steelsword.ogg"));
            CM.I.LoadSound("swordB", Path.Combine("Content", "Sounds", "wv-sword.ogg"));

            CM.I.LoadTexture("wall_tile", TextureFabric.PredrawWallTexture(g_CameraScale));

            CM.I.LoadTexture("stair_tile", TextureFabric.PredrawStairTexture(g_CameraScale));
            CM.I.LoadTexture("sword_sprite", TextureFabric.PredrawSwordTexture(g_CameraScale));
            CM.I.LoadTexture("potion", Path.Combine("Content", "Textures", "GameObjects", "Potion.png"));
            CM.I.LoadTexture("arrow_sprite", TextureFabric.PredrawArrowTexture(g_CameraScale));
            CM.I.LoadTexture("bow_sprite", Path.Combine("Content", "Textures", "Weapons", "Bow.png"));
            CM.I.LoadTexture("chest_sprite", Path.Combine("Content", "Textures", "GameObjects", "Chest.png"));
            CM.I.LoadTexture("door_sprite", Path.Combine("Content", "Textures", "GameObjects", "Door.png"));
            CM.I.LoadTexture("spikes_sprite", Path.Combine("Content", "Textures", "GameObjects", "Spikes.png"));
            CM.I.LoadTexture("anvil_sprite", Path.Combine("Content", "Textures", "GameObjects", "Anvil.png"));
            CM.I.LoadTexture("OgreHealthBar", Path.Combine("Content", "Textures", "GameObjects", "VerticalSpikes.png"));//wtf???
            CM.I.LoadTexture("bomb_sprite", Path.Combine("Content", "Textures", "GameObjects", "Bomb.png"));
            
            CM.I.LoadSound("backgroundSound", Path.Combine("Content", "Sounds", "rainfall.ogg"));
            CM.I.LoadSound("drinkPotion", Path.Combine("Content", "Sounds", "water-gulp.ogg"));
            CM.I.LoadSound("bowStreches", Path.Combine("Content", "Sounds", "bow-streches.ogg"));
            CM.I.LoadSound("bowShoot", Path.Combine("Content", "Sounds", "bow-shoot.ogg"));
            CM.I.LoadSound("earthquake", Path.Combine("Content", "Sounds", "earthquake.ogg"));
            CM.I.LoadSound("explosion", Path.Combine("Content", "Sounds", "explosion.ogg"));
            CM.I.LoadSound("heroFall", Path.Combine("Content", "Sounds", "hero-fall.ogg"));
            CM.I.LoadSound("bat1", Path.Combine("Content", "Sounds", "bat_chrip_1.ogg"));
            CM.I.LoadSound("bat2", Path.Combine("Content", "Sounds", "bat_chrip_2.ogg"));
            CM.I.LoadSound("bat3", Path.Combine("Content", "Sounds", "bat_chrip_3.ogg"));
            CM.I.LoadSound("bat4", Path.Combine("Content", "Sounds", "bat_chrip_4.ogg"));
            CM.I.LoadSound("bat5", Path.Combine("Content", "Sounds", "bat_chrip_5.ogg"));
        }


        protected override void Update(float dt)
        {
            g_TotalTime += dt;

            g_Gamepad_1.Update(dt);
            g_Gamepad_2.Update(dt);
            if ( _stateManager.Exit || g_Keyboard[Key.F12])
                Exit();
            if (g_Keyboard[Key.Tilde] || g_Gamepad_1.LeftShoulder)
                if (DiagnosticsCenter.Instance.Visible) DiagnosticsCenter.Instance.Hide();
                else DiagnosticsCenter.Instance.Show();

            _stateManager.Update(dt);
            _animationManager.Update(dt);
            DiagnosticsCenter.Instance.Update(dt);
            base.Update(dt);
        }

        protected override void Render(float dt)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            _stateManager.Draw(dt);
            DiagnosticsCenter.Instance.Draw(dt);
            //Thread.Sleep((int)(1 / (dt * 32)));
            base.Render(dt);
        }
    }
}
