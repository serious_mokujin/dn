﻿using Blueberry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Blueberry.Input;
using DN.GUI;
using DN.GUI.Windows;
using DN.GameObjects;
using OpenTK.Input;
using DN.LevelGeneration;
using OpenTK;

namespace DN
{
    public partial class GameWorld
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public TileMap TileMap { get; private set; }
        public Statistic Statistic { get; private set; }
        public SolidObjectManager SolidObjectManager { get; private set; }
        public List<ILevelComponent> LevelComponents { get; private set; }

        public IGameObject[] Heroes { get; private set; }
        public IGameObject Leader { get; private set; }
        public const float G = 15f;
        public static readonly Vector2 GravityDirection = new Vector2(0, 1);

        public GameObjectsManager GameObjectsManager;


        public Camera Camera { get; private set; }
        public bool LevelFinished { get; private set; }

        private float _scale = 1.0f;
        private float _userScale = 0.0f;


        public QuadTree<CollisionComponent> QuadTree;


        public GameWorld(int width, int height)
        {
            Width = width;
            Height = height;
            TileMap = new TileMap(Width, Height);
            SolidObjectManager = new SolidObjectManager(Width, Height);
            LevelComponents = new List<ILevelComponent>(4);

            Heroes = new IGameObject[4];


            QuadTree = new QuadTree<CollisionComponent>(new Rectangle(0, 0, Width*64, Height*64));
            QuadTree.MaxGeneration = 4;

            Camera = new Camera(Game.g_screenSize, new Point(Game.g_screenSize.Width/2, Game.g_screenSize.Height/2),
                                true);
            _scale = Game.g_CameraScale;
            Camera.ScaleTo(_scale);
            Camera.MoveSpeed = 7;
            Camera.AllowedSpace = new Rectangle(0, 0, TileMap.Width*64, TileMap.Height*64);

            GameObjectsManager = new GameObjectsManager(Camera);

            InitRendering();

            Statistic = new Statistic();
            
            Game.g_Keyboard.KeyDown += new EventHandler<KeyboardKeyEventArgs>(Game_g_Keyboard_KeyDown);
            
        }

        void Game_g_Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
        	if(e.Key == Key.I) inverse = !inverse;
        }
        
        public void InitHeroGui(IGameObject hero, CraftingGUI craftingGUI)
        {
        }

        public IGameObject GetHero(int index)
        {
            return Heroes[index];
        }
        public bool EveryoneIsDead()
        {
            foreach (var gameObject in Heroes)
            {
                if(gameObject == null)
                    continue;
                if (!gameObject.GetComponent<HealthComponent>().IsDead)
                    return false;
            }
            return true;
        }

        public void Update(float dt)
        {
            GameObjectsManager.Update(dt);

            Camera.ScaleTo(_scale + _userScale);
            Camera.Update(dt);
            if (Leader != null)
                Camera.MoveTo(Leader.GetComponent<BoundaryComponent>().Position);

            background.Update(dt);
            
            foreach (var levelComponent in LevelComponents)
            {
                levelComponent.Update(dt);
            }
            PopupTextThrower.Instance.Update(dt);


            if (Game.g_Keyboard[Key.Plus])
            {
                _userScale += dt;
            }
            else if (Game.g_Keyboard[Key.Minus])
            {
                _userScale -= dt;
            }
            _userScale = Math.Max(_userScale+Game.g_CameraScale, Camera.MinScale)-Game.g_CameraScale;
            
            CheckLeader();
        }

        public void InsertHero(int index, GameObjectsFabric fabric, Point cell, Color color1, Color color2,
                               KeyboardDevice keyboardDevice, MouseDevice mouseDevice, GamepadDevice gamepadDevice)
        {
            LevelFinished = false;
            if (Heroes[index] == null)
            {
                Heroes[index] = fabric.CreateHero(cell, color1, color2);
                fabric.AddPlayerControlls(Heroes[index], keyboardDevice, mouseDevice, gamepadDevice);
                GameObjectsManager.AddObject(Heroes[index]);
            }
        }

        public void FinishLevel()
        {
            LevelFinished = true;
        }
        public void DestroyMapCell(int x, int y)
        {
            TileMap[x, y] = CellType.Free;// todo: add effects
        }

        private void CheckLeader()
        {
            if (Leader == null || Leader.GetComponent<HealthComponent>().IsDead)
            {
                foreach (var hero in Heroes.Where(p => p != null && !p.GetComponent<HealthComponent>().IsDead))
                {
                    Leader = hero;
                    break;
                }
            }
        }
    }


    public class CollidedCell
    {
        public Rectangle Rectangle;
        public CellType CellType;

        /// <summary>
        /// Determines in which direction object collided this tile
        /// </summary>
        public Point Direction;

        public IGameObject CollidedObject;
         
        public CollidedCell(Rectangle rect, CellType cellType,IGameObject collidedObject = null)
        {
            CellType = cellType;
            Rectangle = rect;
            CollidedObject = collidedObject;
        }
    }
}
