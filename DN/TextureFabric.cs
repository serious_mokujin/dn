using System;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using System.Drawing;
using OpenTK.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Drawing.Text;

namespace DN
{
    public static class TextureFabric
    {
        public static Texture PredrawWallTexture(float scaleFactor)
        {
            // load font from file
            PrivateFontCollection pfc = new PrivateFontCollection();
            pfc.AddFontFile(Path.Combine("Content", "Fonts", "MATURASC.TTF"));
            var fontFamily = pfc.Families[0];
            Font f = new Font(fontFamily, 34 * scaleFactor, GraphicsUnit.Pixel);
            CM.I.LoadFont("maturasc", f, "walj"); // letter j is duct tape xD
            // create texture
            Texture back = new Texture(new Size((int)(64*scaleFactor), (int)(64*scaleFactor)));

            BitmapFont font = CM.I.Font("maturasc");

            SpriteBatch.Instance.Begin();
            SpriteBatch.Instance.PrintText(font, "llwa\nwall", new Vector2(back.Size.Width/2, back.Size.Height/2), Color4.White, 0, 1, 0.5f, 0.5f);
            SpriteBatch.Instance.End(back, false);
            // set wrap more to avoid texture repeating
            back.VerticalWrap = TextureWrapMode.Clamp;
            back.HorizontalWrap = TextureWrapMode.Clamp;
            return back;
        }
        public static Texture PredrawStairTexture(float scaleFactor)
        {
            // load font from file
            PrivateFontCollection pfc = new PrivateFontCollection();
            pfc.AddFontFile(Path.Combine("Content", "Fonts", "speculum.ttf"));
       //     pfc.AddFontFile(Path.Combine("Content", "Fonts", "courbi.ttf"));
            var fontFamily = pfc.Families[0];
            Font f = new Font(fontFamily, 26 * scaleFactor,FontStyle.Bold, GraphicsUnit.Pixel);
            CM.I.LoadFont("courbi", f, "stairj"); // letter j is duct tape xD
            // create texture
            Texture back = new Texture(new Size((int)(64*scaleFactor), (int)(64*scaleFactor)));
            
            BitmapFont font = CM.I.Font("courbi");
            font.Options.Monospacing = FontMonospacing.Yes;

            SpriteBatch.Instance.Begin();
            SpriteBatch.Instance.PrintText(font, "stair\n\nstair", new Vector2(back.Size.Width/2, back.Size.Height/2), new Color4(1,1,1,0.9f), 0, 1, 0.5f, 0.5f);
            SpriteBatch.Instance.End(back, false);
            // set wrap more to avoid texture repeating
            back.AnisotropicFilter = 1;
            back.MagFilter = TextureMagFilter.Linear;
            back.MinFilter = TextureMinFilter.Linear;
            back.VerticalWrap = TextureWrapMode.Clamp;
            back.HorizontalWrap = TextureWrapMode.Clamp;
            return back;
        }
        public static Texture PredrawSwordTexture(float scaleFactor)
        {
            BitmapFont font = CM.I.Font("consolas16");
            Texture tex = new Texture(font.Measure("sword").ToSize());
            GL.ClearColor(0, 0, 0, 0);
            SpriteBatch.Instance.Begin();
            SpriteBatch.Instance.PrintText(font, "sword", 0, 0, Color4.White, 0, 1, 0, 0, false, false);
            SpriteBatch.Instance.End(tex, false);
            return tex;
        }
        public static Texture PredrawArrowTexture(float scaleFactor)
        {
            BitmapFont font = CM.I.Font("consolas9");
            Texture tex = new Texture(font.Measure("arrow").ToSize());
            GL.ClearColor(0, 0, 0, 0);
            SpriteBatch.Instance.Begin();
            SpriteBatch.Instance.PrintText(font, "arrow", 0, 0, Color4.White, 0, 1, 0, 0, false, false);
            SpriteBatch.Instance.End(tex, false);
            return tex;
        }
   
    }
}

