﻿using System;
using System.Drawing;
using OpenTK;

namespace DN.Helpers
{ 
    static public class FunctionHelper
    {
        static public float Vector2ToRadians(Vector2 velocity)
        {
            velocity.Normalize();
          //  float t1 = (float)Math.Acos(velocity.X / Math.Sqrt(velocity.X * velocity.X + velocity.Y * velocity.Y));
            float t1 = (float)Math.Atan2(velocity.Y, velocity.X);
            if (float.IsNaN(t1))
                return 0;
            return t1;
        }

        static public Vector2 RadiansToVector2(float radians)
        {
            return new Vector2((float)Math.Cos(radians), (float)Math.Sin(radians));
        }

        static public bool GetLineOfSight(TileMap tileMap, Point startCell, Point endCell)
        {
            return GetLineOfSight(tileMap, startCell.X, startCell.Y, endCell.X, endCell.Y);
        }

        static public bool GetLineOfSight(TileMap tileMap, float x1, float y1, float x2, float y2)
        {
            float deltaX = Math.Abs(x2 - x1);
            float deltaY = Math.Abs(y2 - y1);
            float signX = x1 < x2 ? 1 : -1;
            float signY = y1 < y2 ? 1 : -1;
            float error = deltaX - deltaY;

            while (true)
            {
                if (tileMap[(int)x1, (int)y1] == CellType.Wall) return false;

                if (x1 == x2 && y1 == y2)
                    return true;

                float error2 = error * 2;

                if (error2 > -deltaY)
                {
                    error -= deltaY;
                    x1 += signX;
                }
                else if (error2 < deltaX)
                {
                    error += deltaX;
                    y1 += signY;
                }
            }
        }
        static public sbyte GetSign(float number)
        {
            if (number < 0)
                return -1;
            if (number > 0)
                return 1;
            else 
                return 0;
        }
        static public Vector2 DirectionToObject(Vector2 p1, Vector2 p2)
        {
            float angle = (float)Math.Atan2(p1.Y - p2.Y, p1.X - p2.X);
            return new Vector2(-(float)Math.Cos(angle), -(float)Math.Sin(angle));
        }
        //todo: add lerp approximation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="endValue"></param>
        /// <param name="grow"></param>
        /// <returns>true if finished, false if still working</returns>
        static public bool Approximation(ref float value, float endValue, float grow)
        {
            if (value < endValue)
            {
                value += grow;

                if (value > endValue)
                {
                    value = endValue;
                    return true;
                }
                return false;
            }
            else
            {
                value -= grow;

                if (value < endValue)
                {
                    value = endValue;
                    return true;
                }
                return false;
            }
        }
    }
}
