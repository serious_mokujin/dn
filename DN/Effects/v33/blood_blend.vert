﻿#version 330 core

in vec2 vposition; 
in vec2 vtexcoord;

out vec2 ftexcoord;

void main(void) 
{
	ftexcoord = vtexcoord;
	gl_Position = vec4(vposition, 0, 1); 
}