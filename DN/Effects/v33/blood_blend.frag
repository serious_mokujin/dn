﻿#version 330 core

uniform sampler2D tex1; // background
uniform sampler2D tex2; // blood texture

in vec2 ftexcoord;

out vec4 color;

void main(void) 
{ 
	vec4 tex2color = texture(tex2, ftexcoord);
	vec4 tex1color = texture(tex1, ftexcoord);
	if(tex2color.r > 0 && (tex1color.r + tex1color.g + tex1color.b) > 0)
	{
		//color = vec4(1, 0, 0, 1);
		color = vec4(tex1color.rgb*(1 - tex2color.a) + tex2color.rgb*tex2color.a, tex1color.a);
	}
	else
		color = vec4(tex1color.rgb, tex1color.a);
}