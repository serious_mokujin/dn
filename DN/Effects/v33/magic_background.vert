﻿#version 330 core

in vec2 vposition; 
in vec4 vcolor; 
in vec4 voldcolor;

out vec4 fcolor; 
out vec4 foldcolor;

void main(void) 
{
	fcolor = vcolor;
	foldcolor = voldcolor;
	gl_Position = vec4(vposition, 0, 1); 
}