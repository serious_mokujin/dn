﻿using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Particles;
using Blueberry.Particles.Shapes;
using DN.Effects.ParticleEffects;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace DN.Effects
{
    public class ParallaxBackground
    {
        private GameWorld world;
        ParticleEmitter fallingEmitter;
        FallingLetterStateManager fallingLetterParticleManager;
        RectangleShape emitterShape;
        
        ParticleEmitter staticEmitter;
        StaticLetterStateManager staticLetterParticleManager;
		
        float timeToReleaseFallingLetter;
        
        public ParallaxBackground(GameWorld world)
        {
        	
        	emitterShape = new RectangleShape(new Size(600, 200), Vector2.UnitY, false);
        	fallingLetterParticleManager = new FallingLetterStateManager(20);
        	fallingEmitter = new ParticleEmitter(20, 20, emitterShape, fallingLetterParticleManager);
        	fallingEmitter.ReleaseQuantity = 1;
        	
        	staticLetterParticleManager = new StaticLetterStateManager(100);
        	staticEmitter = new ParticleEmitter(100, 5, new PointShape(), staticLetterParticleManager);
            staticEmitter.ReleaseQuantity = 1;
            
        	fallingLetterParticleManager.OnPutLetter += PutLetter;
			
            this.world = world;
            
        }
        void PutLetter(Vector2 position, float opacity)
        {
        	staticEmitter.Trigger((float)Game.g_TotalTime, position);
        }
        public void Update(float dt)
        {
        	Vector2 point = world.Camera.ToWorld(new Vector2(Game.g_screenSize.Width/2, 0), 0.3f);
        	if(timeToReleaseFallingLetter <= 0)
        	{
        		fallingEmitter.Trigger((float)Game.g_TotalTime, point);
        		timeToReleaseFallingLetter = 2;
        	}
        	else
        		timeToReleaseFallingLetter -= dt;
        	
        	fallingEmitter.Update((float)Game.g_TotalTime, dt);
        	staticEmitter.Update((float)Game.g_TotalTime, dt);

        }
        public void Draw(float dt)
        {
        	SpriteBatch.Instance.Begin(world.Camera.GetViewMatrix(0.3f));
            
            staticEmitter.Render();
			fallingEmitter.Render();
            
			SpriteBatch.Instance.End();
        }
    }
}
