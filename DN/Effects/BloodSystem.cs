﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using Blueberry;
using Blueberry.Graphics;
using Blueberry.Particles;
using Blueberry.Particles.Shapes;
using DN.Effects.ParticleEffects;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace DN.Effects
{
	public class BloodSystem : IDisposable
    {
        GameWorld world;

        Shader blood_blend_shader;
		
        ParticleEmitter spotEmitter;
        BloodSpotStateManager spotParticleManager;
        
        Texture blood = new Texture(Game.g_screenSize); // texture with blood spots
        Texture back;                                   // texture to blend blood with
        private VertexBuffer vBuffer;

        public BloodSystem(GameWorld world)
        {
            this.world = world;
        }
        public void Init()
        {
            blood_blend_shader = new Shader();
            
            float v = Shader.Version;

            blood_blend_shader.LoadVertexFile(Path.Combine("Effects", v < 3.3f ? "v12" : "v33", "blood_blend.vert"));
            blood_blend_shader.LoadFragmentFile(Path.Combine("Effects", v < 3.3f ? "v12" : "v33", "blood_blend.frag"));

            blood_blend_shader.Link();
            
            vBuffer = new VertexBuffer(4);
            vBuffer.DeclareNextAttribute("vposition", 2);
            vBuffer.DeclareNextAttribute("vtexcoord", 2);
            vBuffer.AddVertex(-1, -1, 0, 1);
            vBuffer.AddVertex(1, -1, 1, 1);
            vBuffer.AddVertex(1, 1, 1, 0);
            vBuffer.AddVertex(-1, 1, 0, 0);
            vBuffer.AddIndices(0, 1, 2, 0, 2, 3);
            vBuffer.UpdateBuffer();

            vBuffer.Attach(blood_blend_shader);
            
            spotParticleManager = new BloodSpotStateManager(400);
            spotEmitter = new ParticleEmitter(400, 15, new PointShape(), spotParticleManager);
            spotEmitter.ReleaseQuantity = 1;

        }
        public void BlendWith(Texture tex)
        {
            back = tex;
        }

        public void AddSpot(Vector2 position)
        {
        	Point cell = new Point((int)(position.X / 64f), (int)(position.Y / 64f));
        	if(world.TileMap.InRange(cell) && !world.TileMap.IsFree(cell))
        		spotEmitter.Trigger((float)Game.g_TotalTime, position);
        }

        public void DrawBackground(float dt)
        {
        	//back.SaveToDisk("back.png");
            blood_blend_shader.Use();
            vBuffer.Bind();

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, back.ID);

            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, blood.ID);

            GL.ActiveTexture(TextureUnit.Texture0);

            blood_blend_shader.SetUniform("tex1", 0);
            blood_blend_shader.SetUniform("tex2", 1);


            GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, 0);

            GL.UseProgram(0);
        }
        public void PredrawBloodTexture(float dt)
        {
        	spotEmitter.Update((float)Game.g_TotalTime, dt);
        	SpriteBatch.Instance.Begin(world.Camera.GetViewMatrix());
        	spotEmitter.Render();
            SpriteBatch.Instance.End(blood, true);
        }
		
		public void Dispose()
		{
			this.vBuffer.Dispose();
		}
    }
}
