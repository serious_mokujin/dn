﻿using Blueberry;
using Blueberry.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DN.Effects
{
	public class MagicBackground : IDisposable
    {
        Shader shader;
        VertexBuffer buffer;
        float time = 0;

        public MagicBackground()
        {
            shader = new Shader();

            float v = Shader.Version;

            shader.LoadVertexFile(Path.Combine("Effects", v < 3.3f ? "v12" : "v33", "magic_background.vert"));
            shader.LoadFragmentFile(Path.Combine("Effects", v < 3.3f ? "v12" : "v33", "magic_background.frag"));

            shader.Link();
			
            buffer = new VertexBuffer(4);
            buffer.DeclareNextAttribute("vposition", 2);
            buffer.DeclareNextAttribute("vcolor", 4);
            buffer.DeclareNextAttribute("voldcolor", 4);
            buffer.Attach(shader);
            
            buffer.AddVertex(-1, -1, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            buffer.AddVertex(1, -1, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            buffer.AddVertex(1, 1, 1, 1, 1, 1, 1f, 1f, 1f, 1f);
            buffer.AddVertex(-1, 1, 1, 1, 1, 1, 1f, 1f, 1f, 1f);
            buffer.AddIndices(0, 1, 2, 0, 2, 3);
            buffer.UpdateBuffer();
        }
        int way = 1;
        Vector3[] rc = new Vector3[] { new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1)),
                                        new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1)),
        new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1)),
        new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1))};
        Vector3[] oc = new Vector3[] { new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1)),
                                        new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1)),
        new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1)),
        new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1))};
        public void Draw(float dt)
        {
            time += way*dt;
            if (time >= 1 || time <= 0) 
            { 
                //way *= -1;
                time = 0;
                for (int i = 0; i < 4; i++)
                {
                    oc[i] = rc[i];
                    rc[i] = new Vector3(RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1), RandomTool.NextSingle(0, 1));
                }
                
                buffer.ClearBuffer();
                buffer.AddVertex(-1, -1, rc[0].X, rc[0].Y, rc[0].Z, 1, oc[0].X, oc[0].Y, oc[0].Z, 1);
                buffer.AddVertex(1, -1, rc[1].X, rc[1].Y, rc[1].Z, 1, oc[1].X, oc[1].Y, oc[1].Z, 1);
                buffer.AddVertex(1, 1, rc[2].X, rc[2].Y, rc[2].Z, 1, oc[2].X, oc[2].Y, oc[2].Z, 1);
                buffer.AddVertex(-1, 1,rc[3].X, rc[3].Y, rc[3].Z, 1, oc[3].X, oc[3].Y, oc[3].Z, 1);
                buffer.UpdateVertexBuffer();

            };
            shader.Use();
            buffer.Bind();
            shader.SetUniform("time",time);

            GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, 0);

            GL.UseProgram(0);
        }

		
		public void Dispose()
		{
			buffer.Dispose();
		}
    }
}
