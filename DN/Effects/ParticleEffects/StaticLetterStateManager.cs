﻿/*
 * Created by SharpDevelop.
 * User: Denis
 * Date: 10.11.2012
 * Time: 21:22
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Particles;
using OpenTK.Graphics;

namespace DN.Effects.ParticleEffects
{
	/// <summary>
	/// Description of StaticLetterStateManager.
	/// </summary>
	public class StaticLetterStateManager: ParticleStateManager<StaticLetterStateManager.StaticLetterParticleState>, IParticleStateManager
	{
		public struct StaticLetterParticleState
		{
			public char letter;
			public float scale;
			public float opacity;
		}
		private static char[] aviableLetters = "0123456789abcdefghigklmnopqrstuvwxyz?!@#$%&^*+=~".ToCharArray();
		BitmapFont font;
		
		public StaticLetterStateManager(int capacity):base(capacity)
		{
			font = CM.I.Font("speculum16");
		}
		public unsafe void Trigger(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (StaticLetterParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->letter = RandomTool.NextChoice(aviableLetters);
                    state->scale = 1f;
                    state->opacity = 0.5f;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Update(ref ParticleIterator iterator, float deltaSeconds)
		{
			var meta = iterator.First;

            fixed (StaticLetterParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    state->opacity -=  deltaSeconds/10;
		            state->scale += deltaSeconds/7f;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Render(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (StaticLetterParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    SpriteBatch.Instance.PrintSymbol(font, state->letter, meta->ReleaseInfo.Position, 
                                                     new Color4(0.6039f,0.8078f, 0.9215f, state->opacity), 0, state->scale);
                }
                while (iterator.MoveNext(&meta));
            }
		}
	}
}
