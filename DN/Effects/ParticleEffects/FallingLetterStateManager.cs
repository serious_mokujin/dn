﻿using System;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Particles;
using OpenTK;
using OpenTK.Graphics;

namespace DN.Effects.ParticleEffects
{
	/// <summary>
	/// Description of FallingLetterStateManager.
	/// </summary>
	public class FallingLetterStateManager: ParticleStateManager<FallingLetterStateManager.FallingLetterParticleState>, IParticleStateManager
	{
		public struct FallingLetterParticleState
		{
			public char letter;
			public float scale;
			public float opacity;
			public Vector2 position;
			public Vector2 velocity;
			public float timeToChangeLetter;
			public float timeToPutLetter;
		}
		private static char[] aviableLetters = "0123456789abcdefghigklmnopqrstuvwxyz?!@#$%&^*+=~".ToCharArray();
		BitmapFont font;
		public event Action<Vector2, float> OnPutLetter;
		
		public FallingLetterStateManager(int capacity):base(capacity)
		{
			font = CM.I.Font("speculum16");
		}
		public unsafe void Trigger(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (FallingLetterParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->letter = RandomTool.NextChoice(aviableLetters);
                    state->scale = 1f;
                    state->opacity = 0.8f;
                    state->position = meta->ReleaseInfo.Position;
                    state->velocity = new Vector2(0, 70);
                    state->timeToChangeLetter = 0.02f;
                    state->timeToPutLetter = 0.5f;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Update(ref ParticleIterator iterator, float deltaSeconds)
		{
			var meta = iterator.First;

            fixed (FallingLetterParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    state->opacity = 0.8f - meta->Age*0.8f;
		            state->position += state->velocity*deltaSeconds;
		            state->timeToChangeLetter -= deltaSeconds;
		            if(state->timeToChangeLetter <= 0)
		            {
		            	state->letter = RandomTool.NextChoice(aviableLetters);
		            	state->timeToChangeLetter = 0.02f;
		            }
		            state->timeToPutLetter -= deltaSeconds;
		            if(state->timeToPutLetter <= 0)
		            {
		            	if(OnPutLetter != null)
		            		OnPutLetter(state->position, state->opacity);
		            	state->timeToPutLetter = 0.5f;
		            }
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Render(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (FallingLetterParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    SpriteBatch.Instance.PrintSymbol(font, state->letter, state->position, 
                                                     new Color4(0.6039f,0.8078f, 0.9215f, state->opacity), 0, state->scale);
                }
                while (iterator.MoveNext(&meta));
            }
		}
	}
}
