﻿using System;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Particles;
using DN.States;
using OpenTK;
using OpenTK.Graphics;

namespace DN.Effects.ParticleEffects
{
	/// <summary>
	/// Description of DustParticleStateManager.
	/// </summary>
	public class DustParticleStateManager : ParticleStateManager<DustParticleStateManager.DustParticleState>, IParticleStateManager
	{
		public struct DustParticleState
		{
			public Vector2 position;
			public Vector2 velocity;
			public float rotation;
			public float scale;
			public float opacity;
			public char letter;
			public bool cw;
		}
		public Vector2 SpreadingDirection{get; set;}
		BitmapFont font;
		
		public DustParticleStateManager(int capacity):base(capacity)
		{
			font = CM.I.Font("Small");
		}
		public unsafe void Trigger(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (DustParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->letter = RandomTool.NextChoice('d','u','s','t');
                    state->cw = RandomTool.NextBool();
                    state->scale = 0.5f;
                    state->opacity = 0.8f;
                    
                    Vector2 forse = -Vector2.UnitY;
            		MathUtils.RotateVector2(ref forse, (SpreadingDirection.X > 0 ? 1f : -1f) * RandomTool.NextSingle(0.1f, 2f));
            		forse *= 10;
                    
                    state->velocity = (forse + meta->ReleaseInfo.Direction) * RandomTool.NextSingle(5, 10);
                    state->rotation = RandomTool.NextSingle(-0.2f, 0.2f);
                    state->position = meta->ReleaseInfo.Position;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Update(ref ParticleIterator iterator, float deltaSeconds)
		{
			var meta = iterator.First;

            fixed (DustParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->opacity = 0.8f - meta->Age*0.8f;
                    state->rotation += (state->cw ? -1.5f : 1.5f) * deltaSeconds;
					state->velocity.X = MathUtils.Lerp(state->velocity.X, 0.05f, deltaSeconds*2);       
		            state->velocity.Y += deltaSeconds*200;
		            state->scale += deltaSeconds/1.5f;
                    state->position += state->velocity * deltaSeconds;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Render(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (DustParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    SpriteBatch.Instance.PrintSymbol(font, state->letter, state->position, new Color4(0.828f,0.828f,0.828f, state->opacity),state->rotation,state->scale);
                }
                while (iterator.MoveNext(&meta));
            }
		}
	}
}
