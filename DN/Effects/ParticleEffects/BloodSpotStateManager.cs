﻿/*
 * Created by SharpDevelop.
 * User: Denis
 * Date: 09.11.2012
 * Time: 23:33
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Particles;
using OpenTK;
using OpenTK.Graphics;

namespace DN.Effects.ParticleEffects
{
	/// <summary>
	/// Description of BloodSpotStateManager.
	/// </summary>
	public class BloodSpotStateManager : ParticleStateManager<BloodSpotStateManager.BloodSpotState>, IParticleStateManager
	{
		public struct BloodSpotState
		{
			public float size;
		}
		public Vector2 SpreadingDirection{get; set;}
		
		public BloodSpotStateManager(int capacity):base(capacity)
		{
		}
		public unsafe void Trigger(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (BloodSpotState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    state->size = RandomTool.NextSingle(4, 16);
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Update(ref ParticleIterator iterator, float deltaSeconds)
		{
		}
		
		public unsafe void Render(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (BloodSpotState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    SpriteBatch.Instance.FillCircle(meta->ReleaseInfo.Position, state->size,new Color4(1, 0, 0, 1 - (meta->Age)), 5);
                }
                while (iterator.MoveNext(&meta));
            }
		}
	}
}
