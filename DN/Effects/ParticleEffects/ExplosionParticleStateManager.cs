﻿
using System;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Graphics.Fonts;
using Blueberry.Particles;
using OpenTK;
using OpenTK.Graphics;

namespace DN.Effects.ParticleEffects
{
	/// <summary>
	/// Description of ExplosionParticleStateManager.
	/// </summary>
	public class ExplosionParticleStateManager: ParticleStateManager<ExplosionParticleStateManager.ExplosionParticleState>, IParticleStateManager
	{
		public struct ExplosionParticleState
		{
			public Vector2 position;
			public Vector2 velocity;
			public Vector2 forse;
			public float rotation;
			public float scale;
			public char letter;
			public bool cw;
			public Color4 color;
		}
		BitmapFont font;
		
		public ExplosionParticleStateManager(int capacity):base(capacity)
		{
			font = CM.I.Font("speculum16");
		}
		public unsafe void Trigger(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (ExplosionParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->letter = RandomTool.NextChoice('e','x','p', 'l', 'o', 's', 'i', 'o', 'n');
                    state->cw = RandomTool.NextBool();
                    state->scale = 0.4f;
                    
                    state->forse = meta->ReleaseInfo.Direction * RandomTool.NextSingle(1, 400);
                    state->velocity = state->forse;
                    state->rotation = RandomTool.NextSingle(-0.2f, 0.2f);
                    state->position = meta->ReleaseInfo.Position;
                    float a = RandomTool.NextSingle(0.5f, 1);
                    if(RandomTool.NextBool(0.35f))
                    	state->color = new Color4(0.2f*a, 0.2f*a, 0.2f*a, 0.7f*a);
                    else
                   		state->color = new Color4(1f, 0.2f*a, 0, 1*a);
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Update(ref ParticleIterator iterator, float deltaSeconds)
		{
			var meta = iterator.First;

            fixed (ExplosionParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    if(meta->Age > 0.6f)
                    	state->color.A = 1-((meta->Age - 0.6f)*2);
                    state->rotation += (state->cw ? -1.5f : 1.5f) * deltaSeconds;
                    if(meta->Age < 0.06f)
                    	state->velocity = MathUtils.Lerp(state->velocity, state->forse, deltaSeconds*2);
                    else
                    	state->velocity = MathUtils.Lerp(state->velocity, Vector2.Zero, deltaSeconds*2);
                    if(meta->Age < 0.2)
                    	state->scale = MathUtils.Lerp(state->scale, 3 + (1-(state->forse.Length/400))*7, deltaSeconds*2);
                    else
                    	state->scale = MathUtils.Lerp(state->scale, 0, deltaSeconds*2);
                    state->position += state->velocity * deltaSeconds;
                    
                    //state->color.B = meta->Age * 0.8f;
                    if(meta->Age < 0.2f)
                    	state->color.G = MathUtils.Lerp(state->color.G, 0.8f, deltaSeconds);
                    else
                    {
                    	state->color.R = MathUtils.Lerp(state->color.R, 0.5f, deltaSeconds*2);
                    	state->color.G = MathUtils.Lerp(state->color.G, 0.5f, deltaSeconds*2);
                    	state->color.B = MathUtils.Lerp(state->color.B, 0.5f, deltaSeconds*2);
                    }
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Render(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (ExplosionParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    SpriteBatch.Instance.PrintSymbol(font, state->letter, state->position, state->color, state->rotation,state->scale);
                }
                while (iterator.MoveNext(&meta));
            }
		}
	}
}
