﻿using System;
using Blueberry;
using Blueberry.Graphics;
using Blueberry.Particles;
using OpenTK;
using OpenTK.Graphics;

namespace DN.Effects.ParticleEffects
{
	/// <summary>
	/// Description of BloodParticleStateManager.
	/// </summary>
	public class BloodParticleStateManager : ParticleStateManager<BloodParticleStateManager.BloodParticleState>, IParticleStateManager
	{
		public struct BloodParticleState
		{
			public Vector2 position;
			public Vector2 velocity;
			public float rotation;
			public float scale;
			public float opacity;
			//public char letter;
			public bool cw;
		}
		public Vector2 SpreadingDirection{get; set;}
		public event Action<Vector2> OnLeavingSpot;
		
		public BloodParticleStateManager(int capacity):base(capacity)
		{
		}
		public unsafe void Trigger(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (BloodParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->cw = RandomTool.NextBool();
                    state->scale = 1f;
                    state->opacity = 1f;
                    
                    Vector2 forse = -Vector2.UnitY;
            		MathUtils.RotateVector2(ref forse, (SpreadingDirection.X > 0 ? 1f : -1f) * RandomTool.NextSingle(0.1f, 2f));
            		forse *= 10;
                    
                    state->velocity = (forse + meta->ReleaseInfo.Direction) * RandomTool.NextSingle(5, 10);
                    state->rotation = RandomTool.NextSingle(-0.2f, 0.2f);
                    state->position = meta->ReleaseInfo.Position;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Update(ref ParticleIterator iterator, float deltaSeconds)
		{
			var meta = iterator.First;

            fixed (BloodParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
					
                    state->opacity = 1f - meta->Age;
                    state->rotation += meta->Age * deltaSeconds;
		            state->velocity += 10*deltaSeconds*GameWorld.G * GameWorld.GravityDirection;
		            state->velocity.X = MathUtils.Lerp(state->velocity.X, 0, deltaSeconds);
                    state->position += state->velocity * deltaSeconds;
                }
                while (iterator.MoveNext(&meta));
            }
		}
		
		public unsafe void Render(ref ParticleIterator iterator)
		{
			var meta = iterator.First;

            fixed (BloodParticleState* buffer = _states)
            {
                do
                {
                    var state = buffer + meta->Index;
                    SpriteBatch.Instance.FillRegularPolygon(3, 5 * state->scale, state->position, state->rotation, new Color4(1, 0, 0, state->opacity));
                	if(RandomTool.NextBool(0.005) && OnLeavingSpot != null) OnLeavingSpot(state->position);
                }
                while (iterator.MoveNext(&meta));
            }
		}
	}
}
