﻿#version 120

attribute vec2 vposition; 
attribute vec4 vcolor; 
attribute vec4 voldcolor;

varying vec4 fcolor; 
varying vec4 foldcolor;

void main(void) 
{
	fcolor = vcolor;
	foldcolor = voldcolor;
	gl_Position = vec4(vposition, 0, 1); 
}
