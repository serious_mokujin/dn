﻿#version 120

varying vec4 fcolor; 
varying vec4 foldcolor;

uniform float time;

void main(void) 
{ 
	gl_FragColor = vec4(foldcolor.r + (fcolor.r - foldcolor.r) * time, foldcolor.g + (fcolor.g - foldcolor.g) * time, foldcolor.b + (fcolor.b - foldcolor.b) * time, 0.5);//vec4(ftexcoord.x * (1.0-time), ftexcoord.y * time*0.6-0.655*time, time, 1); 
}