﻿#version 120

uniform sampler2D tex;
uniform int inverse;
varying vec2 uv;

uniform float sampleDist;
uniform float sampleStrength; 

void main(void)
{
    float samples[10];
    samples[0] = -0.08;
    samples[1] = -0.05;
    samples[2] = -0.03;
    samples[3] = -0.02;
    samples[4] = -0.01;
    samples[5] =  0.01;
    samples[6] =  0.02;
    samples[7] =  0.03;
    samples[8] =  0.05;
    samples[9] =  0.08;

    vec2 dir = 0.5 - uv; 
    float dist = sqrt(dir.x*dir.x + dir.y*dir.y); 
    dir = dir/dist; 

    vec4 c = texture2D(tex,uv); 
    vec4 sum = c;

    for (int i = 0; i < 10; i++)
        sum += texture2D( tex, uv + dir * samples[i] * sampleDist );

    sum *= 1.0/11.0;
    float t = dist * sampleStrength;
    t = clamp( t, 0.0, 1.0);
	vec4 color = mix(c, sum, t );
    
	color.r *= 1 + sampleStrength;
	if(inverse == 1)
	{
	color.r = 0.5-(color.r-0.5);
	color.g = 0.5-(color.g-0.5);
	color.b = 0.5-(color.b-0.5);
	
	}
	gl_FragColor = color;
}