﻿#version 120

uniform sampler2D tex1; // background
uniform sampler2D tex2; // blood texture

varying vec4 fcolor; 
varying vec2 ftexcoord;

void main(void) 
{ 
	vec4 tex2color = texture(tex2, ftexcoord);
	vec4 tex1color = texture(tex1, ftexcoord);
	if(tex2color.r > 0 && (tex1color.r + tex1color.g + tex1color.b) > 0)
	{
		gl_FragColor = vec4(tex1color.rgb*(1 - tex2color.a) + tex2color.rgb*tex2color.a, tex1color.a);
	}
	else
		gl_FragColor = vec4(tex1color.rgb, tex1color.a);
}