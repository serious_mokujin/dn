﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using DN.GameObjects;

namespace DN
{
    public class CraftingManager
    {
        static private CraftingManager _instance;
        static public CraftingManager Instance
        {
            get
            {
                return _instance ?? (_instance = new CraftingManager());
            }
        }

        private bool _initialized;

        private Dictionary<string, GameObjectType> _gameObjectRecipes;
        private Dictionary<string, EnemyType> _enemiesRecipes;
        private GameObjectsFabric _fabric;

        private CraftingManager()
        {
            _gameObjectRecipes = new Dictionary<string, GameObjectType>(16);
            _enemiesRecipes = new Dictionary<string, EnemyType>(16);
        }

        public void Init(GameObjectsFabric fabric)
        {
            if(_initialized)
                return;
            _initialized = true;

            _fabric = fabric;
            _gameObjectRecipes.Add("bow", GameObjectType.Bow);
            _gameObjectRecipes.Add("sword", GameObjectType.Sword);
            _gameObjectRecipes.Add("potion", GameObjectType.Potion);
            _gameObjectRecipes.Add("bomb", GameObjectType.Granade);

            _enemiesRecipes.Add("ogre", EnemyType.Ogre);
            _enemiesRecipes.Add("troll", EnemyType.Troll);
            _enemiesRecipes.Add("bat", EnemyType.Bat);
        }

        public IGameObject TryCraft(string str)
        {
            if (_gameObjectRecipes.ContainsKey(str))
                return _fabric.CreateObject(_gameObjectRecipes[str], Point.Empty);
            else if (_enemiesRecipes.ContainsKey(str))
                return _fabric.CreateEnemy(_enemiesRecipes[str], Point.Empty, _fabric.GameWorld.Heroes);
            return null;
        }

    }
}
