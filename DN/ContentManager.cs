﻿using System.Drawing;
using Blueberry.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blueberry.Graphics.Fonts;
using System.IO;
using Blueberry.Audio;
using System.Drawing.Text;


namespace DN
{
    /// <summary>Content Manager</summary>
    public class CM
    {
        private static CM _instance;

        /// <summary>Instance</summary>
        public static CM I { get { return _instance ?? (_instance = new CM()); }}

        private readonly Dictionary<string, Texture> _textures;
        private readonly Dictionary<string, BitmapFont> _fonts;

        private readonly Dictionary<string, AudioClip> _sounds;

        private CM()
        {
            _textures = new Dictionary<string, Texture>();
            _fonts = new Dictionary<string, BitmapFont>();
            _sounds = new Dictionary<string, AudioClip>();
        }
        public void LoadTexture(string asset, string file)
        {
            if(!_textures.ContainsKey(asset))
                _textures.Add(asset, new Texture(file));
        }
        public void LoadTexture(string asset, Texture texture)
        {
            if(!_textures.ContainsKey(asset))
                _textures.Add(asset, texture);
        }
        public void LoadFont(string asset, string file, float size)
        {
            if (!_fonts.ContainsKey(asset))
                _fonts.Add(asset, new BitmapFont(file, size));
        }
        public void LoadFont(string asset, string file, float size, FontStyle style)
        {
            if (!_fonts.ContainsKey(asset))
                _fonts.Add(asset, new BitmapFont(file, size, style));
        }
        public void LoadFont(string asset, Font font, string chrset)
        {
            if (!_fonts.ContainsKey(asset))
                _fonts.Add(asset, new BitmapFont(font, new FontBuilderConfiguration(){ charSet = chrset}));
        }
        public void LoadFont(string asset, string filename, FontStyle style, float originalSize, float scaleFactor, string charset)
        {
            if (_fonts.ContainsKey(asset)) return;
            PrivateFontCollection pfc = new PrivateFontCollection();
            pfc.AddFontFile(filename);
            var fontFamily = pfc.Families[0];
            Font f = new Font(fontFamily, originalSize * scaleFactor, style, GraphicsUnit.Pixel);
            LoadFont(asset, f, charset);
        }
        public void LoadSound(string asset, string file)
        {
            if (!_sounds.ContainsKey(asset))
                _sounds.Add(asset, new AudioClip(file));
        }
        public void UnloadTexture(string texture)
        {
            _textures[texture].Dispose();
            _textures.Remove(texture);
        }

        public Texture tex(string asset)
        {
             return _textures[asset];
        }

        public BitmapFont Font(string asset)
        {
            return _fonts[asset];
        }
        public AudioClip Sound(string asset)
        {
            return _sounds[asset];
        }

    }
}
