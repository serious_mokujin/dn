﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN.GameObjects;

namespace DN
{
    public class Statistic
    {
        private Dictionary<ObjectGroup, int> _killed;



        public Statistic()
        {
            _killed = new Dictionary<ObjectGroup, int>(3){
                              {ObjectGroup.Enemy, 0}, 
                              {ObjectGroup.Hero, 0},
                          };
        }

        public Dictionary<ObjectGroup, int> GetStatistic()
        {
            return _killed.ToDictionary(entry => entry.Key, entry => entry.Value);
        }

        public void AddKill(ObjectGroup type)
        {
            _killed[type]++;
        }


    }
}
