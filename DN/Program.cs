﻿using System;
using System.Drawing;
using OpenTK;

namespace DN
{
    class Program
    {
    	// some screen size constants
        public readonly static Size fullHD = new Size(1920, 1080);
        public readonly static Size fullHDP = new Size(1080, 1920);
        public readonly static Size HD = new Size(1280, 720);
        public readonly static Size qHD = new Size( 960, 540);
        public readonly static Size nHD = new Size(640, 360);

        static void Main(string[] args)
       {

            using (var game = new Game(qHD, false))
            {
                game.Run(60, 120);
            }
       }
    }
}
