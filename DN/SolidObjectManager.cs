﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Blueberry;
using DN.GameObjects;

namespace DN
{
    public class SolidObjectManager
    {
        private QuadTree<CollisionComponent> _quadTree;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="width">width of level in cells</param>
        /// <param name="height">height of level in cells</param>
        public SolidObjectManager(int width, int height)
        {
            _quadTree = new QuadTree<CollisionComponent>(new Rectangle(0, 0, width*64, height*64));
        }

        public void Add(CollisionComponent component)
        {
            _quadTree.Insert(component);
        }
        public void Remove(CollisionComponent component)
        {
            _quadTree.RemoveItem(component);
        }


        public List<CollidedCell> GetCollisions(RectangleF rectangle)
        {
            Rectangle rect = new Rectangle((int) Math.Round(rectangle.X), (int) Math.Round(rectangle.Y),
                                           (int) Math.Round(rectangle.Width), (int) Math.Round(rectangle.Height));

            List<CollisionComponent> collisions = _quadTree.Query(rect);

            List<CollidedCell> collidedCells = new List<CollidedCell>(collisions.Count);

            foreach (var collision in collisions)
            {
                collidedCells.Add(new CollidedCell(collision.Bounds, CellType.Wall, collision.GetOwner));
            }
            return collidedCells;
        }

    }
}
