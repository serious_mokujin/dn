﻿using Blueberry;
using Blueberry.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN
{
    public enum CellType:byte
    {
        Free = 0,
        Wall = 1,
        Ladder = 2,
        VRope = 3

    }
    public class TileMap
    {
        public RectangleF Bounds { get; private set; }

        private Texture _wallTile;
        private Texture _stairTile;
        private CellType[,] _map;

        public CellType this[int i, int j]
        {
            get { return _map[i, j]; }
            set { _map[i, j] = value; }
        }

        public readonly int Width;
        public readonly int Height;

        public TileMap(int width, int height)
        {
            Width = width;
            Height = height;
            _map = new CellType[Width, Height];

            Bounds = new Rectangle(0,0, width*64, height*64);
        }

        public void InitTextures()
        {
            _wallTile = CM.I.tex("wall_tile");
            _stairTile = CM.I.tex("stair_tile");
        }
        
        public void FillWith(CellType cellType)
        {
            FillWith(_map, Width, Height, cellType);
        }

        static public void FillWith(CellType[,] map, int width, int height, CellType cellType)
        {
            for (var i = 0; i < width; i++)
                for (var j = 0; j < height; j++)
                    map[i, j] = cellType;
        }


        public Rectangle GetRect(int x, int y)
        {
            return new Rectangle(x * 64, y * 64, 64, 64);
        }
        public Rectangle GetRect(Point pos)
        {
            return new Rectangle(pos.Y * 64, pos.Y * 64, 64, 64);
        }



        public bool IsFree(Point p)
        {
            return _map[p.X, p.Y] == CellType.Free; 
        }
        public bool IsFree(int x, int y)
        {
            return _map[x, y] == CellType.Free;
        }
        public bool IsEndOfLadder(int x, int y)
        {
            return _map[x, y] == CellType.Ladder && _map[x, y - 1] == CellType.Free;
        }

        public Point GetBottomFreeCell()
        {
            for (int i = Height - 1; i >= 0; i--)
                for (int j = 1; j < Width; j++)
                    if (_map[j, i] == CellType.Free)
                        return new Point(j, i);
            return Point.Empty;
        }
        public Point GetTopFreeCell()
        {
            for (int i = 1; i < Height; i++)
                for (int j = Width - 1; j >= 0 ; j--)
                    if (_map[j, i] == CellType.Free)
                        return new Point(j, i);
            return Point.Empty;
        }

        public Point GetLeftCell(int y, CellType cellType)
        {
            for (int i = 0; i < Width; i++)
                if(_map[i, y] == cellType)
                    return new Point(i, y);
            return Point.Empty;
        }
        public Point GetRightCell(int y, CellType cellType)
        {
            for (int i = Width - 1; i >= 0; i--)
                if (_map[i, y] == cellType)
                    return new Point(i, y);
            return Point.Empty;
        }
        public Point GetRightCell(CellType cellType)
        {
            Point max = Point.Empty;
            for (int i = 0; i < Height; i++)
            {
                Point p = GetRightCell(i, cellType);
                if (p.X > max.X)
                    max = p;
            }
            return max;
        }


        public Point GetRandomPoint()
        {
            Point p;
            do
            {
                p = new Point(RandomTool.NextInt(0, Width), RandomTool.NextInt(0, Height));
            } while (!IsFree(p));

            return p;
        }
        public Point GetRandomStandablePoint()
        {
            Point p;
            do
            {
                p = new Point(RandomTool.NextInt(0, Width), RandomTool.NextInt(0, Height - 1));
            } while (!IsFree(p) || _map[p.X, p.Y + 1] != CellType.Wall);

            return p;
        }

        public void Draw(Rectangle region)
        {
            Size ts = new Size(64, 64);//_wallTile.Size;
            int ifrom = Math.Max(0, region.Left);
            int ito = Math.Min(region.Right, Width);
            int jfrom = Math.Max(0, region.Top);
            int jto = Math.Min(region.Bottom, Height);

            for (int i = ifrom; i < ito; i++)
            {
                for (int j = jfrom; j < jto; j++)
                {
                    switch(_map[i,j])
                    {
                        case CellType.Wall:
                            SpriteBatch.Instance.DrawTexture(_wallTile, i * ts.Width, j * ts.Height, ts.Width, ts.Height, Rectangle.Empty, Color.White, 0, 0, 0);
                            break;
                    }
                }
            }
            for (int i = ifrom; i < ito; i++)
            {
                for (int j = jfrom; j < jto; j++)
                {
                    switch (_map[i, j])
                    {
                        case CellType.Ladder:
                            SpriteBatch.Instance.DrawTexture(_stairTile, i * ts.Width, j * ts.Height, ts.Width, ts.Height, Rectangle.Empty, Color.White, 0, 0, 0);
                            break;
                        case CellType.VRope:
                            SpriteBatch.Instance.DrawTexture(_stairTile, i * ts.Width, j * ts.Height, ts.Width, ts.Height, Rectangle.Empty, Color.Brown, 0, 0, 0);
                            break;
                    }
                }
            }
        }

        public List<CollidedCell> GetCollisionsWithTiles(RectangleF rectangle)
        {
            return GetCollisionsWithTiles(new Rectangle((int)Math.Round(rectangle.X),
                                               (int)Math.Round(rectangle.Y),
                                               (int)Math.Round(rectangle.Width),
                                               (int)Math.Round(rectangle.Height)));
        }

        public List<CollidedCell> GetCollisionsWithTiles(Rectangle rectangle)
        {
            var list = new List<CollidedCell>();

            Point startPoint = new Point(rectangle.X / 64, rectangle.Y / 64);
            Point endPoint = new Point(Math.Min(Width - 1, (rectangle.Right - 1) / 64),
                                       Math.Min(Height - 1, (rectangle.Bottom - 1) / 64));

            for (int i = Math.Max(0, startPoint.X); i <= endPoint.X; i++)
                for (int j = startPoint.Y; j <= endPoint.Y; j++)
                    if(_map[i, j] != CellType.Free)
                        list.Add(new CollidedCell(GetRect(i, j), this[i, j]));
            return list;
        }

        public bool InRange(Point cell)
        {
            return InRange(cell.X, cell.Y);
        }
        public bool InRange(int x, int y)
        {
            return x > 0 && x < Width && y > 0 && y < Height;
        }

        public void PrintDebug()
        {
            for (int j = 0; j < Height; j++)
            {
                Console.WriteLine();
                for (int i = 0; i < Width; i++)
                {
                    Console.Write((byte)_map[i,j]);
                }
            }
        }
    }
}
